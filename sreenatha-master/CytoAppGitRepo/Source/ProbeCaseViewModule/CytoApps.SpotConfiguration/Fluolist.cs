﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Media;


namespace CytoApps.SpotConfiguration
{
    public class CVFluoroChrome
    {
        public string Name { get; set; }
        public int flagsBitField;
        //public bool IsCounterstain { get { return (flagsBitField & 0x2) != 0 ? true : false; } } // Seems to be unreliable!
        public bool IsCounterstain;
        public Color colour;
        public int zStackNumImages;
        public int zStackFocusDist; // Hundredths of a micron.

        public CVFluoroChrome()
        {
            colour = Colors.White;
        }
    }

    class Fluolist
    {
        public List<CVFluoroChrome> cvFluoroChromes;

        public Fluolist()
        {
            cvFluoroChromes = new List<CVFluoroChrome>();
        }

        public bool Load(string cvFluolistFilePath)
        {
            bool status = false;

            cvFluoroChromes.Clear();

            // Refer to readList() in ProbeUtils/flfileio.c and struct spectra_chrome in Common/include/settings.h.
            // Just need names, zstack size and spacing.
            if (File.Exists(cvFluolistFilePath))
            {
                status = true;

                try
                {
                    using (var stream = File.OpenRead(cvFluolistFilePath))
                    {
                        byte[] headerBytes = new byte[16];

                        while (stream.Read(headerBytes, 0, headerBytes.Length) == headerBytes.Length)
                        {
                            string headerString = System.Text.Encoding.ASCII.GetString(headerBytes);

                            if (headerString.Substring(0, 10) != "<spchrome>")
                                break;
                            else
                            {
                                // Not relevant:  int version; int.TryParse(headerString.Substring(10), out version);

                                // Now read spectrachrome structure members that we are interested in.
                                CVFluoroChrome fluor = new CVFluoroChrome();
                                int red = -1, green = -1, blue = -1;

                                var binaryReader = new BinaryReader(stream);

                                while (true)
                                {
                                    short id = binaryReader.ReadInt16();
                                    short size = binaryReader.ReadInt16();

                                    if (id == 101)
                                    {
                                        // Name
                                        if (size > 0)
                                        {
                                            byte[] nameBytes = new byte[size];
                                            stream.Read(nameBytes, 0, size);
                                            fluor.Name = System.Text.Encoding.ASCII.GetString(nameBytes);
                                        }
                                    }
                                    else if (id == 102)
                                    {   // TODO throw exception if size != 2
                                        red = binaryReader.ReadInt16();
                                    }
                                    else if (id == 103)
                                    {   // TODO throw exception if size != 2
                                        green = binaryReader.ReadInt16();
                                    }
                                    else if (id == 104)
                                    {   // TODO throw exception if size != 2
                                        blue = binaryReader.ReadInt16();
                                    }
                                    else if (id == 106)
                                    {
                                        // Flags.
                                        // TODO throw exception if size != 4
                                        fluor.flagsBitField = binaryReader.ReadInt32();
                                    }
                                    else if (id == 402)
                                    {
                                        // Number of zStack planes.
                                        // TODO throw exception if size != 2
                                        fluor.zStackNumImages = binaryReader.ReadInt16();
                                    }
                                    else if (id == 403)
                                    {
                                        // Plane separation for zStack.
                                        // TODO throw exception if size != 2
                                        fluor.zStackFocusDist = binaryReader.ReadInt16();
                                    }
                                    else if (id == 110)
                                    {
                                        // Skip filter information.
                                        // This breaks the rules so must be handled separately, even just to skip!
                                        size -= 4; // Stored size always bigger than string size.
                                        if (size > 0)
                                        {
                                            int filterIndex = binaryReader.ReadInt32();
                                            int filterPosition = binaryReader.ReadInt32();
                                            byte[] nameBytes = new byte[size];
                                            stream.Read(nameBytes, 0, size);
                                            string filterName = System.Text.Encoding.ASCII.GetString(nameBytes);
                                        }
                                    }
                                    else
                                    {
                                        // Skip this field.
                                        if (size > 0)
                                            stream.Seek(size, SeekOrigin.Current);

                                        if (id == 999)
                                            break; // Have read end of fluorochrome marker.
                                    }


                                    /*
                                    else if (   (id == 105 || id == 107) // gamma, maxIntegration
                                             ||  id == 111 || id == 112  // maxGain, minContrast
                                             || (id >= 201 && id <= 207) // xOffset, yOffset, zOffset, x, y, xStretch, yStretch
                                             || (id >= 301 && id <= 305) // gain, offset, integration, averaging, lamp
                                             ||  id == 404 || id == 405  // zStack percentBelow and objectSize (IDs 404 and 405),
                                             || (id >= 501 && id <= 504) // xROI, yROI, wROI, hROI (IDs 501 to 504),
                                             ||  id == 109               // fluo id
                                             || (id >= 701 && id <= 707) // brightening, topEndThresh, contrastStretch, sharpen1, sharpen2, bottomEndThresh, spare2
                                            )
                                    {
                                        // Skip these short (2 byte) fields and the id and size fields for each.
                                        stream.Seek(2 + 2 + 2, SeekOrigin.Current);
                                    }*/
                                    // Skip floats for autosetup min and max (IDs 601 and 602) and int for autosetup probe (ID 603)
                                    // - all 4 bytes - plus id and size fields.
                                    //stream.Seek((2 + 2 + 4) * 3, SeekOrigin.Current);
                                }

                                red   = (red   >> 8) & 0xFF;
                                green = (green >> 8) & 0xFF;
                                blue  = (blue  >> 8) & 0xFF;

                                if (red >= 0 && green >= 0 && blue >= 0)
                                    fluor.colour = Color.FromRgb((byte)red, (byte)green, (byte)blue);

                                cvFluoroChromes.Add(fluor);
                            }
                        }
                    }
                }
                catch (IOException)
                {
                    status = false;
                }
            }

            return status;
        }
    }
}
