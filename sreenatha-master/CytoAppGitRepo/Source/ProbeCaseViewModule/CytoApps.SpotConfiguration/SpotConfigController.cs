﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Windows.Media;
using System.IO;
using CytoApps.Infrastructure.UI.Utilities;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Linq;
using System.Text;

namespace CytoApps.SpotConfiguration
{
    public class SpotConfigController : AI.Notifier
    {
        private string assayFilePath;
        private CVFluoroChrome counterStain;
        private Dictionary<string, ParamSet> scanParameters;
        private int probeSeparation;
        private int fusionSeparation;

        private ObservableCollection<CVFluoroChrome> cvFluoroChromes;
        private ObservableCollection<Probe> probeList;
        private ObservableCollection<Fusion> fusionList;
        private ObservableCollection<Ratio> ratioList;
        private ObservableCollection<SpotClass> classList;
        private ObservableCollection<string> cellClassifiers;
        private AssayInfo _assayInfo;
        private byte[] _assayData;

        public SpotConfigController(byte[] assayData, AssayInfo assayinfo, bool reprocessMode = false, string defaultName = null)
        {
            _assayData = assayData;
            _assayInfo = assayinfo;
            ReprocessMode = reprocessMode;

            Paths = new CVPaths();
            AssayName = "[unnamed]"; // HACK
            if (defaultName != null)
                AssayName = defaultName;

            CounterStain = new CVFluoroChrome(); // Default to a non-null value, in case this isn't set later.

            cellClassifiers = new ObservableCollection<string>(_assayInfo.CellClassifierNames);

            // Load fluorochrome list from file in default folder.
            Fluolist fluolist = new Fluolist();
            fluolist.Load(Paths.FluolistFilePath);
            cvFluoroChromes = new ObservableCollection<CVFluoroChrome>(fluolist.cvFluoroChromes.OrderBy(f => f.Name));

            CurrentCellClassifier = "Everything";
            scanParameters = new Dictionary<string, ParamSet>();
            Assay.AddParamsFromScript(_assayInfo.SpotScript, scanParameters);
            ScriptParams = new ScriptParameters(scanParameters);
            probeList = new ObservableCollection<Probe>();
            fusionList = new ObservableCollection<Fusion>();
            ratioList = new ObservableCollection<Ratio>();

            // Choose a default counterstain, so that the user doesn't have to be reminded to not leave this unselected.
            if (cvFluoroChromes.Count > 0)
            {
                IEnumerable<CVFluoroChrome> dapis = cvFluoroChromes.Where(f => f.Name.ToUpper().Equals("DAPI"));
                if (dapis.Count() > 0)
                    CounterStain = dapis.First();
                else
                {
                    dapis = cvFluoroChromes.Where(f => f.Name.ToUpper().Contains("DAPI"));
                    if (dapis.Count() > 0)
                        CounterStain = dapis.First();
                    else
                    {
                        dapis = cvFluoroChromes.Where(f => f.IsCounterstain); // This flag can't normally be relied upon, but try it anyway!
                        if (dapis.Count() > 0)
                            CounterStain = dapis.First();
                        else
                            CounterStain = cvFluoroChromes[0];
                    }
                }
            }

            ProbeSeparation = 15; // Recommended default
            FusionSeparation = 8; // Recommended default

            classList = new ObservableCollection<SpotClass>();
            var newClass = AddClass();
            SetNormal(newClass);
        }

        //public static SpotConfigController FromFile(string assayFilePath, bool reprocessMode = true)
        //{
        //    // Note that reprocessMode is for an assay associated with a particular scan, it isn't just for when re-analysing.
        //    // It means that a) After LoadAssay() is called, AssayName will be the original name, loaded from inside
        //    //                  the assay file, not the current file name (see comments in LoadAssay()).
        //    //               b) A SpotConfiguration GUI using this controller will not be allowed to modify the probe list.
        //    SpotConfigController c = new SpotConfigController(assayFilePath, reprocessMode);

        //    if (!c.LoadAssay())
        //        return null;

        //    return c;
        //}

        public static SpotConfigController FromStream(byte[] assayData, AssayInfo assayinfo, bool reprocessMode = true, string defaultname = null)
        {
            // Note that reprocessMode is for an assay associated with a particular scan, it isn't just for when re-analysing.
            // It means that a) After LoadAssay() is called, AssayName will be the original name, loaded from inside
            //                  the assay file, not the current file name (see comments in LoadAssay()).
            //               b) A SpotConfiguration GUI using this controller will not be allowed to modify the probe list.
            SpotConfigController c = new SpotConfigController(assayData, assayinfo, reprocessMode, defaultname);
            if(assayData != null)
            {
                using (var ms = new MemoryStream(assayData))
                {
                    if (!c.LoadAssay(ms))
                        return null;
                }
            }           
            return c;
        }

        public IEnumerable<string> UndeleteableClassNames
        {
            get;
            set;
        }

        #region Trivial / ReadOnly Properties

        // UnitTests set this to something weird, you probably should leave it well alone..
        public CVPaths Paths
        {
            get;
            set;
        }

        public bool ReprocessMode
        {
            get;
            set;
        }

        public string AssayName
        {
            get;
            set;
        }

        public string DefaultName
        {
            get;
            private set;
        }

        public IEnumerable<CVFluoroChrome> Fluorochromes
        {
            get { return cvFluoroChromes; }
        }

        public IEnumerable<CVFluoroChrome> FreeFluorochromes
        {
            get
            {
                return cvFluoroChromes.Where(f =>
                {
                    bool isCounterStain = CounterStain != null && CounterStain.Name == f.Name;
                    bool isInUse = probeList.FirstOrDefault(p => p.Name == f.Name) != null;
                    return (!isCounterStain && !isInUse);
                });
            }
        }

        public IEnumerable<Probe> ProbeList
        {
            get { return probeList; }
        }

        public IEnumerable<Fusion> FusionList
        {
            get { return fusionList; }
        }

        public IEnumerable<Ratio> RatioList
        {
            get { return ratioList; }
        }

        public IEnumerable<SpotClass> ClassList
        {
            get { return classList; }
        }

        public IEnumerable<string> CellClassifiers
        {
            get { return cellClassifiers; }
        }

        public CVFluoroChrome CounterStain
        {
            get { return counterStain; }
            set { counterStain = value; Notify("CounterStain", "CounterStainPseudoColour"); }
        }

        public Color CounterStainPseudoColour
        {
            get { return CounterStain.colour; }
            set { CounterStain.colour = value; Notify("CounterStainPseudoColour"); }
        }

        public bool CanAddProbes
        {
            get { return probeList.Count < Constants.MAXPROBES && !ReprocessMode; }
        }

        public bool FusionsAreDefined
        {
            get { return fusionList.Count > 0; }
        }

        public bool AnyAmplified
        {
            get { return probeList.Where(p => p.IsAmplified == true).Count() > 0; }
            set { Notify("AnyAmplified", "CanAddFusions"); }
        }

        public bool CanAddFusions
        {
            get { return fusionList.Count < Constants.MAXFUSIONS && probeList.Count > 1 && !AnyAmplified; }
        }

        public bool MultipleFusionsAllowed
        {
            get { return Constants.MAXFUSIONS > 1; }
        }

        public bool CanAddRatio
        {
            get { return probeList.Count > 1 && ratioList.Count < Constants.MAXRATIOS; }
        }

        public string CurrentCellClassifier
        {
            get;
            set;
        }

        public int AbnormalCount
        {
            get;
            set;
        }

        public int InformativeCount
        {
            get;
            set;
        }

        public int UninformativeCount
        {
            get;
            set;
        }

        public int ProbeSeparation
        {
            get { return probeSeparation; }
            set { probeSeparation = value; Notify("ProbeSeparation"); Notify("ProbeSeparationMicrons"); }
        }

        public double ProbeSeparationMicrons
        {
            get { return ProbeSeparation / 10.0; }
        }

        public int FusionSeparation
        {
            get { return fusionSeparation; }
            set { fusionSeparation = value; Notify("FusionSeparation"); Notify("FusionSeparationMicrons"); }
        }

        public double FusionSeparationMicrons
        {
            get { return FusionSeparation / 10.0; }
        }

        public ScriptParameters ScriptParams
        {
            get;
            private set;
        }

        #endregion

        #region ParamSet Set/Get

        private bool GetParamSetValue(string key, out string value)
        {
            bool gotIt = false;
            value = String.Empty;
            if (scanParameters != null)
            {
                ParamSet ps;
                gotIt = scanParameters.TryGetValue(key, out ps);
                if (gotIt && ps != null)
                    value = ps.value;
            }
            return gotIt;
        }
        private int GetParamSetValue(string key, int defaultValue)
        {
            int value = defaultValue;
            string stringValue;
            if (GetParamSetValue(key, out stringValue))
            {
                if (!Int32.TryParse(stringValue, out value))
                    value = defaultValue;
            }
            return value;
        }
        private bool GetParamSetColourValue(string key, out Color value)
        {
            bool status = false;

            int colVal = GetParamSetValue(key, -1);
            if (colVal >= 0)
                status = true;

            value = Color.FromRgb((byte)(colVal & 0xFF), (byte)((colVal >> 8) & 0xFF), (byte)((colVal >> 16) & 0xFF));

            return status;
        }

        #endregion

        public Probe AddProbe(CVFluoroChrome fluor)
        {
            // Add this fluorochrome to the probe list, unless the maximum number of probes has been reached
            // or this probe is already in the list.
            if (probeList.Count < Constants.MAXPROBES && probeList.Where(p => p.Name.Equals(fluor.Name)).Count() < 1)
            {
                var p = new Probe(fluor);
                probeList.Add(p);

                // For each fusion, add a component corresponding to the new probe.
                foreach (var f in fusionList)
                    f.Components.Add(new FusionComponent() { ProbeName = fluor.Name });

                // For each class, add a CountOrRatio corresponding to the new probe.
                foreach (var c in classList)
                    c.ProbeCounts.Add(new CountOrRatio());

                Notify("CanAddProbes", "CanAddFusions", "CanAddRatio", "CanAddRatio", "FreeFluorochromes", "AnyAmplified");
                return p;
            }

            return null;
        }

        public bool IsProbeInUse(Probe p)
        {
            // Is this probe used by a ratio or fusion?
            // (If it is, better not delete it, otherwise the ratio of fusion will be invalid).
            bool isInUse = false;

            foreach (var r in ratioList)
            {
                if (r.NumeratorProbeName.Equals(p.Name) || r.DenominatorProbeName.Equals(p.Name))
                    isInUse = true;
            }

            foreach (var f in fusionList)
            {
                foreach (var c in f.Components)
                {
                    if (c.ProbeName.Equals(p.Name) && c.IsSelected)
                        isInUse = true;
                }
            }

            return isInUse;
        }

        public void RemoveProbe(Probe p)
        {
            // For each class, remove the CountOrRatio corresponding with this probe.
            int indexOfCurrentProbe = probeList.IndexOf(p);
            if (indexOfCurrentProbe >= 0)
            {
                foreach (var c in classList)
                    c.ProbeCounts.RemoveAt(indexOfCurrentProbe);
            }

            // For each fusion, remove the component corresponding to the new probe.
            foreach (var f in fusionList)
            {
                FusionComponent fc = null;
                foreach (var c in f.Components)
                    if (c.ProbeName.Equals(p.Name)) fc = c;
                if (fc != null)
                    f.Components.Remove(fc);
            }

            // Now remove the probe from the probe list.
            probeList.Remove(p);

            Notify("CanAddProbes", "CanAddFusions", "CanAddRatio", "FreeFluorochromes", "AnyAmplified");
        }

        public int GetIndexOfProbeByName(string name)
        {
            int foundAtIndex = -1;
            int i = 0;
            foreach (var p in probeList)
            {
                if (p.Name.Equals(name))
                    foundAtIndex = i;
                i++;
            }
            return foundAtIndex;
        }


        public void AddRatio(Ratio newRatio)
        {
            if (ratioList.Count < Constants.MAXRATIOS)
            {
                ratioList.Add(newRatio);

                foreach (var c in classList)
                    c.Ratios.Add(new CountOrRatio());

                Notify("CanAddRatio");
            }
        }
        public void RemoveRatio(Ratio ratio)
        {
            int index = ratioList.IndexOf(ratio);
            ratioList.Remove(ratio);

            foreach (var c in classList)
                c.Ratios.RemoveAt(index);

            Notify("CanAddRatio");
        }

        public void RemoveRatiosWithoutCurrentProbes()
        {
            for (int i = 0; i < ratioList.Count; i++)
            {
                Ratio r = ratioList[i];
                if (GetIndexOfProbeByName(r.NumeratorProbeName) < 0 || GetIndexOfProbeByName(r.DenominatorProbeName) < 0)
                {
                    RemoveRatio(r);
                    i--;
                }
            }
        }


        public Fusion CreateNewFusion()
        {
            Fusion newFusion = new Fusion() { Colour = Colors.White };

            foreach (var p in probeList)
                newFusion.Components.Add(new FusionComponent() { ProbeName = p.Name });

            return newFusion;
        }
        public void AddFusion(Fusion newFusion)
        {
            if (fusionList.Count < Constants.MAXFUSIONS)
            {
                fusionList.Add(newFusion);

                foreach (var c in classList)
                    c.FusionCounts.Add(new CountOrRatio());

                Notify("FusionsAreDefined", "CanAddFusions", "AnyAmplified");
            }
        }
        public void RemoveFusion(Fusion f)
        {
            // For each class, remove the CountOrRatio corresponding with this fusion.
            int indexOfCurrentFusion = fusionList.IndexOf(f);
            if (indexOfCurrentFusion >= 0)
            {
                foreach (var c in classList)
                    c.FusionCounts.RemoveAt(indexOfCurrentFusion);
            }

            // Now remove the fusion from the fusion list.
            fusionList.Remove(f);

            Notify("FusionsAreDefined", "CanAddFusions", "AnyAmplified");
        }

        public bool IsFusionCombinationUnique(Fusion fusionToCheck)
        {
            foreach (var f in fusionList)
            {
                if (f == fusionToCheck)
                    continue;

                int matchingCount = 0;
                foreach (var c in f.Components)
                {
                    foreach (var fc in fusionToCheck.Components)
                    {
                        if (c.ProbeName.Equals(fc.ProbeName) && c.IsSelected == fc.IsSelected)
                            matchingCount++;
                    }
                }
                if (matchingCount == f.Components.Count && matchingCount > 0)
                    return false;
            }

            return true;
        }

        public bool IsFusionNameInUse(string name)
        {
            foreach (var f in fusionList)
            {
                if (f.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                    return true;
            }

            return false;
        }

        public void AddClass(SpotClass newClass)
        {
            classList.Add(newClass);

            foreach (var p in probeList)
                newClass.ProbeCounts.Add(new CountOrRatio());

            foreach (var p in ratioList)
                newClass.Ratios.Add(new CountOrRatio());

            foreach (var f in fusionList)
                newClass.FusionCounts.Add(new CountOrRatio());

            EnsureExactlyOneClassMarkedAsNormal();
        }

        public SpotClass AddClass()
        {
            if (classList.Count < Constants.MAXCLASSES)
            {
                SpotClass newClass = new SpotClass() { Name = "New Class", DisplayColour = Colors.White };

                foreach (var p in probeList)
                    newClass.ProbeCounts.Add(new CountOrRatio());

                foreach (var p in ratioList)
                    newClass.Ratios.Add(new CountOrRatio());

                foreach (var f in fusionList)
                    newClass.FusionCounts.Add(new CountOrRatio());

                classList.Add(newClass);

                EnsureExactlyOneClassMarkedAsNormal();

                return newClass;
            }

            return null;
        }


        public void DeleteClass(SpotClass classToDelete)
        {
            classList.Remove(classToDelete);
            EnsureExactlyOneClassMarkedAsNormal(); // In case the normal class was deleted.
        }

        public void SetNormal(SpotClass classToSet)
        {
            foreach (var c in classList)
                c.IsNormal = (c != classToSet) ? false : true;
        }

        private void EnsureExactlyOneClassMarkedAsNormal()
        {
            bool foundOne = false;
            foreach (var c in classList)
            {
                if (foundOne)
                    c.IsNormal = false;
                else if (c.IsNormal)
                    foundOne = true;
            }

            if (!foundOne && classList.Count > 0)
                classList[0].IsNormal = true;
        }


        /////////////////////////////////////////////////////////////////////////////////////////
        //
        // Serialisation code. Load and save.
        //
        // Hmm, are 'serialisation' and 'load and save' the same thing? Discuss.
        //

        private bool LoadAssay(Stream assayStream)
        {
            // Load assay file, or if one with this name doesn't exist, create one.
            Assay assay = new Assay(_assayInfo);//Assay(Paths.AIISharedPath);
            if (!assay.Load(assayStream))
                return false;


            // Get the assay name. This is the name that will be displayed and that will be stored inside
            // the assay when it is saved, not necessarily the name of the file.
            //
            // If reprocessing, the assay is assumed to be a file in the scan folder, that was copied from
            // the default assay. Its filename may be different, but its internal name is still the name of
            // the default assay that it was copied from. Since the name of the original default assay is
            // retained in the file, the name of the source assay can be presented to the user and the
            // 'save as default' button can know which default assay to overwrite (or re-create).
            // 
            // Otherwise, the assay is assumed to be a default assay and its internal name should be the
            // name as its filename. In case it isn't, the name is taken from the filename (without extension),
            // so that this also becomes the internal name when it is saved.
            // An example of how the internal name could become different from the filename for a default
            // assay, is if a copy of the file has been made manually. In this case the user would expect
            // the new assay's name to be the name they gave the file, however if they subsequently used
            // this assay for scanning and eventually reprocessed the scan, 'save as default' would overwrite
            // the default assay matching the internal name, not the default assay that was used to create
            // the scan.
            // Note however, that the 'duplicate assay' button in CytoVision copies an assay file AND updates
            // its internal name to match the new file's name.
            DefaultName = assay.GetInternalName();

            if (ReprocessMode)
                AssayName = DefaultName;
            else
                AssayName = Path.GetFileNameWithoutExtension(assayFilePath);


            CurrentCellClassifier = assay.GetClassifierName();


            // Extract parameter sets from assay and script and use to fill various data structures.
            scanParameters = assay.GetParamSets();

            ScriptParams = new ScriptParameters(scanParameters);


            // There are fluorochromes stored in the assay in an Ariol-style 'spectrachrome list',
            // however these are only used for looking up some properties of fluorochromes, such
            // as Z-stack settings. The master list of fluorochromes is determined by the values
            // of various ParamSets in the assay (though these should match the spectrachromes).
            Dictionary<string, SpectraChromeData> spectraChromes = assay.GetSpectraChromes();

            probeList = ParseProbesFromParamSets(spectraChromes);

            ratioList = ParseRatiosFromParamSets();

            fusionList = ParseFusionsFromParamSets();


            string counterstainName;
            if (GetParamSetValue("COUNTERSTAIN", out counterstainName))
            {
                CounterStain = null;
                foreach (var f in cvFluoroChromes)
                {
                    if (f.Name.Equals(counterstainName))
                        CounterStain = f;
                }

                // If this counterstain isn't in the fluorlist on this system, create a CVFluoroChrome object for it.
                // It might not be possible to scan with this assay if this counterstain isn't configured on this system,
                // but it must be possible to reprocess with it, as images will already have been captured with it.
                if (CounterStain == null)
                {
                    CounterStain = new CVFluoroChrome() { Name = counterstainName };
                    cvFluoroChromes.Add(CounterStain);
                }
            }

            Color colour;
            if (GetParamSetColourValue("COUNTERSTAIN_PSEUDOCOLOUR", out colour))
                CounterStainPseudoColour = colour;
            else
                CounterStainPseudoColour = Colors.White;


            AbnormalCount = GetParamSetValue("ABNORMALCELLCOUNT", 0);
            InformativeCount = GetParamSetValue("TOTALCELLCOUNT", 0);
            UninformativeCount = GetParamSetValue("UNINFORMATIVECELLCOUNT", 0);

            ProbeSeparation = GetParamSetValue("SAMESPOTPERCENT", 15); // Tenths of a micron: named 'SAMESPOTPERCENT' for historical reasons.
            FusionSeparation = GetParamSetValue("FUSEDPERCENT", 8); // Tenths of a micron: named 'FUSEDPERCENT' for historical reasons.


            classList = ParseClassesFromParamSets();
            EnsureExactlyOneClassMarkedAsNormal();

            return true;
        }


        public bool SaveAssay(/*bool overwriteOriginalDefault = false*/)
        {
            Assay newAssay = new Assay(_assayInfo);//Assay(Paths.AIISharedPath);

            Dictionary<string, ParamSet> spotParams = new Dictionary<string, ParamSet>();
            Dictionary<string, SpectraChromeData> spectraChromeData = new Dictionary<string, SpectraChromeData>();

            spotParams.Add("ASSAYNAME", new ParamSet("Assay Name", AssayName));

            spotParams.Add("DISEASENAME", new ParamSet("Disease Name", "DISEASENAME")); // Is DISEASENAME still used?? If not, remove it from here?
            spotParams.Add("SEGMENTATIONMODE", new ParamSet("Segmentation Mode", 0, 0, 2)); // Is SEGMENTATIONMODE still needed?? If not, remove it from here?
            spotParams.Add("SPOTMEASUREMENTMODE", new ParamSet("Spot Measurement Mode", 2, 1, 2)); // Is SPOTMEASUREMENTMODE still used?? If not, remove it from here?

            spotParams.Add("COUNTERSTAIN", new ParamSet("Counterstain", CounterStain.Name));
            spotParams.Add("COUNTERSTAIN_PSEUDOCOLOUR", new ParamSet("Counterstain Pseudo Colour", CounterStainPseudoColour, false));

            spotParams.Add("NUMBERFLUORS", new ParamSet("Number Fluors", probeList.Count, 1, Constants.MAXPROBES));
            int probeIndex = 0;
            foreach (var p in probeList)
            {
                spotParams.Add("FLUOROCHROMENAMES" + probeIndex, new ParamSet("Fluorochrome Names", p.Name));
                spotParams.Add(p.Name + "_PSEUDOCOLOUR", new ParamSet("Pseudo Colours", p.PseudoColour, false));
                spotParams.Add(p.Name + "_ISAMPLIFIED", new ParamSet("Amplification Flag", p.IsAmplified ? 1 : 0, 0, 1));
                spectraChromeData.Add(p.Name, new SpectraChromeData() { zNumImages = p.ZPlanes.ToString(), zDist = (p.ZDistMicrons * 100).ToString("F0") });
                probeIndex++;
            }

            // Before saving the ratio descriptions, remove any that refer to probes that are no longer in probeList (though
            // the GUI should never allow this to happen).
            // This is essential, as they are saved with reference to indices into the probe list, so the indices must be valid.
            RemoveRatiosWithoutCurrentProbes();

            spotParams.Add("RATIOCOUNT", new ParamSet("Number Of Ratios", ratioList.Count, 0, Constants.MAXRATIOS));
            int ratioIndex = 0;
            foreach (var r in ratioList)
            {
                // Both indices should be valid, because RemoveRatiosWithoutCurrentProbes() was previously called.
                int iNumer = GetIndexOfProbeByName(r.NumeratorProbeName);
                int iDenom = GetIndexOfProbeByName(r.DenominatorProbeName);
                spotParams.Add("RATIO" + ratioIndex + "_NAME", new ParamSet("Ratio Name", r.Name));
                spotParams.Add("RATIO" + ratioIndex + "_NUMERATORPROBEINDEX", new ParamSet("Ratio Numerator Probe Index", iNumer, 0, probeList.Count));
                spotParams.Add("RATIO" + ratioIndex + "_DENOMINATORPROBEINDEX", new ParamSet("Ratio Denominator Probe Index", iDenom, 0, probeList.Count));
                ratioIndex++;
            }

            // For backward compatibility, add ParamSets that define a fusion in the old format. As this only
            // supports at most one fusion, involving at most two probes, just use the first fusion.
            // TODO: When required, also save all the fusions in a new format that supports multiple fusions.
            int fusionEnabled = 0;
            int fusionProbeIndex1 = 0;
            int fusionProbeIndex2 = 0;
            if (fusionList.Count > 0 && probeList.Count > 1) // Can't have a fusion if only one probe!
            {
                Fusion firstFusion = fusionList[0];
                int numDone = 0;
                for (int componentIndex = 0; componentIndex < firstFusion.Components.Count; componentIndex++)
                {
                    if (firstFusion.Components[componentIndex].IsSelected)
                    {
                        if (numDone == 0) { fusionProbeIndex1 = componentIndex; numDone++; }
                        else if (numDone == 1) { fusionProbeIndex2 = componentIndex; numDone++; }
                    }
                }
                fusionEnabled = (numDone > 1) ? 1 : 0;
            }
            spotParams.Add("FUSIONENABLED", new ParamSet("Fusion enabled", fusionEnabled, 0, 1));
            spotParams.Add("FUSIONPROBEIMAGE1", new ParamSet("Fusion Probe Image1", fusionProbeIndex1, 0, Math.Max(0, probeList.Count - 1)));
            spotParams.Add("FUSIONPROBEIMAGE2", new ParamSet("Fusion Probe Image2", fusionProbeIndex2, 0, Math.Max(0, probeList.Count - 1)));

            spotParams.Add("ABNORMALCELLCOUNT", new ParamSet("Total Abnormal Cells", AbnormalCount, 1, 100000));
            spotParams.Add("TOTALCELLCOUNT", new ParamSet("Total Cells", InformativeCount, 1, 100000));
            spotParams.Add("UNINFORMATIVECELLCOUNT", new ParamSet("Total Uninformative Cells", UninformativeCount, 1, 100000));

            spotParams.Add("SAMESPOTPERCENT", new ParamSet("Same Spot Percent", ProbeSeparation, 0, 1000)); // Tenths of a micron: named 'SAMESPOTPERCENT' for historical reasons.
            spotParams.Add("FUSEDPERCENT", new ParamSet("Fused Percent", FusionSeparation, 0, 1000)); // Tenths of a micron: named 'FUSEDPERCENT' for historical reasons.

            spotParams.Add("CLASSESSTORED", new ParamSet("Classes Stored", classList.Count, 1, Constants.MAXCLASSES));
            List<string> classNames = new List<string>();
            List<string[]> classesProbeFormulae = new List<string[]>();
            List<string[]> classesRatioFormulae = new List<string[]>();
            List<string[]> classesFusionFormulae = new List<string[]>();
            int classIndex = 0;
            foreach (var c in classList)
            {
                spotParams.Add("CLASS" + classIndex + "_NAME", new ParamSet("CLASS" + classIndex + "_NAME", c.Name));
                spotParams.Add("CLASS" + classIndex + "_COLOUR", new ParamSet("CLASS" + classIndex + "_COLOUR", c.DisplayColour, true));
                // It is allowed to have multiple classes with the same name (the final classification is then an OR of each criteria),
                // however due a design flaw in the assay file format, the per-class stop counts are by class name, rather than class index.
                // In the original implementation, the count for each class would be stored in the file, so there could be multiple counts
                // with the same name. However, as the counts were looked up by name, only one of them would be used when the assay was read.
                // In this implementation, if there are multiple classes with the same name, only the stop count for the first one will be
                // saved, then this will be used for all of them when the assay is loaded (an equivalent result to before).
                if (!spotParams.ContainsKey(c.Name))
                    spotParams.Add(c.Name, new ParamSet("Count Per Class " + classIndex, c.StopCount, 1, 100000));

                if (c.IsNormal)
                {
                    // The normal class is referenced principally by index, so that the correct class can be selected when there are other classes with the same name.
                    spotParams.Add("NORMALCLASS", new ParamSet("Normal Class", classIndex, 0, Math.Max(0, classList.Count - 1)));
                    spotParams.Add("NORMALCLASSNAME", new ParamSet("Normal Class Name", c.Name));
                }

                classNames.Add(c.Name);

                string[] probeFormulae = new string[probeList.Count];
                int probeFormulaIndex = 0;
                foreach (var p in c.ProbeCounts)
                {
                    probeFormulae[probeFormulaIndex] = p.Formula;

                    if (c.IsNormal)
                        spotParams.Add("DEFAULT_COUNT" + probeFormulaIndex, new ParamSet("Default count", Assay.ExtractDefaultCountFromFormula(p.Formula), 1, 100000));

                    probeFormulaIndex++;
                }
                classesProbeFormulae.Add(probeFormulae);

                string[] ratioFormulae = new string[ratioList.Count];
                int ratioFormulaIndex = 0;
                foreach (var f in c.Ratios)
                    ratioFormulae[ratioFormulaIndex++] = f.Formula;
                classesRatioFormulae.Add(ratioFormulae);

                string[] fusionFormulae = new string[fusionList.Count];
                int fusionFormulaIndex = 0;
                foreach (var f in c.FusionCounts)
                    fusionFormulae[fusionFormulaIndex++] = f.Formula;
                classesFusionFormulae.Add(fusionFormulae);

                classIndex++;
            }


            spotParams.Add("CLASSIFIERFUNCTION2", new ParamSet("Classifier Function v2", Assay.CreateClassifyFunction2(classNames, classesProbeFormulae, classesRatioFormulae, classesFusionFormulae)));
            // Save a standard classifier function too, to prevent old code crashing when it doesn't find it.
            //spotParams.Add("CLASSIFIERFUNCTION", new ParamSet("Classifier Function", Assay.CreateClassifyFunction(null, null, null)));
            spotParams.Add("CLASSIFIERFUNCTION", new ParamSet("Classifier Function", Assay.CreateClassifyFunction1(classNames, classesProbeFormulae, classesFusionFormulae)));


            spotParams.Add("STOPAUTOCAPTUREFUNCTION", new ParamSet("Stop Function", Assay.CreateStopFunction(classNames)));


            ScriptParams.AddToParamSets(spotParams);


            newAssay.Create(AssayName, CurrentCellClassifier, spotParams, Paths.SpectrachromesFilePath, spectraChromeData, CounterStain.Name);

            //TODO Sreenatha: Commented out 
            //////bool status = false;

            //////if (assayFilePath != null && assayFilePath.Length > 0 && !SpotConfiguration.AssayInfo.IsCEPXYAssayName(AssayName))
            //////{
            //////    // A temporary measure to prevent existing default assay files from potentially being destroyed by this untested new app:
            //////    // if there is an existing assay file with this name, but no backup of it, create a backup before overwriting.
            //////    // TODO: remove this before building the final release!
            //////    if (File.Exists(assayFilePath) && !File.Exists(assayFilePath + ".scbak"))
            //////        File.Copy(assayFilePath, assayFilePath + ".scbak");

            //////    if (newAssay.Save(assayFilePath))
            //////    {
            //////        /*if (overwriteOriginalDefault)
            //////        {
            //////            if (DefaultName.Length > 0)
            //////            {
            //////                string defaultFile = newAssay.GetSpotAssayFilePath() + @"\" + DefaultName + ".assay";

            //////                if (File.Exists(defaultFile) && !File.Exists(defaultFile + ".bup"))
            //////                    File.Copy(defaultFile, defaultFile + ".bup");

            //////                if (newAssay.Save(defaultFile))
            //////                    status = true;
            //////            }
            //////        }
            //////        else*/
            //////        status = true;
            //////    }
            //////}

            ////// return status;
            return true;
        }

        //TODO:Sreenatha: Not sure all the things are required
        public byte[] SaveByteAssay(/*bool overwriteOriginalDefault = false*/string newAssyname= null)
        {
            Assay newAssay = new Assay(_assayInfo);//Assay(Paths.AIISharedPath);
            //to save as assay file
            if(newAssyname != null)
            {
                AssayName = newAssyname;
            }

            Dictionary<string, ParamSet> spotParams = new Dictionary<string, ParamSet>();
            Dictionary<string, SpectraChromeData> spectraChromeData = new Dictionary<string, SpectraChromeData>();

            spotParams.Add("ASSAYNAME", new ParamSet("Assay Name", AssayName));

            spotParams.Add("DISEASENAME", new ParamSet("Disease Name", "DISEASENAME")); // Is DISEASENAME still used?? If not, remove it from here?
            spotParams.Add("SEGMENTATIONMODE", new ParamSet("Segmentation Mode", 0, 0, 2)); // Is SEGMENTATIONMODE still needed?? If not, remove it from here?
            spotParams.Add("SPOTMEASUREMENTMODE", new ParamSet("Spot Measurement Mode", 2, 1, 2)); // Is SPOTMEASUREMENTMODE still used?? If not, remove it from here?

            spotParams.Add("COUNTERSTAIN", new ParamSet("Counterstain", CounterStain.Name));
            spotParams.Add("COUNTERSTAIN_PSEUDOCOLOUR", new ParamSet("Counterstain Pseudo Colour", CounterStainPseudoColour, false));

            spotParams.Add("NUMBERFLUORS", new ParamSet("Number Fluors", probeList.Count, 1, Constants.MAXPROBES));
            int probeIndex = 0;
            foreach (var p in probeList)
            {
                spotParams.Add("FLUOROCHROMENAMES" + probeIndex, new ParamSet("Fluorochrome Names", p.Name));
                spotParams.Add(p.Name + "_PSEUDOCOLOUR", new ParamSet("Pseudo Colours", p.PseudoColour, false));
                spotParams.Add(p.Name + "_ISAMPLIFIED", new ParamSet("Amplification Flag", p.IsAmplified ? 1 : 0, 0, 1));
                spectraChromeData.Add(p.Name, new SpectraChromeData() { zNumImages = p.ZPlanes.ToString(), zDist = (p.ZDistMicrons * 100).ToString("F0") });
                probeIndex++;
            }

            // Before saving the ratio descriptions, remove any that refer to probes that are no longer in probeList (though
            // the GUI should never allow this to happen).
            // This is essential, as they are saved with reference to indices into the probe list, so the indices must be valid.
            RemoveRatiosWithoutCurrentProbes();

            spotParams.Add("RATIOCOUNT", new ParamSet("Number Of Ratios", ratioList.Count, 0, Constants.MAXRATIOS));
            int ratioIndex = 0;
            foreach (var r in ratioList)
            {
                // Both indices should be valid, because RemoveRatiosWithoutCurrentProbes() was previously called.
                int iNumer = GetIndexOfProbeByName(r.NumeratorProbeName);
                int iDenom = GetIndexOfProbeByName(r.DenominatorProbeName);
                spotParams.Add("RATIO" + ratioIndex + "_NAME", new ParamSet("Ratio Name", r.Name));
                spotParams.Add("RATIO" + ratioIndex + "_NUMERATORPROBEINDEX", new ParamSet("Ratio Numerator Probe Index", iNumer, 0, probeList.Count));
                spotParams.Add("RATIO" + ratioIndex + "_DENOMINATORPROBEINDEX", new ParamSet("Ratio Denominator Probe Index", iDenom, 0, probeList.Count));
                ratioIndex++;
            }

            // For backward compatibility, add ParamSets that define a fusion in the old format. As this only
            // supports at most one fusion, involving at most two probes, just use the first fusion.
            // TODO: When required, also save all the fusions in a new format that supports multiple fusions.
            int fusionEnabled = 0;
            int fusionProbeIndex1 = 0;
            int fusionProbeIndex2 = 0;
            if (fusionList.Count > 0 && probeList.Count > 1) // Can't have a fusion if only one probe!
            {
                Fusion firstFusion = fusionList[0];
                int numDone = 0;
                for (int componentIndex = 0; componentIndex < firstFusion.Components.Count; componentIndex++)
                {
                    if (firstFusion.Components[componentIndex].IsSelected)
                    {
                        if (numDone == 0) { fusionProbeIndex1 = componentIndex; numDone++; }
                        else if (numDone == 1) { fusionProbeIndex2 = componentIndex; numDone++; }
                    }
                }
                fusionEnabled = (numDone > 1) ? 1 : 0;
            }
            spotParams.Add("FUSIONENABLED", new ParamSet("Fusion enabled", fusionEnabled, 0, 1));
            spotParams.Add("FUSIONPROBEIMAGE1", new ParamSet("Fusion Probe Image1", fusionProbeIndex1, 0, Math.Max(0, probeList.Count - 1)));
            spotParams.Add("FUSIONPROBEIMAGE2", new ParamSet("Fusion Probe Image2", fusionProbeIndex2, 0, Math.Max(0, probeList.Count - 1)));

            spotParams.Add("ABNORMALCELLCOUNT", new ParamSet("Total Abnormal Cells", AbnormalCount, 1, 100000));
            spotParams.Add("TOTALCELLCOUNT", new ParamSet("Total Cells", InformativeCount, 1, 100000));
            spotParams.Add("UNINFORMATIVECELLCOUNT", new ParamSet("Total Uninformative Cells", UninformativeCount, 1, 100000));

            spotParams.Add("SAMESPOTPERCENT", new ParamSet("Same Spot Percent", ProbeSeparation, 0, 1000)); // Tenths of a micron: named 'SAMESPOTPERCENT' for historical reasons.
            spotParams.Add("FUSEDPERCENT", new ParamSet("Fused Percent", FusionSeparation, 0, 1000)); // Tenths of a micron: named 'FUSEDPERCENT' for historical reasons.

            spotParams.Add("CLASSESSTORED", new ParamSet("Classes Stored", classList.Count, 1, Constants.MAXCLASSES));
            List<string> classNames = new List<string>();
            List<string[]> classesProbeFormulae = new List<string[]>();
            List<string[]> classesRatioFormulae = new List<string[]>();
            List<string[]> classesFusionFormulae = new List<string[]>();
            int classIndex = 0;
            foreach (var c in classList)
            {
                spotParams.Add("CLASS" + classIndex + "_NAME", new ParamSet("CLASS" + classIndex + "_NAME", c.Name));
                spotParams.Add("CLASS" + classIndex + "_COLOUR", new ParamSet("CLASS" + classIndex + "_COLOUR", c.DisplayColour, true));
                // It is allowed to have multiple classes with the same name (the final classification is then an OR of each criteria),
                // however due a design flaw in the assay file format, the per-class stop counts are by class name, rather than class index.
                // In the original implementation, the count for each class would be stored in the file, so there could be multiple counts
                // with the same name. However, as the counts were looked up by name, only one of them would be used when the assay was read.
                // In this implementation, if there are multiple classes with the same name, only the stop count for the first one will be
                // saved, then this will be used for all of them when the assay is loaded (an equivalent result to before).
                if (!spotParams.ContainsKey(c.Name))
                    spotParams.Add(c.Name, new ParamSet("Count Per Class " + classIndex, c.StopCount, 1, 100000));

                if (c.IsNormal)
                {
                    // The normal class is referenced principally by index, so that the correct class can be selected when there are other classes with the same name.
                    spotParams.Add("NORMALCLASS", new ParamSet("Normal Class", classIndex, 0, Math.Max(0, classList.Count - 1)));
                    spotParams.Add("NORMALCLASSNAME", new ParamSet("Normal Class Name", c.Name));
                }

                classNames.Add(c.Name);

                string[] probeFormulae = new string[probeList.Count];
                int probeFormulaIndex = 0;
                foreach (var p in c.ProbeCounts)
                {
                    probeFormulae[probeFormulaIndex] = p.Formula;

                    if (c.IsNormal)
                        spotParams.Add("DEFAULT_COUNT" + probeFormulaIndex, new ParamSet("Default count", Assay.ExtractDefaultCountFromFormula(p.Formula), 1, 100000));

                    probeFormulaIndex++;
                }
                classesProbeFormulae.Add(probeFormulae);

                string[] ratioFormulae = new string[ratioList.Count];
                int ratioFormulaIndex = 0;
                foreach (var f in c.Ratios)
                    ratioFormulae[ratioFormulaIndex++] = f.Formula;
                classesRatioFormulae.Add(ratioFormulae);

                string[] fusionFormulae = new string[fusionList.Count];
                int fusionFormulaIndex = 0;
                foreach (var f in c.FusionCounts)
                    fusionFormulae[fusionFormulaIndex++] = f.Formula;
                classesFusionFormulae.Add(fusionFormulae);

                classIndex++;
            }


            spotParams.Add("CLASSIFIERFUNCTION2", new ParamSet("Classifier Function v2", Assay.CreateClassifyFunction2(classNames, classesProbeFormulae, classesRatioFormulae, classesFusionFormulae)));
            // Save a standard classifier function too, to prevent old code crashing when it doesn't find it.
            //spotParams.Add("CLASSIFIERFUNCTION", new ParamSet("Classifier Function", Assay.CreateClassifyFunction(null, null, null)));
            spotParams.Add("CLASSIFIERFUNCTION", new ParamSet("Classifier Function", Assay.CreateClassifyFunction1(classNames, classesProbeFormulae, classesFusionFormulae)));


            spotParams.Add("STOPAUTOCAPTUREFUNCTION", new ParamSet("Stop Function", Assay.CreateStopFunction(classNames)));


            ScriptParams.AddToParamSets(spotParams);

            var assay = newAssay.CreateAssayAsXML(AssayName, CurrentCellClassifier, spotParams, Paths.SpectrachromesFilePath, spectraChromeData, CounterStain.Name);          
            return assay;
        }

        public void CacheAssayData(byte[] assayData)
        {
            this._assayData = assayData;
        }

        private ObservableCollection<Probe> ParseProbesFromParamSets(Dictionary<string, SpectraChromeData> spectraChromes)
        {
            // Before this function is called, the assay's ParamSets must have been loaded (into this->scanParameters).

            ObservableCollection<Probe> probes = new ObservableCollection<Probe>();
            int numProbes = GetParamSetValue("NUMBERFLUORS", 0);
            for (int i = 0; i < numProbes; i++)
            {
                string probeName;
                if (GetParamSetValue("FLUOROCHROMENAMES" + i, out probeName))
                {
                    int zPlanes = 0;
                    double zDistMicrons = 0.0;
                    if (spectraChromes.ContainsKey(probeName))
                    {
                        SpectraChromeData sc = spectraChromes[probeName];
                        Int32.TryParse(sc.zNumImages, out zPlanes);
                        int zDistHundredths = 0;
                        Int32.TryParse(sc.zDist, out zDistHundredths);
                        zDistMicrons = zDistHundredths / 100.0;
                    }

                    Color probeColor;
                    if (!GetParamSetColourValue(probeName + "_PSEUDOCOLOUR", out probeColor))
                        probeColor = Colors.White;

                    bool amplificationFlag = GetParamSetValue(probeName + "_ISAMPLIFIED", 0) > 0 ? true : false;

                    Probe newProbe = new Probe()
                    {
                        Name = probeName,
                        ZPlanes = zPlanes,
                        ZDistMicrons = zDistMicrons,
                        PseudoColour = probeColor,
                        IsAmplified = amplificationFlag
                    };

                    probes.Add(newProbe);
                }
            }

            return probes;
        }

        private ObservableCollection<Ratio> ParseRatiosFromParamSets()
        {
            // Before this function is called, this->probeList must have been populated (by calling and ParseProbesFromParamSets()).

            ObservableCollection<Ratio> ratios = new ObservableCollection<Ratio>();
            int numRatios = GetParamSetValue("RATIOCOUNT", 0);
            for (int ratioIndex = 0; ratioIndex < numRatios; ratioIndex++)
            {
                string ratioName;
                if (GetParamSetValue("RATIO" + ratioIndex + "_NAME", out ratioName))
                {
                    int numeratorProbeIndex = GetParamSetValue("RATIO" + ratioIndex + "_NUMERATORPROBEINDEX", -1);
                    int denominatorProbeIndex = GetParamSetValue("RATIO" + ratioIndex + "_DENOMINATORPROBEINDEX", -1);

                    if (numeratorProbeIndex >= 0 && numeratorProbeIndex < probeList.Count
                        && denominatorProbeIndex >= 0 && denominatorProbeIndex < probeList.Count)
                    {
                        ratios.Add(new Ratio(ratioName, probeList[numeratorProbeIndex].Name, probeList[denominatorProbeIndex].Name));
                    }
                }
            }

            return ratios;
        }

        private ObservableCollection<Fusion> ParseFusionsFromParamSets()
        {
            // Before this function is called, the assay's ParamSets must have been loaded (into this->scanParameters)
            // and the probes must have been loaded (into probeList).

            ObservableCollection<Fusion> fusions = new ObservableCollection<Fusion>();

            // TODO: for newer assays that support multiple fusions, load fusion information in the new format instead (yet to be defined)

            if (GetParamSetValue("FUSIONENABLED", 0) != 0)
            {
                int fusionProbeIndex1 = GetParamSetValue("FUSIONPROBEIMAGE1", 0);
                int fusionProbeIndex2 = GetParamSetValue("FUSIONPROBEIMAGE2", 1);

                Fusion newFusion = new Fusion() { Name = "Fusion" };

                int probeIndex = 0;
                foreach (var p in probeList)
                {
                    bool isSelected = (probeIndex == fusionProbeIndex1 || probeIndex == fusionProbeIndex2) ? true : false;

                    newFusion.Components.Add(new FusionComponent() { ProbeName = p.Name, IsSelected = isSelected });

                    probeIndex++;
                }

                fusions.Add(newFusion);
            }

            return fusions;
        }

        private ObservableCollection<SpotClass> ParseClassesFromParamSets()
        {
            // Before this function is called, the assay's ParamSets must have been loaded (into this->scanParameters)
            // and the probes and fusions must have been loaded (into probeList and fusionList).

            Dictionary<string, List<ClassFormulae>> classesFormulae = new Dictionary<string, List<ClassFormulae>>();
            string classifierFunctionString;


            if (GetParamSetValue("CLASSIFIERFUNCTION2", out classifierFunctionString))
                classesFormulae = Assay.ParseClassifyFunction2(classifierFunctionString, probeList.Count, ratioList.Count, fusionList.Count);
            else
            {
                if (GetParamSetValue("CLASSIFIERFUNCTION", out classifierFunctionString))
                    classesFormulae = Assay.ParseClassifyFunction1(classifierFunctionString, probeList.Count, fusionList.Count);
            }

            int normalClassIndex = GetParamSetValue("NORMALCLASS", 0);


            ObservableCollection<SpotClass> classes = new ObservableCollection<SpotClass>();
            int numClasses = GetParamSetValue("CLASSESSTORED", 0);
            for (int i = 0; i < numClasses; i++)
            {
                string className;
                if (GetParamSetValue("CLASS" + i + "_NAME", out className) && className.Length > 0)
                {
                    string classColourString;
                    GetParamSetValue("CLASS" + i + "_COLOUR", out classColourString);
                    Color classColour = ColorParse.HexStringToColor(classColourString);


                    // Note that the stop counts are retrieved from individual ParamSets - they
                    // don't have to be parsed from the stop function stored in the STOPAUTOCAPTUREFUNCTION ParamSet.
                    // There may be more than one class with the same name - this is how an OR is done with class formulae,
                    // however there's a flaw in the design of the assay file, in that stop counts are referenced by class
                    // name rather than index, which means that multiple classes with the same name cannot have separate
                    // stop counts.
                    int stopCount = GetParamSetValue(className, 0);


                    SpotClass newClass = new SpotClass()
                    {
                        Name = className,
                        DisplayColour = classColour,
                        IsNormal = (i != normalClassIndex) ? false : true,
                        StopCount = stopCount
                    };


                    // There may be more than one class with the same name - this is how an OR is done with class formulae.
                    // So remove each ClassFormulae from the main list as it is used, so that the same one isn't used twice.
                    ClassFormulae formulaeForThisClass = null;
                    List<ClassFormulae> formulaeForThisClassName;
                    if (classesFormulae.TryGetValue(className, out formulaeForThisClassName) && formulaeForThisClassName.Count > 0)
                    {
                        formulaeForThisClass = formulaeForThisClassName[0];
                        formulaeForThisClassName.RemoveAt(0);
                    }


                    // Ensure that the number of counts or ratios stored in the class is the same as the number in
                    // the probe, ratio and fusion lists, even if corresponding formulae aren't found for them.
                    // This ensures that there will be placeholders for storing formulae, even if the classifier
                    // function was missing, incomplete, or could not be parsed.
                    newClass.SetProbeRatioFusionCounts(probeList.Count, ratioList.Count, fusionList.Count);


                    // Add the formula for this class to each probe and fusion.
                    if (formulaeForThisClass != null)
                    {
                        if (formulaeForThisClass.probeFormulae.Count == probeList.Count)
                        {
                            for (int probeIndex = 0; probeIndex < probeList.Count; probeIndex++)
                                newClass.ProbeCounts[probeIndex] = new CountOrRatio(formulaeForThisClass.probeFormulae[probeIndex]);
                        }

                        if (formulaeForThisClass.ratioFormulae.Count == ratioList.Count)
                        {
                            for (int ratioIndex = 0; ratioIndex < ratioList.Count; ratioIndex++)
                                newClass.Ratios[ratioIndex] = new CountOrRatio(formulaeForThisClass.ratioFormulae[ratioIndex]);
                        }

                        if (formulaeForThisClass.fusionFormulae.Count == fusionList.Count)
                        {
                            for (int fusionIndex = 0; fusionIndex < fusionList.Count; fusionIndex++)
                                newClass.FusionCounts[fusionIndex] = new CountOrRatio(formulaeForThisClass.fusionFormulae[fusionIndex]);
                        }
                    }

                    classes.Add(newClass);
                }
            }

            return classes;
        }

        public SpotConfigController Clone()
        {
            return FromStream(_assayData, _assayInfo, ReprocessMode, AssayName);
        }
    }
}


