﻿using System;
using System.Linq;
using System.Windows.Media;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Localization;

namespace CytoApps.SpotConfiguration
{
    public partial class SpotConfigDialog
    {
        private SpotConfigController controller;

        public SpotConfigDialog(SpotConfigController _controller)
        {
            this.controller = _controller;
            DataContext = this.controller;

            InitializeComponent();

            Title = string.Format("{0} - {1}", controller.AssayName, Title);

            Closing += OnClosing;
        }

        private Color ChooseColour(Color colourIn)
        {
            Color colourOut = colourIn;

            try
            {
                System.Windows.Forms.ColorDialog cd = new System.Windows.Forms.ColorDialog();

                // Have to convert from System.Windows.Media.Color to System.Drawing.Color!!
                cd.Color = System.Drawing.Color.FromArgb(colourIn.R, colourIn.G, colourIn.B);

                if (cd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    colourOut = Color.FromArgb(255, cd.Color.R, cd.Color.G, cd.Color.B);

                    // Don't allow colour to be set to pure black, as for spectrachrome overlays this is treated as
                    // the transparent colour by ProbeCaseView (see ScoredCell.LoadOverlayImagesForReals() in ScoredCell.cs),
                    // so pure black overlays would be invisible.
                    if (colourOut == Color.FromArgb(255, 0, 0, 0))
                        colourOut = Color.FromArgb(255, 1, 1, 1);
                }
            }
            catch (Win32Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            return colourOut;
        }

        private void OnSetCounterStainColour(object sender, RoutedEventArgs e)
        {
            controller.CounterStainPseudoColour = ChooseColour(controller.CounterStainPseudoColour);
        }

        private void OnSetProbeColour(object sender, MouseButtonEventArgs e)
        {
            Probe probe = (Probe)((FrameworkElement)sender).Tag;

            probe.PseudoColour = ChooseColour(probe.PseudoColour);
        }

        private void OnSetFusionColour(object sender, RoutedEventArgs e)
        {
            Fusion fusion = (Fusion)((FrameworkElement)sender).Tag;

            fusion.Colour = ChooseColour(fusion.Colour);
        }

        private void OnSetClassColour(object sender, RoutedEventArgs e)
        {
            SpotClass spotClass = (SpotClass)((FrameworkElement)sender).Tag;

            spotClass.DisplayColour = ChooseColour(spotClass.DisplayColour);
        }

        private void OnClickProbe(object sender, RoutedEventArgs e)
        {
            Probe probe = (Probe)((FrameworkElement)sender).Tag;

            if (probe != null)
            {
                probePopup.Tag = probe;
                probePopup.IsOpen = true;
            }
        }

        private void OnClickRatio(object sender, RoutedEventArgs e)
        {
            var thing = (Ratio)((FrameworkElement)sender).Tag;
            ratioPopup.Tag = thing;
            ratioPopup.IsOpen = true;
        }

        private void OnClickRemoveProbe(object sender, RoutedEventArgs e)
        {
            if (controller.IsProbeInUse((Probe)probePopup.Tag))               
               CustomPopUp.Show(Literal.Strings.Lookup("cbStr-ProbeInUseMessage"), Literal.Strings.Lookup("cbStr-ProbeTitle"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information, this);                   
            else
                controller.RemoveProbe((Probe)probePopup.Tag);

            probePopup.IsOpen = false;
        }

        private void OnClickFusion(object sender, RoutedEventArgs e)
        {
            Fusion fusion = (Fusion)((FrameworkElement)sender).Tag;
            fusionPopup.Tag = fusion;
            fusionPopup.IsOpen = true;
        }

        private void OnClickRemoveFusion(object sender, RoutedEventArgs e)
        {
            var f = (Fusion)fusionPopup.Tag;
            controller.RemoveFusion(f);
            fusionPopup.IsOpen = false;
        }

        private void OnClickSelectFusionComponent(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            Fusion fusion = (Fusion)fusionPopup.Tag;

            if (fusion.CountSelected() > Constants.MAXPROBESPERFUSION)
            {
                CustomPopUp.Show(string.Format(Literal.Strings.Lookup("cbStr-FusionMaxProbesMessage"), Constants.MAXPROBESPERFUSION), Literal.Strings.Lookup("cbStr-FusionTitle"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information, this);


                //  var result = MessageBox.Show(String.Format((string)FindResource("scFusionMaxProbesMessage"), Constants.MAXPROBESPERFUSION));
                checkBox.IsChecked = !checkBox.IsChecked; // Roll the change back.
            }
            else if (!controller.IsFusionCombinationUnique(fusion))
            {
                CustomPopUp.Show(Literal.Strings.Lookup("cbStr-FusionNotUniqueMessage"), Literal.Strings.Lookup("cbStr-FusionTitle"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information, this);                
                 //  var result = MessageBox.Show((string)FindResource("scFusionNotUniqueMessage"));
                checkBox.IsChecked = !checkBox.IsChecked; // Roll the change back.
            }
        }

        private void OnClickAddClass(object sender, RoutedEventArgs e)
        {
            controller.AddClass();
        }

        private void OnDeleteClass(object sender, RoutedEventArgs e)
        {
            SpotClass spotClass = (SpotClass)((FrameworkElement)sender).Tag;

            controller.DeleteClass(spotClass);
        }

        private void OnClickNormal(object sender, RoutedEventArgs e)
        {
            SpotClass spotClass = (SpotClass)((FrameworkElement)sender).Tag;

            controller.SetNormal(spotClass);
        }

        private void OnClickPerClassStopCounts(object sender, RoutedEventArgs e)
        {
            stopCountsPopup.IsOpen = true;
        }

        private void OnClickScriptParams(object sender, RoutedEventArgs e)
        {
            scriptParamsPopup.IsOpen = true;
        }

        private void OnClickAddProbe(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("OnClickAddProbeButton");

            probeListPopup.PlacementTarget = addProbeButton;
            probeListPopup.IsOpen = true;
        }

        private void OnClickAddRatio(object sender, RoutedEventArgs e)
        {
            if (controller.ProbeList.Count() < 2)
                return;

            addRatioPopup.IsOpen = true;
        }
        private void OnClickRemoveRatio(object sender, RoutedEventArgs e)
        {
            var ratio = (Ratio)ratioPopup.Tag;
            controller.RemoveRatio(ratio);
            ratioPopup.Tag = null;
            ratioPopup.IsOpen = false;
        }
        private void OnAddRatioPopupOK(object sender, EventArgs e)
        {
            var dlg = (AddRatioView)sender;
            controller.AddRatio(dlg.Ratio);
            dlg.Reset();

            addRatioPopup.IsOpen = false;
        }

        private void OnProbeListPopupOpened(object sender, EventArgs e)
        {
            probeListBox.SelectedItem = null;
        }

        private void OnSelectionChangedNewProbe(object sender, SelectionChangedEventArgs e)
        {
            if (probeListPopup.IsOpen)
            {
                CVFluoroChrome fl = probeListBox.SelectedItem as CVFluoroChrome;
                if (fl != null)
                {
                    controller.AddProbe(fl);
                    probeListPopup.IsOpen = false;
                }
            }
        }

        private void OnClickAddFusion(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;

            Fusion newFusion = controller.CreateNewFusion();
            AddFusionWindow dlg = new AddFusionWindow(newFusion, controller);

            Point p = b.PointToScreen(new Point(-2, b.ActualHeight));

            dlg.Top = p.Y;
            dlg.Left = p.X;

            if (dlg.ShowDialog() == true)
                controller.AddFusion(newFusion); // This assigns the new fusion to CurrentFusion.
        }

        private void OnLoadedClassTextBox(object sender, RoutedEventArgs e)
        {
            TextBox t = sender as TextBox;
            if (t != null)
            {
                t.BringIntoView();
                t.Focus();
                t.SelectAll();
            }
        }

        private void OnPreviewGridCountText(object sender, TextCompositionEventArgs e)
        {
            // TODO: it might be nicer if a time-limited message (e.g tool-tip) appeared, rather than having to click OK
            //       in a message box. Or use a ValidationRule. If a ValidationRule is used, must not let the assay be saved
            //       until all validation errors are corrected, as otherwise what is saved will not match what is shown
            //       (validation errors prevent input being transferred to the target of the data binding, but don't remove
            //       the erronous input from display).
            if (!e.Text.All(Char.IsDigit) && e.Text.IndexOfAny(new char[] { '<', '>', '=', '*', '&' }) < 0)
            {
                if (e.Text.All(a => a != '\r'))
                    CustomPopUp.Show(Literal.Strings.Lookup("cbStr-CountInvalidCharacterMessage"), Literal.Strings.Lookup("cbStr-ProbeTitle"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information, this);          
               // MessageBox.Show((string)FindResource("scCountInvalidCharacterMessage"), (string)FindResource("scCountString"), MessageBoxButton.OK, MessageBoxImage.Error);
                e.Handled = true;
            }
        }
        private void OnPreviewGridRatioText(object sender, TextCompositionEventArgs e)
        {
            if (!e.Text.All(Char.IsDigit) && e.Text.IndexOfAny(new char[] { '.', '<', '>', '=', '*', '&' }) < 0)
            {
                if (e.Text.All(a => a != '\r'))
                    CustomPopUp.Show(Literal.Strings.Lookup("cbStr-RatioInvalidCharacterMessage"), Literal.Strings.Lookup("cbStr-RatioTitle"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information, this);
              //  MessageBox.Show((string)FindResource("scRatioInvalidCharacterMessage"), (string)FindResource("scRatioString"), MessageBoxButton.OK, MessageBoxImage.Error);
                e.Handled = true;
            }
        }


        private void OnClickSetScriptParamsToDefaults(object sender, RoutedEventArgs e)
        {
            controller.ScriptParams.SetToDefaults();
        }


        private bool IsValid(DependencyObject node)
        {
            // Validate all dependency objects in a window
            if (node != null)
            {
                // Check if dependency object is valid.
                // NOTE: Validation.GetHasError works for controls that have validation rules attached.
                bool isValid = !Validation.GetHasError(node);
                if (!isValid)
                {
                    // If the dependency object is invalid, and it can receive the focus, set the focus.
                    if (node is IInputElement)
                        Keyboard.Focus((IInputElement)node);

                    return false;
                }
            }

            // If this dependency object is valid, check all child dependency objects.
            // Need to walk visual tree not logical tree, in order to pick up items in itemscontrols
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(node); i++)
            {
                var child = VisualTreeHelper.GetChild(node, i);
                if (child is DependencyObject)
                {
                    if (!IsValid((DependencyObject)child))
                        return false;
                }
            }

            return true; // All dependency objects are valid
        }

        private bool CheckInputsAreOk()
        {
            bool inputsOk = false;

            if (controller.ProbeList.Count() < 1 && !controller.ReprocessMode) // Can't add probes in reprocess mode.
                CustomPopUp.Show(Literal.Strings.Lookup("cbStr-NoProbesMessage"), Literal.Strings.Lookup("cbStr-CloseSpotConfigurationTitle"), MessageBoxButton.OK, MessageBoxImage.Error, this);
            //MessageBox.Show((string)FindResource("scNoProbesMessage"));
            else if (controller.ClassList.Where(c => c.Name.Trim() == String.Empty).Count() > 0)
                CustomPopUp.Show(Literal.Strings.Lookup("cbStr-EmptyClassNameMessage"), Literal.Strings.Lookup("cbStr-CloseSpotConfigurationTitle"), MessageBoxButton.OK, MessageBoxImage.Error, this);
                       
           // MessageBox.Show((string)FindResource("scEmptyClassNameMessage"));
            // This doesn't work for items in items control: think an ItemBindingGroup is needed, but couldn't get it to work.
            else if (!IsValid(this)) // Check ValidationRule for each control that has one (assumed to just be grid cells).
                CustomPopUp.Show(Literal.Strings.Lookup("cbStr-InvalidExpressionMessage"), Literal.Strings.Lookup("cbStr-CloseSpotConfigurationTitle"), MessageBoxButton.OK, MessageBoxImage.Error, this);        
           // MessageBox.Show((string)FindResource("scInvalidExpressionMessage"));
            else
                inputsOk = true;

            return inputsOk;
        }

        /*
        private void OnSaveAsDefault(object sender, RoutedEventArgs e)
        {
            if (CheckInputsAreOk())
            {
                if (!controller.SaveAssay(true))
                    MessageBox.Show((string)FindResource("scErrorSavingMessage"));

                this.DialogResult = true;
                Close();
            }
        }
        */

        private void OnOK(object sender, RoutedEventArgs e)
        {
            if (CheckInputsAreOk())
            {
                // Cache changes in spot controller in to local variable on ok click.
                controller.CacheAssayData(controller.SaveByteAssay());
                //if (!controller.SaveAssay())
                //{
                //    MessageBoxResult result = CustomPopUp.Show("Error in saving assay file!", "Spot Configuartion", MessageBoxButton.OK, MessageBoxImage.Error, this);
                //}
                 this.DialogResult = true;
                Close();
            }
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            if(DialogResult != true)
            {
              
                MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-CloseSpotConfigurationMessage"), Literal.Strings.Lookup("cbStr-CloseSpotConfigurationTitle"), MessageBoxButton.OKCancel, MessageBoxImage.Question, this);
                // If close isn't the result of clicking the OK button, confirm it with the user.
                if (result == MessageBoxResult.Cancel || result==MessageBoxResult.None)
                {
                    e.Cancel = true; // Cancel closure.  
                }         
            }
           
        }

        private void OnCheckAmplified(object sender, RoutedEventArgs e)
        {
            controller.AnyAmplified = true;
        }
    }

    public class CountValidator : System.Windows.Controls.ValidationRule
    {
        public override System.Windows.Controls.ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            // Note that if this validation fails, the input will not be transferred to the target of the data binding,
            // but will still be shown unmmodified in the display.
            return new System.Windows.Controls.ValidationResult(value is string ? Assay.IsFormulaValidForDisplay((string)value, true) : false, null);
        }
    }

    public class RatioValidator : System.Windows.Controls.ValidationRule
    {
        public override System.Windows.Controls.ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            // Note that if this validation fails, the input will not be transferred to the target of the data binding,
            // but will still be shown unmmodified in the display.
            return new System.Windows.Controls.ValidationResult(value is string ? Assay.IsFormulaValidForDisplay((string)value, false) : false, null);
        }
    }

    public class ClassDeleteVisible : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var className = (string)values[0];
            IEnumerable<string> noDeleteClasses = (IEnumerable<string>)values[1];

            if (noDeleteClasses == null)
                return Visibility.Visible;

            var isClass = noDeleteClasses.Where(c => c.ToLower() == className.ToLower()).FirstOrDefault();

            return isClass == null ? Visibility.Visible : Visibility.Hidden;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class CanModifyClassConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var className = (string)values[0];
            IEnumerable<string> noDeleteClasses = (IEnumerable<string>)values[1];

            if (noDeleteClasses == null)
                return true;

            var isClass = noDeleteClasses.Where(c => c.ToLower() == className.ToLower()).FirstOrDefault();

            return isClass == null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
