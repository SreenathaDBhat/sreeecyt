// This is the main DLL file.

#include "stdafx.h"

#include "interface.h"

#include "util.h"
#include "defaulttraits.h"
#include "losslesstraits.h"
#include "colortransform.h"
#include "streams.h"
#include "processline.h"

#include "ManagedCompressor.h"

namespace Renderer
{
	void JPEGLS::WriteFile(const char *strName, std::vector<unsigned char>& vec, int Sz)
	{
		FILE* pfile = fopen(strName, "w+b");
		if( !pfile ) 
		{
			fprintf( stderr, "Could not open %s\n", strName );
			return;
		}

		fwrite(&vec[0],1, Sz, pfile);

		fclose(pfile);	
	}

	array<unsigned short>^ JPEGLS::Load(array<unsigned char>^ data, REFOUT int %W, REFOUT int %H, REFOUT int %BPP)
	{
		pin_ptr<unsigned char> pd = &data[0];
		unsigned char* ppd = pd;

		JlsParamaters metadata;
		if (JpegLsReadHeader(ppd, data->Length, &metadata) != OK)
			return nullptr;

		W = metadata.width;
		H = metadata.height;
		BPP = metadata.bitspersample;

		long Size = W * H * ((BPP + 7) / 8);
		BYTE* decompressed = new BYTE[Size];
		JLS_ERROR err = JpegLsDecode(decompressed, (int)Size, ppd, data->Length);

		if(err != OK)
		{
			delete decompressed;
			return nullptr;
		}

		if(BPP == 8)
		{
			return nullptr;
		}
		else
		{
			array<unsigned short>^ r = gcnew array<unsigned short>(W * H);
			pin_ptr<unsigned short> p = &r[0];
			unsigned short* pp = p;

			BYTE* s = decompressed;
			int n = W*H;
			
			for (int i = 0; i < n; i++)
			{
				*pp++ = *s + ((*(s+1)) << 8 );
				s += 2;
			}

			delete decompressed;
			return r;
		}
	}

	void JPEGLS::Save(System::String^ FileName, array<unsigned char>^ data, int W, int H, int BPP)
	{
		std::vector<unsigned char> rgbyteCompressed;
		std::vector<unsigned char> rgbyteRaw;

		long Size = W * H * ((BPP + 7) / 8);

		rgbyteRaw.resize(Size);

		rgbyteCompressed.resize(W * H * BPP / 4);

		pin_ptr<unsigned char> pd = &data[0];
		unsigned char* ppd = pd;

		memcpy(&rgbyteRaw[0], ppd, Size);

		JlsParamaters params = JlsParamaters();

		params.components = 1;
		params.bitspersample = BPP;
		params.height = H;
		params.width  = W;
		params.allowedlossyerror = 0;

		size_t cbyteCompressed;

		JLS_ERROR err = JpegLsEncode(&rgbyteCompressed[0], rgbyteCompressed.size(), &cbyteCompressed, &rgbyteRaw[0], Size, &params);
	
		// Marshal the managed string to unmanaged memory.
			char* stringPointer = (char*) Marshal::StringToHGlobalAnsi(FileName).ToPointer();

		WriteFile(stringPointer, rgbyteCompressed, cbyteCompressed);
	}

}