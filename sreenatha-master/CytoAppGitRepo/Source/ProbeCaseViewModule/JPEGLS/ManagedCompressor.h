// ManagedCompressor.h

#pragma once

#include "stdafx.h"
#include <vector>

using namespace System;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;

#define REFOUT [Runtime::InteropServices::Out] 

namespace Renderer
{

	public ref class JPEGLS
	{
	private:
		JPEGLS() {}
		static void WriteFile(const char *strName, std::vector<unsigned char>& vec, int Sz);

	public:
		static array<unsigned short>^ Load(array<unsigned char>^ data, REFOUT int %W, REFOUT int %H, REFOUT int %BPP);	
		static void Save(System::String^ FileName, array<unsigned char>^ data, int W, int H, int BPP);
	};
}
