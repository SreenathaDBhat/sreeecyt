﻿using PCVImageRenderers.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PCVImageRenderers
{
    public class FrameRenderer : IDisposable
    {
        #region Private members

        private int _z;
        private Frame _frame;
        private IEnumerable<ComponentLevels> _levels;
        private bool _attenuateCounterstain;
        private bool _maximumProjection;
        private bool _invertCounterstain;
        private bool _backgroundSubtract;
        private int _backgroundSubtractLevel;
        private bool _invalidateHistogramLevels;
        private object syncRoot = new object();

        private FrameRendererCache<ushort> _cache;
        private byte[] _renderPixels;

        #endregion


        public FrameRenderer(Frame frame, FrameRenderSettings options, IEnumerable<ComponentLevels> levels, int z, FrameRendererCache<ushort> cache)
        {
            _frame = frame;
            _levels = levels;
            _z = z;

            // Ensure that the internal max projection flag corresponds to whether z is -1.
            // Don't change z to match the max projection flag present in the options, because it's assumed that the
            // value of the z parameter is the value that was used when adding the required components to the cache,
            // so changing the z value would result in cache retrieval errors.
            _maximumProjection = (z == -1);

            _invertCounterstain = options == null ? false : options.InvertCounterstain;
            _attenuateCounterstain = options == null ? true : options.AttenuateCounterstain;
            _backgroundSubtract = options == null ? false : options.SubtractBackground;
            _backgroundSubtractLevel = options == null ? 2 : options.BackgroundLevel;

            _cache = cache;

            _invalidateHistogramLevels = true;
        }


        public byte[] RenderToBitmapData(bool compress = true)
        {
            var bitmap = Render();
   
            using (var stream = new System.IO.MemoryStream())
            {
                if (compress)
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(bitmap));
                    encoder.QualityLevel = 75;
                    encoder.Save(stream);
                    stream.Flush();
                    return stream.ToArray();
                }
                else
                {
                    BmpBitmapEncoder encoder = new BmpBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(bitmap));
                    encoder.Save(stream);
                    stream.Flush();
                    return stream.ToArray();
                }
            }
        }


        public BitmapSource Render()
        {
            // take a copy of the members that can change.
            var state = new FrameRenderState
            {
                Z = _z,
                AttenuateCounterstain = _attenuateCounterstain,
                MaxProjection = _maximumProjection,
                InvertCounterstain = _invertCounterstain,
                BackgroundSubtract = _backgroundSubtract,
                BSLevel = _backgroundSubtractLevel,
                ChannelStates = (from c in _frame.Components
                                 select new ChannelRendererState(_levels, c)
                                 {
                                     ImageDataFilename = c.ImageForZ(_z)

                                 }).ToArray()
            };

            return RenderBitmap(state);
        }


        #region private methods
        private unsafe BitmapSource RenderBitmap(FrameRenderState frameState)
        {
            //
            // Before this method can be called successfully, code external to this class must have loaded the required (i.e. currently visible)
            // channels into the cache. This can be done by iterating through the ComponentLevels, ensuring that visible ones (at the current Z)
            // are loaded into the cache. Also, the Frame object used by the FrameRenderer must have had its dimensions set (these are discovered
            // when a component image is loaded from disk).
            //
            int numPixels = _frame.Width * _frame.Height * 4;

            if (_renderPixels == null)
                _renderPixels = new byte[numPixels];

            bool anyChannelsToRender = (frameState.ChannelStates.Where(c => c.IsVisible).Count() > 0);

            if (anyChannelsToRender)
            {
                fixed (byte * displayBytesPtr = &_renderPixels[0])
                {
                    bool first = true;
                    ChannelRendererState counterstain = null;

                    bool cacheRetrievalFailuresEncountered = false;

                    foreach (var channelState in frameState.ChannelStates)
                    {
                        var levelInfo = _levels.Where(l => l.Component == channelState.Component).First();

                        if (levelInfo.IsVisible)
                        {
                            int bsLevel = (frameState.BackgroundSubtract && frameState.MaxProjection) ? frameState.BSLevel : -1;

                            channelState.Pixels = _cache.Retrieve(channelState.Component, frameState.Z, bsLevel);

                            if (channelState.Pixels != null)
                            {
                                channelState.FilteredPixels = channelState.Pixels;
                            }
                            else
                            {
                                if (frameState.BackgroundSubtract && frameState.MaxProjection)
                                {
                                    // To get the background subtracted image, get the cached raw (un-background subtracted)
                                    // image then apply the Filter(). Note we cache the raw image and the BS image.
                                    channelState.Pixels = _cache.Retrieve(channelState.Component, frameState.Z);

                                    if (channelState.Pixels != null)
                                    {
                                        Filter(channelState, frameState);
                                        _cache.AddToCache(channelState.FilteredPixels, channelState.Component, frameState.Z, bsLevel);
                                    }
                                    else
                                    {
                                        cacheRetrievalFailuresEncountered = true;
                                    }
                                }
                                else
                                {
                                    cacheRetrievalFailuresEncountered = true;
                                }

                                if (cacheRetrievalFailuresEncountered)
                                    channelState.FilteredPixels = null;
                            }

                            channelState.Histogram = new Histogram(_frame.Width, _frame.Height, _frame.BitsPerPixel, channelState.FilteredPixels);

                            // If we dont have a histogram yet, estimate a starting point.
                            if (_invalidateHistogramLevels && !channelState.Levels.SetByUser) // TODO 
                            {
                                var est = HistogramEstimator.Estimate(channelState.Histogram, channelState.Component.IsCounterstain);
                                channelState.Low = est[0];
                                channelState.High = est[1];
                            }
                        }

                        if (channelState.Component.IsCounterstain)
                        {
                            counterstain = channelState; // Counterstain must be rendered last, so it can be attenuated, so keep for later.
                        }
                        else
                        {
                            if (RenderComponent(displayBytesPtr, first, channelState, frameState))
                                first = false;
                        }
                    }

                    if (counterstain != null)
                    {
                        RenderComponent(displayBytesPtr, first, counterstain, frameState);
                    }

                    // If cache retrieval errors were encountered, clear the whole display buffer, so it's obvious to the user that the frame
                    // hasn't loaded - one missing channel might not be immediately obvious if it contained weak probe signals, for example.
                    if (cacheRetrievalFailuresEncountered)
                    {
                        Array.Clear(_renderPixels, 0, _renderPixels.Length);
                    }
                }
            }
            else
            {
                Array.Clear(_renderPixels, 0, _renderPixels.Length); // No visible channels, so clear the buffer of anything left from last time.
            }

            if (_invalidateHistogramLevels) // TODO
            {
                foreach (var cs in frameState.ChannelStates)
                {
                    cs.Levels.Histogram = cs.Histogram;
                    if (!cs.Levels.SetByUser)
                        cs.Levels.SetLowHighValues(cs.Low, cs.High, true);
                    // store in histogram for serialization
                    if (cs.Levels.Histogram != null)
                    {
                        cs.Levels.Histogram.RenderedLow = cs.Levels.Low;
                        cs.Levels.Histogram.RenderedHigh = cs.Levels.High;
                    }
                }

                _invalidateHistogramLevels = false;
            }

            var b = BitmapSource.Create(_frame.Width, _frame.Height, 96, 96, PixelFormats.Pbgra32, null, _renderPixels, _frame.Width * 4);
            b.Freeze();

            return b;
        }


        private void RenderComponent(byte[] renderedData, bool first, ChannelRendererState ch, FrameRenderState state)
        {
            unsafe
            {
                fixed (byte* bytePtr = renderedData)
                {
                    RenderComponent(bytePtr, first, ch, state);
                }
            }
        }

        private unsafe bool RenderComponent(byte* displayBytesPtr, bool first, ChannelRendererState ch, FrameRenderState state)
        {
            if (ch.FilteredPixels == null) // Perhaps pixels failed to load from disk.
                return false;

            int slices = 1;
            int rowsPerSlice = _frame.Height / slices;

            fixed (ushort* inputDataPtr = &ch.FilteredPixels[0])
            {
                ushort* inputData = inputDataPtr;

                Parallel.For(0, slices, (i) =>
                {
                    int numRows = i == slices ? _frame.Height - (i * rowsPerSlice) : rowsPerSlice;
                    Mixin(ch, inputData, first, displayBytesPtr, i * rowsPerSlice, numRows, state);
                });
            }

            return true;
        }

        private unsafe void Mixin(ChannelRendererState a, ushort* inputPtr, bool first, byte* displayBytesPtr, int firstRow, int numRows, FrameRenderState state)
        {
            int numPixels = numRows * _frame.Width;
            float maxPixelValue = (float)Math.Pow(2, _frame.BitsPerPixel);
            var color = a.RenderColor;

            byte* output = displayBytesPtr;
            ushort* input = inputPtr;

            output += firstRow * (_frame.Width * 4);
            input += firstRow * _frame.Width;

            int len = _frame.Width * _frame.Height;
            ushort* pixelsEndInput = inputPtr + len;
            byte* pixelsEndOutput = output + (len * 4);

            if (a.OffsetX > 0)
            {
                output += (a.OffsetX * 4);
            }
            else
            {
                input += -a.OffsetX;
            }

            if (a.OffsetY > 0)
            {
                output += (a.OffsetY * _frame.Width) * 4;
            }
            else
            {
                input += -a.OffsetY * _frame.Width;
            }

            ushort* inEnd = input + numPixels;
            byte* outEnd = output + (numPixels * 4);

            if (inEnd > pixelsEndInput)
                inEnd = pixelsEndInput;
            if (outEnd > pixelsEndOutput)
                outEnd = pixelsEndOutput;

            bool drawingInvertedCounterstainONLY = first && a.Component.IsCounterstain && state.InvertCounterstain;

            // To avoid a relatively expensive CLEAR, we essentially blit the first channel
            if (drawingInvertedCounterstainONLY)
            {
                float v;

                while (output < outEnd && input < inEnd)
                {
                    v = Clamp(*input++, a, maxPixelValue);
                    v = (1 - v) * 255;

                    *output++ = (byte)v;
                    *output++ = (byte)v;
                    *output++ = (byte)v;
                    *output++ = 255;
                }
            }


            if (first)
            {
                while (output < outEnd && input < inEnd)
                {
                    float v = Clamp(*input++, a, maxPixelValue);
                    *output++ = (byte)(v * color.B);
                    *output++ = (byte)(v * color.G);
                    *output++ = (byte)(v * color.R);
                    *output++ = 255;
                }
            }
            else if (a.Component.IsCounterstain && state.AttenuateCounterstain)
            {
                float v = 0;
                float f = 0;
                int er, eg, eb;
                float fm = 0;
                bool invert = state.InvertCounterstain;

                while (output < outEnd && input < inEnd)
                {
                    v = Clamp(*input++, a, maxPixelValue);

                    fm = fm < v ? v : fm;

                    eb = *output;
                    eg = *(output + 1);
                    er = *(output + 2);
                    f = eb > er ? eb : er;
                    f = eg > f ? eg : f;
                    f /= 255;
                    v -= f * 0.4f;
                    v = v < 0 ? 0 : v;

                    *output = (byte)Math.Min(255.0f, *output + (v * color.B));
                    output++;
                    *output = (byte)Math.Min(255.0f, *output + (v * color.G));
                    output++;
                    *output = (byte)Math.Min(255.0f, *output + (v * color.R));
                    output++;
                    output++;
                }

                fm.ToString();
            }
            else if (a.Component.IsCounterstain && state.InvertCounterstain)
            {
                float v, x;

                while (output < outEnd && input < inEnd)
                {
                    v = Clamp(*input++, a, maxPixelValue);
                    v = 1 - v;

                    x = *output + (v * 255);
                    *output++ = (byte)(255 < x ? 255 : x);

                    x = *output + (v * 255);
                    *output++ = (byte)(255 < x ? 255 : x);

                    x = *output + (v * 255);
                    *output++ = (byte)(255 < x ? 255 : x);

                    *output++ = 255;
                }
            }
            else
            {
                float v, x;

                while (output < outEnd && input < inEnd)
                {
                    v = Clamp(*input++, a, maxPixelValue);

                    x = *output + (v * color.B);
                    *output++ = (byte)(255 < x ? 255 : x);

                    x = *output + (v * color.G);
                    *output++ = (byte)(255 < x ? 255 : x);

                    x = *output + (v * color.R);
                    *output++ = (byte)(255 < x ? 255 : x);

                    *output++ = 255;
                }
            }
        }

        // returns 0..1
        private float Clamp(float v, ChannelRendererState a, float maxPixelValue)
        {
            // low and high in levelInfo are normalized 0..1
            var low = a.Low * maxPixelValue;
            var high = a.High * maxPixelValue;

            v = v - low;
            v /= (high - low);
            v = v < 0 ? 0 : v;
            v = v > 1 ? 1 : v;

            return v;
        }

        private void Filter(ChannelRendererState a, FrameRenderState s)
        {
            if (a.Component.IsCounterstain != true && s.BackgroundSubtract && s.MaxProjection)
            {
                a.FilteredPixels = BackgroundSubtract.Subtract(a.Pixels, _frame.Width, _frame.Height, _frame.BitsPerPixel, s.BSLevel);
            }
            else
            {
                a.FilteredPixels = a.Pixels;
            }
        }
        #endregion

        #region IDisposable implementation
        public void Dispose()
        {
            Dispose(false);
        }

        private void Dispose(bool fromdtor)
        {
            if (!fromdtor)
                GC.SuppressFinalize(this);

            // TEMP: the render thread, if it is active, will continue to process its current frame.
            //*TODO StopRendering();
        }
        #endregion
    }
}
