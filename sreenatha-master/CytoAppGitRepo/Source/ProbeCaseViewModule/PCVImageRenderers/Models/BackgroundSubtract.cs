﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCVImageRenderers.Models
{
    internal static unsafe class BackgroundSubtract
    {
        public static ushort[] Subtract(ushort[] pixels, int w, int h, int bpp, int halfWidth)
        {
            return fsExtract(pixels, w, h, bpp, halfWidth);
        }


        static ushort[] fsExtract(ushort[] frame, int cols, int lines, int bpp, int halfwidth)
        {
            // 2 * halfwidth must be < cols or lines
            int fw = (halfwidth * 2) + 2;
            if (fw > cols || fw > lines)
                return null;

            int size = lines * cols;
            var f1 = fsClose(frame, cols, lines, bpp, 4);
            var f2 = fsOpen(f1, cols, lines, bpp, halfwidth);

            if (f2 != null)
            {
                fixed (ushort* frameP = &frame[0])
                {
                    ushort* imptr = frameP;
                    fixed (ushort* f2p = &f2[0])
                    {
                        ushort* im2ptr = f2p;
                        ushort min = 0;

                        do
                        {
                            //find min of original and processed images
                            min = *imptr < *im2ptr ? *imptr : *im2ptr;

                            //subtract min from original image
                            *im2ptr++ = (ushort)(*imptr++ - min);
                        }
                        while (--size != 0);
                    }
                }
            }

            return f2;
        }

        static ushort[] fsOpen(ushort[] frame, int cols, int lines, int bpp, int halfwidth)
        {
            /* Erode image */
            var f1 = fsMin(frame, cols, lines, bpp, halfwidth);
            var f2 = fsMax(f1, cols, lines, bpp, halfwidth);
            return f2;
        }

        static ushort[] fsClose(ushort[] frame, int cols, int lines, int bpp, int halfwidth)
        {
            var f1 = fsMax(frame, cols, lines, bpp, halfwidth);
            var f2 = fsMin(f1, cols, lines, bpp, halfwidth);
            return f2;
        }

        static ushort[] fsMin(ushort[] frameA, int cols, int lines, int bpp, int halfwidth)
        {
            if (frameA == null)
                return null;

            ushort* fptr;
            ushort* oldfptr;
            ushort* hfptr;
            ushort* vfptr;
            int col, line;
            ushort min;
            int size, fw;
            ushort numGrays = (ushort)(Math.Pow(2, bpp) * 2);
            int[] rack = new int[numGrays];


            // 2 * halfwidth must be < cols or lines
            fw = (halfwidth * 2) + 2;
            if (fw > cols || fw > lines)
                return null;

            size = lines * cols;

            /* allocate temporary frame */
            ushort[] hframeA = new ushort[size];
            ushort[] vframeA = new ushort[size];

            fixed (ushort* frameP = &frameA[0])
            fixed (ushort* hframeAP = &hframeA[0])
            fixed (ushort* vframeAP = &vframeA[0])
            {
                ushort* frame = frameP;
                ushort* hframe = hframeAP;
                ushort* vframe = vframeAP;

                /* PASS1 */
                /* perform horizontal min on frame and save in hframe */

                for (line = 0; line < lines; line++)
                {
                    for (int i = 0; i < numGrays; ++i) rack[i] = 0;

                    /* initialise min */
                    min = ushort.MaxValue;

                    /* initialise frame pointers */
                    oldfptr = fptr = frame + cols * line;
                    hfptr = hframe + cols * line;

                    /* put first halfwidth+1 points into rack and find min */
                    for (col = 0; col < halfwidth + 1; col++)
                    {
                        if (*fptr < min)
                            min = *fptr;

                        rack[*fptr]++;

                        fptr++;
                    }


                    /* deal with pixels within half filter 
                       width of left edge of image */

                    for (col = 0; col < halfwidth; col++)
                    {
                        /* store min in hframe */
                        *hfptr++ = min;

                        /* update rack counts */
                        rack[*fptr]++;

                        /* check if newval is the min */
                        if (*fptr < min)
                            min = *fptr;

                        fptr++;
                    }

                    /* deal with pixels in main body of image */

                    for (col = halfwidth; col < cols - halfwidth - 1; col++)
                    {
                        /* store min in hframe */
                        *hfptr++ = min;

                        /* remove oldval from rack, insert newval
                           and determine new min value */

                        if (*oldfptr != *fptr)
                        {
                            /* update rack counts */
                            rack[*fptr]++;
                            rack[*oldfptr]--;

                            /* check if newval is the min */
                            if (*fptr < min)
                                min = *fptr;

                            /* check if oldval was the min
                               and if so find new min */
                            while (rack[min] == 0)
                                min++;
                        }

                        fptr++;
                        oldfptr++;
                    }

                    /* deal with pixels within half filter 
                       width of right edge of image */

                    for (col = cols - halfwidth - 1; col < cols; col++)
                    {
                        *hfptr++ = min;

                        /* update rack counts */
                        rack[*oldfptr]--;

                        /* check if oldval was the min
                           and if so find new min */
                        while (rack[min] == 0)
                            min++;

                        oldfptr++;
                    }
                }

                /* PASS2 */

                /* perform vertical min on hframe and save in vframe */

                for (col = 0; col < cols; col++)
                {   /* for each image column */

                    for (int i = 0; i < numGrays; ++i) rack[i] = 0;

                    min = numGrays;
                    oldfptr = fptr = hframe + col;
                    vfptr = vframe + col;


                    /* put first halfwidth+1 points into rack and find min */
                    for (line = 0; line < halfwidth + 1; line++)
                    {
                        if (*fptr < min)
                            min = *fptr;

                        rack[*fptr]++;

                        fptr += cols;
                    }

                    /* deal with pixels within half filter 
                       width of bottom edge of image */

                    for (line = 0; line < halfwidth; line++)
                    {
                        /* store min in vframe */
                        *vfptr = min;
                        vfptr += cols;

                        /* update rack counts */
                        rack[*fptr]++;

                        /* check if newval is the min */
                        if (*fptr < min)
                            min = *fptr;

                        fptr += cols;
                    }

                    /* deal with pixels in main body of image */

                    for (line = halfwidth; line < lines - halfwidth - 1; line++)
                    {

                        /* store min in hframe */
                        *vfptr = min;
                        vfptr += cols;

                        /* remove oldval from rack, insert newval
                           and determine new min value */

                        if (*oldfptr != *fptr)
                        {
                            /* update rack counts */
                            rack[*fptr]++;
                            rack[*oldfptr]--;

                            /* check if newval is the min */
                            if (*fptr < min)
                                min = *fptr;

                            /* check if oldval was the min
                               and if so find new min */
                            while (rack[min] == 0)
                                min++;
                        }

                        fptr += cols;
                        oldfptr += cols;
                    }

                    /* deal with pixels within half filter 
                       width of top edge of image */

                    for (line = lines - halfwidth - 1; line < lines; line++)
                    {
                        *vfptr = min;
                        vfptr += cols;

                        /* update rack counts */
                        rack[*oldfptr]--;

                        /* check if oldval was the min
                           and if so find new min */
                        while (rack[min] == 0)
                            min++;

                        oldfptr += cols;
                    }
                }
            }

            return vframeA;
        }

        static ushort[] fsMax(ushort[] frameA, int cols, int lines, int bpp, int halfwidth)
        {
            if (frameA == null)
                return null;

            ushort* fptr;
            ushort* oldfptr;
            ushort* hfptr;
            ushort* vfptr;
            int col, line;
            ushort max;
            int size, fw;

            ushort numGrays = (ushort)(Math.Pow(2, bpp) * 2);
            int[] rack = new int[numGrays];

            // 2 * halfwidth must be < cols or lines
            fw = (halfwidth * 2) + 2;
            if (fw > cols || fw > lines)
                return null;

            size = lines * cols;

            /* allocate temporary frame */
            ushort[] hframeA = new ushort[size];
            ushort[] vframeA = new ushort[size];

            fixed (ushort* frameP = &frameA[0])
            fixed (ushort* hframeAP = &hframeA[0])
            fixed (ushort* vframeAP = &vframeA[0])
            {
                ushort* hframe = hframeAP;
                ushort* vframe = vframeAP;
                ushort* frame = frameP;

                /* perform horizontal max on frame and save in hframe */
                for (line = 0; line < lines; line++)
                {
                    for (int i = 0; i < numGrays; ++i) rack[i] = 0;

                    /* initialise max */
                    max = 0;

                    /* initialise frame pointers */
                    oldfptr = fptr = frame + cols * line;
                    hfptr = hframe + cols * line;

                    /* put first halfwidth+1 points into rack and find max */
                    for (col = 0; col < halfwidth + 1; col++)
                    {
                        if (*fptr > max)
                            max = *fptr;

                        rack[*fptr]++;

                        fptr++;
                    }



                    /* deal with pixels within half filter 
		               width of left edge of image */

                    for (col = 0; col < halfwidth; col++)
                    {
                        /* store max in hframe */
                        *hfptr++ = max;

                        /* update rack counts */
                        rack[*fptr]++;

                        /* check if newval is the max */
                        if (*fptr > max)
                            max = *fptr;

                        fptr++;
                    }

                    /* deal with pixels in main body of image */

                    for (col = halfwidth; col < cols - halfwidth - 1; col++)
                    {
                        /* store max in hframe */
                        *hfptr++ = max;

                        /* remove oldval from rack, insert newval
		   	               and determine new max value */

                        if (*oldfptr != *fptr)
                        {

                            /* update rack counts */
                            rack[*fptr]++;
                            rack[*oldfptr]--;

                            /* check if newval is the max */
                            if (*fptr > max)
                                max = *fptr;

                            /* check if oldval was the max
		   		               and if so find new max */
                            while (rack[max] == 0)
                                max--;
                        }

                        fptr++;
                        oldfptr++;
                    }

                    /* deal with pixels within half filter 
		               width of right edge of image */

                    for (col = cols - halfwidth - 1; col < cols; col++)
                    {
                        *hfptr++ = max;

                        /* update rack counts */
                        rack[*oldfptr]--;

                        /* check if oldval was the max
		   	               and if so find new max */
                        while (rack[max] == 0)
                            max--;

                        oldfptr++;
                    }
                }

                /* PASS2 */

                /* perform vertical max on hframe and save in vframe */

                for (col = 0; col < cols; col++)
                {	/* for each image column */

                    for (int i = 0; i < numGrays; ++i) rack[i] = 0;

                    max = 0;
                    oldfptr = fptr = hframe + col;
                    vfptr = vframe + col;


                    /* put first halfwidth+1 points into rack and find max */
                    for (line = 0; line < halfwidth + 1; line++)
                    {
                        if (*fptr > max)
                            max = *fptr;

                        rack[*fptr]++;

                        fptr += cols;
                    }

                    /* deal with pixels within half filter 
		               width of bottom edge of image */

                    for (line = 0; line < halfwidth; line++)
                    {
                        /* store max in vframe */
                        *vfptr = max;
                        vfptr += cols;

                        /* update rack counts */
                        rack[*fptr]++;

                        /* check if newval is the max */
                        if (*fptr > max)
                            max = *fptr;

                        fptr += cols;
                    }



                    /* deal with pixels in main body of image */

                    for (line = halfwidth; line < lines - halfwidth - 1; line++)
                    {
                        /* store max in hframe */
                        *vfptr = max;
                        vfptr += cols;

                        /* remove oldval from rack, insert newval
		   	               and determine new max value */

                        if (*oldfptr != *fptr)
                        {

                            /* update rack counts */
                            rack[*fptr]++;
                            rack[*oldfptr]--;

                            /* check if newval is the max */
                            if (*fptr > max)
                                max = *fptr;

                            /* check if oldval was the max
		   		               and if so find new max */
                            while (rack[max] == 0)
                                max--;
                        }

                        fptr += cols;
                        oldfptr += cols;
                    }

                    /* deal with pixels within half filter 
		               width of top edge of image */

                    for (line = lines - halfwidth - 1; line < lines; line++)
                    {
                        *vfptr = max;
                        vfptr += cols;

                        /* update rack counts */
                        rack[*oldfptr]--;

                        /* check if oldval was the max
		   	               and if so find new max */
                        while (rack[max] == 0)
                            max--;

                        oldfptr += cols;
                    }
                }
            }

            /* vframe now contains 2 dimensional max */
            return vframeA;
        }
    }
}
