﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCVImageRenderers.Models
{
    public class ChannelDisplay
    {
        public ChannelDisplay()
        {
            HistogramLow = 0.0;
            HistogramHigh = 1.0;
            Gamma = 1.0;
            IsVisible = true;
            ColourString = "#ffffff";
        }
        public string Name { get; set; }
        public double HistogramLow { get; set; }
        public double HistogramHigh { get; set; }
        public double Gamma { get; set; }
        public bool IsVisible { get; set; }
        public string ColourString { get; set; }
        public bool IsCounterstain { get; set; }
    }
}
