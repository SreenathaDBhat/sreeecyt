﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PCVImageRenderers.Models
{
    public class ChannelRendererState
    {
        public FrameComponent Component { get; set; }
        public string ImageDataFilename { get; set; }
        public ushort[] Pixels { get; set; }
        public ushort[] FilteredPixels { get; set; }

        public float Low { get; set; }
        public float High { get; set; }
        public int OffsetX { get; set; }
        public int OffsetY { get; set; }
        public bool IsVisible { get; set; }
        public ComponentLevels Levels { get; set; }
        public Color RenderColor { get; set; }
        public Histogram Histogram { get; set; }

        public ChannelRendererState(IEnumerable<ComponentLevels> levels, FrameComponent c)
        {
            Component = c;
            Levels = levels.Where(l => l.Component == c).First(); //* TODO - Revisit for levels

            // cache values that can change DURING render.
            Low = Levels.Low;
            High = Levels.High;
            OffsetX = Levels.OffsetX;
            OffsetY = Levels.OffsetY;
            IsVisible = Levels.IsVisible;
            RenderColor = Levels.RenderColor;
            Histogram = Levels.Histogram;
        }
    }
}
