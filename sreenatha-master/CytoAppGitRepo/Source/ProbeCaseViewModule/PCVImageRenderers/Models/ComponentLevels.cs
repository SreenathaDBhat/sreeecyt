﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace PCVImageRenderers.Models
{
    [DataContract]
    public class ComponentLevels : INotifyPropertyChanged
    {
        //private FrameRenderer parent;
        private FrameComponent component;
        private float low, high;
        private int offsetX, offsetY;
        private bool visible, setByUser;
        private Histogram hist;
        private Color renderColor;

        public ComponentLevels()
        {

        }

        public ComponentLevels(/*FrameRenderer p,*/FrameComponent c, ComponentOffset offset)
        {
            //parent = p;
            component = c;
            visible = true;
            low = 0;
            high = 1;
            renderColor = c.RenderColor;

            offsetX = offset.X;
            offsetY = offset.Y;
        }

        [XmlIgnore]
        public FrameComponent Component
        {
            get
            {
                return component;
            }
        }

        private float Clamp(float value)
        {
            return Math.Min(1, Math.Max(0, value));
        }

        [DataMember]
        public float Low
        {
            get
            {
                return low;
            }
            set
            {
                low = Clamp(value);
                setByUser = true;
                Notify("Low");
                //parent.InvalidateRender(); TODO
            }
        }

        [DataMember]
        public float High
        {
            get
            {
                return high;
            }
            set
            {
                high = Clamp(value);
                setByUser = true;
                Notify("High");
                //parent.InvalidateRender(); TODO
            }
        }

        [DataMember]
        public bool IsVisible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
                Notify("IsVisible");
                //if (parent != null)
                    //parent.InvalidateRender(); TODO
            }
        }

        [XmlIgnore]
        public Histogram Histogram
        {
            get
            {
                return hist;
            }
            set
            {
                if (hist != value)
                {
                    hist = value;
                    Notify("Histogram");
                }
            }
        }

        // these are here, as we seriously need to refactor the frame renderer from using VM instead of models
        private PointCollection _histogramPoints;
        [XmlIgnore]
        public PointCollection HistogramPoints
        {
            get { return _histogramPoints; }
            set { _histogramPoints = value; Notify("HistogramPoints"); }
        }

        [DataMember]
        public int OffsetX
        {
            get
            {
                return offsetX;
            }
            set
            {
                offsetX = value;
                Notify("OffsetX");
                Notify("IsOffsetApplied");
                //if (parent != null)
                //    parent.InvalidateRender(); TODO
            }
        }

        [DataMember]
        public int OffsetY
        {
            get
            {
                return offsetY;
            }
            set
            {
                offsetY = value;
                Notify("OffsetY");
                Notify("IsOffsetApplied");
                //if (parent != null)
                //    parent.InvalidateRender(); TODO
            }
        }

        [XmlIgnore]
        public bool IsOffsetApplied
        {
            get
            {
                return (offsetX != 0 || offsetY != 0); 
            }
        }

        [DataMember]
        public Color RenderColor
        {
            get
            {
                return renderColor;
            }
            set
            {
                renderColor = value;
                Notify("RenderColor");
                //if (parent != null)
                //    parent.InvalidateRender(); TODO
            }
        }

        [DataMember]
        public bool SetByUser
        {
            get
            {
                return setByUser;
            }
            set
            {
                setByUser = value;
            }
        }

        public float[] EstimateHistogram()
        {
            var lowHigh = HistogramEstimator.Estimate(hist, Component.IsCounterstain);
            return lowHigh;
        }

        public void SetLowHighValues(float low, float high, bool shouldRender)
        {
            if (low != high)
            {
                this.low = low;
                this.high = high;
            }
            else
            {
                this.low = 0;
                this.high = 1;
            }
            this.setByUser = false;

            Notify("Low");
            Notify("High");

            //if (shouldRender)
            //    parent.InvalidateRender(); TODO
        }

        public void Hibernate()
        {
            //parent = null;
            hist = null;
        }

        public void Wake(/*FrameRenderer renderer, */ComponentOffset offset)
        {
            offsetX = offset.X;
            offsetY = offset.Y;
            //parent = renderer;
        }

        /// <summary>
        /// Remember to 'wake' this object before use.
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static ComponentLevels FromXml(XElement c, IEnumerable<FrameComponent> components)
        {
            var componentName = c.Attribute("Id").Value;
            var component = components.Where(cc => cc.Name == componentName).FirstOrDefault();

            if (component == null)
                return null;

            return new ComponentLevels(/*null, */component, ComponentOffset.Zero)
            {
                // Need to parse floating-point numbers using the invariant culture, in case the OS
                // is set to a culture that uses a different decimal separator (e.g. French), in
                // which case Parse() would fail to recognise a decimal point.
                low = float.Parse(c.Attribute("Low").Value, CultureInfo.InvariantCulture),
                high = float.Parse(c.Attribute("High").Value, CultureInfo.InvariantCulture),
                visible = bool.Parse(c.Attribute("Visible").Value),
                setByUser = true
            };
        }

        public object ToXml()
        {
            return new XElement("Channel", new XAttribute("Id", Component.Name),
                                            new XAttribute("Low", Low),
                                            new XAttribute("High", High),
                                            new XAttribute("Visible", IsVisible));
        }

        public ComponentLevels Clone()
        {
            var c = new ComponentLevels(/*null, */component, new ComponentOffset("", offsetX, offsetY));
            c.low = low;
            c.high = high;
            c.renderColor = renderColor;
            c.visible = visible;
            c.setByUser = setByUser;
            return c;
        }

        public ComponentLevels CloneForDifferentFrame(FrameComponent differentFrameComponent)
        {
            var c = new ComponentLevels(/*null, */differentFrameComponent, new ComponentOffset("", offsetX, offsetY));
            c.low = 0;
            c.high = 1;
            c.renderColor = renderColor;
            c.visible = visible;
            c.setByUser = false;
            return c;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void Notify(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
