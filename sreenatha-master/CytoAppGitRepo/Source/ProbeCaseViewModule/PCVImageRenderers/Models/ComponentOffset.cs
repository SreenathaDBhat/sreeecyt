﻿using System.Runtime.Serialization;

namespace PCVImageRenderers.Models
{
    [DataContract]
    public class ComponentOffset
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int X { get; set; }

        [DataMember]
        public int Y { get; set; }

        public ComponentOffset()
        {
        }

        public ComponentOffset(string n, int x, int y)
        {
            Name = n;
            X = x;
            Y = y;
        }

        private static ComponentOffset zero = new ComponentOffset("Zero", 0, 0);

        [IgnoreDataMember]
        public static ComponentOffset Zero
        {
            get { return zero; }
        }
    }
}
