﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace PCVImageRenderers.Models
{
    [DataContract]
    public sealed class Frame
    {
        [DataMember]
        public FrameComponent[] Components { get; set; }
        [DataMember]
        public string ImageFramesFileLocation { get;set; }
        [DataMember]
        public string Location { get; set; }
        [DataMember]
        public double XScale { get; set; }
        [DataMember]
        public double YScale { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public int BitsPerPixel { get; set; }
        [DataMember]
        public int ZLevels { get; set; }
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public double IdealX { get; set; }
        [DataMember]
        public double IdealY { get; set; }
        [DataMember]
        public double IdealZ { get; set; }
        [DataMember]
        public bool Calibrated { get; set; }

        public Frame()
        { }

        public Frame(XElement frameNode)
        {
            Components = (from c in frameNode.Descendants("Component")
                          select new FrameComponent(c)).ToArray();

            Location = frameNode.Attribute("Location").Value;

            SlidePoint position = SlidePoint.Decode(Location);
            IdealX = position.IdealX;
            IdealY = position.IdealY;
            IdealZ = position.IdealZ;
            Location = position.EnglandFinder;

            XAttribute xAttrib = frameNode.Attribute("XScale");
            XAttribute yAttrib = frameNode.Attribute("YScale");

            if (xAttrib != null && yAttrib != null)
            {
                XScale = double.Parse(xAttrib.Value, CultureInfo.InvariantCulture);
                YScale = double.Parse(yAttrib.Value, CultureInfo.InvariantCulture);
                Calibrated = true;
            }
            else
            {
                XScale = YScale = 1.0;
                Calibrated = false;
            }

            ZLevels = Components.Max(c => { return c.ImageFilenames.Count(); });
        }

        public static IEnumerable<Frame> LoadFromImageFramesXml(XElement xml, string rootDir)
        {
            return from f in xml.Descendants("Frame")
                   select new Frame(f)
                   {
                       Id = Guid.Parse(f.Attribute("Id").Value),
                       ImageFramesFileLocation = rootDir
                   };
        }

        public static IEnumerable<Frame> LoadFromImageFramesXml(string imageFramesFileName)
        {
            XElement xml = XElement.Load(imageFramesFileName);
            return LoadFromImageFramesXml(xml, Path.GetDirectoryName(imageFramesFileName));
        }

        public void SetSizes(int w, int h, int bpp)
        {
            Width = w;
            Height = h;
            BitsPerPixel = bpp;
        }

        public int ZLevelsMinusOne { get { return ZLevels - 1; } }

        public string ThumbnailFilename { get { return Id.ToString() + ".thumbnail.jpg"; } }

        public bool HasZStacks { get { return Components.Where(x => x.HasZStack).FirstOrDefault() != null; } }
    }
}
