﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml.Linq;

namespace PCVImageRenderers.Models
{
    [DataContract]
    public sealed class FrameComponent
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool IsCounterstain { get; set; }
        [DataMember]
        public string RenderColorString { get; set; }    
        [DataMember]
        public string[] ImageFilenames { get; set; }
        [DataMember]
        public string ProjectionImageFilename { get; set; }

        public FrameComponent()
        {
        }
       
        public FrameComponent(XElement componentNode)
        {
            Name = componentNode.Attribute("Name").Value;
            IsCounterstain = bool.Parse(componentNode.Attribute("IsCounterstain").Value);
            RenderColorString = componentNode.Attribute("RenderColor").Value;
            ImageFilenames = (from f in componentNode.Descendants("Image") select f.Attribute("Path").Value).ToArray();

            var pi = componentNode.Element("ProjectionImage");
            if (pi != null)
                ProjectionImageFilename = pi.Attribute("Path").Value;
        }

        public string ImageForZ(int z)
        {
            if (z < 0 || ImageFilenames == null || ImageFilenames.Count() == 0)
            {
                if (ProjectionImageFilename != null)
                    return ProjectionImageFilename;
                else
                    return ImageFilenames.FirstOrDefault();
            }

            var names = (string[])ImageFilenames;

            if (z >= names.Length)
                return names.FirstOrDefault();

            return names[z];
        }

        public override string ToString()
        {
            return Name;
        }

        public int NumberOfSlices
        {
            get
            {
                return ImageFilenames.Count() + (this.ProjectionImageFilename == null ? 0 : 1);
            }
        }

        public bool HasZStack
        {
            get
            {
                if (ImageFilenames == null || ImageFilenames.Count() == 0)
                    return false;

                if (ImageFilenames.Count() == 1 && ProjectionImageFilename == null)
                    return false;

                return true;
            }
        }

        public Color RenderColor
        {
            get { return (Color)ColorConverter.ConvertFromString(RenderColorString); }
        }

        public Brush RenderColorBrush
        {
            get { return new SolidColorBrush(RenderColor); }
        }

    }



}
