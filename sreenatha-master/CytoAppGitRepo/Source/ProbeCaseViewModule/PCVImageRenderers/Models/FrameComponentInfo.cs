﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCVImageRenderers.Models
{
    public class FrameComponentInfo
    {
        public FrameComponent Component { get; set; }
        public double HistoLo { get; set; }
        public double HistoHi { get; set; }
        public double Gamma { get; set; }

        public FrameComponentInfo()
        {
            HistoLo = 0;
            HistoHi = 1;
        }
    }
}
