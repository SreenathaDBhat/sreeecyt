﻿using Renderer;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCVImageRenderers.Models
{
    public unsafe static class FrameLoad
    {
        public static ushort[] Load(byte [] data, string componentID)
        {
            int w, h, bpp;
            return Load(data, componentID, out w, out h, out bpp);
        }

        public static ushort[] Load(byte [] data, string ComponentId, out int w, out int h, out int bpp)
        {
            if (data == null )
            {
                w = h = bpp = -1;
                return null;
            }
            
            // JPEG LS
            if (ComponentId.ToLower().EndsWith(".stk"))
            {
                return JPEGLS.Load(data, out w, out h, out bpp);
            }
            else
            {
                var stream = new MemoryStream(data);
                using (var reader = new BinaryReader(stream))
                {
                    int type = reader.ReadInt32();
                    bool compressed = false;

                    if (type == 80085)
                    {
                        compressed = true;
                        w = reader.ReadInt32();
                    }
                    else
                    {
                        w = type;
                    }

                    h = reader.ReadInt32();
                    bpp = reader.ReadInt32();
                    int len = w * h;
                    byte[] bytes = new byte[len * 2];

                    if (compressed)
                    {
                        int n = (int)(stream.Length - stream.Position);
                        byte[] compressedBytes = new byte[n];
                        stream.Read(compressedBytes, 0, n);
                        reader.Close();

                        MemoryStream ms = new MemoryStream(compressedBytes);
                        GZipStream zs = new GZipStream(ms, CompressionMode.Decompress);

                        zs.Read(bytes, 0, len * 2);
                        reader.Close();
                    }
                    else
                    {
                        stream.Read(bytes, 0, len * 2);
                        reader.Close();
                    }

                    ushort[] shortBytes = new ushort[len];
                    fixed (ushort* pDest = &shortBytes[0])
                    fixed (byte* pSrc = &bytes[0])
                    {
                        byte* src = pSrc;
                        ushort* dst = pDest;

                        for (int i = 0; i < len; ++i)
                        {
                            *dst++ = *(ushort*)src;
                            src += 2;
                        }
                    }

                    return shortBytes;
                }
            }
        }

        public static ushort[] Load(string file, out int w, out int h, out int bpp)
        {
            if (!File.Exists(file))
            {
                w = h = bpp = -1;
                return null;
            }

            var data = File.ReadAllBytes(file);

            return Load(data, file, out w, out h, out bpp);
        }
    }
}
