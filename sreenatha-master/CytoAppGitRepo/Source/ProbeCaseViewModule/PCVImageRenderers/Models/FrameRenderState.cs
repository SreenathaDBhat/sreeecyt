﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCVImageRenderers.Models
{
    public class FrameRenderState
    {
        public ChannelRendererState[] ChannelStates { get; set; }
        public int Z { get; set; }
        public bool AttenuateCounterstain { get; set; }
        public bool InvertCounterstain { get; set; }
        public bool MaxProjection { get; set; }
        public bool BackgroundSubtract { get; set; }
        public int BSLevel { get; set; }
    }
}
