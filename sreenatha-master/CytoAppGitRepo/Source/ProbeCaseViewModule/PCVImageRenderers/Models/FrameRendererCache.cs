﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCVImageRenderers.Models
{
    /// <summary>
    /// Caches images of the templated type, for one frame.
    /// To use the cache for a different Frame, assign the new frame to the Frame property.
    /// This will cause the cache to be reset, so that components from different Frames will not be mixed up.
    /// If an instance of this cache is shared between multiple threads, they must all be using the same Frame!
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FrameRendererCache<T>
    {
        private class CacheRef
        {
            public int backgroundSubtractLevel; // -1 indicates the non-background-subtracted image, i.e. a raw image
            public int Z;                       // -1 indicates the max projection image
            public FrameComponent Component;
            public T[] FilteredPixels;
        }

        private const int MAX_CACHE_SIZE = 100;

        private List<CacheRef> cache;
        private Frame frame;
        private object sync = new object();
 
        public FrameRendererCache(Frame f)
        {
            cache = new List<CacheRef>();
            frame = f;
        }
        
        public Frame Frame
        {
            get
            {
                return frame;
            }
            set
            {
                if (frame == null || frame.Id != value.Id)
                {
                    frame = value;
                    Clear();
                }
            }
        }
        
        /// <summary>
        /// Add to cache.
        /// </summary>
        /// <param name="filteredPixels"></param>
        /// <param name="component"></param>
        /// <param name="z"> -1 indicates the max projection image.</param>
        /// <param name="bsLevel"> -1 indicates the un-background subtracted image, ie a raw image.</param>
        public void AddToCache(T[] filteredPixels, FrameComponent component, int z, int bsLevel = -1)
        {
            lock (sync)
            {
                var existing = Find(component, z, bsLevel);
                if (existing == null)
                {
                    CacheRef cr = new CacheRef();
                    cr.backgroundSubtractLevel = bsLevel;
                    cr.Z = z;
                    cr.Component = component;
                    cr.FilteredPixels = filteredPixels;
                    cache.Add(cr);
                }

                while (cache.Count > MAX_CACHE_SIZE)
                    cache.RemoveAt(0);
            }
        }

        /// <summary>
        /// Retrieve an image.
        /// </summary>
        /// <param name="component"></param>
        /// <param name="z"> -1 indicates the max projection image.</param>
        /// <param name="bsLevel"> -1 indicates the un-background subtracted image, ie a raw image.</param>
        /// <returns></returns>
        public T[] Retrieve(FrameComponent component, int z, int bsLevel = -1)
        {
            lock (sync)
            {
                var cr = Find(component, z, bsLevel);
                return cr == null ? null : cr.FilteredPixels;
            }
        }

        private CacheRef Find(FrameComponent component, int z, int bsLevel)
        {
            var cr = cache.Where(c => c.backgroundSubtractLevel == bsLevel && c.Z == z && c.Component == component).FirstOrDefault();
            return cr;
        }

        public void Clear()
        {
            lock (sync)
            {
                cache.Clear();
            }
        }
    }
}
