﻿using Renderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PCVImageRenderers.Models
{
    public class FrameSave
    {
        public static void Save(string file, byte[] data, int w, int h, int bpp)
        {
            JPEGLS.Save(file, data, w, h, bpp);
        }
    }
}
