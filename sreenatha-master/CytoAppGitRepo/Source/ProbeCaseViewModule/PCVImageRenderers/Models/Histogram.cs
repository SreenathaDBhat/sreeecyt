﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Xml.Serialization;

namespace PCVImageRenderers.Models
{
    [DataContract]
    public unsafe class Histogram
    {
        private int[] rawBins;
        private int numberOfPixels;
        private int bpp;

        public Histogram()
        {

        }

        public Histogram(int width, int height, int bitsPerPixel, ushort[] imageData)
        {
            if (imageData == null)
                return;

            bpp = bitsPerPixel;

            fixed (ushort* p = &imageData[0])
            {
                Generate(width, height, p);
            }
        }

        public Histogram(int width, int height, int bitsPerPixel, ushort* imageData)
        {
            if (imageData == null)
                return;

            bpp = bitsPerPixel;

            Generate(width, height, imageData);
        }

        private void Generate(int width, int height, ushort* ptr)
        {
            var pts = new List<Point>();

            numberOfPixels = width * height;
            int numberOfGreys = (int)Math.Pow(2, bpp) - 1;
            MaximumGrey = numberOfGreys;
            int[] bins = new int[numberOfGreys + 1];
            double greatestCount = 0;

            fixed (int* binPtr = &bins[0])
            {
                ushort* p = ptr;
                ushort* end = p + numberOfPixels;
                while (p != end)
                {
                    ushort intensity = *p++;
                    if (intensity == 0)
                        continue;

                    if (intensity <= numberOfGreys)
                    {
                        int c = bins[intensity];
                        c++;
                        greatestCount = c > greatestCount ? c : greatestCount;
                        bins[intensity] = c;
                    }
                }

                rawBins = bins;
            }

            if (bins.Length > 256)
                bins = ReduceBins(bins, 256);

            fixed (int* binPtr = &bins[0])
            {
                pts.Add(new Point(0, 50));

                int* bin = binPtr;
                int* binEnd = binPtr + bins.Length;
                while (bin != binEnd)
                {
                    double v = *bin++;
                    if (Math.Abs(v) > double.Epsilon)
                        v = Math.Min(1, Math.Log(v, 3) / 9.5);

                    pts.Add(new Point(bin - binPtr, (1 - v) * 50));
                }

                pts.Add(new Point(numberOfGreys - 1, 50));
            }
            Points = pts.ToArray();
        }

        [DataMember]
        public int[] Bins
        {
            get { return rawBins; }
            set { rawBins = value; }
        }

        private int[] ReduceBins(int[] bins, int len)
        {
            int[] newBins = new int[len];
            double p = 0;
            double delta = bins.Length / (double)len;

            for (int i = 0; i < len; ++i)
            {
                //take the max value sample in the range delta to avoid
                //a zero value resulting from the 'tooth-comb' nature
                //of the histogram 
                int max = 0;
                for (int j = 0; j < delta; j++)
                    if (bins[(int)p + j] > max)
                        max = bins[(int)p + j];
                newBins[i] = max;
                p += delta;
            }

            return newBins;
        }

        [DataMember]
        public Point[] Points
        {
            get;
            set;
        }

        [DataMember]
        public int Bpp
        {
            get { return bpp; }
            set { bpp = value; }
        }

        [DataMember]
        public int MaximumGrey
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfPixels
        {
            get { return numberOfPixels; }
            set { numberOfPixels = value; }
        }

        [DataMember]
        public float RenderedLow
        {
            get; set;
        }

        [DataMember]
        public float RenderedHigh
        {
            get; set;
        }

    }
}
