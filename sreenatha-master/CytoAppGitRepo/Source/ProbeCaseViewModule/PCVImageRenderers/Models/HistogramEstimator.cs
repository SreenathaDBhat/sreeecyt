﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCVImageRenderers.Models
{
    public static class HistogramEstimator
    {
        public static float[] Estimate(Histogram hd, bool isCounterstain)
        {
            if (hd.Bpp < 1) // Empty histogram - maybe from a null image.
                return new float[] { 0, 0 };

            if (isCounterstain)
                return TopEndByValue(hd);

            return Aggressive(hd);
        }

        private static float[] TopEndByValue(Histogram hd)
        {
            double maxPixelValue = Math.Pow(2, hd.Bpp);

            int peakLocation = 0;

            int endOfScale = (int)maxPixelValue;

            double tinyPercentage = 0.05 * hd.NumberOfPixels;

            int Total = 0;
            for (int i = 1; i < maxPixelValue; i++)
            {
                Total += hd.Bins[i];
                if (Total > tinyPercentage)
                {
                    peakLocation = i;
                    break;
                }
            }

            for (int i = peakLocation; i < maxPixelValue; i++)
            {
                int bin = hd.Bins[i];

                if (bin > 0)
                    endOfScale = i;

            }

            return new float[]
            {
                (float)(peakLocation / maxPixelValue),
                (float)(endOfScale / maxPixelValue)
            };
        }

        private static float[] TopEndBySignificancePlusHalf(Histogram hd)
        {
            double maxPixelValue = Math.Pow(2, hd.Bpp);

            int peakLocation = 0;
            int peakValue = 0;
            int endOfSignificanceScale = (int)maxPixelValue;
            int endOfValueScale = (int)maxPixelValue;
            double tinyPercentage = Math.Min(5, hd.NumberOfPixels / 500000);

            for (ushort i = 0; i < maxPixelValue; i++)
            {
                int bin = hd.Bins[i];

                if (bin > 0)
                    endOfValueScale = i;

                if (bin > peakValue)
                {
                    peakValue = bin;
                    peakLocation = i;
                }
            }

            int halfway = (int)((endOfSignificanceScale + endOfValueScale) / 2);

            return new float[]
            {
                peakLocation / (float)maxPixelValue,
                halfway / (float)maxPixelValue
            };
        }

        private static float[] Aggressive(Histogram hd)
        {


            ushort maxPixelValue = (ushort)Math.Pow(2, hd.Bpp);

            int peakLocation = 0;
            int peakValue = 0;
            int endOfScale = (int)maxPixelValue;
            double tinyPercentage = Math.Min(5, hd.NumberOfPixels / 500000);

            tinyPercentage = 0.00005 * hd.NumberOfPixels;

            for (ushort i = 0; i < maxPixelValue; i++)
            {
                int bin = hd.Bins[i];

                if (bin > peakValue)
                {
                    peakValue = bin;
                    peakLocation = i;
                }
            }

            int Total = 0;
            for (int i = maxPixelValue - 1; i >= 0; i--)
            {
                Total += hd.Bins[i];
                if (Total >= tinyPercentage)
                {
                    endOfScale = i;
                    break;
                }
            }


            if (endOfScale < peakLocation + 128)
                endOfScale = peakLocation + 128;

            peakLocation += 20;

            return new float[] { peakLocation / (float)maxPixelValue, endOfScale / (float)maxPixelValue };
        }
    }
}
