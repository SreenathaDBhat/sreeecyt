﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace PCVImageRenderers.Models
{
    public class ScoreInfo
    {
        public Guid RegionId { get; set; }
        public double X { get; set; }
        public double Y { get; set; }

        private PointCollection _region;
        public PointCollection Region
        {
            get { return _region; }
            set
            {
                _region = value;
            }
        }

    }
}
