﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace PCVImageRenderers.Models
{
    public class ScoreThumbnail
    {
        public Guid ScoreId { get; set; }
        public Guid RegionId { get; set; }
        public Guid FrameID { get; set; }
        public Point Coordinates { get; set; }
        public IEnumerable<ChannelDisplay> Channels { get; set; }
        public bool AttenuateCounterstain { get; set; }
        public bool InvertCounterstain { get; set; }
        public int StackIndex { get; set; }
    }
}
