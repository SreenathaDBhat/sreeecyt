﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PCVImageRenderers.Models
{
    public class ScriptProcHelper
    {
        public unsafe static byte[] GetPlaneFromBitmap32(BitmapSource src, int offset)
        {
            unsafe
            {
                var len = src.PixelWidth * src.PixelHeight;

                byte[] pixels = new byte[len * 4];
                src.CopyPixels(pixels, src.PixelWidth * 4, 0);
                byte[] PixelsOut = new byte[len];

                fixed (byte* inPixels = &pixels[0])
                fixed (byte* outPixels = &PixelsOut[0])
                {
                    byte* inPixel = inPixels;
                    byte* outPixel = outPixels;
                    for (int i = 0; i < len; i++)
                    {
                        *outPixel = (*(inPixel + offset) != 0) ? (byte)255 : (byte)0; // can't switch interpolation off in DrawGeometry, so make sure its all white, no grays
                        outPixel++;
                        inPixel += 4;
                    }
                }
                return PixelsOut;
            }
        }

        public static byte[] Convert32To8(byte[] bytes, int w, int h, int byteOffset)
        {
            unsafe
            {
                byte[] pixelBuffer = new byte[w * h];

                int len = w * h;

                fixed (byte* outPixels = &pixelBuffer[0])
                fixed (byte* inPixels = &bytes[0])
                {
                    byte* outPixel = outPixels;
                    byte* inPixel = inPixels;

                    for (int index = 0; index < len; ++index)
                    {
                        *outPixel = *(inPixel + byteOffset); // chose which of the 4 bytes to extract data from 0 - 3
                        inPixel += 4;
                        outPixel++;
                    }
                }
                return pixelBuffer;
            }
        }

        public static BitmapSource Bitmap32FromBytes(byte[] bytes, int w, int h, byte r, byte g, byte b, byte a)
        {
            unsafe
            {
                byte[] pixelBuffer = new byte[w * h * 4];

                int len = w * h;

                fixed (byte* pixels = &pixelBuffer[0])
                fixed (byte* channelPixels = &bytes[0])
                {
                    byte* inPixel = channelPixels;
                    byte* outPixel = pixels;

                    for (int index = 0; index < len; ++index)
                    {
                        double intensity = *(inPixel++) / 255.0;
                        byte br = (byte)Math.Min((b * intensity) + (*outPixel), 255);
                        *(outPixel) = br;
                        outPixel++;

                        byte bg = (byte)Math.Min((g * intensity) + (*outPixel), 255);
                        *(outPixel) = bg;
                        outPixel++;

                        byte bb = (byte)Math.Min((r * intensity) + (*outPixel), 255);
                        *(outPixel) = bb;
                        outPixel++;

                        *(outPixel) = (byte)a;
                        outPixel++;
                    }
                }

                BitmapSource bitmap = BitmapSource.Create(w, h, 96, 96, PixelFormats.Bgra32, null, pixelBuffer, w * 4);
                bitmap.Freeze();
                return bitmap;
            }
        }

        public static BitmapSource CreateBlackBitmap(int width, int height)
        {
            int len = width * height;
            var bytes = new byte[len];
            unsafe
            {
                fixed (byte* pixels = &bytes[0])
                {
                    byte* inPixel = pixels;
                    for (int index = 0; index < len; ++index)
                    {
                        *(inPixel++) = 255;
                    }
                }
            }
            return Bitmap32FromBytes(bytes, width, height, 0, 0, 0, 255);
        }

        public static byte[] CutoutRectAndDWordAlign(byte[] bytes, int w, int h, Int32Rect roi, out int paddedWidth)
        {
            int padding = (roi.Width % 4) > 0 ? 4 - (roi.Width % 4) : 0;
            int len = (roi.Width + padding) * roi.Height;
            var dest = new byte[len];
            unsafe
            {
                fixed (byte* inPixels = &bytes[0])
                fixed (byte* outPixels = &dest[0])
                {
                    byte* outPixel = outPixels;
                    for (int line = 0; line < roi.Height; ++line)
                    {
                        var inPixel = inPixels + ((roi.Y + line) * w) + roi.X;
                        for (int column = 0; column < roi.Width; ++column)
                            *(outPixel++) = *(inPixel++);
                        for (int pad = 0; pad < padding; pad++)
                            *(outPixel++) = 0;
                    }
                }
            }
            paddedWidth = roi.Width + padding;
            return dest;
        }

        public static Int32Rect BoundingInt32Rect(IEnumerable<System.Windows.Point> maskPoints)
        {
            int rl = int.MaxValue;
            int rt = int.MaxValue;
            int rr = int.MinValue;
            int rb = int.MinValue;
            foreach (var pt in maskPoints)
            {
                rl = Math.Min(rl, (int)pt.X);
                rt = Math.Min(rt, (int)pt.Y);
                rr = Math.Max(rr, (int)pt.X);
                rb = Math.Max(rb, (int)pt.Y);
            }

            return new Int32Rect(rl, rt, rr - rl, rb - rt);
        }

        private static Point OffsetAndScale(Point p, Size offset, Double scale)
        {
            Point np = new Point(p.X, p.Y);
            np.Offset(-offset.Width, -offset.Height);
            np.X *= scale;
            np.Y *= scale;
            return np;
        }

        public static BitmapSource CreateMaskBitmapForFrame(BitmapSource frame, Int32Rect view, IEnumerable<Point> points)
        {
            double scale = frame.PixelWidth / view.Width;

            Point start = OffsetAndScale(points.First(), new Size(view.X, view.Y), scale);

            var segments = (from p in points.Skip(1)
                            select new LineSegment(OffsetAndScale(p, new Size(view.X, view.Y), scale), true)).ToArray();

            var geom = new PathGeometry(new PathFigure[] { new PathFigure(start, (IEnumerable<PathSegment>)segments, true) });
            DrawingVisual visual = new DrawingVisual();

            DrawingContext context = visual.RenderOpen();

            context.DrawRectangle(Brushes.Black, null, new Rect(0, 0, frame.PixelWidth, frame.PixelHeight));
            context.DrawGeometry(Brushes.White, null, geom);

            context.Close();

            var renderTarget = new RenderTargetBitmap((int)frame.PixelWidth, (int)frame.PixelHeight, 96, 96, PixelFormats.Pbgra32);

            renderTarget.Render(visual);

            return renderTarget;
        }

        public static PointCollection CreateEllipsePoints(double x, double y, double a, double b, double angle, int steps)
        {
            var points = new PointCollection();
            var beta = angle * (Math.PI / 180);
            var sinBeta = Math.Sin(beta);
            var cosBeta = Math.Cos(beta);
            var interval = (int)(360 / steps);

            for (int i = 0; i < 360; i += interval)
            {
                var alpha = i * (Math.PI / 180);
                var sinAlpha = Math.Sin(alpha);
                var cosAlpha = Math.Cos(alpha);

                var pX = x + (a * cosAlpha * cosBeta - b * sinAlpha * sinBeta);
                var pY = y + (a * cosAlpha * sinBeta + b * sinAlpha * cosBeta);
                points.Add(new Point(pX, pY));
            }
            return points;
        }
        
        public static byte [] GetMaskBytes(double width, double height, IEnumerable<SerializiblePoint> regionPoints)
        {
            Int32Rect view = new Int32Rect(0, 0, (int)width, (int)height);
            var points = from p in regionPoints select new Point(p.X, p.Y);
            return GetMaskBytes(view, points);
        }
       
        public static byte[] GetMaskBytes(Int32Rect view, IEnumerable<Point> points)
        {
            var black = CreateBlackBitmap(view.Width, view.Height);
            var bitmap = CreateMaskBitmapForFrame(black, view, points);
            return ScriptProcHelper.GetPlaneFromBitmap32(bitmap, 0);
        }

        public static void DumpDataAsPng(byte[] data, int w, int h, string path)
        {
            var image = ScriptProcHelper.Bitmap32FromBytes(data, w, h, 255, 255, 255, 255);
            ScriptProcHelper.DumpPng(image, path);
        }

        public static void DumpPng(BitmapSource bitmap, string p)
        {
            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(p)))
                    Directory.CreateDirectory(Path.GetDirectoryName(p));

                using (Stream s = File.Create(p))
                {
                    PngBitmapEncoder encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(bitmap));
                    encoder.Save(s);
                    s.Flush();
                }
            }
            catch { }; // for debug so don't bomb if you can't dump
        }

        public static ImageSource LoadImage(string path, int maxwidth = 0)
        {
            if (File.Exists(path))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                if (maxwidth > 0)
                    bmp.DecodePixelWidth = maxwidth;
                bmp.CacheOption = BitmapCacheOption.OnLoad;
                bmp.UriSource = new Uri(path);
                bmp.EndInit();
                bmp.Freeze();
                return bmp;
            }
            else
            {
                return null;
            }
        }
    }
}
