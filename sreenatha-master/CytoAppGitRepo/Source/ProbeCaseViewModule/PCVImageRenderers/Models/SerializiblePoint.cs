﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PCVImageRenderers.Models
{
    [Serializable()]
    public class SerializiblePoint
    {
        public double X { get; set; }
        public double Y { get; set; }
    }
}
