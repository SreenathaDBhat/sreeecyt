﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCVImageRenderers.Models
{
    public struct SlidePoint
    {
        private double x;
        private double y;
        private double z;
        private string ef;

        public SlidePoint(double x, double y, double z, string ef)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.ef = ef;
        }

        public double IdealX
        {
            get { return x; }
        }

        public double IdealY
        {
            get { return y; }
        }

        public double IdealZ
        {
            get { return z; }
        }

        public string EnglandFinder
        {
            get { return ef; }
        }

        public static string Encode(SlidePoint slidePoint)
        {
            // This must be in the same format as the position string created by GenerateTissueFishFileNames() in caplib/capman.cpp.
            // It looks like e.g. "10205.61, 32711.35, 14040.60, K44/3". 
            return string.Format(CultureInfo.InvariantCulture, "{0:0.000}, {1:0.000}, {2:0.000}, {3}", slidePoint.IdealX, slidePoint.IdealY, slidePoint.IdealZ, slidePoint.EnglandFinder);
        }

        public static SlidePoint Decode(string slidePointString)
        {
            // Some old data (pre CV v7.3 release), created on systems configured to use a comma as the floating-point decimal
            // separator (e.g. French systems), contains slidepoint strings that use a comma as the decimal separator. For this reason,
            // a comma followed by a space is used to split the string, as this won't pick up the decimal separators in any language.
            // Then the numbers are parsed using Parsing.TryParseDoubleMultilingual, which will cope with either decimal separator.
            string[] bits = slidePointString.Trim().Split(new string[] { ", " }, StringSplitOptions.None);

            // Initialise to defaults.
            double x = 0, y = 0, z = 0;
            string ef = "N/A";

            // Sometimes EF, or Z and EF values may not be present, so get as many as possible.
            if (bits.Length > 1)
            {
                Parsing.TryParseDoubleMultilingual(bits[0], out x);
                Parsing.TryParseDoubleMultilingual(bits[1], out y);
            }
            if (bits.Length > 2)
                Parsing.TryParseDoubleMultilingual(bits[2], out z);
            if (bits.Length > 3)
                ef = bits[3];

            return new SlidePoint(x, y, z, ef);
        }
    }
}
