﻿using System;
using System.Diagnostics;

namespace PCVImageRenderers.Models
{
    public static class SoftwareHistogramApplier
    {
        public static byte[] ApplyFit(Frame frame, ushort[] rawPixels, bool counterstain, double low, double high)
        {
            byte[] finalPixels = new byte[rawPixels.Length];
            StretchPixelsUsingHistogramSettings(rawPixels, frame, finalPixels, low, high);
            return finalPixels;
        }

        public static byte[] ApplyFit(Frame frame, ushort[] rawPixels, bool counterstain)
        {
            byte[] finalPixels = new byte[rawPixels.Length];

            var est = HistogramEstimator.Estimate(new Histogram(frame.Width, frame.Height, frame.BitsPerPixel, rawPixels), counterstain);

            StretchPixelsUsingHistogramSettings(rawPixels, frame, finalPixels, est[0], est[1]);
            return finalPixels;
        }

        /// <summary>
        /// Linearly scale rawPixels greylevels [low,high] to [0,255]
        /// </summary>
        /// <param name="rawPixels"></param>
        /// <param name="frame"></param>
        /// <param name="finalPixels"></param>
        /// <param name="low"> Is in the range [0.0, 1.0]</param>
        /// <param name="high">Is in the range [0.0, 1.0]</param>
        private unsafe static void StretchPixelsUsingHistogramSettings(ushort[] rawPixels, Frame frame, byte[] finalPixels, double low, double high)
        {
            ushort numGreys = (ushort)Math.Pow(2, frame.BitsPerPixel);

            if ((low < 0.0) || (low > 1.0)) Debug.WriteLine("StretchPixelsUsingHistogramSettings: low out of range.");
            if ((high < 0.0) || (high > 1.0)) Debug.WriteLine("StretchPixelsUsingHistogramSettings: high out of range.");

            ushort l = Math.Max((ushort)0, Math.Min((ushort)(low * numGreys + 0.5), numGreys));
            ushort h = Math.Max((ushort)0, Math.Min((ushort)(high * numGreys + 0.5), numGreys));

            if (h <= l)
            {
                Debug.WriteLine("StretchPixelsUsingHistogramSettings: high should be greater than low.");
                ushort temp = l;
                l = h;
                h = temp;
            }

            double factor = 255.0 / (h - l);

            fixed (ushort* rp = &rawPixels[0])
            fixed (byte* bp = &finalPixels[0])
            {
                ushort* r = rp;
                byte* b = bp;
                byte* end = bp + rawPixels.Length;
                ushort usValue;

                while (b != end)
                {
                    usValue = *r;
                    if (usValue <= l)
                        *b = 0;
                    else if (usValue >= h)
                        *b = 255;
                    else
                        *b = (byte)(factor * (usValue - l));

                    b++;
                    r++;
                }
            }
        }
    }
}
