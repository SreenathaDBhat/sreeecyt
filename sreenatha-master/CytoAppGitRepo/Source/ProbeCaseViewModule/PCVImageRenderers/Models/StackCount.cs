﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCVImageRenderers.Models
{
    internal class StackCount
    {
        public StackCount(FrameComponent f)
        {
            Fluor = f;
            StackSize = f.NumberOfSlices;
        }

        public FrameComponent Fluor { get; private set; }
        public int StackSize { get; set; }
    }

    
}
