﻿using PCVImageRenderers.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PCVImageRenderers
{
    public sealed class StackedThumbnailRenderer
    {
        public const uint ThumbnailCutTricklerMagicNumber = 0xABCDEF12;

        private IEnumerable<FrameComponent> _components;
        private StackCount[] _stackSizes;
        private int _width, _height;
        private bool _isHeaderLoaded;

        private int HeaderSize = 12 + (256 * 6);

        public StackedThumbnailRenderer(IEnumerable<FrameComponent> components)
        {
            this._components = components;
        }

        public int Width { get { return _width; } }
        public int Height { get { return _height; } }

        private unsafe void LoadHeader(MemoryStream msThumbnailData)
        {
            List<string> probeNames = new List<string>();
            int numStackImages = 0;

            var reader = new BinaryReader(msThumbnailData);

            var firstInt = reader.ReadInt32();
            bool newVersion = ThumbnailCutTricklerMagicNumber == (uint)firstInt;

            _width = (newVersion) ? reader.ReadInt32() : firstInt;
            _height = reader.ReadInt32();
            int bpp = reader.ReadInt32(); // read it even though we don't use it right now!

            int nComponents = (newVersion) ? reader.ReadInt32() : 6;
            HeaderSize = (newVersion) ? 20 + (256 * nComponents) : 12 + (256 * 6);

            var probes = reader.ReadChars(256 * nComponents);
            fixed (char* probesPtr = &probes[0])
            {
                for (int i = 0; i < _components.Count(); i++)
                {
                    int offset = 256 * i;
                    string s = new string(probesPtr, offset, 256);
                    int end = s.IndexOf('\0');
                    string probeName = s.Substring(0, end);
                    probeNames.Add(probeName);
                }
            }

            numStackImages = (int)(msThumbnailData.Length - HeaderSize) / (_width * _height);
            
            int S = 0;
            _stackSizes = new StackCount[probeNames.Count];
            for (int i = 0; i < probeNames.Count; i++)
            {
                var channel = _components.Where(c => c.Name.ToUpper() == probeNames[i].ToUpper()).FirstOrDefault();
                _stackSizes[i] = new StackCount(channel);
                S += _stackSizes[i].StackSize;
            }

            if (S != numStackImages && probeNames.Count > 1)
            {
                // NOTE this assumes the first channel is an unstacked one i.e. the counterstain
                _stackSizes[0].StackSize = 1;
                int StacksPerChannel = (numStackImages - 1) / (probeNames.Count - 1);
                for (int i = 1; i < probeNames.Count; i++)
                {
                    _stackSizes[i].StackSize = StacksPerChannel;
                }
            }

            _isHeaderLoaded = true;
        }

        public BitmapSource Render(byte [] thumbnailData, int stackIndex, IEnumerable<ChannelDisplay> channelDisplay, bool attenuateCounterstain, bool invertCounterstain)
        {
            if (thumbnailData == null)
                return null;

            using (var memoryStream = new MemoryStream(thumbnailData))
            {
                // HACK ALERT: Could cause issues if single instance of render with different thumbnail data, also thread safety??
                // depends how we're using it?
                if (!_isHeaderLoaded) 
                {
                    LoadHeader(memoryStream);
                }

                // -1 is max proj.
                stackIndex += 1;

                var channels = _components;
                var finalImageBytes = new byte[_width * _height * 4];
                bool handleCounterstainSpecially = attenuateCounterstain && !invertCounterstain;

                // Bit odd, but joins visiblity to FrameComponent
                List<FrameComponentInfo> visibleChannels = new List<FrameComponentInfo>();
                foreach (var v in _components)
                {
                    var display = channelDisplay.Where(d => d.Name == v.Name && d.IsVisible).FirstOrDefault();
                    if (display != null)
                        visibleChannels.Add(new FrameComponentInfo { Component = v, HistoLo = display.HistogramLow, HistoHi = display.HistogramHigh, Gamma = display.Gamma });
                }

                foreach (var ch in channels)
                {
                    var info = visibleChannels.Where(v => string.Compare(v.Component.Name, ch.Name, true) == 0).FirstOrDefault();
                    if (info == null || (handleCounterstainSpecially && ch.IsCounterstain))
                        continue;

                    var chan = _stackSizes.Where(s => s.Fluor == ch).FirstOrDefault();
                    if (chan != null)
                    {
                        var stackSizeForChannel = chan == null ? 1 : chan.StackSize - 1;
                        int stackIndexToUse = Math.Min(stackSizeForChannel, stackIndex);
                        var channelBytes = ReadImage(ch, stackIndexToUse, memoryStream);

                        AddToFinalImage(info, channelBytes, finalImageBytes, (ch.IsCounterstain && invertCounterstain));
                    }
                }

                if (handleCounterstainSpecially)
                {
                    var counterstain = visibleChannels.Where(c => c.Component.IsCounterstain).FirstOrDefault();
                    if (counterstain != null)
                    {
                        var stackSizeForChannel = _stackSizes.Where(s => s.Fluor.Name == counterstain.Component.Name).First().StackSize - 1;
                        int stackIndexToUse = Math.Min(stackSizeForChannel, stackIndex);
                        var channelBytes = ReadImage(counterstain.Component, stackIndexToUse, memoryStream);
                        AddCounterstainAttenuationToFinalImage(counterstain, channelBytes, finalImageBytes, false);
                    }
                }

                var b = BitmapSource.Create(_width, _height, 96, 96, PixelFormats.Pbgra32, null, finalImageBytes, _width * 4);
                b.Freeze();
                return b;
            }
        }

        public byte[] RenderToBitmapData(byte[] thumbnailData, int stackIndex, IEnumerable<ChannelDisplay> channelDisplay, bool attenuateCounterstain, bool invertCounterstain)
        {
            var bm = Render(thumbnailData, stackIndex, channelDisplay, attenuateCounterstain, invertCounterstain);
            using (var ms = new MemoryStream())
            {
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bm));
                encoder.QualityLevel = 75;
                encoder.Save(ms);
                ms.Flush();
                return ms.ToArray();
            }
        }

        private bool IsChannelVisible(FrameComponent ch, IEnumerable<FrameComponentInfo> visibleChannels)
        {
            var m = visibleChannels.Where(v => string.Compare(v.Component.Name, ch.Name, true) == 0).FirstOrDefault();
            return m != null;
        }

        private unsafe void AddCounterstainAttenuationToFinalImage(FrameComponentInfo ch, byte[] channelBytes, byte[] finalImageBytes, bool invert)
        {
            double range = (ch.HistoHi - ch.HistoLo) * 255;
            double lo = ch.HistoLo * 255;
            Color renderColor = ch.Component.IsCounterstain && invert ? Colors.White : ch.Component.RenderColor;

            byte[] gammaLut = null;
            if (ch.Component.IsCounterstain)
                gammaLut = GammaLUT(ch.Gamma);

            fixed (byte* pDest = &finalImageBytes[0])
            fixed (byte* pIn = &channelBytes[0])
            {
                int numPixels = _width * _height;
                byte* d = pDest;
                byte* s = pIn;

                for (int i = 0; i < numPixels; ++i)
                {
                    byte v = *s++;
                    double dv = Math.Min(1.0, Math.Max(0, (v - lo)) / range);

                    double maxSignal = *d;
                    maxSignal = Math.Max(maxSignal, *(d + 1));
                    maxSignal = Math.Max(maxSignal, *(d + 2));
                    dv *= 1 - (maxSignal / 255);

                    if (invert)
                        dv = 1 - dv;

                    *(d + 0) = (byte)Math.Min(255, (*d + (byte)(renderColor.B * dv)));
                    *(d + 1) = (byte)Math.Min(255, (*(d + 1) + (byte)(renderColor.G * dv)));
                    *(d + 2) = (byte)Math.Min(255, (*(d + 2) + (byte)(renderColor.R * dv)));
                    *(d + 3) = 255;

                    if (gammaLut != null)
                    {
                        *(d + 0) = gammaLut[*(d + 0)];
                        *(d + 1) = gammaLut[*(d + 1)];
                        *(d + 2) = gammaLut[*(d + 2)];
                    }

                    d += 4;
                }
            }
        }

        private byte[] GammaLUT(double gamma)
        {
            var lut = new byte[256];
            for (int i = 0; i < 256; i++)
                lut[i] = (byte)Math.Min(255, (int)((255.0 * Math.Pow(i / 255.0, 1.0 * gamma)) + 0.5));
            return lut;
        }

        private unsafe void AddToFinalImage(FrameComponentInfo ch, byte[] channelBytes, byte[] finalImageBytes, bool invert)
        {
            double range = (ch.HistoHi - ch.HistoLo) * 255;
            double lo = ch.HistoLo * 255;

            byte[] gammaLut = null;
            if (ch.Component.IsCounterstain)
                gammaLut = GammaLUT(ch.Gamma);

            fixed (byte* pDest = &finalImageBytes[0])
            fixed (byte* pIn = &channelBytes[0])
            {
                int numPixels = _width * _height;
                byte* d = pDest;
                byte* s = pIn;
                Color renderColor = (invert && ch.Component.IsCounterstain) ? Colors.White : ch.Component.RenderColor;

                for (int i = 0; i < numPixels; ++i)
                {
                    byte v = *s++;
                    double dv = Math.Min(1.0, Math.Max(0, (v - lo)) / range);

                    if (invert)
                        dv = 1 - dv;

                    *(d + 0) = (byte)Math.Min(255, (*d + (byte)(renderColor.B * dv)));
                    *(d + 1) = (byte)Math.Min(255, (*(d + 1) + (byte)(renderColor.G * dv)));
                    *(d + 2) = (byte)Math.Min(255, (*(d + 2) + (byte)(renderColor.R * dv)));
                    *(d + 3) = 255;

                    if (gammaLut != null)
                    {
                        *(d + 0) = gammaLut[*(d + 0)];
                        *(d + 1) = gammaLut[*(d + 1)];
                        *(d + 2) = gammaLut[*(d + 2)];
                    }

                    d += 4;
                }
            }
        }

        //public byte[] ReadImage(FrameComponent ch, int stackIndex)
        //{
        //    using (var str = File.OpenRead(_filePath))
        //    {
        //        return ReadImage(ch, stackIndex, str);
        //    }
        //}

        public byte[] ReadImage(FrameComponent ch, int stackIndex, MemoryStream memoryStream)
        {
            int offset = 0;
            int chunkSize = _width * _height;

            foreach (var c in _stackSizes)
            {
                if (c.Fluor.Name != ch.Name)
                {
                    offset += c.StackSize;
                    continue;
                }

                offset += stackIndex;
                memoryStream.Seek(HeaderSize + (offset * chunkSize), SeekOrigin.Begin);

                byte[] bytes = new byte[_width * _height];
                memoryStream.Read(bytes, 0, _width * _height);
                return bytes;
            }

            return null;
        }
    }
}
