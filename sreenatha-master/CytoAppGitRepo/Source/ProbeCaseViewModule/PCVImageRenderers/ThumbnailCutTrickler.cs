﻿using PCVImageRenderers.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCVImageRenderers
{
    public class ThumbnailCutTrickler
    {
        private BlockingCollection<Item> queue;
        private Task trickle;
        private FrameRendererCache<byte> cache;
        private bool stop;

        private class Item
        {
            public ScoreInfo ScoreInfo { get; set; }
            public Frame Frame { get; set; }
        }

        public const uint MagicNumber = 0xABCDEF12;
        public delegate byte[] LoadComponentDataDelegate(string componentId);
        LoadComponentDataDelegate _loadComponentDelegate;

        public ThumbnailCutTrickler(LoadComponentDataDelegate loadComponentDelegate)
        {
            _loadComponentDelegate = loadComponentDelegate;
            queue = new BlockingCollection<Item>();
            //trickle = Task.Factory.StartNew(() => ThreadMain(null));
        }

        public void Enqueue(ScoreInfo score, Frame frame)
        {
            queue.Add(new Item()
            {
                ScoreInfo = score,
                Frame = frame,
            });
        }

        public byte[] ThumbData(ScoreInfo score, Frame frame)
        {
            return GetThumbnailFromFrame(new Item() { ScoreInfo = score, Frame = frame });
        }

        public void WaitUntilComplete(Action<Task> continuation, TaskScheduler sched)
        {
            queue.CompleteAdding();
            trickle.ContinueWith(continuation, sched);
        }

        public Task StopAccepting()
        {
            queue.CompleteAdding();
            return trickle;
        }

        public bool StillAcceptingQueueAdditions
        {
            get { return !queue.IsAddingCompleted; }
        }

        public bool IsComplete
        {
            get { return queue.IsCompleted; }
        }

        public void Cancel()
        {
            stop = true;
        }

        void ThreadMain(object state)
        {
            var enumerable = queue.GetConsumingEnumerable();

            foreach (var item in enumerable)
            {
                if (stop)
                    continue; // Consume the remaining tasks but don't process them - this way queue.IsCompleted will end up true.

                try
                {
                    GetThumbnailFromFrame(item);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());

                    //try
                    //{
                    //    if (File.Exists(item.DestPath))
                    //        File.Delete(item.DestPath);
                    //}
                    //catch { }
                }
            }
        }

        private byte[] GetThumbnailFromFrame(Item item)
        {
            if (cache != null)
                cache.Frame = item.Frame; // This will reset the cache if the Frame doesn't match the previously cached one.
            else
                cache = new FrameRendererCache<byte>(item.Frame);

            var scoreInfo = item.ScoreInfo;

            var tempFile = Path.GetTempFileName();
            BinaryWriter writer = new BinaryWriter(File.OpenWrite(tempFile));
            try
            {
                int ThumbWidth = 200;
                int ThumbHeight = 200;

                if (item.ScoreInfo.RegionId != Guid.Empty)
                {
                    var points = item.ScoreInfo.Region;
                    var roi = ScriptProcHelper.BoundingInt32Rect(points);

                    var w = roi.Width + 50;
                    var h = roi.Height + 50;

                    w = (int)((w + 4) / 4) * 4;
                    ThumbWidth = Math.Max(200, w);
                    ThumbHeight = Math.Max(200, h);
                }

                int nComponents = item.Frame.Components.Count();
                writer.Write(MagicNumber);
                writer.Write(ThumbWidth);
                writer.Write(ThumbHeight);
                writer.Write((int)8);
                writer.Write(nComponents);

                var channelNames = new byte[256 * nComponents];
                int i = 0;

                var channels = item.Frame.Components.ToArray();
                foreach (var c in channels)
                {
                    var channelNamesStream = new MemoryStream(channelNames, 256 * (i++), 256);

                    using (var channelNamesWriter = new BinaryWriter(channelNamesStream))
                    {
                        foreach (var character in c.Name)
                            channelNamesWriter.Write(character);

                        channelNamesWriter.Flush();
                    }
                }

                writer.Write(channelNames, 0, 256 * nComponents);

                foreach (var c in channels)
                {
                    int zs = item.Frame.ZLevels;
                    if (c.IsCounterstain)
                        zs = 0;

                    for (int ci = -1; ci < zs; ++ci)
                    {
                        var cropped = Crop(item.Frame, c, item.ScoreInfo.X, item.ScoreInfo.Y, ThumbWidth, ThumbHeight, ci);
                        if (cropped == null)
                        {
                            System.Diagnostics.Debug.Fail("Crop failed. What to do");
                            return null;
                        }
                        writer.Write(cropped, 0, cropped.Length);
                    }
                }
                writer.Close();
                return File.ReadAllBytes(tempFile);
            }
            finally
            {
                //writer.Flush();
                //writer.Close();
            }
        }

        private unsafe byte[] Crop(Frame frame, FrameComponent component, double centerX, double centerY, int width, int height, int z)
        {
            //var filename = component.ImageForZ(z);
            byte[] pixelData = cache.Retrieve(component, z);

            string componentId = string.Empty;
            if (component == null)
                return null;
            componentId = component.ImageFilenames.FirstOrDefault();
            if (componentId == null)
                componentId = component.ProjectionImageFilename;

            if (frame.BitsPerPixel == 0)
            {
                componentId = frame.Components.FirstOrDefault().ImageFilenames.FirstOrDefault();
                if (componentId == null)
                    componentId = component.ProjectionImageFilename;
            }

            var frameData = _loadComponentDelegate(componentId);

            if (pixelData == null)
            {
                int w, h, bpp;
                ushort[] raw = FrameLoad.Load(frameData, componentId, out w, out h, out bpp);
                if (raw == null)
                {
                    return null;
                }
                frame.SetSizes(w, h, bpp);

                pixelData = SoftwareHistogramApplier.ApplyFit(frame, raw, component.IsCounterstain);
                cache.AddToCache(pixelData, component, z);
            }

            int inputWidth = frame.Width;
            centerX = Math.Max(width / 2, Math.Min(inputWidth - (width / 2) - 1, centerX));
            centerY = Math.Max(height / 2, Math.Min(frame.Height - (height / 2) - 1, centerY));

            int top = (int)(centerY - (height / 2));
            int bottom = top + height;
            int left = (int)(centerX - (width / 2));
            double maxPixelValue = Math.Pow(2, frame.BitsPerPixel);

            byte[] resultPixels = new byte[width * height];
            fixed (byte* pixelsPtr = &pixelData[0])
            fixed (byte* resultPtr = &resultPixels[0])
            {
                byte* o = resultPtr;

                for (int y = top; y < bottom; ++y)
                {
                    byte* i = &pixelsPtr[((inputWidth * y) + left)];

                    for (int x = 0; x < width; ++x)
                    {
                        byte v = *i++;
                        *o++ = v;
                    }
                }
            }

            return resultPixels;
        }
    }
}
