﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PCVImageRenderers
{
    public class ThumbnailOverlayRenderer
    {
        public static BitmapSource Render(byte[] bmpData, Color color)
        {
            // pain in the ass as its not already got alpha set; also doesn't seem to use Color; Review!!!
            int w = 0, h = 0;

            byte[] rgb = null;
            using (MemoryStream ms = new MemoryStream(bmpData))
            {
                rgb = GetImageDataFromBitmap(ms, out w, out h);
                if (rgb != null)
                    rgb = MakeBackgroundTransparent(rgb);
            }

            if (rgb == null)
                return null;

            BitmapSource b = BitmapSource.Create(w, h, 96, 96, PixelFormats.Bgra32, null, rgb, w * 4);
            b.Freeze();
            return b;
        }

        /// <summary>
        /// take pixel data and alpha backgound to transparent
        /// </summary>
        /// <param name="pixels">image pixel data</param>
        /// <returns>pixels</returns>
        public static unsafe byte[] MakeBackgroundTransparent(byte [] pixels)
        {
            // alpha out any black....
            fixed (byte* ptr = &pixels[0])
            {
                byte* p = ptr;
                byte* pend = p + pixels.Length;
                for (; p < pend; p += 4)
                {
                    if (*p == 0 && *(p + 1) == 0 && *(p + 2) == 0)
                        *(p + 3) = 0;
                }
            }
            return pixels;
        }

        /// <summary>
        /// Get image data from bitmap into byte array, dword aligned
        /// </summary>
        /// <param name="stream">bmp format image data</param>
        /// <param name="w">out value for bmp width</param>
        /// <param name="h">out value for bmp height</param>
        /// <returns></returns>
        public static byte [] GetImageDataFromBitmap(Stream stream, out int w, out int h)
        {
            byte[] pixels = null;

            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = stream;
            bmp.CacheOption = BitmapCacheOption.OnLoad;
            bmp.EndInit();
            bmp.Freeze();

            w = bmp.PixelWidth;
            h = bmp.PixelHeight;

            int length = w * h * 4;
            int stride = w * 4;
            pixels = new byte[length];
            bmp.CopyPixels(pixels, stride, 0);

            return pixels;
        }
    }
}
