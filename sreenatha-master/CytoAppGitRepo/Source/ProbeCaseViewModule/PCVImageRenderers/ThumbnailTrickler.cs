﻿using PCVImageRenderers.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;

namespace PCVImageRenderers
{
    public class ThumbnailTrickler
    {
        private const uint MagicNumber = 0xABCDEF12;
        private FrameRendererCache<byte> cache;

        public byte[] GetThumbnailData(ThumbInfo info)
        {
            if (cache != null)
                cache.Frame = info.Frame; // This will reset the cache if this Frame doesn't match the previously cached one.
            else
                cache = new FrameRendererCache<byte>(info.Frame);

            var id = info.ScoreThumbnail.ScoreId;
            var path = info.SlidePath;
            Stream thumbnailDataStream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(thumbnailDataStream);
            try
            {
                int ThumbWidth = 200;
                int ThumbHeight = 200;

                if (info.ScoreThumbnail.RegionId != Guid.Empty)
                {
                    // if a region, calculate the final fragment same as script
                    var regionPath = Path.Combine(Directory.GetParent(path).FullName, info.ScoreThumbnail.RegionId.ToString()) + ".region";
                    if (File.Exists(regionPath))
                    {
                        var points = LoadRegion(regionPath);
                        var roi = ScriptProcHelper.BoundingInt32Rect(points);
                        var w = roi.Width + 50;
                        var h = roi.Height + 50;
                        w = (int)((w + 4) / 4) * 4;
                        ThumbWidth = Math.Max(200, w);
                        ThumbHeight = Math.Max(200, h);
                    }
                }

                int nComponents = info.Frame.Components.Count();
                writer.Write(MagicNumber);
                writer.Write(ThumbWidth);
                writer.Write(ThumbHeight);
                writer.Write((int)8);
                writer.Write(nComponents);

                var channelNames = new byte[256 * nComponents];
                int i = 0;

                var channels = info.Frame.Components.ToArray();
                foreach (var c in channels)
                {
                    var channelNamesStream = new MemoryStream(channelNames, 256 * (i++), 256);
                    using (var channelNamesWriter = new BinaryWriter(channelNamesStream))
                    {
                        foreach (var character in c.Name)
                            channelNamesWriter.Write(character);

                        channelNamesWriter.Flush();
                    }
                }

                writer.Write(channelNames, 0, 256 * nComponents);

                foreach (var c in channels)
                {
                    int zs = info.Frame.ZLevels;
                    if (c.IsCounterstain)
                        zs = 0;

                    for (int ci = -1; ci < zs; ++ci)
                    {
                        var cropped = Crop(info.Frame, c, info.ScoreThumbnail.Coordinates.X, info.ScoreThumbnail.Coordinates.Y, ThumbWidth, ThumbHeight, ci);
                        if (cropped == null)
                        {
                            System.Diagnostics.Debug.Fail("Crop failed. What to do");
                            return null;
                        }
                        writer.Write(cropped, 0, cropped.Length);
                    }
                }
                writer.Flush();
                writer.Close();
                return ToArray(thumbnailDataStream);
            }
            finally
            {
            }
        }

        private unsafe byte[] Crop(Frame frame, FrameComponent component, double centerX, double centerY, int width, int height, int z)
        {
            var filename = component.ImageForZ(z);
            byte[] pixelData = cache.Retrieve(component, z);

            if (pixelData == null)
            {
                int w, h, bpp;
                ushort[] raw = FrameLoad.Load(Path.Combine(frame.ImageFramesFileLocation, filename), out w, out h, out bpp);
                if (raw == null)
                {
                    return null;
                }
                frame.SetSizes(w, h, bpp);

                pixelData = SoftwareHistogramApplier.ApplyFit(frame, raw, component.IsCounterstain);
                cache.AddToCache(pixelData, component, z);
            }

            int inputWidth = frame.Width;
            centerX = Math.Max(width / 2, Math.Min(inputWidth - (width / 2) - 1, centerX));
            centerY = Math.Max(height / 2, Math.Min(frame.Height - (height / 2) - 1, centerY));

            int top = (int)(centerY - (height / 2));
            int bottom = top + height;
            int left = (int)(centerX - (width / 2));
            double maxPixelValue = Math.Pow(2, frame.BitsPerPixel);

            byte[] resultPixels = new byte[width * height];
            fixed (byte* pixelsPtr = &pixelData[0])
            fixed (byte* resultPtr = &resultPixels[0])
            {
                byte* o = resultPtr;

                for (int y = top; y < bottom; ++y)
                {
                    byte* i = &pixelsPtr[((inputWidth * y) + left)];

                    for (int x = 0; x < width; ++x)
                    {
                        byte v = *i++;
                        *o++ = v;
                    }
                }
            }

            return resultPixels;
        }

        public System.Windows.Media.PointCollection LoadRegion(string path)
        {
            if (!File.Exists(path))
                return new System.Windows.Media.PointCollection();

            using (Stream stream = File.Open(path, FileMode.Open))
            {
                BinaryFormatter bin = new BinaryFormatter();
                var pts = (IEnumerable<Point>)bin.Deserialize(stream);
                var pc = from p in pts select new Point(p.X, p.Y);
                return new System.Windows.Media.PointCollection(pc); ;
            }
        }

        public static byte[] ToArray(Stream stream)
        {
            if (stream is MemoryStream)
            {
                return ((MemoryStream)stream).ToArray();
            }
            else
            {
                return null;
            }
        }
    }
    public class ThumbInfo
    {
        public ScoreThumbnail ScoreThumbnail { get; set; }
        public string SlidePath { get; set; }
        public Frame Frame { get; set; }
    }
}
