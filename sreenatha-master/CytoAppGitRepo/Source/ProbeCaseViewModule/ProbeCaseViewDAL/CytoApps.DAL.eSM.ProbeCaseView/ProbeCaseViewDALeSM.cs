﻿using ProbeCaseViewDataAccess;
using System;
using System.Collections.Generic;
using CytoApps.Models;
using PCVImageRenderers.Models;
using ProbeCaseViewDataAccess.Models;
using System.Xml.Linq;
using Core.DataServer.SoapClient;

namespace CytoApps.DAL.eSM.ProbeCaseView
{
    /// <summary>
    /// Provides Data Access to PCV FISH data via eSlideManager DataServer Extension
    /// </summary>
    [DalTypeId(Name = "eSM DataServer")]
    public class ProbeCaseViewDALeSM : IProbeCaseViewDataAccess, ILocking
    {
        private const string PCVFrames = "PCVFrames";
        private string _baseUrl;
        private string _token;

        public ProbeCaseViewDALeSM()
        {
        }

        public ProbeCaseViewDALeSM(string baseUrl, string token)
        {
            _baseUrl = baseUrl;
            _token = token;
        }

        public bool Initialize(DataSource dataSource)
        {
            var token = dataSource.SessionObject as string;
            _baseUrl = dataSource.ConnectionParameter[0].Value;
            _token = token;

            return true; //TODO, validate
        }

        public bool CanAnalyseSlide(Slide slide)
        {
            var eSmCytoAppsPCV = GetProxy();

            return eSmCytoAppsPCV.CanAnalyse(slide.CaseId, slide.Id);
        }

        // Enhanced Cell access
        public string GetEnhancedCellImageInfo(Session selectedSession, string scoreID)
        {
            throw new NotImplementedException();
        }

        public string GetRenderInstructionCellInfo(Session selectedSession, string scoreID)
        {
            throw new NotImplementedException();
        }

        public void SaveEnhancedScoredCells(Session session, List<string> enhancedCells)
        {
            throw new NotImplementedException();
        }

        public bool WriteRenderInstructionToEnhancedCell(Session selectedSession, byte[] bmp, string scoreID, XElement renderInstructionForBmp)
        {
            throw new NotImplementedException();
        }


        public byte[] GetFrameComponentData(Slide slide, string frameId, string componentId)
        {
            var eSmCytoAppsPCV = GetProxy();

            return eSmCytoAppsPCV.GetFrameComponentData(slide, frameId, componentId);
        }

        public byte[] GetFrameBitmap(Slide slide, string scanAreaId, Frame frame, IEnumerable<ComponentLevels> existingLevels, int requestedZ, FrameRenderSettings settings)
        {
            var eSmCytoAppsPCV = GetProxy();

            return eSmCytoAppsPCV.GetFrameBitmap(slide, scanAreaId, frame, existingLevels, requestedZ, settings);
        }

        public IEnumerable<Frame> GetFrames(ScanArea scanArea)
        {
            var eSmCytoAppsPCV = GetProxy();

            return eSmCytoAppsPCV.GetFrames(scanArea);
        }

        public byte[] GetOverlayData(Session session, Score score, Overlay overlay)
        {
            var eSmCytoAppsPCV = GetProxy();

            return eSmCytoAppsPCV.GetOverlayData(session, score, overlay);
        }

        public List<ScanArea> GetScanAreas(Slide slide)
        {
            var eSmCytoAppsPCV = GetProxy();

            return eSmCytoAppsPCV.GetScanAreas(slide);
        }

        public SessionScores GetScores(Session session)
        {
            var eSmCytoAppsPCV = GetProxy();

            return eSmCytoAppsPCV.GetSessionScores(session);
        }

        public Score[] GetScoreStats(Session session)
        {
            var eSmCytoAppsPCV = GetProxy();

            return eSmCytoAppsPCV.GetScoreStats(session);
        }

        public byte[] GetThumbnailData(Session session, Score score)
        {
            var eSmCytoAppsPCV = GetProxy();

            return eSmCytoAppsPCV.GetThumbnailData(session, score);
        }

        public byte[] GetThumbnailBitmap(Session session, Score score, int stackIndex, IEnumerable<ChannelDisplay> channelDisplay, bool attenuateCounterstain, bool invertCounterstain, bool isTemporarySaved)
        {
            var eSmCytoAppsPCV = GetProxy();

            return eSmCytoAppsPCV.GetThumbnailBitmap(session, score, stackIndex, channelDisplay, attenuateCounterstain, invertCounterstain);
        }

        public void SaveSession(Session session, List<ScoreSettings> scoreSettingsList, SessionScores scores)
        {
            var eSmCytoAppsPCV = GetProxy();

            eSmCytoAppsPCV.SaveScoredCells(session, scoreSettingsList, scores);
        }

        private eSmCytoAppsPCVProxy GetProxy()
        {
            var dsRouter = new DataServerProxyRouterHttp();
            dsRouter.URIHost = _baseUrl;
            dsRouter.URIPort = 80;
            dsRouter.URIPath = "dataserver";

            if (_token == null)
            {
                var coreProxy = new CoreProxy(dsRouter);
                coreProxy.Logon("administrator", "scanscope");
                _token = coreProxy.Token;
            }

            return new eSmCytoAppsPCVProxy(dsRouter) { Token = _token };
        }

        public List<ScoreSettings> LoadScoredCellsInfo(Session session, string scanAreaName)
        {
            // HACK not implemented yet
            return new List<ScoreSettings>();
        }

        public void ClearTemporarySavedThumb(Session session)
        {
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Create a new grid thumbnail for the score
        /// </summary>
        /// <param name="scoreThumbnail"></param>
        /// <param name="session"></param>
        /// <returns>vyte array of serialized encoded bitmap(jpeg)</returns>
        public byte[] CreateThumbnailBitmap(ScoreThumbnail scoreThumbnail, Session session)
        {
            var eSmCytoAppsPCV = GetProxy();

            return eSmCytoAppsPCV.CreateThumbnailBitmap(scoreThumbnail, session);
        }

        public Session CreateNewSession(ScanArea scanArea, string sessionName, bool useSpotScores, bool isCreatedWithSelectedFrames = false, List<string> selectedFrameIdList = null)
        {
            var eSmCytoAppsPCV = GetProxy();

            return eSmCytoAppsPCV.CreateNewSession(scanArea, sessionName, useSpotScores);
        }

        public void DeleteSession(Session session)
        {
            var eSmCytoAppsPCV = GetProxy();
            eSmCytoAppsPCV.DeleteSession(session);
        }

        public byte[] GetCachedFrameBitmap(string caseId, string slideId, string scanAreaId, string frameId)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetCachedFrameBitmaps(caseId, slideId, frameId);
        }

        public List<string> GetSelectedFramesForSession(Session session)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetSelectedFramesForSession(session);
        }

        public long GetFrameCellCount(ScanArea scanArea, string frameId)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetFrameCellCount(scanArea, frameId);
        }

        public void UpdateScores(Session session, List<string> frameIdList)
        {
            var eSmCytoAppsPCV = GetProxy();
            eSmCytoAppsPCV.UpdateScores(session, frameIdList);
        }

        public byte[] GetSlideOverviewImage(Slide slide, string etchImage)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetSlideOverviewImage(slide, etchImage);
        }

        public List<FrameOutline> GetFrameOutlines(Slide slide)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetFrameOutlines(slide);
        }

        public List<string> GetCompatibleAssays(FrameComponent[] components)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetCompatibleAssays(components);
        }

        public byte[] GetDefaultAssayData(string assayName)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetDefaultAssayData(assayName);
        }

        public byte[] GetAssayData(Session session)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetAssayData(session);
        }

        public AssayInfoWrapper GetClassifierNamesAndScripts(Session session = null)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetClassifierNamesAndScripts(session);
        }

        public void SaveAsDefaultAssay(byte[] assayData, string assayName)
        {
            var eSmCytoAppsPCV = GetProxy();
            eSmCytoAppsPCV.SaveAsDefaultAssay(assayData, assayName);
        }

        public List<string> GetAllDefaultAssays()
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetAllDefaultAssays();
        }

        public void QueueRegionForAnalysis(AnalysisRegion analysisRegion)
        {
            var eSmCytoAppsPCV = GetProxy();
            eSmCytoAppsPCV.QueueRegionForAnalysis(analysisRegion);
        }

        public void DeleteRegions(Session session, IEnumerable<string> regions)
        {
            var eSmCytoAppsPCV = GetProxy();
            eSmCytoAppsPCV.DeleteRegions(session, regions);
        }

        public List<AnnotationIdentifier> GetAllFramesAnnotation(Session session, List<Frame> frames)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetAllFramesAnnotation(session, frames);
        }

        public List<Score> GetScoreByRegionAnalysis(IEnumerable<AnalysisRegion> analysisRegions)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetScoreByRegionAnalysis(analysisRegions);
        }

        public bool SaveAnnotation(Session session, List<AnnotationIdentifier> frameAnnotations)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.SaveAnnotation(session, frameAnnotations);
        }

        public List<string> GetStoredCommonAnnotations()
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetStoredCommonAnnotations();
        }

        public void SaveCommonStoredAnnotation(List<string> annotationTexts)
        {
            var eSmCytoAppsPCV = GetProxy();
            eSmCytoAppsPCV.SaveCommonStoredAnnotation(annotationTexts);
        }

        public IEnumerable<AnalysisRegion> GetAllAnalysisRegions(Session session)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetAllAnalysisRegions(session);
        }

        public void SaveAssayData(byte[] assayData, Session session)
        {
            var eSmCytoAppsPCV = GetProxy();
            eSmCytoAppsPCV.SaveAssayData(assayData, session);
        }

        public byte[] GetAssayDataAndCreateBackup(Session session)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetAssayDataAndCreateBackup(session);
        }

        public void RestoreAssayData(Session session)
        {
            var eSmCytoAppsPCV = GetProxy();
            eSmCytoAppsPCV.RestoreAssayData(session);
        }

        public LockResult CheckKLockExists(UserDetails userDetails, string caseName, object lockingObject)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.CheckKLockExists(userDetails, caseName, lockingObject);
        }

        public void LockFeatureorObject(UserDetails userDetails, string caseName, object lockingObject)
        {
            var eSmCytoAppsPCV = GetProxy();
            eSmCytoAppsPCV.LockFeatureorObject(userDetails, caseName, lockingObject);
        }

        public void RemoveLock(UserDetails userDetails, string caseName)
        {
            var eSmCytoAppsPCV = GetProxy();
            eSmCytoAppsPCV.RemoveLock(userDetails, caseName);
        }
        public IEnumerable<string> GetSpotFolderNamesForReprocess(Session session, string frameId=null)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetSpotFolderNamesForReprocess(session, frameId);
        }

        public bool StartReprocessing(Session session, IEnumerable<string> spotFolders)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.StartReprocessing(session, spotFolders);
        }

        public ReprocessScores GetScoresFromSpotFoldersProcessed(Session session, IEnumerable<string> spotFolders)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.GetScoresFromSpotFoldersProcessed(session, spotFolders);
        }

        public bool CanCreateNewSession(string sessionName, Slide slide, string scanAreaName)
        {
            var eSmCytoAppsPCV = GetProxy();
            return eSmCytoAppsPCV.CanCreateNewSession(sessionName, slide, scanAreaName);
        }

        public void RestoreSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions)
        {
            var eSmCytoAppsPCV = GetProxy();
           eSmCytoAppsPCV.RestoreSession(session, scoreSettingsList, regions);
        }

        public void DeleteNewScoresAndRegionsFromSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions)
        {
            var eSmCytoAppsPCV = GetProxy();
            eSmCytoAppsPCV.DeleteNewScoresAndRegionsFromSession(session, scoreSettingsList, regions);
        }
    }
}
