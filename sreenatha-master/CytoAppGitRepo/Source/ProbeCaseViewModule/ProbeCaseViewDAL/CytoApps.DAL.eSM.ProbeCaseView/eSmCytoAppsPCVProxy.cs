﻿using Core.DataServer.SoapClient;
using CytoApps.Models;
using ProbeCaseViewDataAccess.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using PCVImageRenderers.Models;
using System.Xml;

namespace CytoApps.DAL.eSM.ProbeCaseView
{
    internal class eSmCytoAppsPCVProxy : DataServerProxy
    {
        protected const string APERIO_NAMESPACE = "http://www.aperio.com/webservices/";
        protected const string CytoAppsProxyEndPoint = "Aperio/CytoAppsServices";

        private XNamespace _nsAperio = APERIO_NAMESPACE;

        private DsResponse _lastResponse;

        internal eSmCytoAppsPCVProxy(IDataServerProxyRouter dsProxyRouter) : base(dsProxyRouter)
        {

        }

        internal bool CanAnalyse(string caseId, string slideId)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", caseId);
            AddMethodParameter("SlideId", slideId);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.CanAnalyse);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.CanAnalyse).Code;

            bool ret = false;
            if (retCode == 0)
            {
                var success = response.GetElement("Success");
                if (success != null)
                {
                    ret = (success.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase));
                }
            }

            _lastResponse = response;
            return ret;
        }

        internal byte[] GetFrameComponentData(Slide slide, string frameId, string componentId)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", slide.CaseId);
            AddMethodParameter("SlideId", slide.Id);
            AddMethodParameter("FrameId", frameId);
            AddMethodParameter("ComponentId", componentId);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetFrameComponentData);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetFrameComponentData).Code;

            byte[] imageRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("ImageData");
                if (imageDataBase64 != null)
                {
                    imageRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }

            _lastResponse = response;
            return imageRet;
        }

        internal byte[] GetFrameBitmap(Slide slide, string scanAreaId, Frame frame, IEnumerable<ComponentLevels> existingLevels, int requestedZ, FrameRenderSettings settings)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", slide.CaseId);
            AddMethodParameter("SlideId", slide.Id);
            AddMethodParameter("ScanAreaId", scanAreaId);
            AddMethodParameter("FrameId", frame.Id.ToString());
            AddMethodParameter("RequestedZ", requestedZ);

            // TODO, need to refactor ComponentLevel model for Serialization, currently only serializing changing values
            var existingLevelsXml = Serialize<List<ComponentLevels>>(existingLevels.ToList());
            AddMethodParameter("Levels", Convert.ToBase64String(Encoding.Unicode.GetBytes(existingLevelsXml)));

            var settingsXml = Serialize<FrameRenderSettings>(settings);
            AddMethodParameter("Settings", Convert.ToBase64String(Encoding.Unicode.GetBytes(settingsXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetFrameBitmap);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetFrameBitmap).Code;

            byte[] imageRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("ImageData");
                if (imageDataBase64 != null)
                {
                    imageRet = Convert.FromBase64String(imageDataBase64.Value);
                }

                var histoData = response.GetElement("ArrayOfHistogram");
                if (histoData != null)
                {
                    var histoLevels = eSmXmlDeserialize<List<Histogram>>(histoData.ToString()).ToArray();

                    // now assign to passed in levels, Legacy way of doing things
                    if (histoLevels.Length == existingLevels.Count())
                    {
                        var index = 0;
                        foreach (var c in existingLevels)
                        {
                            c.Histogram = histoLevels[index];
                            index++;
                        }
                    }
                }
            }

            _lastResponse = response;
            return imageRet;
        }

        internal void SaveScoredCells(Session session, List<ScoreSettings> scoreSettingsList, SessionScores scores)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);

            // Serialize to XML and encode, doesn't seem a way to send xml without a parser at the other end
            var scoresSettingsXml = Serialize<List<ScoreSettings>>(scoreSettingsList);
            var scoresXml = Serialize<SessionScores>(scores);

            AddMethodParameter("ScoreSettingsList", Convert.ToBase64String(Encoding.Unicode.GetBytes(scoresSettingsXml)));
            AddMethodParameter("Scores", Convert.ToBase64String(Encoding.Unicode.GetBytes(scoresXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.SaveSession);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.SaveSession).Code;

            _lastResponse = response;
        }

        internal List<ScanArea> GetScanAreas(Slide slide)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", slide.CaseId);
            AddMethodParameter("SlideId", slide.Id);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetScanAreas);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetScanAreas).Code;

            List<ScanArea> scanAreaRet = null;
            if (retCode == 0)
            {
                var scanAreaXml = response.GetElement("ArrayOfScanArea").ToString();
                scanAreaRet = eSmXmlDeserialize<List<ScanArea>>(scanAreaXml);
            }
            _lastResponse = response;
            return scanAreaRet;
        }

        internal Session CreateNewSession(ScanArea scanArea, string sessionName, bool useSpotScores)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", scanArea.Slide.CaseId);
            AddMethodParameter("SlideId", scanArea.Slide.Id);
            AddMethodParameter("ScanAreaId", scanArea.Id);
            AddMethodParameter("SessionId", sessionName);
            AddMethodParameter("UseSpotScores", useSpotScores);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.CreateNewSession);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.CreateNewSession).Code;

            Session sessionRet = null;
            if (retCode == 0)
            {
                var sessionXml = response.GetElement("Session");
                if (sessionXml != null)
                    sessionRet = eSmXmlDeserialize<Session>(sessionXml.ToString());
            }

            _lastResponse = response;
            return sessionRet;
        }

        internal List<FrameOutline> GetFrameOutlines(Slide slide)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", slide.CaseId);
            AddMethodParameter("SlideId", slide.Id);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetFrameOutlines);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetFrameOutlines).Code;

            List<FrameOutline> framesRet = new List<FrameOutline>();
            if (retCode == 0)
            {
                var framesXml = response.GetElement("ArrayOfFrameOutline");
                if (framesXml != null)
                    framesRet = eSmXmlDeserialize<List<FrameOutline>>(framesXml.ToString());
            }
            _lastResponse = response;
            return framesRet;
        }

        internal byte[] GetSlideOverviewImage(Slide slide, string etchImage)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", slide.CaseId);
            AddMethodParameter("SlideId", slide.Id);
            AddMethodParameter("ImageName", etchImage);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetSlideOverviewImage);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetSlideOverviewImage).Code;

            byte[] imageRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("ImageData");
                if (imageDataBase64 != null)
                {
                    imageRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }

            _lastResponse = response;
            return imageRet;
        }

        internal void SaveAsDefaultAssay(byte[] assayData, string assayName)
        {
            ClearMethodParameters();
            AddMethodParameter("AssayData", Convert.ToBase64String(assayData));
            AddMethodParameter("AssayName", assayName);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.SaveAsDefaultAssay);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.SaveAsDefaultAssay).Code;

            _lastResponse = response;
        }

        internal List<string> GetAllDefaultAssays()
        {
            ClearMethodParameters();

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetAllDefaultAssays);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetAllDefaultAssays).Code;

            List<string> assaysRet = null;
            if (retCode == 0)
            {
                var assaysXml = response.GetElement("ArrayOfString").ToString();
                assaysRet = eSmXmlDeserialize<List<string>>(assaysXml);
            }
            _lastResponse = response;
            return assaysRet;
        }

        internal byte[] GetDefaultAssayData(string assayName)
        {
            ClearMethodParameters();
            AddMethodParameter("AssayName", assayName);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetDefaultAssayData);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetDefaultAssayData).Code;

            byte[] assayData = null;
            if (retCode == 0)
            {
                var assayDataBase64 = response.GetElement("AssayData");
                if (assayDataBase64 != null)
                {
                    assayData = Convert.FromBase64String(assayDataBase64.Value);
                }
            }
            _lastResponse = response;
            return assayData;
        }

        internal List<string> GetCompatibleAssays(FrameComponent[] components)
        {
            ClearMethodParameters();

            var componentsXml = Serialize<List<FrameComponent>>(components.ToList());
            AddMethodParameter("FrameComponents", Convert.ToBase64String(Encoding.Unicode.GetBytes(componentsXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetCompatibleAssays);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetCompatibleAssays).Code;

            List<string> compatibleRet = null;
            if (retCode == 0)
            {
                var element = response.GetElement("ArrayOfString");
                if (element != null)
                {
                    var compatibleXml = element.ToString();
                    compatibleRet = eSmXmlDeserialize<List<string>>(compatibleXml);
                }
            }
            _lastResponse = response;
            return compatibleRet;
        }

        internal AssayInfoWrapper GetClassifierNamesAndScripts(Session sesson = null)
        {
            ClearMethodParameters();

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetClassifiersAndScriptNames);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetClassifiersAndScriptNames).Code;

            AssayInfoWrapper classifiersRet = null;
            if (retCode == 0)
            {
                var classifiersXml = response.GetElement("AssayInfoWrapper").ToString();
                classifiersRet = eSmXmlDeserialize<AssayInfoWrapper>(classifiersXml);
            }
            _lastResponse = response;
            return classifiersRet;
        }

        internal byte[] GetCachedFrameBitmaps(string caseId, string slideId, string frameId)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", caseId);
            AddMethodParameter("SlideId", slideId);
            AddMethodParameter("FrameId", frameId);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetCachedFrameBitmap);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetCachedFrameBitmap).Code;

            byte[] imageRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("ImageData");
                if (imageDataBase64 != null)
                {
                    imageRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }

            _lastResponse = response;
            return imageRet;
        }

        internal void DeleteSession(Session session)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.DeleteSession);

            _lastResponse = response;
        }

        internal byte[] GetOverlayData(Session session, Score score, Overlay overlay)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);
            AddMethodParameter("ScoreId", score.Id);
            AddMethodParameter("OverlayId", overlay.ID);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetOverlayData);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetOverlayData).Code;

            byte[] imageRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("ImageData");
                if (imageDataBase64 != null)
                {
                    imageRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }

            _lastResponse = response;
            return imageRet;
        }

        internal byte[] CreateThumbnailBitmap(ScoreThumbnail score, Session session)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);
            AddMethodParameter("ScoreId", score.ScoreId);
            AddMethodParameter("FrameId", score.FrameID);
            AddMethodParameter("PosX", score.Coordinates.X);
            AddMethodParameter("PosY", score.Coordinates.Y);
            AddMethodParameter("StackIndex", score.StackIndex);
            AddMethodParameter("Attenuate", score.AttenuateCounterstain);
            AddMethodParameter("Invert", score.InvertCounterstain);

            // add channel settings
            var channelElements = new XElement("Channels");
            foreach (var channel in score.Channels)
            {
                if (channel.IsVisible)
                {
                    channelElements.Add(new XElement("Channel",
                        new XElement("Name", channel.Name),
                        new XElement("Min", channel.HistogramLow),
                        new XElement("Max", channel.HistogramHigh),
                        new XElement("Visible", true)));
                }
            }
            AddMethodParameter(channelElements);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.CreateThumbnailBitmap);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.CreateThumbnailBitmap).Code;

            byte[] imageRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("ImageData");
                if (imageDataBase64 != null)
                {
                    imageRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }

            _lastResponse = response;
            return imageRet;
        }

        internal SessionScores GetSessionScores(Session session)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetSessionScores);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetSessionScores).Code;

            SessionScores scoresRet = null;
            if (retCode == 0)
            {
                var scoresString = response.GetElement("SessionScores").ToString();

                scoresRet = eSmXmlDeserialize<SessionScores>(scoresString);
            }

            _lastResponse = response;
            return scoresRet;
        }

        internal Score[] GetScoreStats(Session session)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetScoreStats);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetScoreStats).Code;

            Score[] scoresRet = null;
            if (retCode == 0)
            {
                var scoresString = response.GetElement("ArrayOfScore").ToString();
                scoresRet = eSmXmlDeserialize<Score[]>(scoresString);
            }

            _lastResponse = response;
            return scoresRet;
        }

        internal byte[] GetAssayData(Session session)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetAssayData);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetAssayData).Code;

            byte[] assayRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("AssayData");
                if (imageDataBase64 != null)
                {
                    assayRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }

            _lastResponse = response;
            return assayRet;
        }

        internal byte[] GetThumbnailData(Session session, Score score)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);
            AddMethodParameter("ScoreId", score.Id);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetThumbnailData);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetThumbnailData).Code;

            byte[] imageRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("ImageData");
                if (imageDataBase64 != null)
                {
                    imageRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }

            _lastResponse = response;
            return imageRet;
        }

        internal byte[] GetThumbnailBitmap(Session session, Score score, int stackIndex, IEnumerable<ChannelDisplay> channelDisplay, bool attenuateCounterstain, bool invertCounterstain)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);
            AddMethodParameter("ScoreId", score.Id);
            AddMethodParameter("StackIndex", stackIndex);
            AddMethodParameter("Attenuate", attenuateCounterstain);
            AddMethodParameter("Invert", invertCounterstain);

            // add channel settings
            var channelElements = new XElement("Channels");
            foreach (var channel in channelDisplay)
            {
                if (channel.IsVisible)
                {
                    channelElements.Add(new XElement("Channel",
                        new XElement("Name", channel.Name),
                        new XElement("Min", channel.HistogramLow),
                        new XElement("Max", channel.HistogramHigh),
                        new XElement("Visible", true)));
                }
            }
            AddMethodParameter(channelElements);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetThumbnailBitmap);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetThumbnailBitmap).Code;

            byte[] imageRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("ImageData");
                if (imageDataBase64 != null)
                {
                    imageRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }

            _lastResponse = response;
            return imageRet;
        }

        internal IEnumerable<Frame> GetFrames(ScanArea scanArea)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", scanArea.Slide.CaseId);
            AddMethodParameter("SlideId", scanArea.Slide.Id);
            AddMethodParameter("ScanAreaId", scanArea.Id);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetFrames);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetFrames).Code;

            List<Frame> framesRet = null;
            if (retCode == 0)
            {
                var framesXml = response.GetElement("ArrayOfFrame").ToString();
                framesRet = eSmXmlDeserialize<List<Frame>>(framesXml);
            }

            _lastResponse = response;
            return framesRet;
        }

        internal List<string> GetSelectedFramesForSession(Session session)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetSelectedFramesForSession);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetSelectedFramesForSession).Code;

            List<string> framesRet = null;
            if (retCode == 0)
            {
                var framesXml = response.GetElement("ArrayOfString").ToString();
                framesRet = eSmXmlDeserialize<List<string>>(framesXml);
            }

            _lastResponse = response;
            return framesRet;
        }

        internal void UpdateScores(Session session, List<string> frameIdList)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);

            var frameIdXml = Serialize<List<string>>(frameIdList);

            AddMethodParameter("FrameIDList", Convert.ToBase64String(Encoding.Unicode.GetBytes(frameIdXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.UpdateScores);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.UpdateScores).Code;

            _lastResponse = response;
        }

        internal long GetFrameCellCount(ScanArea scanArea, string frameId)
        {
            long count = -1;
            ClearMethodParameters();
            AddMethodParameter("CaseId", scanArea.Slide.CaseId);
            AddMethodParameter("SlideId", scanArea.Slide.Id);
            AddMethodParameter("ScanAreaId", scanArea.Id);
            AddMethodParameter("FrameId", frameId);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetFrameCellCount);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetFrameCellCount).Code;

            if (retCode == 0)
            {
                count = long.Parse(response.GetElement("CellCount").Value);
            }

            _lastResponse = response;
            return count;
        }


        public List<AnalysisRegion> GetAllAnalysisRegions(Session session)
        {
            ClearMethodParameters();
            var sessionXml = Serialize<Session>(session);
            AddMethodParameter("Session", Convert.ToBase64String(Encoding.Unicode.GetBytes(sessionXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetAllAnalysisRegions);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetAllAnalysisRegions).Code;

            List<AnalysisRegion> analysisRegions = null;
            if (retCode == 0)
            {
                var element = response.GetElement("ArrayOfAnalysisRegion");
                if (element != null)
                {
                    var analysisRegionXml = response.GetElement("ArrayOfAnalysisRegion").ToString();
                    analysisRegions = eSmXmlDeserialize<List<AnalysisRegion>>(analysisRegionXml);
                }
            }
            _lastResponse = response;
            return analysisRegions;
        }

        public void QueueRegionForAnalysis(AnalysisRegion analysisRegion)
        {
            ClearMethodParameters();

            // Serialize to XML and encode, doesn't seem a way to send xml without a parser at the other end
            var analysisRegionXml = Serialize<AnalysisRegion>(analysisRegion);
            AddMethodParameter("AnalysisRegion", Convert.ToBase64String(Encoding.Unicode.GetBytes(analysisRegionXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.QueueRegionForAnalysis);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.QueueRegionForAnalysis).Code;

            _lastResponse = response;
        }

        public void DeleteRegions(Session session, IEnumerable<string> regions)
        {
            ClearMethodParameters();

            var sessionXml = Serialize<Session>(session);
            AddMethodParameter("Session", Convert.ToBase64String(Encoding.Unicode.GetBytes(sessionXml)));

            var regionIds = Serialize<List<string>>(regions.ToList());
            AddMethodParameter("Regions", Convert.ToBase64String(Encoding.Unicode.GetBytes(regionIds)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.DeleteRegions);

            _lastResponse = response;
        }

        public List<Score> GetScoreByRegionAnalysis(IEnumerable<AnalysisRegion> analysisRegions)
        {
            ClearMethodParameters();

            var analysisRegionsXml = Serialize<List<AnalysisRegion>>(analysisRegions.ToList());
            AddMethodParameter("Regions", Convert.ToBase64String(Encoding.Unicode.GetBytes(analysisRegionsXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetScoreByRegionAnalysis);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetScoreByRegionAnalysis).Code;

            List<Score> scores = new List<Score>();
            if (retCode == 0)
            {
                var element = response.GetElement("ArrayOfScore");
                if (element != null)
                {
                    var scoresXml = response.GetElement("ArrayOfScore").ToString();
                    scores = eSmXmlDeserialize<List<Score>>(scoresXml);
                }
            }

            _lastResponse = response;
            return scores;

        }

        public List<string> GetStoredCommonAnnotations()
        {
            ClearMethodParameters();
            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetStoredCommonAnnotations);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetStoredCommonAnnotations).Code;

            List<string> annotationTexts = new List<string>();
            if (retCode == 0)
            {
                var element = response.GetElement("ArrayOfString");
                if (element != null)
                {
                    var annotationXml = response.GetElement("ArrayOfString").ToString();
                    annotationTexts = eSmXmlDeserialize<List<string>>(annotationXml);
                }
            }

            _lastResponse = response;
            return annotationTexts;
        }

        public List<AnnotationIdentifier> GetAllFramesAnnotation(Session session, List<Frame> frames)
        {
            ClearMethodParameters();

            var sessionXml = Serialize<Session>(session);
            AddMethodParameter("Session", Convert.ToBase64String(Encoding.Unicode.GetBytes(sessionXml)));

            var framesXml = Serialize<List<Frame>>(frames);
            AddMethodParameter("Frames", Convert.ToBase64String(Encoding.Unicode.GetBytes(framesXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetAllFramesAnnotation);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetAllFramesAnnotation).Code;

            List<AnnotationIdentifier> annotationTexts = new List<AnnotationIdentifier>();
            if (retCode == 0)
            {
                var element = response.GetElement("ArrayOfAnnotationIdentifier");
                if (element != null)
                {
                    var annotationXml = response.GetElement("ArrayOfAnnotationIdentifier").ToString();
                    annotationTexts = eSmXmlDeserialize<List<AnnotationIdentifier>>(annotationXml);
                }
            }

            _lastResponse = response;
            return annotationTexts;
        }

        public bool SaveAnnotation(Session session, List<AnnotationIdentifier> frameAnnotations)
        {
            ClearMethodParameters();
            var sessionXml = Serialize<Session>(session);
            AddMethodParameter("Session", Convert.ToBase64String(Encoding.Unicode.GetBytes(sessionXml)));

            var annotationIdentifiersXml = Serialize<List<AnnotationIdentifier>>(frameAnnotations);
            AddMethodParameter("AnnotationIdentifiers", Convert.ToBase64String(Encoding.Unicode.GetBytes(annotationIdentifiersXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.SaveAnnotation);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.SaveAnnotation).Code;

            bool ret = false;
            if (retCode == 0)
            {
                var success = response.GetElement("Success");
                if (success != null)
                {
                    ret = (success.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase));
                }
            }

            _lastResponse = response;
            return ret;
        }

        public void SaveCommonStoredAnnotation(List<string> annotationTexts)
        {
            ClearMethodParameters();

            var annotationTextsXml = Serialize<List<string>>(annotationTexts);
            AddMethodParameter("Annotations", Convert.ToBase64String(Encoding.Unicode.GetBytes(annotationTextsXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.SaveCommonStoredAnnotation);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.SaveCommonStoredAnnotation).Code;

            _lastResponse = response;
        }


        public byte[] GetAssayDataAndCreateBackup(Session session)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);
            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetAssayDataAndCreateBackup);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetAssayDataAndCreateBackup).Code;
            byte[] assayRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("AssayData");
                if (imageDataBase64 != null)
                {
                    assayRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }
            _lastResponse = response;
            return assayRet;
        }

        internal void RestoreAssayData(Session session)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);
            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.RestoreAssayData);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.RestoreAssayData).Code;
            _lastResponse = response;
        }


        internal void SaveAssayData(byte[] assayData, Session session)
        {
            ClearMethodParameters();
            AddMethodParameter("AssayData", Convert.ToBase64String(assayData));
            var sessionXml = Serialize<Session>(session);
            AddMethodParameter("Session", Convert.ToBase64String(Encoding.Unicode.GetBytes(sessionXml)));
            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.SaveAssayData);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.SaveAssayData).Code;
            _lastResponse = response;
        }

        internal LockResult CheckKLockExists(UserDetails userDetails, string caseName, object lockingObject)
        {
            ClearMethodParameters();
            var userXml = Serialize<UserDetails>(userDetails);
            AddMethodParameter("UserDetails", Convert.ToBase64String(Encoding.Unicode.GetBytes(userXml)));
            AddMethodParameter("CaseName", caseName);
            var sessionXml = Serialize<Session>(lockingObject as Session);
            AddMethodParameter("Session", Convert.ToBase64String(Encoding.Unicode.GetBytes(sessionXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.CheckKLockExists);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.CheckKLockExists).Code;

            LockResult lockResult = null;
            if (retCode == 0)
            {
                var lockResultString = response.GetElement("LockResult").ToString();
                lockResult = eSmXmlDeserialize<LockResult>(lockResultString);
            }
            _lastResponse = response;
            return lockResult;
        }

        internal void LockFeatureorObject(UserDetails userDetails, string caseName, object lockingObject)
        {
            ClearMethodParameters();
            var userXml = Serialize<UserDetails>(userDetails);
            AddMethodParameter("UserDetails", Convert.ToBase64String(Encoding.Unicode.GetBytes(userXml)));
            AddMethodParameter("CaseName", caseName);
            var sessionXml = Serialize<Session>(lockingObject as Session);
            AddMethodParameter("Session", Convert.ToBase64String(Encoding.Unicode.GetBytes(sessionXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.LockFeatureorObject);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.LockFeatureorObject).Code;
            _lastResponse = response;
        }

        internal void RemoveLock(UserDetails userDetails, string caseName)
        {
            ClearMethodParameters();
            var userXml = Serialize<UserDetails>(userDetails);
            AddMethodParameter("UserDetails", Convert.ToBase64String(Encoding.Unicode.GetBytes(userXml)));
            AddMethodParameter("CaseName", caseName);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.RemoveLock);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.RemoveLock).Code;
            _lastResponse = response;
        }

        internal bool CanCreateNewSession(string sessionName, Slide slide, string scanAreaName)
        {
            ClearMethodParameters();
            AddMethodParameter("SessionName", sessionName);
            var slideXml = Serialize<Slide>(slide);
            AddMethodParameter("Slide", Convert.ToBase64String(Encoding.Unicode.GetBytes(slideXml)));
            AddMethodParameter("ScanAreaName", scanAreaName);
            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.CanCreateNewSession);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.CanCreateNewSession).Code;

            bool ret = false;
            if (retCode == 0)
            {
                var success = response.GetElement("Success");
                if (success != null)
                {
                    ret = (success.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase));
                }
            }

            _lastResponse = response;
            return ret;

        }

        internal IEnumerable<string> GetSpotFolderNamesForReprocess(Session session, string frameId = null)
        {
            ClearMethodParameters();
            var sessionXml = Serialize<Session>(session);
            AddMethodParameter("Session", Convert.ToBase64String(Encoding.Unicode.GetBytes(sessionXml)));
            if(frameId!= null)
            AddMethodParameter("FrameId", frameId);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetSpotFolderNamesForReprocess);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetSpotFolderNamesForReprocess).Code;

            List<string> spotFolders = new List<string>();
            if (retCode == 0)
            {
                var element = response.GetElement("ArrayOfString");
                if (element != null)
                {
                    var annotationXml = response.GetElement("ArrayOfString").ToString();
                    spotFolders = eSmXmlDeserialize<List<string>>(annotationXml);
                }
            }
            _lastResponse = response;
            return spotFolders;
        }

       internal bool StartReprocessing(Session session, IEnumerable<string> spotFolders)
        {
            ClearMethodParameters();
            var sessionXml = Serialize<Session>(session);
            AddMethodParameter("Session", Convert.ToBase64String(Encoding.Unicode.GetBytes(sessionXml)));
            var SpotFoldersXml = Serialize<List<string>>(spotFolders.ToList());
            AddMethodParameter("SpotFolders", Convert.ToBase64String(Encoding.Unicode.GetBytes(SpotFoldersXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.StartReprocessing);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.StartReprocessing).Code;

            bool ret = false;
            if (retCode == 0)
            {
                var success = response.GetElement("Success");
                if (success != null)
                {
                    ret = (success.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase));
                }
            }
            _lastResponse = response;
            return ret;
        }


        internal ReprocessScores GetScoresFromSpotFoldersProcessed(Session session, IEnumerable<string> SpotFolders)
        {
            ClearMethodParameters();
            var sessionXml = Serialize<Session>(session);
            AddMethodParameter("Session", Convert.ToBase64String(Encoding.Unicode.GetBytes(sessionXml)));
            var SpotFoldersXml = Serialize<List<string>>(SpotFolders.ToList());
            AddMethodParameter("SpotFolders", Convert.ToBase64String(Encoding.Unicode.GetBytes(SpotFoldersXml)));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.GetScoresFromSpotFoldersProcessed);
            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.GetScoresFromSpotFoldersProcessed).Code;

            ReprocessScores reprocessScores = null;
            if (retCode == 0)
            {
                var lockResultString = response.GetElement("ReprocessScores").ToString();
                reprocessScores = eSmXmlDeserialize<ReprocessScores>(lockResultString);
            }
            _lastResponse = response;
            return reprocessScores;
        }



        internal void RestoreSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions)
        {

            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);
 
            var scoresSettingsXml = Serialize<List<ScoreSettings>>(scoreSettingsList);
          
            AddMethodParameter("ScoreSettingsList", Convert.ToBase64String(Encoding.Unicode.GetBytes(scoresSettingsXml)));

            var regionIds = Serialize<List<string>>(regions.ToList());
            AddMethodParameter("Regions", Convert.ToBase64String(Encoding.Unicode.GetBytes(regionIds)));
            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.RestoreSession);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.RestoreSession).Code;
            _lastResponse = response;
        }

        internal void DeleteNewScoresAndRegionsFromSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions)
        {

            ClearMethodParameters();
            AddMethodParameter("CaseId", session.Slide.CaseId);
            AddMethodParameter("SlideId", session.Slide.Id);
            AddMethodParameter("ScanAreaId", session.ScanAreaId);
            AddMethodParameter("SessionId", session.Id);

            var scoresSettingsXml = Serialize<List<ScoreSettings>>(scoreSettingsList);

            AddMethodParameter("ScoreSettingsList", Convert.ToBase64String(Encoding.Unicode.GetBytes(scoresSettingsXml)));

            var regionIds = Serialize<List<string>>(regions.ToList());
            AddMethodParameter("Regions", Convert.ToBase64String(Encoding.Unicode.GetBytes(regionIds)));
            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsPCVProxyMethodNames.DeleteNewScoresAndRegionsFromSession);

            int retCode = response.GetResult(eSmCytoAppsPCVProxyMethodNames.DeleteNewScoresAndRegionsFromSession).Code;
            _lastResponse = response;
        }

        
        /// <summary>
        /// Deserialize class from XML to object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toDeserialize"></param>
        /// <returns></returns>
        private static T eSmXmlDeserialize<T>(string toDeserialize)
        {
            // hack out the namespace (added by eSM) from the xml string otherwise no deserialization
            toDeserialize = toDeserialize.Replace("xmlns=\"http://www.aperio.com/webservices/\"", "");

            // deserialize to class of type T
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            StringReader textReader = new StringReader(toDeserialize);
            return (T)xmlSerializer.Deserialize(textReader);
        }

        /// <summary>
        /// Serialize object to XML string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toSerialize"></param>
        /// <returns></returns>
        public static string Serialize<T>(T toSerialize)
        {
            // Reform xml declaration as we want to add it in another xml doc
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            var xmlSettings = new XmlWriterSettings
            {
                Indent = false,
                OmitXmlDeclaration = true
            };

            // Serialize object into xml using settings
            using (StringWriter stringWriter = new StringWriter())
            using (var xmlWriter = XmlWriter.Create(stringWriter, xmlSettings))
            {
                var namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);

                xmlSerializer.Serialize(xmlWriter, toSerialize, namespaces);
                return stringWriter.ToString();
            }
        }

    }
}
