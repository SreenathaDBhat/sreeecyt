﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CytoApps.DAL.eSM.ProbeCaseView
{
    public class eSmCytoAppsPCVProxyMethodNames
    {
        public static string CanAnalyse = "PCVCanAnalyse";
        public static string GetScanAreas = "PCVGetScanAreas";
        public static string GetSessionScores = "PCVGetSessionScores";
        public static string GetScoreStats = "PCVGetScoreStats";
        public static string GetAssayData = "PCVGetAssayData";
        public static string GetFrames = "PCVGetFrames";
        public static string GetThumbnailData = "PCVGetThumbnailData";
        public static string GetThumbnailBitmap = "PCVGetThumbnailBitmap";
        public static string GetOverlayData = "PCVGetOverlayData";
        public static string GetFrameComponentData = "PCVGetFrameComponentData";
        public static string GetFrameBitmap = "PCVGetFrameBitmap";
        public static string CreateThumbnailBitmap = "PCVCreateThumbnailBitmap";
        public static string SaveSession = "PCVSaveSession";
        public static string CreateNewSession = "PCVCreateNewSession";
        public static string DeleteSession = "PCVDeleteSession";
        public static string GetCachedFrameBitmap = "PCVGetCachedFrameBitmap";
        public static string GetFrameCellCount = "PCVGetFrameCellCount";
        public static string GetSelectedFramesForSession = "PCVGetSelectedFramesForSession";
        public static string UpdateScores = "PCVUpdateScores";
        public static string GetClassifiersAndScriptNames = "PCVGetClassifiers";
        public static string GetCompatibleAssays = "PCVGetCompatibleAssays";
        public static string GetDefaultAssayData = "PCVGetDefaultAssayData";
        public static string SaveAsDefaultAssay = "PCVSaveAsDefaultAssay";
        public static string GetAllDefaultAssays = "PCVGetAllDefaultAssays";
        public static string GetSlideOverviewImage = "PCVGetSlideOverviewImage";
        public static string GetFrameOutlines = "PCVGetFrameOutlines";
        public static string QueueRegionForAnalysis = "PCVQueueRegionForAnalysis";
        public static string GetAllAnalysisRegions = "PCVGetAllAnalysisRegions";
        public static string GetScoreByRegionAnalysis = "PCVGetScoreByRegionAnalysis";
        public static string DeleteRegions = "PCVDeleteRegions";
        public static string GetStoredCommonAnnotations = "PCVGetStoredCommonAnnotations";
        public static string GetAllFramesAnnotation = "PCVGetAllFramesAnnotation";
        public static string SaveAnnotation = "PCVSaveAnnotation";
        public static string SaveCommonStoredAnnotation = "PCVSaveCommonStoredAnnotation";
        public static string GetAssayDataAndCreateBackup = "PCVGetAssayDataAndCreateBackup";
        public static string RestoreAssayData = "PCVRestoreAssayData";
        public static string SaveAssayData = "PCVSaveAssayData";
        public static string CheckKLockExists = "PCVCheckKLockExists";
        public static string LockFeatureorObject = "PCVLockFeatureorObject";
        public static string RemoveLock = "PCVRemoveLock";
        public static string CanCreateNewSession = "PCVCanCreateNewSession";
        public static string GetSpotFolderNamesForReprocess = "PCVGetSpotFolderNamesForReprocess";
        public static string StartReprocessing = "PCVStartReprocessing";
        public static string GetScoresFromSpotFoldersProcessed = "PCVGetScoresFromSpotFoldersProcessed";

        public static string RestoreSession = "PCVRestoreSession";
        public static string DeleteNewScoresAndRegionsFromSession = "PCVDeleteNewScoresAndRegionsFromSession";
    }
}
