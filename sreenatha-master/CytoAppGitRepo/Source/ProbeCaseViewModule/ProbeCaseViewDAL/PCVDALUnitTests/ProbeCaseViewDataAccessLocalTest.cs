﻿using CytoApps.Models;
using CytoApps.SpotConfiguration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PCVImageRenderers;
using PCVImageRenderers.Models;
using ProbeCaseViewDataAccess;
using ProbeCaseViewDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PCVDALUnitTests
{
    [TestClass]
    public class ProbeCaseViewDataAccessLocalTest
    {
        public ProbeCaseViewDataAccessLocalTest()
        {
            TestSlide = new Slide("24848-NICR", "033500000106_19", "slide0");
            Remote = false;
        }

        private Slide TestSlide
        {
            get;
            set;
        }

        private bool Remote { get; set; }

        private string CaseBaseFolder
        {
            get
            {
                return @"\\localhost\CaseBase\cases\";
            }
        }

        private string RemoteBaseUrl
        {
            get { return "http://localhost/cytoappsrest/"; }
        }

        private IProbeCaseViewDataAccess GetPCVDAL()
        {
            if (!Remote)
                return new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(CaseBaseFolder);
            else
                //return new ProbeCaseViewDataAccessRemote.ProbeCaseViewDataAccessRemote(RemoteBaseUrl);
                return new CytoApps.DAL.eSM.ProbeCaseView.ProbeCaseViewDALeSM("vs2012-update", null);

        }

        [TestMethod]
        public void CheckCounts()
        {
            var dal = GetPCVDAL();

            var scanareas = dal.GetScanAreas(TestSlide).ToArray();

            Assert.IsTrue(scanareas.Count() == 2);

            var scanarea = scanareas.FirstOrDefault();

            Assert.IsTrue(scanarea.Sessions.Count() == 1);

            var session = scanarea.Sessions.FirstOrDefault();

            var sessionScores = dal.GetScores(scanarea.Sessions.FirstOrDefault());

            Assert.IsTrue(sessionScores.Scores.Count() == 164);

            var scores = dal.GetScoreStats(scanarea.Sessions.FirstOrDefault());

            Assert.IsTrue(scores.Count() == 164);

            var frames = dal.GetFrames(scanarea);

            Assert.IsTrue(frames.Count() == 16);


        }

        [TestMethod]
        public void CheckFrameAnnotations()
        {
            var dal = GetPCVDAL();
            var scanAreas = dal.GetScanAreas(TestSlide);
            Assert.IsTrue(scanAreas.Count() == 2);

            var scanarea = scanAreas.FirstOrDefault();
            var session = scanarea.Sessions.FirstOrDefault();
            var frames = dal.GetFrames(scanarea).ToList();
            Assert.IsTrue(frames.Count() == 10);

            var annotations = dal.GetAllFramesAnnotation(session, frames);
            Assert.IsNotNull(annotations);
            annotations.Where(l => l.Annotations.Count != 0).ToList();
            Assert.IsTrue(dal.SaveAnnotation(session, annotations));
        }
        [TestMethod]
        public void CheckRegionScoring()
        {
            var dal = GetPCVDAL();
            var scanAreas = dal.GetScanAreas(TestSlide);
            Assert.IsTrue(scanAreas.Count() == 1);
            var scanarea = scanAreas.FirstOrDefault();
            var session = scanarea.Sessions.FirstOrDefault();
            var frames = dal.GetFrames(scanarea).ToList();
            Assert.IsTrue(frames.Count() == 20);

            //get the regions
            var regions = dal.GetAllAnalysisRegions(session);
            Assert.IsNotNull(regions);
            //check region has scored
            var scores = dal.GetScoreByRegionAnalysis(regions);
            Assert.IsNotNull(scores);
        }

        [TestMethod]
        public void CheckRegionDrawnAndScored()
        {
            var dal = GetPCVDAL();
            var scanAreas = dal.GetScanAreas(TestSlide);
            Assert.IsTrue(scanAreas.Count() == 1);
            var scanarea = scanAreas.FirstOrDefault();
            var session = scanarea.Sessions.FirstOrDefault();
            var frames = dal.GetFrames(scanarea).ToList();
            Assert.IsTrue(frames.Count() == 20);

            var sessionScores = dal.GetScores(scanarea.Sessions.FirstOrDefault());

            var frame = frames[0];
            frame.Height = 1600;
            frame.Width = 1200;
            //Create new region
            var drawRegions = DrawRegion(session, frame);
            //this collection is delete the regions
            List<AnalysisRegion> regions = new List<AnalysisRegion>();

            dal.QueueRegionForAnalysis(drawRegions[0]);
            dal.QueueRegionForAnalysis(drawRegions[1]);

            //Checking the region has scored or not
            //try for 5 times to check the score
            int count = 0;
            while (true)
            {
                count++;
                var scores = dal.GetScoreByRegionAnalysis(drawRegions);
                if (scores != null)
                {
                    foreach (Score score in scores)
                    {
                        AnalysisRegion region = drawRegions.Where(x => x.Id == score.RegionId).FirstOrDefault();
                        //This is for delete
                        regions.Add(region);
                        if (drawRegions.Contains(region))
                        {
                            if (score.Class != null)
                                // Remove processed Region from regions collection.
                                drawRegions.Remove(region);
                            Assert.IsNotNull(score);
                        }
                    }
                }
                if (drawRegions.Count == 0 || count == 5)
                {
                    break;
                }
            }
            dal.DeleteRegions(session, regions.Select(x => x.Id.ToString()).ToList());
        }

        private List<AnalysisRegion> DrawRegion(Session session, PCVImageRenderers.Models.Frame frame)
        {
            List<AnalysisRegion> regions = new List<AnalysisRegion>();
            var pts = ScriptProcHelper.CreateEllipsePoints(57.629277896809356, 211.98679715096418, 49, 49, 0, 25);
            AnalysisRegion analysisRegion1 = new AnalysisRegion()
            {
                Id = Guid.NewGuid(),
                Session = session,
                Frame = frame,
                CellCount = 0, // keep as zero,
                Points = (from p in pts select new SerializiblePoint { X = p.X, Y = p.Y }).ToList(),
            };
            regions.Add(analysisRegion1);

            var pts2 = ScriptProcHelper.CreateEllipsePoints(1259.78342695003, 361.387457293416, 49, 49, 0, 25);
            AnalysisRegion analysisRegion2 = new AnalysisRegion()
            {
                Id = Guid.NewGuid(),
                Session = session,
                Frame = frame,
                CellCount = 0, // keep as zero,
                Points = (from p in pts2 select new SerializiblePoint { X = p.X, Y = p.Y }).ToList(),
            };
            regions.Add(analysisRegion2);
            return regions;
        }

        [TestMethod]
        public void CheckAssayData()
        {
            var dal = GetPCVDAL();

            var scanareas = dal.GetScanAreas(TestSlide).ToArray();
            var scanarea = scanareas.FirstOrDefault();
            var session = scanarea.Sessions.FirstOrDefault();

            var assayData = dal.GetAssayData(session);
            Assert.IsNotNull(assayData);

            //var hackyPathToAiishared = Path.Combine(CaseBaseFolder, @"..\aii_shared"); 
            AssayInfo _assayInfo = new AssayInfo();
            var spotController = SpotConfigController.FromStream(assayData, _assayInfo);

            Assert.IsNotNull(spotController);
            Assert.IsTrue(spotController.ClassList.Count() == 6);
        }

        [TestMethod]
        public void CheckStoredAnnotations()
        {
            var dal = GetPCVDAL();
            var textAnnotation = dal.GetStoredCommonAnnotations();
            Assert.IsNotNull(textAnnotation);
        }

        [TestMethod]
        public void CheckSaveAnnotations()
        {
            var dal = GetPCVDAL();
            var textAnnotation = dal.GetStoredCommonAnnotations();
            Assert.IsNotNull(textAnnotation);

            textAnnotation.Add("Nirvana 1");
            textAnnotation.Add("Nirvana 2");
            textAnnotation.Add("Nirvana 3");
            dal.SaveCommonStoredAnnotation(textAnnotation);
            var savedTexts = dal.GetStoredCommonAnnotations();
            Assert.IsTrue(savedTexts.Count() == 13);
        }

        [TestMethod]
        public void CheckThumbnailRenderer()
        {
            var dal = GetPCVDAL();

            // Need a score to render its thumbnail
            var scanarea = dal.GetScanAreas(TestSlide).FirstOrDefault();

            Assert.IsNotNull(scanarea);

            var session = scanarea.Sessions.FirstOrDefault();

            Assert.IsNotNull(session);

            var scores = dal.GetScores(session);
            var score = scores.Scores.FirstOrDefault();

            Assert.IsNotNull(score);

            // ===== GOT SCORE, BELOW IS NOW RENDER THAT SCORE =======================================

            // we just need a representative frame to get FrameComponents, thunmnail doesn't have these included but
            // are the same as the frames. All frames fro that ScanArea have same components so just need the first
            var representativeFrame = dal.GetFrames(scanarea).FirstOrDefault();

            Assert.IsNotNull(representativeFrame);

            var cachedFrame = dal.GetCachedFrameBitmap(TestSlide.CaseId, TestSlide.Id, scanarea.Id, representativeFrame.Id.ToString());

            ImageTestHelper.DumpPng(cachedFrame, @"E:\DumpTestImages\cachedFrame.png");

            // Get the raw thumbnail data for selected score
            var thumbnailData = dal.GetThumbnailData(session, score);
            Assert.IsNotNull(thumbnailData);


            // having to pass in Components is a limitation in the legacy format but saves space
            var thumbRenderer = new StackedThumbnailRenderer(representativeFrame.Components);

            // renders as a BitmapSource, using the ChannelDisplays to adjust visibility / contrast etc        
            var bitmap = thumbRenderer.Render(thumbnailData, -1, scores.ChannelDisplays, false, false);

            Assert.IsNotNull(bitmap);

            // Will dump it to this file for visual inspection
            ImageTestHelper.DumpPng(bitmap, @"E:\DumpTestImages\firstimage.png");

            // Invert the dapi and dump
            bitmap = thumbRenderer.Render(thumbnailData, -1, scores.ChannelDisplays, false, true);
            ImageTestHelper.DumpPng(bitmap, @"E:\DumpTestImages\firstimageInverted.png");

            // attenuate and dump
            bitmap = thumbRenderer.Render(thumbnailData, -1, scores.ChannelDisplays, true, false);
            ImageTestHelper.DumpPng(bitmap, @"E:\DumpTestImages\firstimageAttenuate.png");

            // switch off counterstain and dump
            Assert.IsTrue(scores.ChannelDisplays.Count() > 0);
            scores.ChannelDisplays.FirstOrDefault().IsVisible = false;
            bitmap = thumbRenderer.Render(thumbnailData, -1, scores.ChannelDisplays, false, false);
            ImageTestHelper.DumpPng(bitmap, @"E:\DumpTestImages\firstimageNoDapi.png");

            // Stretch contrast counterstain and dump
            var channelDisplay = scores.ChannelDisplays.FirstOrDefault();
            channelDisplay.IsVisible = true;
            channelDisplay.HistogramHigh = 0.3;
            channelDisplay.HistogramLow = 0.25;

            bitmap = thumbRenderer.Render(thumbnailData, -1, scores.ChannelDisplays, false, false);
            ImageTestHelper.DumpPng(bitmap, @"E:\DumpTestImages\firstimageStretch.png");
        }

        [TestMethod]
        public void CheckOverlayRenderer()
        {
            var dal = GetPCVDAL();

            // Need a score to render its thumbnail
            var scanarea = dal.GetScanAreas(TestSlide).FirstOrDefault();
            var session = scanarea.Sessions.FirstOrDefault();
            var scores = dal.GetScores(session);
            var score = scores.Scores.FirstOrDefault();

            // ===== GOT SCORE, BELOW IS NOW RENDER OVERLAYS FOR THAT SCORE =======================================

            BitmapSource bitmap = null;

            foreach (var overlay in scores.Overlays)
            {
                var oData = dal.GetOverlayData(session, score, overlay);

                Assert.IsNotNull(oData);

                var color = (Color)ColorConverter.ConvertFromString(overlay.RenderColor); // color not used in Render, need to check
                bitmap = ThumbnailOverlayRenderer.Render(oData, color);
                ImageTestHelper.DumpPng(bitmap, @"E:\DumpTestImages\overlay" + overlay.Name + ".png");
            }

            // check transparency by compositing the overlays; use WPF Grid
            Grid grid = new Grid() { Width = 200, Height = 200 };
            grid.Children.Add(new Border() { Background = Brushes.White });
            foreach (var overlay in scores.Overlays)
            {
                // Add each overlay to the grid
                var oData = dal.GetOverlayData(session, score, overlay);

                bitmap = ThumbnailOverlayRenderer.Render(oData, Colors.Black);
                grid.Children.Add(new Image() { Source = bitmap });
            }

            grid.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            grid.Arrange(new Rect(new Point(0, 0), grid.DesiredSize));

            // utility to render WPF ui element to bitmap, seems quite slow interestingly
            bitmap = ImageTestHelper.ConvertUIToImage(grid, 200, 200);
            ImageTestHelper.DumpPng(bitmap, @"E:\DumpTestImages\overlayComposite.png");
        }

        [TestMethod]
        public void CheckFrameRenderer()
        {
            var dal = GetPCVDAL();

            // Need a score to render its thumbnail
            var scanarea = dal.GetScanAreas(TestSlide).FirstOrDefault();

            Assert.IsNotNull(scanarea);

            var frames = dal.GetFrames(scanarea);

            Assert.IsNotNull(frames);

            FrameRendererGlobalOptions rendererOptions = new FrameRendererGlobalOptions();
            IEnumerable<ComponentLevels> componentLevels = new List<ComponentLevels>();
            foreach (var frame in frames)
            {
                var frameRenderer = new FrameRenderer(frame, rendererOptions.RenderSettings, componentLevels, -1, new FrameRendererCache<ushort>(frame));
                var bitmap = frameRenderer.Render();

                ImageTestHelper.DumpPng(bitmap, @"E:\DumpTestImages\Frames\" + frame.Id.ToString() + ".png");
            }
        }

        internal class Load
        {
            IProbeCaseViewDataAccess _dal;
            Slide _slide;
            public Load(IProbeCaseViewDataAccess dal, Slide slide) { _dal = dal; _slide = slide; }
            public byte[] LoadFrameFromData(string componentId)
            {
                return _dal.GetFrameComponentData(_slide, string.Empty, componentId);
            }
        }
    }

    public class ImageTestHelper
    {
        /// <summary>
        /// Utiltys function for testing
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="p"></param>
        public static void DumpPng(BitmapSource bitmap, string p)
        {
            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(p)))
                    Directory.CreateDirectory(Path.GetDirectoryName(p));

                using (Stream s = File.Create(p))
                {
                    PngBitmapEncoder encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(bitmap));
                    encoder.Save(s);
                    s.Flush();
                }
            }
            catch { }; // for debug so don't bomb if you can't dump
        }

        public static void DumpPng(byte[] bitmapFileData, string p)
        {
            var bm = BitmapImageFromByteArray(bitmapFileData);
            DumpPng(bm, p);
        }

        public static BitmapImage BitmapImageFromByteArray(byte[] array, int decodeWidth = 0)
        {
            using (var stream = new System.IO.MemoryStream(array, false))
            {
                BitmapImage image = new BitmapImage();

                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                if (decodeWidth > 0)
                    image.DecodePixelWidth = decodeWidth;

                image.StreamSource = stream;

                image.EndInit();
                image.Freeze();
                stream.Close();

                return image;
            }
        }


        public static RenderTargetBitmap ConvertUIToImage(FrameworkElement ctrl, int w, int h)
        {
            if (ctrl == null || ctrl.ActualWidth <= double.Epsilon || ctrl.ActualHeight <= double.Epsilon)
            {
                return null;
            }

            RenderTargetBitmap render = new RenderTargetBitmap(w, h, 96, 96, PixelFormats.Pbgra32);

            render.Render(ctrl);
            render.Freeze();

            return render;
        }

    }
}
