﻿using CytoApps.Models;
using PCVImageRenderers.Models;
using ProbeCaseViewDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace ProbeCaseViewDataAccess
{
    public interface IProbeCaseViewDataAccess
    {
        bool Initialize(DataSource dataSource);
        bool CanAnalyseSlide(Slide slide);
        List<ScanArea> GetScanAreas(Slide slide);
        SessionScores GetScores(Session session);
        IEnumerable<Frame> GetFrames(ScanArea scanArea);
        List<string> GetSelectedFramesForSession(Session session);
        byte[] GetThumbnailData(Session session, Score score);
        byte[] GetThumbnailBitmap(Session session, Score score, int stackIndex, IEnumerable<ChannelDisplay> channelDisplay, bool attenuateCounterstain, bool invertCounterstain, bool isTemporarySaved);
        byte[] GetOverlayData(Session session, Score score, Overlay overlay);
        byte[] GetFrameComponentData(Slide slide, string frameId, string componentId);
        byte[] GetFrameBitmap(Slide slide, string scanAreaId, Frame frame, IEnumerable<ComponentLevels> levels, int z, FrameRenderSettings options);
        byte[] GetCachedFrameBitmap(string caseId, string slideId, string scanAreaId, string frameId);
        Score[] GetScoreStats(Session session);

        // Enhanced cell image
        string GetRenderInstructionCellInfo(Session selectedSession, string scoreID);
        string GetEnhancedCellImageInfo(Session selectedSession, string scoreID);
        bool WriteRenderInstructionToEnhancedCell(Session selectedSession, byte[] bmp, string scoreID, XElement renderInstructionForBmp);

        void SaveSession(Session session, List<ScoreSettings> scoreSettingsList, SessionScores scores);
        void ClearTemporarySavedThumb(Session session);
        List<ScoreSettings> LoadScoredCellsInfo(Session session, string scanAreaName);
        bool CanCreateNewSession(string sessionName, Slide slide, string scanAreaName);
        Session CreateNewSession(ScanArea scanArea, string sessionName, bool useSpotScores, bool isCreatedWithSelectedFrames = false, List<string> selectedFrameIdList = null);

        // Returns saved thumbnail bitmap source
        byte[] CreateThumbnailBitmap(ScoreThumbnail scoreThumbnail, Session session);
        void DeleteSession(Session session);
        long GetFrameCellCount(ScanArea scanArea, string frameId);
        void UpdateScores(Session session, List<string> frameIdList);

        byte[] GetSlideOverviewImage(Slide slide, string etchImage);
        List<FrameOutline> GetFrameOutlines(Slide slide);
        // Assay
        byte[] GetAssayData(Session session);
        AssayInfoWrapper GetClassifierNamesAndScripts(Session session = null);
        List<string> GetCompatibleAssays(FrameComponent[] component);
        byte[] GetDefaultAssayData(string assayName);
        void SaveAsDefaultAssay(byte[] AssayData, string assayName);
        List<string> GetAllDefaultAssays();

        void QueueRegionForAnalysis(AnalysisRegion analysisRegion);
        void DeleteRegions(Session session, IEnumerable<string> regions);
        List<Score> GetScoreByRegionAnalysis(IEnumerable<AnalysisRegion> analysisRegions);

        List<AnnotationIdentifier> GetAllFramesAnnotation(Session session, List<Frame> frames);
        /// <summary>
        /// We can make single service call to get all data stored in AII_SharedFolder such as Assay info files , annotation
        /// </summary>
        /// <returns>list of common stored annotation</returns>
        List<string> GetStoredCommonAnnotations();
        void SaveCommonStoredAnnotation(List<string> annotationTexts);
        bool SaveAnnotation(Session session, List<AnnotationIdentifier> frameAnnotations);
        IEnumerable<AnalysisRegion> GetAllAnalysisRegions(Session session);

        void SaveAssayData(Byte[] assayData, Session session);
        byte[] GetAssayDataAndCreateBackup(Session session);
        void RestoreAssayData(Session session);


        LockResult CheckKLockExists(UserDetails userDetails, string caseName, object lockingObject);
        void LockFeatureorObject(UserDetails userDetails, string caseName, object lockingObject);
        void RemoveLock(UserDetails userDetails, string caseName);

        IEnumerable<string> GetSpotFolderNamesForReprocess(Session session, string frameId);
        bool StartReprocessing(Session session, IEnumerable<string> spotFolders);
        ReprocessScores GetScoresFromSpotFoldersProcessed(Session session, IEnumerable<string> spotFolders);
        void RestoreSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions);
        void DeleteNewScoresAndRegionsFromSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions);

    }
}
