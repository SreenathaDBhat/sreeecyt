﻿using PCVImageRenderers.Models;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ProbeCaseViewDataAccess.Models
{
    /// <summary>
    /// AnalysisRegion class is used to save the region details which is drwan on the frame
    /// </summary>
    [DataContract]
    public class AnalysisRegion
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public List<SerializiblePoint> Points { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Session Session { get; set; } //RVCMT-HA - can we use sessionid instead of session object ? 

        [DataMember]
        public Frame Frame { get; set; } //RVCMT-HA - can we use frameid instead of session object ?
        //RCVMT-SB- Can't  we get the height and width of the frame in dataacesslocal as we not saving it. either we need to pass height  and width sepeartely as property or as a parameter

        [DataMember]
        public int CellCount { get; set; }  
    }
}
