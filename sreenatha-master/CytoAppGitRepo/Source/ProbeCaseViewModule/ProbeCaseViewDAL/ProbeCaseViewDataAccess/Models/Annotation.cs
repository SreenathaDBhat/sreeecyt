﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ProbeCaseViewDataAccess.Models
{
    /// <summary>
    /// Text annotation used for description and commenting 
    /// </summary>
    [DataContract]
    public class Annotation
    {
        [DataMember]
        public double X
        {
            get;
            set;
        }

        [DataMember]
        public double Y
        {
            get;
            set;
        }

        [DataMember]
        public double Scale
        {
            get;
            set;
        }

        [DataMember]
        public double ContentOffsetX
        {
            get;
            set;
        }

        [DataMember]
        public double ContentOffsetY
        {
            get;
            set;
        }

        [DataMember]
        public bool ShowArrowLine
        {
            get;
            set;
        }

        [DataMember]
        public string Content
        {
            get;
            set;
        }
    }

    [DataContract]
    public class AnnotationIdentifier
    {
        [DataMember]
        public string Identifier
        {
            get;
            set;
        }

        [DataMember]
        public List<Annotation> Annotations
        {
            get;
            set;
        }
    }
}
