﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ProbeCaseViewDataAccess.Models
{
    [DataContract]
    public class AssayInfoWrapper
    {
        [DataMember]
        public List<string> CellClassifierNames { get; set; }
        [DataMember]
        public string[] SpotScript { get; set; }
        [DataMember]
        public string[] PreScanScript { get; set; }
        [DataMember]
        public string SpotScriptFilePath { get; set; }
        [DataMember]
        public string PreScanScriptFilePath { get; set; }
        [DataMember]
        public string DefaultAssayName { get; set; }

        public AssayInfoWrapper()
        {

        }

        public AssayInfoWrapper(IEnumerable<string> cellClassifierNames, string[] spotScript, string[] preScanScript)
        {
            CellClassifierNames = cellClassifierNames.ToList();
            SpotScript = spotScript;
            PreScanScript = preScanScript;
        }
    }
}
