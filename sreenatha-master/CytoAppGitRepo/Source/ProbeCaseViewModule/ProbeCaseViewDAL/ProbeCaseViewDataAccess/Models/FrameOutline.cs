﻿using PCVImageRenderers.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

namespace ProbeCaseViewDataAccess.Models
{
    [DataContract]
    public class FrameOutline
    {
        [DataMember]
        public double PositionX
        {
            get;
            set;
        }

        [DataMember]
        public double PositionY
        {
            get;
            set;
        }

        [DataMember]
        public double Width
        {
            get;
            set;
        }

        [DataMember]
        public double Height
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public string ThumbnailPath { get; set; }

        public FrameOutline()
        {

        }

        public FrameOutline(XElement scanarea)
        {
            var dim = scanarea.Descendants("Dimensions").FirstOrDefault();
            if (dim != null)
            {
                double w, h, x, y;

                if (double.TryParse(dim.Attribute("X").Value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out x) &&
                    double.TryParse(dim.Attribute("Y").Value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out y) &&
                    double.TryParse(dim.Attribute("W").Value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out w) &&
                    double.TryParse(dim.Attribute("H").Value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out h))
                {
                    PositionX = x;
                    PositionY = y;
                    Width = w;
                    Height = h;
                    var image = scanarea.Descendants("Image").FirstOrDefault();
                    if (image != null)
                        ThumbnailPath = image.Attribute("File").Value;
                };
            }
        }
    }
}
