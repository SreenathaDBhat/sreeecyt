﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProbeCaseViewDataAccess.Models
{
    [DataContract]
    public class Measurement
    {
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public string OriginalName { get; set; }

        public string DisplayName { get { return OriginalName.Substring(5) + ": " + Value; } }

        public string SignalName { get { return OriginalName.Substring(5); } }

    
        public string UnescapedName { get { return SignalName.Replace("_", " "); } }

    }
}
