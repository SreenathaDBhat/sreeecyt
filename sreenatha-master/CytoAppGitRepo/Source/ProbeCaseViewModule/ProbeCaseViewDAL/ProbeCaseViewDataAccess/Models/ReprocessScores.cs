﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ProbeCaseViewDataAccess.Models
{
    /// RVCMT-REPROCESS - spell check on method comments
    /// <summary>
    /// This class is used to get the scores and spot folders for reproecessing
    /// </summary>
    [DataContract]
    public class ReprocessScores
    {
        [DataMember]
        public List<Score> Scores { get; set; }

        [DataMember]
        public List<string> SpotFolders { get; set; } /// RVCMT-REPROCESS - name can be changed to PendingSpotFolders here only so that it is clear that Scores here does not belong to the spotfolder mentioned here

    }
}
