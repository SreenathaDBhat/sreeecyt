﻿using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProbeCaseViewDataAccess.Models
{
    [DataContract]
    public class ScanArea
    {
        public ScanArea()
        {
        }

        public ScanArea(Slide slide, string name, string id)
        {
            Slide = slide;
            Name = name;
            Id = id;
        }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public Slide Slide { get; set; }
        [DataMember]
        public bool HasSpotScores { get; set; }
        [DataMember]
        public List<Session> Sessions { get; set; }

    }
}
