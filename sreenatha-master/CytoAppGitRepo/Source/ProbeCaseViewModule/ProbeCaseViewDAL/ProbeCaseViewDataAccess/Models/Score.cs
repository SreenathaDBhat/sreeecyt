﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProbeCaseViewDataAccess.Models
{
    [DataContract]
    public class Score
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid RegionId { get; set; }
        [DataMember]
        public Guid ImageFrameId { get; set; }
        [DataMember]
        public string ColourString { get; set; }
        [DataMember]
        public string Class { get; set; }
        [DataMember]
        public double FrameX { get; set; }
        [DataMember]
        public double FrameY { get; set; }
        [DataMember]
        public double X { get; set; }
        [DataMember]
        public double Y { get; set; }
        [DataMember]
        public bool Reviewed { get; set; }
        [DataMember]
        public List<Measurement> SpotMeasurements { get; set; }
    }
}
