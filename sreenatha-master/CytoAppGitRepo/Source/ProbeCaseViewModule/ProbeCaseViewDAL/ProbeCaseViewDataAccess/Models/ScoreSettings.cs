﻿using System;

namespace ProbeCaseViewDataAccess.Models
{
    public class ScoreSettings
    {
        public Guid scoreid
        {
            get;
            set;
        }

        public bool IsReviewed
        {
            get;
            set;
        }

        public bool IsEnhanced
        {
            get;
            set;
        }

        public bool IsRescored
        {
            get;
            set;
        }

        public bool IsDeleted
        {
            get;
            set;
        }

        public bool IsNewlyCreated
        {
            get;
            set;
        }
    }
}
