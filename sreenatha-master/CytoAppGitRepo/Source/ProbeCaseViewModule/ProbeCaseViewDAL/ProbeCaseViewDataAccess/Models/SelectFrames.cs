﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace ProbeCaseViewDataAccess.Models
{
    /// <summary>
    /// Class defination for selected frames to be stored in .selectframes file.
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot(STR_Slide)]
    public class SelectFrames
    {
        private const string STR_Slide = "Slide";
        private const string STR_Name = "Name";
        private const string STR_SelectedScanArea = "SelectedScanArea";
        private const string STR_SessionName = "SessionName";
        private const string STR_SelectedFrame = "SelectedFrame";

        [DataMember]
        [XmlAttribute(STR_Name)]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        [XmlAttribute(STR_SelectedScanArea)]
        public string SelectedScanArea
        {
            get;
            set;
        }

        [DataMember]
        [XmlAttribute(STR_SessionName)]
        public string SessionName
        {
            get;
            set;
        }

        [DataMember]
        [XmlElement(STR_SelectedFrame)]
        public List<string> SelectedFrameList = new List<string>();
    }
}
