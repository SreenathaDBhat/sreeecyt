﻿using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProbeCaseViewDataAccess.Models
{
    [DataContract]
    public class Session
    {
        public Session()
        {
        }

        public Session(Slide slide, string name, string id, string scanAreaId, bool isCreatedWithSelectedFrames = false)
        {
            Slide = slide;
            Name = name;
            Id = id;
            ScanAreaId = scanAreaId;
            IsCreatedWithSelectedFrames = isCreatedWithSelectedFrames;
        }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string ScanAreaId { get; set; }
        [DataMember]
        public Slide Slide { get; set; }
        [DataMember]
        public bool IsCreatedWithSelectedFrames { get; set; }
        [DataMember]
        public string AssayName { get; set; }
    }
}
