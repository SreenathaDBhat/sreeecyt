﻿using PCVImageRenderers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProbeCaseViewDataAccess.Models
{
    [DataContract]
    public class SessionScores
    {
        [DataMember]
        public Session Session { get; set; }
        [DataMember]
        public List<Score> Scores { get; set; }
        [DataMember]
        public List<ChannelDisplay> ChannelDisplays { get; set; }
        [DataMember]
        public List<Overlay> Overlays { get; set; }
        [DataMember]
        public bool AttenuateCounterstain { get; set; }
        [DataMember]
        public bool InvertCounterstain { get; set; }
        [DataMember]
        public int GridSize { get; set; }
    }
}
