﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;


namespace ProbeCaseViewDataAccessLocal
{
    public static class Parsing
    {
        public static int TryParseInt(string str, int fallback)
        {
            int converted = 0;
            if (int.TryParse(str, out converted))
                return converted;
            else
                return fallback;
        }

        public static double TryParseDouble(string str, double fallback,
                                            NumberStyles style = NumberStyles.Float | NumberStyles.AllowThousands,
                                            IFormatProvider provider = null)
        {
            double converted = 0;
            // Default style and provider should be same as used by TryParse(string, double).
            if (double.TryParse(str, style, provider != null ? provider : NumberFormatInfo.CurrentInfo, out converted))
                return converted;
            else
                return fallback;
        }

        public static bool TryParseBool(string str, bool fallback)
        {
            bool converted = false;
            if (bool.TryParse(str, out converted))
                return converted;
            else
                return fallback;
        }


        public static bool TryParseDoubleMultilingual(string s, out double result)
        {
            // First try parsing the number using the invariant culture, which uses a dot as
            // the decimal separator. This is the usual format for numbers stored in files.
            // If this fails, it may be because the number has been formatted in a culture
            // that uses a comma as the decimal separator (e.g. French), so try parsing it
            // in that format. Don't try parsing it in the current culture too, as that will
            // use one or other of these formats.
            // It is important that NumberStyles.Float is specified, as this doesn't
            // include NumberStyles.AllowThousands, which for some cultures would make the
            // parser wrongly interpret the thousands separator as the decimal separator,
            // or vice versa, resulting in the wrong number being returned.
            // E.g. "12,3" when parsed in the invariant culture would return 123, rather
            // than failing.
            if (double.TryParse(s, NumberStyles.Float, CultureInfo.InvariantCulture, out result))
                return true;
            else
            {
                NumberFormatInfo nfi = new NumberFormatInfo();
                nfi.NumberDecimalSeparator = ",";
                if (double.TryParse(s, NumberStyles.Float, nfi, out result))
                    return true;
                else
                    return false;
            }
        }


        public static string ValueOrDefault(XAttribute attribute, string def)
        {
            if (attribute == null)
                return def;
            else
                return attribute.Value;
        }

        public static string ValueOrDefault(XElement xElement, string def)
        {
            if (xElement == null)
                return def;
            else
                return xElement.Value;
        }

        public static bool ValueOrDefault(XAttribute attribute, bool def)
        {
            if (attribute == null)
                return def;
            else
                return (bool)attribute;
        }

        public static bool ValueOrDefault(XElement xElement, bool def)
        {
            if (xElement == null)
                return def;
            else
                return (bool)xElement;
        }
    }
}
