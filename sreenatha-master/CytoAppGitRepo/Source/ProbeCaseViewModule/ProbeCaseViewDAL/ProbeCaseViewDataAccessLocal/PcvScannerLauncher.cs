﻿using System.Diagnostics;
using System.IO;
using System.Globalization;
using System;

namespace ProbeCaseViewDataAccessLocal
{
    /// <summary>
    /// PcvScannerController - Launches PcvScanner.exe process with appropriate cmdline
    /// </summary>
    public class PcvScannerController
    {
        private PcvScannerLauncher pcvScanner;
        private string _casebaseRoot; // e.g \\localhost\cases
        private string _caseName;
        private string _slideName;
        private string _scanAreaName;
        private string _assayName;
        private double _objective, _frameXScale, _frameYScale;

        public PcvScannerController(string casebaseRoot, string caseName, string slideName, string scanAreaName, string assayName, double objective, double frameXScale, double frameYScale)
        {
            _casebaseRoot = casebaseRoot;
            _caseName = caseName;
            _slideName = slideName;
            _scanAreaName = scanAreaName;
            _assayName = assayName;
            _objective = objective;
            _frameXScale = frameXScale;
            _frameYScale = frameYScale;
        }

        public void StartPcvScanner()
        {
            pcvScanner = new PcvScannerLauncher(_casebaseRoot, _caseName, _slideName, _scanAreaName,
                                                  _assayName, _objective, _frameXScale, _frameYScale, _scanAreaName);
            pcvScanner.Launch();
        }
    }
 
 
    /// <summary>
    /// Launches the pcvscanner process with appropriate cmdline parameters
    /// NOTE: pcvScanner will run for about 5 minutes in idle state then exit but will extend life if new scores added
    /// lifetime control is by the pcvscanner process itself based on ScanAreaId. Only one process per ScanAreaId
    /// </summary>
    public class PcvScannerLauncher
    {
        private Process pcvscanner;
        private string scanAreaId, rootPath, caseName, slideName, scanArea, assayName, imageFramesName;
        private double objective, frameXScale, frameYScale;
        private int autospotCounting = 0;
        private int reprocess = 0;

        public PcvScannerLauncher(string casebaseRoot, string caseName, string slideName, string scanArea, string assayName, double objective, double frameXScale, double frameYScale, string imageFramesName)
        {
            scanAreaId = GetSessionId(caseName,slideName,assayName); 
            rootPath = CasebaseFolderParent(casebaseRoot); // e.g. \\localhost\cases -> \\MyComputerName
            this.caseName = caseName;
            this.slideName = slideName;
            this.scanArea = scanArea.Replace(" ", "%");
            this.assayName = assayName.Replace(" ", "%");
            this.objective = objective;
            this.frameXScale = frameXScale;
            this.frameYScale = frameYScale;
            this.imageFramesName = imageFramesName.Replace(" ", "%");
        }

        /// <summary>
        /// sessionId for identifying if another pcvscanner is already processiing this scan area / session
        /// </summary>
        public static string GetSessionId(string caseName, string slideName, string assayName)
        {
            return caseName + "_" + slideName + "_" + assayName.Replace(" ", "-"); // for identifying if another pcvscanner is already processiing this scan area
        }

        private string CasebaseFolderParent(string casebaseFolder)
        {
            // Trim off cases folder to get parent
            const string cases = @"\cases";
            var parent = casebaseFolder.Remove(casebaseFolder.Length - cases.Length);

            // pcvscanner code can't cope with localhost, so handle that here
            parent = parent.Replace(@"\\localhost", @"\\" + Environment.MachineName);

            return parent;
        }

        private string CommandLine
        {
            get
            {
                // Note that floating point numbers are formatted in this string using the invariant culture,
                // so that when running in French, for example, the decimal separator does not become a comma.
                // This is because cvscanner is culture invariant and can only cope with numbers in this format.
                return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12}",
                                     scanAreaId, rootPath,
                                     assayName, scanArea, caseName, slideName,
                                     objective.ToString(CultureInfo.InvariantCulture), autospotCounting, reprocess,
                                     frameXScale.ToString(CultureInfo.InvariantCulture), frameYScale.ToString(CultureInfo.InvariantCulture),
                                     Path.GetFileName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName),
                                     imageFramesName);
            }
        }

        public void Launch()
        {
            Process process = new Process();

            // Path to exe is path to directory current exe is running from, as all our exes are in same folder.
            // Don't use a fixed path, so that the app folder can be put anywhere.
#if DEBUG
            process.StartInfo.FileName = Path.Combine(Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName), @"..\..\source\pcvscanner\buildoutput\debug\pcvscanner.exe");
#else
            //REVISIT //RVCMT-HA - TEMPORARY
            //Manually Copy the release folder to Aperio Dataserver folder in Remote. and rename to pcvscanner
            //Manually Copy the release folder to CytoApps installed path and and rename to pcvscanner
            process.StartInfo.FileName = Path.Combine(Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName), @"pcvscanner\pcvscanner.exe");
            //process.StartInfo.FileName = Path.Combine(Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName), "pcvscanner.exe");
#endif
            if (File.Exists(process.StartInfo.FileName))
            {
                process.StartInfo.Arguments = CommandLine;
                process.StartInfo.UseShellExecute = false;
                process.Start();
                pcvscanner = process;
            }
        }
    }
}
