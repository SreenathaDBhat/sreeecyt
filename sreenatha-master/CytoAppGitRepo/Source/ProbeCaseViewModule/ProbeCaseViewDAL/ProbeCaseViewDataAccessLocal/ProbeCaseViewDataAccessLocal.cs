﻿using ProbeCaseViewDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using CytoApps.Models;
using ProbeCaseViewDataAccess.Models;
using System.IO;
using System.Xml.Linq;
using System.Globalization;
using PCVImageRenderers.Models;
using System.Threading.Tasks;
using PCVImageRenderers;
using CytoApps.Exceptions;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using System.Reflection;
using CytoApps.EntityModel;

namespace ProbeCaseViewDataAccessLocal
{
    [DalTypeId(Name = "Local Casebase")]
    public class ProbeCaseViewDataAccessLocal : IProbeCaseViewDataAccess, ILocking
    {
        private string _casebaseFolder;
        private string _aii_SharedPath;
        private const string TempSessionFolder = "TempSessionFolder";
        private FrameRendererCache<ushort> frameRendererCache;
        private List<string> sessionZeroScoreIDs = null;
        private Chromoscan2Entities _db;
        private IntializeCytoAppsEntityModel _entityModel;
        private const string ModuleType = "probe";

        public ProbeCaseViewDataAccessLocal()
        {
        }

        public ProbeCaseViewDataAccessLocal(string casebaseFolder, string connectionString = null)
        {
            try
            {
                _casebaseFolder = casebaseFolder.ToLower(); // to make the casebase folder case insensitive. the below replace will fail if the casebasefolder string comes with "Cases", "caSEs" etc;
                _aii_SharedPath = casebaseFolder.Replace("cases", "AII_Shared");
                if (connectionString != null)
                {
                    _entityModel = new IntializeCytoAppsEntityModel();
                    _db = _entityModel.GetCurrentDataBase(connectionString);
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ex.Source, ex.Message);
            }
        }

        public bool Initialize(DataSource dataSource)
        {
            _casebaseFolder = dataSource.ConnectionParameter[1].Value.ToLower();
            _aii_SharedPath = _casebaseFolder.Replace("cases", "AII_Shared");
            _entityModel = new IntializeCytoAppsEntityModel();
            _db = _entityModel.GetCurrentDataBase(dataSource.ConnectionParameter[0].Value);
            return true; //TODO, validate
        }

        #region Private Methods
        private string GetSessionNameFromScore(string scoresfile)
        {
            var locator = new SessionFilesLocator(scoresfile);
            return locator.SessionName;
        }

        private Score ParseScoreFromXml(XElement score)
        {
            double d = 0;
            Score probeScore = new Score();

            var guidString = Parsing.ValueOrDefault(score.Attribute("Id"), string.Empty);
            probeScore.Id = guidString == string.Empty ? Guid.NewGuid() : new Guid(score.Attribute("Id").Value);

            double.TryParse(score.Attribute("FrameX").Value, NumberStyles.Float, CultureInfo.InvariantCulture, out d);
            probeScore.X = d;
            double.TryParse(score.Attribute("FrameY").Value, NumberStyles.Float, CultureInfo.InvariantCulture, out d);
            probeScore.Y = d;
            probeScore.Class = score.Attribute("Class").Value;

            probeScore.ColourString = Parsing.ValueOrDefault(score.Attribute("Color"), "#ffffff");
            var reviewedString = Parsing.ValueOrDefault(score.Attribute("Reviewed"), string.Empty);
            probeScore.Reviewed = reviewedString == string.Empty ? false : Parsing.TryParseBool(reviewedString, false);
            probeScore.RegionId = new Guid(Parsing.ValueOrDefault(score.Attribute("RegionId"), Guid.Empty.ToString()));

            var imageFrameId = Parsing.ValueOrDefault(score.Attribute("ImageFrameId"), string.Empty);
            probeScore.ImageFrameId = imageFrameId == string.Empty ? Guid.NewGuid() : new Guid(score.Attribute("ImageFrameId").Value);

            probeScore.SpotMeasurements = ParseSpotMeasurements(score).ToList();

            return probeScore;
        }

        private IEnumerable<Measurement> ParseSpotMeasurements(XElement score)
        {
            var parsedMeasurements = from a in score.Attributes()
                                     where a.Name.LocalName.StartsWith("MEAS_")
                                     select new Measurement
                                     {
                                         OriginalName = a.Name.LocalName,
                                         Value = a.Value
                                     };

            return parsedMeasurements.ToArray();
        }

        private ChannelDisplay CreateChannelDisplay(XElement channel)
        {
            double hi, lo;

            var name = channel.Attribute("Name").Value;

            if (!double.TryParse(channel.Attribute("L").Value, out lo))
                lo = 0;

            if (!double.TryParse(channel.Attribute("H").Value, out hi))
                hi = 1.0;

            return new ChannelDisplay { HistogramHigh = hi, HistogramLow = lo, Name = name };
        }

        //MARV- Please remove unused code
        private string GetSlidePath(Slide slide)
        {
            //MARV- Why?
            string s = Path.Combine(_casebaseFolder, slide.CaseId, slide.Id);

            return Path.Combine(_casebaseFolder, slide.CaseId, slide.Id);
        }

        private string GetSlidePath(string caseId, string slideId)
        {
            return Path.Combine(_casebaseFolder, caseId, slideId);
        }

        private bool IsSelectFramesFileExists(Slide slide, string scanAreaId, string sessionName)
        {
            string selectframesFilePath = Path.Combine(GetSlidePath(slide), scanAreaId) + "$=" + sessionName + ".selectframes";
            if (File.Exists(selectframesFilePath))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private Annotation AnnotationFromNode(XElement annotationXml)
        {
            Annotation a = new Annotation();

            a.X = double.Parse(Parsing.ValueOrDefault(annotationXml.Attribute("X"), "0"), CultureInfo.InvariantCulture);
            a.Y = double.Parse(Parsing.ValueOrDefault(annotationXml.Attribute("Y"), "0"), CultureInfo.InvariantCulture);
            a.ContentOffsetX = double.Parse(Parsing.ValueOrDefault(annotationXml.Attribute("ContentOffsetX"), "20"), CultureInfo.InvariantCulture);
            a.ContentOffsetY = double.Parse(Parsing.ValueOrDefault(annotationXml.Attribute("ContentOffsetY"), "20"), CultureInfo.InvariantCulture);
            a.Scale = double.Parse(Parsing.ValueOrDefault(annotationXml.Attribute("Scale"), "1"), CultureInfo.InvariantCulture);
            a.ShowArrowLine = bool.Parse(Parsing.ValueOrDefault(annotationXml.Attribute("ShowArrowLine"), "true"));
            a.Content = annotationXml.Value;
            return a;
        }

        public string AnnotationXmlPath(Session session, string frameId)
        {
            string annotationPath = Path.Combine(GetSlidePath(session.Slide), frameId + ".annotations" + session.Name);
            return annotationPath;
        }
        #endregion

        #region Interface methods
        public bool CanAnalyseSlide(Slide slide)
        {
            var slideFolder = GetSlidePath(slide);
            if (!Directory.Exists(slideFolder))
                return false;

            return Directory.EnumerateFileSystemEntries(slideFolder, "*" + SessionFilesLocator.ImageFramesExtension).Count() > 0;
        }

        public List<ScanArea> GetScanAreas(Slide slide)
        {
            var slideFolder = GetSlidePath(slide);
            if (!Directory.Exists(slideFolder))
                return new List<ScanArea>();

            var imageframesFiles = Directory.EnumerateFileSystemEntries(slideFolder, "*" + SessionFilesLocator.ImageFramesExtension).ToArray();

            var scanareas = (from s in imageframesFiles
                             select new ScanArea(slide, Path.GetFileNameWithoutExtension(s), Path.GetFileNameWithoutExtension(s))).ToList();

            // for each scan area, also determine what sessions exists (related .scores files)
            foreach (var scanarea in scanareas)
            {
                var scoringSessionFiles = Directory.EnumerateFiles(slideFolder, scanarea.Name + SessionFilesLocator.SessionDelimiter + "*.scores");
                var list = new List<Session>();

                Parallel.ForEach(scoringSessionFiles, (file) =>
                {
                    //Dual Score - Check if session is created with selected frames
                    bool isCreatedWithSelectedFrames = IsSelectFramesFileExists(slide, scanarea.Id, GetSessionNameFromScore(file));
                    var session = new Session(slide, GetSessionNameFromScore(file), GetSessionNameFromScore(file), scanarea.Id, isCreatedWithSelectedFrames);
                    // Get Assay Name it is needed to check if assays in both the session is same or different.
                    session.AssayName = GetDefaultAssayName(session);
                    list.Add(session);
                });
                scanarea.Sessions = list;
                scanarea.HasSpotScores = CheckSessionWithSpotScores(scanarea);
            }

            return scanareas;
        }

        public SessionScores GetScores(Session session)
        {
            var scores = new Score[0];
            var channelDisplays = new ChannelDisplay[0];
            var gridSize = 200;
            var attenuateCounterstain = false;

            var sessionScoreFilePath = Path.Combine(GetSlidePath(session.Slide), SessionFilesLocator.GetScoresFileName(session.ScanAreaId, session.Id));

            if (File.Exists(sessionScoreFilePath))
            {
                var xml = XElement.Load(sessionScoreFilePath);
                scores = (from cellNode in xml.Descendants("ScoredCell")
                          select ParseScoreFromXml(cellNode)).ToArray();

                var display = xml.Element("Display");
                if (display != null)
                {
                    channelDisplays = (from channel in display.Descendants("Channel")
                                       select CreateChannelDisplay(channel)).ToArray();

                    var displayOptionsXml = display.Descendants("DisplayOptions").FirstOrDefault();
                    if (displayOptionsXml != null)
                    {
                        gridSize = Parsing.TryParseInt(Parsing.ValueOrDefault(displayOptionsXml.Attribute("GridSize"), "200"), 200);
                        attenuateCounterstain = Parsing.TryParseBool(Parsing.ValueOrDefault(displayOptionsXml.Attribute("AttenuateCounterstain"), "false"), false);
                    }
                }

                // There is other stuff in the display section of the.scores file that we write but don't seem to read; Review !!!
            }

            var sessionLocator = new SessionFilesLocator(sessionScoreFilePath);

            List<Overlay> overlays = new List<Overlay>();
            if (sessionLocator.OverlaysFile.Exists)
            {

                XElement xml = XElement.Load(sessionLocator.OverlaysFile.FullName);
                foreach (var o in xml.Descendants())
                {
                    overlays.Add(new Overlay()
                    {
                        ID = o.Attribute("Id").Value,
                        Name = o.Attribute("Name").Value,
                        RenderColor = Parsing.ValueOrDefault(o.Attribute("RenderColor"), "#ffffff")
                    });
                }
            }

            return new SessionScores()
            {
                Session = session,
                Scores = scores.ToList(),
                Overlays = overlays,
                ChannelDisplays = channelDisplays.ToList(),
                AttenuateCounterstain = attenuateCounterstain,
                GridSize = gridSize
            };
        }

        public IEnumerable<Frame> GetFrames(ScanArea scanArea)
        {
            var slideFolder = GetSlidePath(scanArea.Slide);

            var imageFramesFilename = Path.Combine(slideFolder, scanArea.Id + ".imageframes");
            if (File.Exists(imageFramesFilename))
                return Frame.LoadFromImageFramesXml(imageFramesFilename).ToArray();
            else
                return new Frame[0];
        }

        public byte[] GetThumbnailData(Session session, Score score)
        {
            var slideFolder = GetSlidePath(session.Slide);
            var blbFilename = Path.Combine(slideFolder, score.Id + ".thumb.blb");
            if (!File.Exists(blbFilename))
                return null;

            return (File.ReadAllBytes(blbFilename));
        }

        public byte[] GetThumbnailBitmap(Session session, Score score, int stackIndex, IEnumerable<ChannelDisplay> channelDisplay, bool attenuateCounterstain, bool invertCounterstain, bool isTemporarySaved)
        {
            var scanArea = new ScanArea() { Slide = session.Slide, Id = session.ScanAreaId };
            var representativeFrame = GetFrames(scanArea).FirstOrDefault();
            var renderer = new StackedThumbnailRenderer(representativeFrame.Components);
            if (!isTemporarySaved)
            {
                var slideFolder = GetSlidePath(session.Slide);
                var blbFilename = Path.Combine(slideFolder, score.Id + ".thumb.blb");
                if (!File.Exists(blbFilename))
                    return null;
                var thumbnailData = File.ReadAllBytes(blbFilename);
                var renderedBitmap = renderer.RenderToBitmapData(thumbnailData, stackIndex, channelDisplay, attenuateCounterstain, invertCounterstain);
                return (renderedBitmap);
            }
            else
            {
                if (!File.Exists(Path.Combine(TempDirectory(session), score.Id + ".thumb.blb")))
                    return null;
                var thumbnailData = File.ReadAllBytes(Path.Combine(TempDirectory(session), score.Id + ".thumb.blb"));
                var renderedBitmap = renderer.RenderToBitmapData(thumbnailData, stackIndex, channelDisplay, attenuateCounterstain, invertCounterstain);
                return (renderedBitmap);
            }
        }

        public byte[] GetFrameComponentData(Slide slide, string frameId, string componentId)
        {
            var slideFolder = GetSlidePath(slide);

            var filename = Path.Combine(slideFolder, componentId);
            if (!File.Exists(filename))
                return null;

            return (File.ReadAllBytes(filename));
        }

        private void LoadComponentIntoCache(Slide slide, Frame frame, FrameComponent component, int z, FrameRendererCache<ushort> cache, out int width, out int height, out int bpp)
        {
            var componentFileName = component.ImageForZ(z);

            var componentEncodedBytes = GetFrameComponentData(slide, frame.Id.ToString(), componentFileName);

            var componentImageBytes = FrameLoad.Load(componentEncodedBytes, componentFileName, out width, out height, out bpp);

            cache.AddToCache(componentImageBytes, component, z);
        }

        public byte[] GetFrameBitmap(Slide slide, string scanAreaId, Frame frame, IEnumerable<ComponentLevels> levels, int z, FrameRenderSettings settings)
        {
            // First ensure that the image data required by the renderer is available, by ensuring that all visible channels
            // are loaded into the cache. The renderer will then be able to access the channels it needs from the cache.
            // If the same frame was used with this cache last time, some of its channels may already be loaded.
            if (frameRendererCache != null)
                frameRendererCache.Frame = frame; // This will reset the cache if the Frame doesn't match the previously cached one.
            else
                frameRendererCache = new FrameRendererCache<ushort>(frame);

            if (settings.MaximumProjection) // In the cache, the maximum projection image is stored with a z value of -1.
                z = -1;

            foreach (var currLevels in levels) // Don't do this loop in parallel - faster to read one file at a time from a local disk.
            {
                if (currLevels.IsVisible && frameRendererCache.Retrieve(currLevels.Component, z) == null)
                {
                    int width, height, bpp;

                    LoadComponentIntoCache(slide, frame, currLevels.Component, z, frameRendererCache, out width, out height, out bpp);

                    frame.SetSizes(width, height, bpp); // Ensure that the frame knows its dimensions.
                }
            }

            var renderer = new FrameRenderer(frame, settings, levels, z, frameRendererCache);

            return renderer.RenderToBitmapData();
        }

        public byte[] GetOverlayData(Session session, Score score, Overlay overlay)
        {
            var slideFolder = GetSlidePath(session.Slide);

            var blbFilename = Path.Combine(slideFolder, score.Id + "." + overlay.ID + ".png");
            if (!File.Exists(blbFilename))
                return null;

            return (File.ReadAllBytes(blbFilename));
        }

        #region Assay & Spot Configuration
        /// <summary>
        /// returns helper class to locate various session files
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        private SessionFilesLocator GetLocator(Session session)
        {
            var sessionScoreFilePath = Path.Combine(GetSlidePath(session.Slide), SessionFilesLocator.GetScoresFileName(session.ScanAreaId, session.Id));
            return new SessionFilesLocator(sessionScoreFilePath);
        }

        /// <summary>
        /// Return the assay file contents as a byte array
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public byte[] GetAssayData(Session session)
        {
            var sessionLocator = GetLocator(session);

            if (!sessionLocator.AssayFile.Exists)
                return null;
            return (File.ReadAllBytes(sessionLocator.AssayFile.FullName));
        }


        /// <summary>
        /// Get the assay file and create the backup for restore it on cancel
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public byte[] GetAssayDataAndCreateBackup(Session session)
        {
            string GridBckExt = ".bck";
            var sessionLocator = GetLocator(session);
            if (!sessionLocator.AssayFile.Exists)
            {
                return null;
            }
            else
            {
                var path = new FileInfo(sessionLocator.AssayFile.FullName + GridBckExt);
                if (path.Exists)
                {
                    path.Delete();
                }
                sessionLocator.AssayFile.CopyTo(sessionLocator.AssayFile.FullName + GridBckExt);
            }
            return (File.ReadAllBytes(sessionLocator.AssayFile.FullName));
        }

        public void RestoreAssayData(Session session)
        {
            string GridBckExt = ".bck";
            var sessionLocator = GetLocator(session);
            if (sessionLocator.AssayFile.Exists)
            {
                var path = new FileInfo(sessionLocator.AssayFile.FullName + GridBckExt);
                if (path.Exists)
                {
                    path.CopyTo(sessionLocator.AssayFile.FullName, true);
                    // remove the back ups
                    path.Delete();
                }
            }
            RequestEndImageProcessing(session);
        }

        private void DeleteBackupAssayFile(Session session)
        {
            string GridBckExt = ".bck";
            var sessionLocator = GetLocator(session);
            var path = new FileInfo(sessionLocator.AssayFile.FullName + GridBckExt);
            if (path.Exists)
            {
                // remove the back ups
                path.Delete();
            }
        }
        public AssayInfoWrapper GetClassifierNamesAndScripts(Session session = null)
        {
            string _spotScriptFilePath = string.Empty;
            string _preScanScriptFilePath = string.Empty;
            var _assayInfo = new AssayInfoWrapper(GetCellClassifierNames(), GetSpotScript(out _spotScriptFilePath), GetPreScanScript(out _preScanScriptFilePath));
            _assayInfo.SpotScriptFilePath = _spotScriptFilePath;
            _assayInfo.PreScanScriptFilePath = _preScanScriptFilePath;
            if (session != null)
                _assayInfo.DefaultAssayName = GetDefaultAssayName(session);
            return _assayInfo;
        }

        private IEnumerable<string> GetCellClassifierNames()
        {
            List<string> names = new List<string>();
            // There is always an 'Everything' option - there is no corresponding 'Everything'
            // classifier file, it just means let everything through the cell classification step.
            names.Add("Everything");
            if (Directory.Exists(_aii_SharedPath))
            {
                string spotCellPath = Path.Combine(_aii_SharedPath, "Classifiers", "SpotCell");

                if (Directory.Exists(spotCellPath))
                {
                    DirectoryInfo spotCellPathInfo = new DirectoryInfo(spotCellPath);
                    foreach (FileInfo modelFile in spotCellPathInfo.GetFiles("*.model"))
                    {
                        names.Add(Path.GetFileNameWithoutExtension(modelFile.Name));
                    }
                }
            }

            // Sort alphabetically by slide name, to match the order in the CytoVision navigator.
            return names.OrderBy(s => s);
        }

        private string[] GetSpotScript(out string spotScriptFilePath)
        {
            spotScriptFilePath = Path.Combine(_aii_SharedPath, "Scripts", "Spot") + @"\SpotHiMagCV72.script";
            return File.Exists(spotScriptFilePath) ? File.ReadAllLines(spotScriptFilePath) : null;
        }

        private string[] GetPreScanScript(out string preScanScriptFilePath)
        {
            preScanScriptFilePath = Path.Combine(_aii_SharedPath, "Scripts", "Spot") + @"\interphasefind.script";
            return File.Exists(preScanScriptFilePath) ? File.ReadAllLines(preScanScriptFilePath) : null;
        }

        public void SaveAssayData(Byte[] assayData, Session session)
        {
            var sessionLocator = GetLocator(session);
            File.WriteAllBytes(sessionLocator.AssayFile.FullName, assayData);
            // Request pcvscanner to exit
            RequestEndImageProcessing(session);
        }

        private void RequestEndImageProcessing(Session session)
        {
            var sessionLocator = GetLocator(session);

            // create flag file to exit IP
            using (sessionLocator.RequestEndFile.Create())
            {

            }
        }

        public byte[] GetSpotScriptData(string scriptName)
        {
            // HACK this may be a bad assumption but is aiishared always cases\..\aiishared????
            var scriptPath = Path.Combine(_casebaseFolder, @"..\aii_shared\scripts\spot", scriptName);
            if (!File.Exists(scriptPath))
                return null;

            return (File.ReadAllBytes(scriptPath));
        }

        ///<summary>
        ///Get the comapatible assays
        /// </summary>
        public List<string> GetCompatibleAssays(FrameComponent[] component)
        {
            List<string> availableAssays = new List<string>();
            string defaultassayPath = "Assays\\Spot\\";
            string SpotAssaysPath = Path.Combine(_aii_SharedPath, defaultassayPath);
            if (component != null)
            {
                try
                {
                    var allAssays = Directory.EnumerateFiles(SpotAssaysPath, "*.assay");
                    foreach (var a in allAssays)
                    {
                        if (IsCEPXYAssayName(Path.GetFileNameWithoutExtension(a)))
                            continue;

                        if (IsCompatibleAssay(a, component))
                            availableAssays.Add(Path.GetFileNameWithoutExtension(a));
                    }
                }
                catch (DALException ex)
                {
                    throw new DALException("Get Compatible Assays  Error", ex, DALException.DALErrorType.Authorization);
                }
            }

            return availableAssays;
        }

        /// <summary>
        /// Get the all the file names in default assay folder
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllDefaultAssays()
        {
            List<string> availebleAssays = new List<string>();
            string defaultassayPath = "Assays\\Spot\\";
            string SpotAssaysPath = Path.Combine(_aii_SharedPath, defaultassayPath);
            var allAssays = Directory.EnumerateFiles(SpotAssaysPath, "*.assay");
            foreach (var a in allAssays)
            {
                availebleAssays.Add(Path.GetFileNameWithoutExtension(a));
            }
            return availebleAssays;
        }

        public bool IsCompatibleAssay(string path, FrameComponent[] fluoros)
        {
            bool ret = false;
            var enumerableNames = GetFluorochromes(path);
            var fl = enumerableNames != null ? enumerableNames.ToArray() : null;

            // Assay has to have same number of fluorochromes, with same names (not case sensitive, as for manual capture
            // names come from a different list that may have different casing), but can be in different order.
            if (fl != null && fluoros != null && fluoros.Count() == fl.Count())
            {
                ret = true;
                foreach (var f in fluoros)
                {
                    if (fl.Where(a => a.Equals(f.Name, StringComparison.OrdinalIgnoreCase)).Count() != 1)
                        return false;
                }
            }

            return ret;
        }

        private IEnumerable<string> GetFluorochromes(string path)
        {
            if (File.Exists(path))
            {
                var xml = XElement.Load(path);
                if (xml != null)
                {
                    var passes = (from sp in xml.Descendants("CScanPass") select sp).ToArray();
                    if (passes != null && passes.Count() > 1)
                    {
                        var pass = passes[1];
                        var fluoros = from f in pass.Descendants("CParamSet")
                                      where (f.Element("Name").Value == "COUNTERSTAIN"
                                                || f.Element("Name").Value.Contains("FLUOROCHROMENAMES"))
                                      orderby f.Element("Name").Value
                                      select f.Element("VtValue").Element("VarValue").Value;
                        return fluoros;
                    }
                }
            }

            return null;
        }

        public bool IsCEPXYAssayName(string assayName)
        {
            return assayName.Equals("CEP XY_BM Transplant", StringComparison.OrdinalIgnoreCase);
        }

        public byte[] GetDefaultAssayData(string assayName)
        {
            string defaultassayPath = "Assays\\Spot\\";
            string parentDirectory = Path.Combine(_aii_SharedPath, defaultassayPath);
            string SpotAssaysPath = Path.Combine(parentDirectory, assayName) + ".assay";
            FileInfo AssayFile = new FileInfo(SpotAssaysPath);
            if (!AssayFile.Exists)
                return null;
            return (File.ReadAllBytes(AssayFile.FullName));
        }

        public void SaveAsDefaultAssay(byte[] AssayData, string assayName)
        {
            try
            {
                string defaultassayPath = "Assays\\Spot\\";
                string parentDirectory = Path.Combine(_aii_SharedPath, defaultassayPath);

                File.WriteAllBytes(parentDirectory + assayName + ".assay", AssayData);
            }
            catch (DALException exception)
            {
                throw new DALException("Create New Session error", exception, DALException.DALErrorType.Authorization);
            }

        }

        /// <summary>
        /// if assay is not present this Function will get the default assay name from .scoreassay file 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="scanAreaName"></param>
        /// <returns></returns>
        private string GetDefaultAssayName(Session session)
        {
            // The assay name is obtained from inside the local assay, unless there is no local assay
            // (e.g. legacy data), in which case it falls back to getting it from the .scoresassay file.
            // Note that the assay name in the .scoresassay file may not be up to date with the name in
            // the local assay, as the .scoresassay is only updated if the scores are updated, not if
            // only the assay or assay name changes.

            string defaultAssayName = string.Empty;
            var sessionLocator = GetLocator(session);
            if (sessionLocator.AssayFile.Exists)
            {
                var xml = XElement.Load(sessionLocator.AssayFile.FullName);
                if (xml != null)
                    defaultAssayName = xml.Element("m_Name").Value;
            }
            else if (sessionLocator.ResultsAssayFile.Exists)
            {
                var xml = XElement.Load(sessionLocator.ResultsAssayFile.FullName);
                var assayNode = xml.Element("Assay");
                if (assayNode != null)
                {
                    var nameNode = assayNode.Attribute("Name");
                    if (nameNode != null)
                    {
                        defaultAssayName = nameNode.Value;
                    }
                }
            }
            return defaultAssayName;
        }

        #endregion

        public Score[] GetScoreStats(Session session)
        {
            var sessionScoreFilePath = Path.Combine(GetSlidePath(session.Slide), SessionFilesLocator.GetScoresFileName(session.ScanAreaId, session.Id));
            var scores = new Score[0];
            if (File.Exists(sessionScoreFilePath))
            {
                var xml = XElement.Load(sessionScoreFilePath);
                scores = (from cellNode in xml.Descendants("ScoredCell")
                          select ParseScoreFromXml(cellNode)).ToArray();
            }
            return scores;
        }

        /// <summary>
        /// Returns only the ScoredCell Ids from provided score file path.
        /// </summary>
        /// <param name="scoresFileName"></param>
        /// <returns></returns>
        private IEnumerable<string> ScoreIds(string scoresFilePath)
        {
            IEnumerable<string> scoreIds = new List<string>();

            FileInfo scoresFile = new FileInfo(scoresFilePath);
            if (scoresFile.Exists)
            {
                XElement xml = XElement.Load(scoresFilePath);
                var ids = from x in xml.Descendants("ScoredCell")
                          select x.Attribute("Id").Value.ToLower();

                scoreIds = from i in ids.Distinct()
                           orderby i
                           select i;
            }

            return scoreIds;
        }

        public string GetRenderInstructionCellInfo(Session selectedSession, string scoreID)
        {
            var renderInstructionFilePath = Path.Combine(GetSlidePath(selectedSession.Slide), scoreID + "$=" + selectedSession.Name + ".enhanced.ri");
            if (!File.Exists(renderInstructionFilePath))
            {
                return null;
            }
            return (File.ReadAllText(renderInstructionFilePath));
        }

        public string GetEnhancedCellImageInfo(Session selectedSession, string scoreID)
        {
            var enhancedCellImagePath = Path.Combine(GetSlidePath(selectedSession.Slide), scoreID + "$=" + selectedSession.Name + ".enhanced");
            if (!File.Exists(enhancedCellImagePath))
            {
                return null;
            }
            return (File.ReadAllText(enhancedCellImagePath));
        }

        public bool WriteRenderInstructionToEnhancedCell(Session selectedSession, byte[] bmp, string scoreID, XElement renderInstructionForBmp)
        {
            string enhancedFilePath = Path.Combine(GetSlidePath(selectedSession.Slide), scoreID + "$=" + selectedSession.Name + ".enhanced");
            File.WriteAllBytes(enhancedFilePath, bmp);
            string renderInstructionFilePath = Path.Combine(GetSlidePath(selectedSession.Slide), scoreID + "$=" + selectedSession.Name + ".enhanced.ri");
            renderInstructionForBmp.Save(renderInstructionFilePath);
            return true; // REFACTOR pointless return, either use it or lose it
        }

        public List<ScoreSettings> LoadScoredCellsInfo(Session session, string scanAreaName)
        {
            List<ScoreSettings> scoreSettingsList = new List<ScoreSettings>();
            string path = Path.Combine(GetSlidePath(session.Slide), scanAreaName + "$=" + session.Name + ".scoresassay");

            FileInfo scoresAssayFile = new FileInfo(path);
            if (!scoresAssayFile.Exists)
            {
                return scoreSettingsList;
            }

            XElement xml = XElement.Load(scoresAssayFile.FullName);

            foreach (XElement element in xml.Descendants("EnhancedCell"))
            {
                scoreSettingsList.Add(new ScoreSettings()
                {
                    scoreid = new Guid(element.Attribute("Name").Value),
                    IsEnhanced = true
                });
            }

            foreach (XElement element in xml.Descendants("RescoredCell"))
            {
                Guid id = new Guid(element.Attribute("Name").Value);
                if (scoreSettingsList.Exists(x => x.scoreid == id))
                {
                    scoreSettingsList.FirstOrDefault(cellid => cellid.scoreid == id).IsRescored = true;
                }
                else
                {
                    scoreSettingsList.Add(new ScoreSettings()
                    {
                        scoreid = new Guid(element.Attribute("Name").Value),
                        IsRescored = true
                    });
                }
            }

            foreach (XElement element in xml.Descendants("ReviwedCell"))
            {
                Guid id = new Guid(element.Attribute("Name").Value);
                if (scoreSettingsList.Exists(x => x.scoreid == id))
                {
                    scoreSettingsList.FirstOrDefault(cellid => cellid.scoreid == id).IsReviewed = true;
                }
                else
                {
                    scoreSettingsList.Add(new ScoreSettings()
                    {
                        scoreid = new Guid(element.Attribute("Name").Value),
                        IsReviewed = true
                    });
                }
            }
            return scoreSettingsList;
        }

        public void SaveSession(Session session, List<ScoreSettings> scoreSettingsList, SessionScores sessionScores)
        {
            // TODO - REFACTOR - Saving all the scores back to .scores file. Need to verify Channel, OVerla, Classes 
            XElement scoresXml = new XElement("CellScores", from s in sessionScores.Scores select ToXml(s));
            scoresXml.Add(
                new XElement("Display",
                    new XElement("Channels", from c in sessionScores.ChannelDisplays
                                             select new XElement("Channel",
                                                 new XAttribute("Name", c.Name),
                                                 new XAttribute("Visible", c.IsVisible),
                                                 new XAttribute("L", c.HistogramLow),
                                                 new XAttribute("H", c.HistogramHigh))),

                    new XElement("Overlays", sessionScores.Overlays == null ? null : from o in sessionScores.Overlays
                                                                                     select new XElement("Overlay",
                                                                                         new XAttribute("Name", o.Name))) //,new XAttribute("Visible", o.IsVisible)

                // TODO - REFACTOR saving classes to scores 

                //new XElement("Classes", from c in classFilterList
                //                        select new XElement("Class",
                //                            new XAttribute("Name", c.Name),
                //                            new XAttribute("Visible", c.IsSelected))),

                //new XElement("DisplayOptions", new XAttribute("AttenuateCounterstain", AttenuateCounterstain),
                //                                new XAttribute("GridSize", GridSize))
                ));
            MoveThumbnailToActualDirectory(session);
            var sessionScoreFilePath = Path.Combine(GetSlidePath(session.Slide), SessionFilesLocator.GetScoresFileName(session.ScanAreaId, session.Id));
            if (File.Exists(sessionScoreFilePath))
            {
                scoresXml.Save(sessionScoreFilePath);
            }
            //Delete the Scores which marked as delete
            DeleteSessionScores(session, scoreSettingsList.Where(cell=> cell.IsDeleted == true).ToList());
            //saving scores assay file
            string path = Path.Combine(GetSlidePath(session.Slide), session.ScanAreaId + "$=" + session.Name + ".scoresassay");
            FileInfo scoresAssayFile = new FileInfo(path);
            scoresAssayFile.Refresh();
            if (scoresAssayFile.Exists)
            {
                scoresAssayFile.Delete();
            }

            var reviewdCellsXml = new XElement("ReviewedCells", from cell in scoreSettingsList
                                                                where cell.IsReviewed == true
                                                                select new XElement("ReviwedCell", new XAttribute("Name", cell.scoreid.ToString())));
            var rescoresCellsXml = new XElement("RescoredCells", from cell in scoreSettingsList
                                                                 where cell.IsRescored == true
                                                                 select new XElement("RescoredCell", new XAttribute("Name", cell.scoreid.ToString())));
            var enhancedCellsXml = new XElement("EnhancedCells", from cell in scoreSettingsList
                                                                 where cell.IsEnhanced == true
                                                                 select new XElement("EnhancedCell", new XAttribute("Name", cell.scoreid.ToString())));
            XElement assayXml = new XElement("Assay");
            XElement scoresAssayXml = new XElement("ScanArea");
            scoresAssayXml.Add(assayXml);
            scoresAssayXml.Add(reviewdCellsXml);
            scoresAssayXml.Add(rescoresCellsXml);
            scoresAssayXml.Add(enhancedCellsXml);
            scoresAssayXml.Save(scoresAssayFile.FullName);

            //Delete the .bck assay File
            DeleteBackupAssayFile(session);
            RequestEndImageProcessing(session);
        }

        public XElement ToXml(Score score)
        {
            var result = new XElement("ScoredCell", new XAttribute("Id", score.Id),
                                                    new XAttribute("Class", score.Class),
                                                    new XAttribute("FrameX", score.X),
                                                    new XAttribute("FrameY", score.Y),
                                                    new XAttribute("ImageFrameId", score.ImageFrameId),
                                                    new XAttribute("Color", score.ColourString),
                                                    new XAttribute("RegionId", score.RegionId.ToString()));

            if (score.SpotMeasurements != null)
            {
                foreach (Measurement m in score.SpotMeasurements)
                {
                    result.Add(new XAttribute(m.OriginalName, m.Value));
                }
            }

            return result;
        }

        /// <summary>
        /// Delete the scores thumbnails data. 
        /// On cancel delete the  newly scored thumbnail data
        /// On ok Delete the CanDelete Scores thumbnail data
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="scoreSettingsList"></param>
        public void DeleteSessionScores(Session session, List<ScoreSettings> scoreSettingsList)
        {
            string scanAreaScoreFilePath = Path.Combine(GetSlidePath(session.Slide), SessionFilesLocator.GetZeroScoresFileName(session.ScanAreaId));
            if (scanAreaScoreFilePath != null)
            {
                var xml = XElement.Load(scanAreaScoreFilePath);
                var machinescores = (from cellNode in xml.Descendants("ScoredCell")
                                     select ParseScoreFromXml(cellNode)).ToArray();
                var deletedScores = scoreSettingsList.Where(cell => !machinescores.Any(x => x.Id.ToString() == cell.scoreid.ToString())).Select(x => x.scoreid.ToString()).ToList();
                if (deletedScores.Count > 0)
                {
                    DeleteScoreThumbnailData(session, deletedScores);
                }
            }
            else
            {
                var deletedScores = scoreSettingsList.Select(x => x.scoreid.ToString()).ToList();
                DeleteScoreThumbnailData(session, deletedScores);
            }
        }

        /// <summary>
        /// Delete the scores thumbnail data and regions  if newly created and not at all required on ok or cancel click
        /// </summary>
        /// <param name="session"></param>
        /// <param name="scoreSettingsList"></param>
        /// <param name="regions"></param>
        public void DeleteNewScoresAndRegionsFromSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions)
        {
            DeleteSessionScores(session, scoreSettingsList.Where(cell => cell.IsNewlyCreated == true).ToList());
            DeleteRegions(session, regions);
        }

        private void DeleteScoreThumbnailData(Session session, IEnumerable<string> scores)
        {
            var slideFolder = GetSlidePath(session.Slide);
            foreach (var score in scores)
            {
                var thumbnailFilename = Path.Combine(slideFolder, score + ".thumb.blb");
                if (File.Exists(thumbnailFilename))
                {
                    File.Delete(thumbnailFilename);
                }
                var overlayFilename = Path.Combine(slideFolder, score + "*.png");
                if (File.Exists(overlayFilename))
                {
                    File.Delete(overlayFilename);
                }
            }
        }

        //public bool SaveThumbnailData(Session session, Score score, byte[] data)
        //{
        //    var scoreThumbFilePath = Path.Combine(GetSlidePath(session.Slide), score.Id.ToString() + ".thumb.blb");
        //    if (!File.Exists(scoreThumbFilePath))
        //    {
        //        File.WriteAllBytes(scoreThumbFilePath, data);
        //        return true;
        //    }
        //    else
        //        return false;
        //}

        public void ClearTemporarySavedThumb(Session session)
        {
            DeleteFilesAndFoldersRecursively(TempDirectory(session));
        }

        /// <summary>
        /// On Cencel Session data will be restored
        /// </summary>
        /// <param name="session"></param>
        /// <param name="regions"></param>
        public void RestoreSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions)
        {
            DeleteFilesAndFoldersRecursively(TempDirectory(session));
            //Delete the thumbnail for newly created scores on cancel session
            DeleteSessionScores(session, scoreSettingsList.Where(cell=>cell.IsNewlyCreated==true).ToList());
            DeleteRegions(session, regions);
            RestoreAssayData(session);
        }

        public byte[] CreateThumbnailBitmap(ScoreThumbnail scoreThumbnail, Session session)
        {
            string ThumbPath = Path.Combine(TempDirectory(session), scoreThumbnail.ScoreId.ToString() + ".thumb.blb");
            var frame = GetFrames(session).Where(xframe => xframe.Id == scoreThumbnail.FrameID).FirstOrDefault();
            ThumbnailTrickler thumbGenerator = new ThumbnailTrickler();
            ThumbInfo info = new ThumbInfo()
            {
                ScoreThumbnail = scoreThumbnail,
                Frame = frame,
                SlidePath = GetSlidePath(session.Slide),
            };
            var data = thumbGenerator.GetThumbnailData(info);
            if (data != null)
            {
                File.WriteAllBytes(ThumbPath, thumbGenerator.GetThumbnailData(info));
                if (!File.Exists(ThumbPath))
                    return null;
                var scanArea = new ScanArea() { Slide = session.Slide, Id = session.ScanAreaId };
                var representativeFrame = GetFrames(scanArea).FirstOrDefault();
                var renderer = new StackedThumbnailRenderer(representativeFrame.Components);
                var thumbnailData = File.ReadAllBytes(ThumbPath);
                var renderedBitmap = renderer.RenderToBitmapData(thumbnailData, scoreThumbnail.StackIndex, scoreThumbnail.Channels, scoreThumbnail.AttenuateCounterstain, scoreThumbnail.InvertCounterstain);
                return (renderedBitmap);
            }
            else
            {
                System.Diagnostics.Debug.Fail("thumbnail creation failed");
                return data;
            }
        }

        private IEnumerable<Frame> GetFrames(Session session)
        {
            var slideFolder = GetSlidePath(session.Slide);

            var imageFramesFilename = Path.Combine(slideFolder, session.ScanAreaId + ".imageframes");
            if (File.Exists(imageFramesFilename))
                return Frame.LoadFromImageFramesXml(imageFramesFilename).ToArray();
            else
                return new Frame[0];
        }

        private string TempDirectory(Session session)
        {
            string tempDirectory = Path.Combine(GetSlidePath(session.Slide), TempSessionFolder + session.Name);
            if (!Directory.Exists(tempDirectory))
            {
                Directory.CreateDirectory(tempDirectory);
            }
            return tempDirectory;
        }

        private void MoveThumbnailToActualDirectory(Session session)
        {
            foreach (string fileNameWithFullPath in Directory.GetFiles(TempDirectory(session)))
            {
                string FileName = Path.GetFileName(fileNameWithFullPath);
                File.Move(fileNameWithFullPath, Path.Combine(GetSlidePath(session.Slide), FileName));
            }
            DeleteFilesAndFoldersRecursively(TempDirectory(session));
        }

        private void DeleteFilesAndFoldersRecursively(string target_dir)
        {
            foreach (string file in Directory.GetFiles(target_dir))
            {
                File.Delete(file);
            }
            foreach (string subDir in Directory.GetDirectories(target_dir))
            {
                DeleteFilesAndFoldersRecursively(subDir);
            }
            Directory.Delete(target_dir);
        }

        public Session CreateNewSession(ScanArea scanArea, string sessionName, bool useSpotScores, bool isCreatedWithSelectedFrames = false, List<string> selectedFrameIdList = null)
        {
            try
            {
                if (scanArea != null)
                {
                    Session newSession = new Session(scanArea.Slide, sessionName, sessionName, scanArea.Id, isCreatedWithSelectedFrames);
                    var updatedScanArea = UpdatedScanArea(newSession);
                    if (updatedScanArea.Sessions != null && updatedScanArea.Sessions.Count < 2 && !updatedScanArea.Sessions.Any(s => s.Name == newSession.Name))
                    {
                        scanArea.Sessions.Add(newSession);
                        var newsessionLocator = Path.Combine(GetSlidePath(newSession.Slide), scanArea.Id);

                        //TODO Check IsAverage
                        //checking other session score exist
                        //var blindsession = _scoringSessionsForCurrentScan.Where(a => a != session && a.IsAverage == false).FirstOrDefault();
                        //return (blindsession == null) ? null : blindsession.Slide;
                        Session otherSession = scanArea.Sessions.Where(s => s != newSession).FirstOrDefault();

                        // Unlike the other data, the assay is copied from the existing session, if there is one, rather than the original
                        // data, so that the second session can benefit from any assay modifications done in the first session. The exception
                        // to this, is if the original data contains fully automatic scores (that may be copied to the new session), in which
                        // case the assay must match these and so must come from the original data also.

                        if (otherSession != null && CheckAssayExist(otherSession) && !(CheckScoreFilesAutoScore(otherSession)))
                        {
                            var sessionLocator = GetLocator(otherSession);
                            sessionLocator.AssayFile.CopyTo(newsessionLocator + "$=" + newSession.Name + ".assay");
                        }
                        else
                        {
                            var defaultAssayFilePath = Path.Combine(GetSlidePath(scanArea.Slide), scanArea.Name + ".assay");
                            FileInfo AssayFilePath = new FileInfo(defaultAssayFilePath);
                            if (AssayFilePath.Exists)
                                AssayFilePath.CopyTo(newsessionLocator + "$=" + newSession.Name + ".assay");
                            // Need to copy the .scoresassay file to the new session (even though one would be created after the session has been opened),
                            // as if no local copy of the assay is found, this file is the only one that will contain the name of the current assay,
                            // e.g. if this is pre-v7.2 data that won't have a local assay (until it is opened in v7.3).
                            string scoresAssayFilepath = Path.Combine(GetSlidePath(scanArea.Slide), scanArea.Name + ".scoresassay");
                            FileInfo scoresAssayFile = new FileInfo(scoresAssayFilepath);
                            if (scoresAssayFile.Exists)
                            {
                                scoresAssayFile.CopyTo(newsessionLocator + "$=" + newSession.Name + ".scoresassay");
                            }
                        }
                        //check the default scores file exist
                        string scanAreaScoreFilePath = Path.Combine(GetSlidePath(scanArea.Slide), scanArea.Name + ".scores");
                        FileInfo scanAreaScoreFile = new FileInfo(scanAreaScoreFilePath);
                        if (scanAreaScoreFile.Exists)
                        {
                            CopyScoresAndMarkasAuto(scanAreaScoreFile.FullName, newsessionLocator + "$=" + newSession.Name + ".scores", !useSpotScores, selectedFrameIdList);

                            //Check for DualScoring session.
                            if (selectedFrameIdList != null && selectedFrameIdList.Count != 0)
                            {
                                SaveSelectedFramesToSelectFramesFile(scanArea, sessionName, selectedFrameIdList, newsessionLocator + "$=" + newSession.Name + ".selectframes");
                            }

                            string measurementsFilePath = Path.Combine(GetSlidePath(scanArea.Slide), scanArea.Name + ".txt");
                            FileInfo measurementsFile = new FileInfo(measurementsFilePath);
                            if (measurementsFile.Exists)
                            {
                                measurementsFile.CopyTo(newsessionLocator + "$=" + newSession.Name + ".txt");
                            }
                        }
                        else if (!File.Exists(newsessionLocator + "$=" + newSession.Name + ".scores"))
                        {
                            // create an empy scores file as a placeholder
                            var doc = new XElement("CellScores");
                            doc.Save(newsessionLocator + "$=" + newSession.Name + ".scores");
                        }

                        string overlaysFilePath = Path.Combine(GetSlidePath(scanArea.Slide), scanArea.Name + ".overlays");
                        if (useSpotScores && File.Exists(overlaysFilePath))
                        {
                            File.Copy(overlaysFilePath, newsessionLocator + "$=" + newSession.Name + ".overlays");
                        }

                        //TODO HasMachineScores need which requires AllowSessionOptions
                        // newSession.HasMachineScores = (AllowSessionOptions && useSpotScores);
                        var HasMachineScores = useSpotScores;
                        CreateSessionInfo(newSession, HasMachineScores);
                        return newSession;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Create New Session error", ex, DALException.DALErrorType.Authorization);
            }
            return null;
        }

        private void CreateSessionInfo(Session session, bool HasMachineScores)
        {
            var doc = new XElement("SessionInfo",
                                    new XElement("Session", new XAttribute("name", session.Name),
                                                            new XAttribute("machineScores", HasMachineScores)));
            doc.Save(Path.Combine(GetSlidePath(session.Slide) + "\\" + session.ScanAreaId + "$=" + session.Name + ".sessioninfo"));
        }

        internal void CopyScoresAndMarkasAuto(string filename, string saveas, bool setUnclassed, List<string> selectedFrameIdList = null)
        {
            var xml = XElement.Load(filename);

            if (selectedFrameIdList != null)
            {
                xml.Descendants("ScoredCell").Where(x => !selectedFrameIdList.Contains(x.Attribute("ImageFrameId").Value.ToLower())).Remove();
            }

            if (setUnclassed)
            {
                foreach (var x in xml.Descendants("ScoredCell"))
                {
                    if (x.Attribute("Class") != null)
                        x.Attribute("Class").Value = "Uninformative";
                    if (x.Attribute("Color") != null)
                        x.Attribute("Color").Value = "#808080";

                    var meas = x.Attributes().Where(a => a.Name.ToString().StartsWith("MEAS_")).ToList();
                    foreach (var a in meas)
                        a.Remove();
                }
            }

            xml.Save(saveas);
        }

        internal bool CheckAssayExist(Session session)
        {
            var sessionLocator = GetLocator(session);
            if (!sessionLocator.AssayFile.Exists)
            {
                return false;
            }
            return true;
        }

        internal bool CheckScoreFilesAutoScore(Session session)
        {
            bool status = false;
            var sessionScoreFilePath = Path.Combine(GetSlidePath(session.Slide), SessionFilesLocator.GetScoresFileName(session.ScanAreaId, session.Id));
            if (File.Exists(sessionScoreFilePath))
            {
                var xml = XElement.Load(sessionScoreFilePath);
                var scores = (from cellNode in xml.Descendants("ScoredCell")
                              select ParseScoreFromXml(cellNode)).ToArray();
                if (scores.Count() > 0)
                {
                    foreach (var score in scores)
                    {
                        // Scores from drawn regions have a non-null GUID. If no spot measurements, must be manual.
                        // If has spot measurements, will be manual unless there are more than two
                        // and one of them is called MEAS_Id (MEAS_Id is only created by the spot script).
                        // So, to recap:
                        // Scores for regions have a non-null RegionID and a MEAS_Id
                        // Manual scores have a null RegionID (Guid.Empty) and no MEAS_Id
                        // Scores for cells automatically found by the spot script have a null RegionID and a MEAS_Id 
                        if (score.RegionId != Guid.Empty || score.SpotMeasurements != null || score.SpotMeasurements.Count < 3 || score.SpotMeasurements.FirstOrDefault(x => x.OriginalName == "MEAS_Id") == null)
                        {
                            status = true;
                            break;
                        }
                    }
                }
            }

            return status;
        }

        internal bool CheckSessionWithSpotScores(ScanArea scanArea)
        {
            bool status = false;
            var ScanAreaPath = Path.Combine(GetSlidePath(scanArea.Slide), scanArea.Id);

            /* TODO:'CanCreateSessions' property in legacy PCV application is used to bind the visibility property of 
             * 'New Session with Spot Scores' button however the binding has not done properly, hence it sets visibilty to 'true' always 
             * making it irrelavent to add to current applcation */

            //if (File.Exists(ScanAreaPath + ".multisessionblock"))// Has this scan area been blocked previously?
            //    status1 = false;
            //else if (!File.Exists(ScanAreaPath + ".assay") && ((File.Exists(ScanAreaPath + ".scores") || File.Exists(ScanAreaPath + ".roi"))))
            //{
            //    status1 = false;
            //    if (!File.Exists(ScanAreaPath + ".autoscoresaremodified"))
            //    {
            //        FileInfo AutoScoresAreModifiedFile = new FileInfo(ScanAreaPath + ".autoscoresaremodified");
            //        AutoScoresAreModifiedFile.Create().Close();
            //    }
            //}

            //'AllowSessionOptions' in legacy PCV application
            string scoreFilePath = ScanAreaPath + ".scores";
            if (File.Exists(scoreFilePath))
            {
                var xml = XElement.Load(scoreFilePath);
                var scores = (from cellNode in xml.Descendants("ScoredCell")
                              select ParseScoreFromXml(cellNode)).ToArray();
                if (scores.Count() > 0)
                    status = true;
            }

            return status;
        }

        public void DeleteSession(Session session)
        {
            try
            {
                var sessionLocator = GetLocator(session);

                if (sessionLocator.AssayFile.Exists)
                {
                    sessionLocator.AssayFile.Delete();
                }
                if (sessionLocator.ScoresFile.Exists)
                {
                    DeleteThumbnailAndOverlaysData(session);
                    sessionLocator.ScoresFile.Delete();
                }
                if (sessionLocator.AutoScoresAreModifiedFile.Exists)
                {
                    sessionLocator.AutoScoresAreModifiedFile.Delete();
                }
                if (sessionLocator.FrameDataFile.Exists)
                {
                    sessionLocator.FrameDataFile.Delete();
                }
                if (sessionLocator.OverlaysFile.Exists)
                {
                    sessionLocator.OverlaysFile.Delete();
                }
                if (sessionLocator.ResultsAssayFile.Exists)
                {
                    sessionLocator.ResultsAssayFile.Delete();
                }
                if (sessionLocator.SessionInfo.Exists)
                {
                    sessionLocator.SessionInfo.Delete();
                }
                if (sessionLocator.MeasurementsFile.Exists)
                {
                    sessionLocator.MeasurementsFile.Delete();
                }
                if (sessionLocator.SelectFramesFile.Exists)
                {
                    sessionLocator.SelectFramesFile.Delete();
                }

                DeleteSpotRegionFolders(session);
                var frameAnnotations = sessionLocator.SlideFolder.EnumerateFiles("*.annotations" + session.Name);
                foreach (var f in frameAnnotations)
                {
                    f.Delete();
                }
                var enhanced = sessionLocator.SlideFolder.EnumerateFiles("*$=" + session.Name + ".enhanced*");
                foreach (var f in enhanced)
                {
                    f.Delete();
                }

                //TODO: Sreenatha Need clarifiaction on  CWSExportFlags class and below functionalities depends on that 
                ////var exporter = CWSExporterForSlide(Area.Slide.Folder.Name);
                ////if (exporter != null)
                ////    exporter.ClearAllSessionFlags(Area.Slide.SessionName, Area.Slide.ScanAreaName);
            }
            catch (Exception ex)
            {
                throw new DALException("Session file access Error", ex, DALException.DALErrorType.DBConnection);
            }
        }

        private void DeleteSpotRegionFolders(Session session)
        {
            var folders = GetFoldersForScanArea(session);
            foreach (var f in folders)
            {
                if (IsRegionSpotFolder(f) && IsFromSession(f, session))
                    SafeDirDelete(f);
            }
        }

        //MARV- Check Scope. Check name?
        public bool IsFromSession(string spotFolder, Session session)
        {
            var path = Path.Combine(spotFolder, "reprocess.info");
            if (File.Exists(path))
            {
                var xml = XElement.Load(path);
                if (xml.Attribute("sessionName").Value == session.Name)
                    return true;
            }
            return false;
        }

        //MARV- Check Scope . Check name IsRegionFileInSpotFolder?
        public bool IsRegionSpotFolder(string spotFolder)
        {
            var path = Path.Combine(spotFolder, "regionmask.stk");
            return File.Exists(path);
        }

        //MARV- Check Scope
        public IEnumerable<string> GetFoldersForScanArea(Session session)
        {
            List<string> FolderList = new List<string>();
            var spotFolders = Directory.EnumerateDirectories(GetSlidePath(session.Slide), "spot*");
            var cellF = Directory.EnumerateDirectories(GetSlidePath(session.Slide), "cell*");
            FolderList.AddRange(spotFolders);
            FolderList.AddRange(cellF);
            return FolderList;
        }

        //MARV- Why its static
        //TODO Sreenatha Do we need to catch the exception ? if unable to delete a folder
        private void SafeDirDelete(string path)
        {
            int count = 1;
            while (true)
            {
                try
                {
                    var dir = new DirectoryInfo(path);
                    if (dir.Exists)
                    {
                        dir.Delete(true);
                    }
                    break;
                }
                catch
                {
                    count++;
                    if (count == 5)
                        throw;
                }
            }

        }

        private void DeleteThumbnailAndOverlaysData(Session session)
        {
            // Get Session generated score File Path
            var sessionScoreFilePath = Path.Combine(GetSlidePath(session.Slide), SessionFilesLocator.GetScoresFileName(session.ScanAreaId, session.Id));
            if (File.Exists(sessionScoreFilePath))
            {
                //Get Session Score IDs
                var sessionScoreIDs = ScoreIds(sessionScoreFilePath).ToList();
                if (sessionScoreIDs != null && sessionScoreIDs.Count() > 0)
                {
                    //Get Machine generated ScoreFile path
                    var sessionZeroScoreFilePath = Path.Combine(GetSlidePath(session.Slide), SessionFilesLocator.GetZeroScoresFileName(session.ScanAreaId));
                    foreach (var scoreID in sessionScoreIDs)
                    {
                        // Check if score is not present in machine generated score file
                        // If true delete the related files
                        if (!IsSessionZeroScore(scoreID, sessionZeroScoreFilePath))
                        {
                            //thumbnail
                            FileInfo thumbnailFile = new FileInfo(Path.Combine(GetSlidePath(session.Slide), scoreID + ".thumb.blb"));
                            if (thumbnailFile.Exists)
                                thumbnailFile.Delete();

                            // overlays
                            var pngsOverlay = Directory.EnumerateFiles(thumbnailFile.Directory.FullName, scoreID + "*.png");
                            foreach (var png in pngsOverlay)
                                File.Delete(png);
                        }
                    }
                }
            }
            // Clear collection when its task is over. Otherwise it will check new session with previous score file.
            sessionZeroScoreIDs = null;
        }
        /// <summary>
        /// The scanner/manual capture create a cached jpg of the frame, we can use this for a quick display image 
        /// no rendering
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="slideId"></param>
        /// <param name="scanAreaId">no necessary but conceptually useful</param>
        /// <param name="frameId"></param>
        /// <returns></returns>
        public byte[] GetCachedFrameBitmap(string caseId, string slideId, string scanAreaId, string frameId)
        {
            var slideFolder = GetSlidePath(caseId, slideId);
            var cachedImageFilename = Path.Combine(slideFolder, frameId + ".thumbnail.jpg");
            if (!File.Exists(cachedImageFilename))
                return null;
            // returns fullsized bitmap, maybe worth adding a quality value for further compression (for http)
            return (File.ReadAllBytes(cachedImageFilename));
        }

        /// <summary>
        /// Save the selected frames of dual score session to .selectframes file.
        /// </summary>
        /// <param name="session">session</param>
        /// <param name="scanAreaName">scan area name</param>
        /// <param name="selectedFrames">selected frames</param>
        internal void SaveSelectedFramesToSelectFramesFile(ScanArea scanArea, string sessionName, List<string> selectedFrames, string selectFramesfilepath)
        {
            SelectFrames selectFrames = new SelectFrames() { Name = scanArea.Slide.Name, SelectedScanArea = scanArea.Name, SessionName = sessionName };
            if (selectedFrames.Count != 0)
            {
                selectFrames.SelectedFrameList.Clear();
                foreach (var item in selectedFrames)
                {
                    selectFrames.SelectedFrameList.Add(item);
                }
            }

            //Serialise & save to .selectframes file.
            XmlSerializer serialiser = new XmlSerializer(selectFrames.GetType());
            XmlSerializerNamespaces defaultNameSpaces = new XmlSerializerNamespaces();
            string emptyVariable = "";
            defaultNameSpaces.Add(emptyVariable, emptyVariable);
            if (File.Exists(selectFramesfilepath))
            {
                File.Delete(selectFramesfilepath);
            }

            using (XmlWriter writer = XmlWriter.Create(selectFramesfilepath))
            {
                serialiser.Serialize(writer, selectFrames, defaultNameSpaces);
            }
        }

        /// <summary>
        /// Get the GUID of the selected frames stored in .selectframes file.
        /// </summary>
        /// <param name="scanArea"></param>
        /// <returns></returns>
        public List<string> GetSelectedFramesForSession(Session session)
        {
            //Get previous session selected frames & marked already selected for current session
            string previousSessionSelectedFramesFilePath = String.Format("{0}$={1}.selectframes", Path.Combine(GetSlidePath(session.Slide), session.ScanAreaId), session.Name);
            List<string> selectedFramesInPreviousSession = new List<string>();
            if (File.Exists(previousSessionSelectedFramesFilePath))
            {
                selectedFramesInPreviousSession = GetSelectedFrames(previousSessionSelectedFramesFilePath).SelectedFrameList;
            }

            return selectedFramesInPreviousSession;
        }

        /// <summary>
        /// Fetch the selected frames from .selectframes file.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        internal SelectFrames GetSelectedFrames(string path)
        {
            SelectFrames selectedframes = new SelectFrames();
            using (StreamReader reader = new StreamReader(path))
            {
                selectedframes = (SelectFrames)new XmlSerializer(typeof(SelectFrames)).Deserialize(reader);
            }

            return selectedframes;
        }

        /// <summary>
        /// Returns the cell count for the particular image frame Id
        /// </summary>
        /// <param name="scanArea"></param>
        /// <param name="frameId"></param>
        /// <returns></returns>
        public long GetFrameCellCount(ScanArea scanArea, string frameId)
        {
            long cellCount = -1;
            string scanAreaScoreFilePath = Path.Combine(GetSlidePath(scanArea.Slide), scanArea.Id + ".scores");
            FileInfo scanAreaScoreFile = new FileInfo(scanAreaScoreFilePath);

            if (scanAreaScoreFile.Exists)
            {
                var xml = XElement.Load(scanAreaScoreFile.FullName);
                cellCount = xml.Descendants("ScoredCell").Where(x => x.Attribute("ImageFrameId").Value.ToLower().Equals(frameId)).ToList().Count;
            }
            return cellCount;
        }

        /// <summary>
        /// Used in Add Cells Feature - Saves the scored cells, add/remove new cells in .scores file
        /// </summary>
        /// <param name="session"></param>
        /// <param name="frameIdList"></param>
        public void UpdateScores(Session session, List<string> frameIdList)
        {
            string machineScoresFilePath = Path.Combine(GetSlidePath(session.Slide), session.ScanAreaId) + ".scores";
            string sessionScoreFilePath = Path.Combine(GetSlidePath(session.Slide), SessionFilesLocator.GetScoresFileName(session.ScanAreaId, session.Id));
            string selectframesFilePath = String.Format("{0}$={1}.selectframes", Path.Combine(GetSlidePath(session.Slide), session.ScanAreaId), session.Name);
            if (File.Exists(selectframesFilePath))
            {
                //update selected frames for DualScoring session.
                XElement selectFramesXML = XElement.Load(selectframesFilePath);
                selectFramesXML.Descendants("SelectedFrame").Remove();
                foreach (string item in frameIdList)
                {
                    selectFramesXML.Add(new XElement("SelectedFrame", item));
                }

                selectFramesXML.Save(selectframesFilePath);
            }

            if (File.Exists(machineScoresFilePath) && File.Exists(sessionScoreFilePath))
            {
                XElement machineScoresXML = XElement.Load(machineScoresFilePath);
                XElement sessionScoreXml = XElement.Load(sessionScoreFilePath);

                var savedImageFrameIDs = sessionScoreXml.Descendants("ScoredCell").GroupBy(x => x.Attribute("ImageFrameId").Value.ToLower()).Distinct().Select(x => x.Key);

                //Remove ID's not in frameIdList
                machineScoresXML.Descendants("ScoredCell").Where(x => !frameIdList.Contains(x.Attribute("ImageFrameId").Value.ToLower())).Remove();

                //Remove ID's in savedImageFrameIDs - to retain saved/analysed cells which is added later
                machineScoresXML.Descendants("ScoredCell").Where(x => savedImageFrameIDs.Contains(x.Attribute("ImageFrameId").Value.ToLower())).Remove();

                //Remove existing IDS not in selection
                var IDListToRemove = savedImageFrameIDs.Where(x => !frameIdList.Contains(x));
                sessionScoreXml.Descendants("ScoredCell").Where(x => IDListToRemove.Contains(x.Attribute("ImageFrameId").Value.ToLower())).Remove();

                foreach (var item in machineScoresXML.Descendants("ScoredCell"))
                {
                    sessionScoreXml.AddFirst(item);
                }
                sessionScoreXml.Save(sessionScoreFilePath);
            }
        }

        //Use to check if Session ScoreID is present in Machine generated Score file.
        private bool IsSessionZeroScore(string scoreID, string sessionZeroScoresFilePath)
        {
            if (sessionZeroScoreIDs == null)
            {
                sessionZeroScoreIDs = ScoreIds(sessionZeroScoresFilePath).ToList();
            }

            return (sessionZeroScoreIDs.BinarySearch(scoreID) >= 0);
        }

        public byte[] GetSlideOverviewImage(Slide slide, string etchImage)
        {
            var path = Path.Combine(GetSlidePath(slide), etchImage);
            if (File.Exists(path))
            {
                return File.ReadAllBytes(path);
            }
            else
                return null;
        }

        public List<FrameOutline> GetFrameOutlines(Slide slide)
        {
            var path = GetSlidePath(slide) + @"\scanareadata.xml";

            if (File.Exists(path))
            {
                var xmlDoc = XElement.Load(path);
                if (xmlDoc != null)
                {
                    var overViewImages = from x in xmlDoc.Descendants("ScanArea")
                                         select new FrameOutline(x);
                    return overViewImages.ToList();
                }
            }
            return new FrameOutline[0].ToList();
        }

        /// <summary>
        /// This function will create the region folder and the files required to proceessed by the PCVScanner
        /// </summary>
        public void QueueRegionForAnalysis(AnalysisRegion analysisRegion)
        {
            try
            {

                if (analysisRegion != null)
                {
                    byte[] MaskData = ScriptProcHelper.GetMaskBytes(analysisRegion.Frame.Width, analysisRegion.Frame.Height, analysisRegion.Points);

                    if (MaskData != null)
                    {
                        string dir = Path.Combine(GetSlidePath(analysisRegion.Session.Slide), "spot" + analysisRegion.Id.ToString());
                        Directory.CreateDirectory(dir);
                        if (dir != null)
                        {
                            var locator = GetLocator(analysisRegion.Session);

                            //RVCMT-HA - Region file is created in Slide folder as well as in Spot folder. Can we optimize ?
                            //RVCMT-HA - the files are not getting deleted from Slide foler, it is getting accumulated.
                            var regionFilename = @"\" + analysisRegion.Id + ".region";
                            // save the points to a file, save in spot folder as well for easy access by script
                            SaveRegion(dir + regionFilename, analysisRegion.Points);
                            string slideFolder = GetSlidePath(analysisRegion.Session.Slide) + regionFilename;
                            SaveRegion(slideFolder, analysisRegion.Points);
                            FrameSave.Save(Path.Combine(dir, "regionmask.stk"), MaskData, (int)analysisRegion.Frame.Width, (int)analysisRegion.Frame.Height, 8);
                            File.WriteAllText(Path.Combine(dir, "FRAME.ID"), analysisRegion.Frame.Id.ToString().ToUpper());
                            File.WriteAllText(Path.Combine(dir, ".objcnt"), (analysisRegion.CellCount).ToString());
                            File.WriteAllLines(Path.Combine(dir, "assay.name"), new string[] { locator.AssayFile.Name, analysisRegion.Session.ScanAreaId });
                            File.WriteAllText(Path.Combine(dir, ".NAME"), analysisRegion.Frame.Location);
                            // The .ideal file will be read by CTrawler::UploadCoords() in ctrawler.cpp.
                            var ideal = string.Format(CultureInfo.InvariantCulture, "{0:0.00} {1:0.00} {2:0.00}", analysisRegion.Frame.IdealX, analysisRegion.Frame.IdealY, analysisRegion.Frame.IdealZ);
                            File.WriteAllText(Path.Combine(dir, ".ideal"), ideal);
                            var coords = string.Format("{0} {1}", (int)(analysisRegion.Frame.IdealX * analysisRegion.Frame.XScale), (int)(analysisRegion.Frame.IdealY * analysisRegion.Frame.YScale));
                            File.WriteAllText(Path.Combine(dir, ".coords"), coords);

                            CreateSessionIndicator(dir, analysisRegion.Session.Name);

                            var launcher = new PcvScannerController(_casebaseFolder,
                                                                    analysisRegion.Session.Slide.CaseId,
                                                                    analysisRegion.Session.Slide.Id,
                                                                    analysisRegion.Session.ScanAreaId,
                                                                    locator.AssayFile.Name,
                                                                    20.0, // from pcv, I guess we always analyse 20X
                                                                    analysisRegion.Frame.XScale,
                                                                    analysisRegion.Frame.YScale);
                            launcher.StartPcvScanner();
                            // This is for Debug Purpose. Just to verify/visualize the mask created. 
                            ScriptProcHelper.DumpDataAsPng(MaskData, analysisRegion.Frame.Width, analysisRegion.Frame.Height, Path.Combine(dir, "regionmask.png"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Unable to save region file error", ex, DALException.DALErrorType.Unknown);
            }
        }

        /// <summary>
        /// save the region points in sepcified path
        /// </summary>
        private void SaveRegion(string path, IEnumerable<SerializiblePoint> points)
        {
            using (Stream stream = File.Open(path, FileMode.Create))
            {
                BinaryFormatter bin = new BinaryFormatter();
                // While Serializing we are changing the assembly name and typeName to support legacy PCV.
                // the legacy PCV expect assembly name "ProbeCaseView" and namespace "AI.ProbeScore+SerializiblePoint" while deserializing the .region file.
                // When we use SerializationBinder, it calls the overriden BindToName method and change the assembly name and namespacce.
                bin.Binder = new SerializiblePointTypeNameConverter();
                var pts = (from p in points select new SerializiblePoint { X = p.X, Y = p.Y }).ToArray();
                bin.Serialize(stream, pts);
            }
        }

        private void CreateSessionIndicator(string spotFolder, string sessionName)
        {
            var xml = new XElement("ReprocessInfo", new XAttribute("sessionName", sessionName));
            xml.Save(Path.Combine(spotFolder, "reprocess.info"));
        }


        /// <summary>
        /// Get the regions for a session 
        /// </summary>
        /// <summary>
        /// Get the regions for a session 
        /// </summary>
        public IEnumerable<AnalysisRegion> GetAllAnalysisRegions(Session session)
        {
            List<AnalysisRegion> regions = new List<AnalysisRegion>();
            List<Frame> frames = GetFrames(session).ToList();
            //MARV- variable name SpotFolders ?
            if (session != null && frames != null)
            {
                var forScanArea = GetSpotFoldersForScanArea(session);
                foreach (var f in forScanArea)
                {
                    var regionFile = Directory.EnumerateFiles(f, "*.region").FirstOrDefault();
                    if (regionFile != null)
                    {
                        string frameId = string.Empty;
                        var frameIdFile = Path.Combine(f, "frame.Id");
                        if (File.Exists(frameIdFile))
                            frameId = File.ReadAllText(frameIdFile);
                        var frame = frames.Where(x => x.Id.ToString().ToLower() == frameId.ToLower()).FirstOrDefault();
                        if (frame == null)
                            continue;

                        AnalysisRegion region = null;
                        var pts = LoadRegion(regionFile);
                        region = new AnalysisRegion()
                        {
                            Id = new Guid(Path.GetFileNameWithoutExtension(regionFile)),
                            Points = pts.ToList(),
                            Frame = frame
                        };
                        regions.Add(region);
                    }
                }
            }
            return regions;
        }

        private IEnumerable<string> GetSpotFoldersForScanArea(Session session, string frameId = null)
        {
            IEnumerable<string> spotFolders = new List<string>();
            List<Frame> frames = null;
            var FolderList = GetFoldersForScanArea(session.Slide);
            if (frameId == null)
            {
                frames = GetFrames(session).ToList();
            }
            else
            {
                frames = GetFrames(session).Where(x => x.Id.ToString() == frameId).ToList();
            }
            if (frames != null)
            {
                spotFolders = from f in FolderList where IsFrameValid(f, session, frames) select f;
            }
            return spotFolders;
        }


        private IEnumerable<string> GetFoldersForScanArea(Slide slide)
        {
            var slidePath = GetSlidePath(slide);
            var spotFolders = Directory.EnumerateDirectories(slidePath, "spot*");
            var cellF = Directory.EnumerateDirectories(slidePath, "cell*");

            List<string> FolderList = new List<string>();
            foreach (string f in cellF)
                FolderList.Add(f);

            foreach (string f in spotFolders)
                FolderList.Add(f);

            return FolderList;
        }
        private bool IsFrameValid(string spotFolder, Session session, List<Frame> frames)
        {
            if (IsRegionSpotFolder(spotFolder))
            {
                if (!IsFromSession(spotFolder, session))
                    return false;
            }
            var path = Path.Combine(spotFolder, "frame.id");
            if (File.Exists(path))
            {
                var lines = File.ReadAllLines(path);
                if (lines.Length > 0)
                {
                    return frames.Where(frame => frame.Id.ToString().ToLower() == lines[0].ToLower()).Count() > 0;
                }
            }
            return false;
        }

        /// <summary>
        /// get the region points saved in .region file
        /// </summary>
        private IEnumerable<SerializiblePoint> LoadRegion(string path)
        {
            if (!File.Exists(path))
                return new List<SerializiblePoint>();
            using (Stream stream = File.Open(path, FileMode.Open))
            {
                BinaryFormatter bin = new BinaryFormatter();
                // When we use SerializationBinder, it calls the overriden BindToTypee method and change the assembly name and namespacce.
                // To support legacy data we are serializing in legacy format, so this is required.
                bin.Binder = new SerializiblePointTypeNameConverter(typeof(SerializiblePoint));
                var pts = (IEnumerable<SerializiblePoint>)bin.Deserialize(stream);
                return pts;
            }
        }

        /// <summary>
        /// Delete the regions 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="regions"></param>
        public void DeleteRegions(Session session, IEnumerable<string> regions)
        {
            try
            {
                if (regions != null && regions.Count() > 0)
                {
                    string slidePath = GetSlidePath(session.Slide);
                    foreach (var id in regions)
                    {
                        if (!string.IsNullOrWhiteSpace(id))
                        {
                            // if its a region delete spot folder, otherwise will appear in reprocessing
                            var path = Path.Combine(slidePath, "spot" + id);
                            SafeDirDelete(path);
                            FileInfo regionFile = new FileInfo(Path.Combine(slidePath, id + ".region"));
                            if (regionFile.Exists)
                            {
                                regionFile.Delete();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Unable Delete region error", ex, DALException.DALErrorType.Authorization);
            }
        }

        /// <summary>
        /// Get All annotation of frames 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="frames"></param>
        /// <returns></returns>
        public List<AnnotationIdentifier> GetAllFramesAnnotation(Session session, List<Frame> frames)
        {
            List<AnnotationIdentifier> framesAnnotation = new List<AnnotationIdentifier>();
            if (session != null && frames != null)
            {
                foreach (var frame in frames)
                {
                    string annotationPath = AnnotationXmlPath(session, frame.Id.ToString());
                    if (!File.Exists(annotationPath))
                        framesAnnotation.Add(new AnnotationIdentifier() { Identifier = frame.Id.ToString(), Annotations = new List<Annotation>() });
                    else
                    {
                        XElement xmlAnnotation = XElement.Load(annotationPath);
                        List<Annotation> annotations = new List<Annotation>(from a in xmlAnnotation.Elements("Annotation") select AnnotationFromNode(a));
                        framesAnnotation.Add(new AnnotationIdentifier() { Identifier = frame.Id.ToString(), Annotations = annotations });
                    }
                }
            }
            return framesAnnotation;
        }


        /// <summary>
        /// Check the region has anaysied by PCV Scanner
        /// </summary>
        /// <param name="analysisRegion"></param>
        /// <returns></returns>
        public List<Score> GetScoreByRegionAnalysis(IEnumerable<AnalysisRegion> analysisRegions)
        {
            List<Score> regionScores = new List<Score>();
            foreach (var analysisRegion in analysisRegions)
            {
                if (analysisRegion != null)
                {
                    string dirPath = Path.Combine(GetSlidePath(analysisRegion.Session.Slide), "spot" + analysisRegion.Id.ToString());
                    if (dirPath != null)
                    {
                        //check HasScores.CVS File exist indiates the completion of cvscanner
                        string hasScoreFile = Path.Combine(dirPath) + @"\HasScores.CVS";
                        if (File.Exists(hasScoreFile))
                        {
                            var xml = XElement.Load(dirPath + @"\.scores");
                            var scores = (from cellNode in xml.Descendants("ScoredCell")
                                          select ParseScoreFromXml(cellNode)).ToArray();
                            if (scores != null && scores.Count() > 0)
                            {
                                //assuming only one score has added by cvscanner
                                regionScores.Add(scores[0]);
                            }
                            else if (scores != null)
                            {
                                SafeDirDelete(dirPath);
                                //if scores  with no class indicates no score presnet  and region is processed by the cvcanner, so delete the region  
                                Score score = new Score()
                                {
                                    RegionId = analysisRegion.Id
                                };
                                regionScores.Add(score);
                            }
                        }
                    }
                }
            }
            // if regionScore null shows not proceesd by cvscanner and empty object indacates processed but not found any scores and new scores indactes scores found
            return regionScores;
            //TODO Reprocessing as in legacy PCV to check 
        }

        public bool SaveAnnotation(Session session, List<AnnotationIdentifier> frameAnnotations)
        {
            if (frameAnnotations != null)
            {
                foreach (var frameAnnotation in frameAnnotations)
                {
                    string annotationPath = AnnotationXmlPath(session, frameAnnotation.Identifier);
                    var xml = new XElement("Annotations", frameAnnotation.Annotations.ConvertAll<XElement>(a => new XElement("Annotation",
                                                                                                   new XAttribute("X", a.X),
                                                                                                   new XAttribute("Y", a.Y),
                                                                                                   new XAttribute("Scale", a.Scale),
                                                                                                   new XAttribute("ContentOffsetX", a.ContentOffsetX),
                                                                                                   new XAttribute("ContentOffsetY", a.ContentOffsetY),
                                                                                                   new XAttribute("ShowArrowLine", a.ShowArrowLine),
                                                                                                   new XCData(a.Content))));
                    if (File.Exists(annotationPath))
                    {
                        File.Delete(annotationPath);
                    }
                    xml.Save(annotationPath);
                }
            }
            return true;
        }

        public List<string> GetStoredCommonAnnotations()
        {
            string annotationsFilePath = Path.Combine(_aii_SharedPath, "pcv.annotations");
            List<string> allAnnotations = new List<string>();
            FileInfo annotationsFile = new FileInfo(annotationsFilePath);
            if (annotationsFile.Exists)
            {
                using (var stream = annotationsFile.OpenText())
                {
                    while (!stream.EndOfStream)
                    {
                        var annotation = stream.ReadLine();
                        allAnnotations.Add(annotation);
                    }
                }
            }
            return allAnnotations;
        }

        public void SaveCommonStoredAnnotation(List<string> annotationTexts)
        {
            if (annotationTexts != null)
            {
                string annotationsFilePath = Path.Combine(_aii_SharedPath, "pcv.annotations");
                FileInfo annotationsFile = new FileInfo(annotationsFilePath);
                if (annotationsFile.Exists)
                {
                    annotationsFile.Delete();
                }
                using (var stream = annotationsFile.Create())
                {
                    StreamWriter sw = new StreamWriter(stream);
                    foreach (var annotation in annotationTexts)
                        sw.WriteLine(annotation);

                    sw.Close();
                }
            }
        }

        /// <summary>
        /// Checks session exists in scanArea ot not
        /// </summary>
        /// <param name="session">new session</param>
        /// <returns></returns>
        private bool IsSessionExistInScanArea(Session session)
        {
            var updatedScanArea = UpdatedScanArea(session);
            return updatedScanArea.Sessions != null && updatedScanArea.Sessions.Any(s => s.Name == session.Name);
        }


        /// <summary>
        /// Gets the updated scanarea
        /// </summary>
        /// <param name="session"></param>
        /// <returns>updated scanArea</returns>
        private ScanArea UpdatedScanArea(Session session)
        {
            ScanArea selectedScanarea = new ScanArea(session.Slide, session.ScanAreaId, session.ScanAreaId);
            var slideFolder = GetSlidePath(session.Slide);
            if (Directory.Exists(slideFolder))
            {
                var scoringSessionFiles = Directory.EnumerateFiles(slideFolder, selectedScanarea.Name + SessionFilesLocator.SessionDelimiter + "*.scores");
                var sessionCollection = new List<Session>();
                Parallel.ForEach(scoringSessionFiles, (file) =>
                {
                    //Dual Score - Check if session is created with selected frames
                    bool isCreatedWithSelectedFrames = IsSelectFramesFileExists(session.Slide, selectedScanarea.Id, GetSessionNameFromScore(file));
                    var scanAreaSessions = new Session(session.Slide, GetSessionNameFromScore(file), GetSessionNameFromScore(file), selectedScanarea.Id, isCreatedWithSelectedFrames);
                    // Get Assay Name it is needed to check if assays in both the session is same or different.
                    scanAreaSessions.AssayName = GetDefaultAssayName(scanAreaSessions);
                    sessionCollection.Add(scanAreaSessions);
                });
                selectedScanarea.Sessions = sessionCollection;
                selectedScanarea.HasSpotScores = CheckSessionWithSpotScores(selectedScanarea);
            }
            return selectedScanarea;
        }

        /// <summary>
        /// Create a entry in db. creates session lock in db
        /// </summary>
        /// <param name="userDetails">logged in user</param>
        /// <param name="caseName">selected case</param>
        /// <param name="lockingObject">session</param>
        public void LockFeatureorObject(UserDetails userDetails, string caseName, object lockingObject)
        {
            var caseEntity = _db.cases.Where(c => c.name == caseName).FirstOrDefault();
            var session = lockingObject as Session;
            caseGenericLock caseGenericLockObj = new caseGenericLock()
            {
                LockID = Guid.NewGuid(),
                CaseID = caseEntity.caseId,
                Type = ModuleType,
                UserID = userDetails.UserID,
                SlideID = session.Slide.Id
            };
            _db.caseGenericLocks.Add(caseGenericLockObj);
            _db.SaveChanges();

            genericObjectLock sessionLockObj = new genericObjectLock()
            {
                GenericLockID = Guid.NewGuid(),
                LockId = caseGenericLockObj.LockID,
                Data = CreateSessionLock(session)
            };
            _db.genericObjectLocks.Add(sessionLockObj);
            _db.SaveChanges();
        }

        /// <summary>
        /// Removes session lock from db
        /// </summary>
        /// <param name="userDetails">logged in user</param>
        /// <param name="caseName">selected case</param>
        public void RemoveLock(UserDetails userDetails, string caseName)
        {
            var caseEntity = _db.cases.Where(c => c.name == caseName).FirstOrDefault();
            var caseSpecificLocks = _db.caseGenericLocks.Where(x => x.UserID == userDetails.UserID && x.CaseID == caseEntity.caseId && x.Type == ModuleType).ToList();
            if (caseSpecificLocks != null)
            {
                foreach (var caseLock in caseSpecificLocks)
                {
                    var genricLockEntity = _db.genericObjectLocks.Where(l => l.LockId == caseLock.LockID).FirstOrDefault();
                    if (genricLockEntity != null)
                    {
                        _db.genericObjectLocks.Remove(genricLockEntity);
                        _db.SaveChanges();
                    }
                    _db.caseGenericLocks.Remove(caseLock);
                    _db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// checks the db for session lock
        /// </summary>
        /// <param name="userDetails">logged in user</param>
        /// <param name="caseName">selected case</param>
        /// <param name="lockingObject">selected session</param>
        /// <returns></returns>
        public LockResult CheckKLockExists(UserDetails userDetails, string caseName, object lockingObject)
        {
            LockResult result = new LockResult();
            if (IsSessionExistInScanArea(lockingObject as Session))
            {
                var caseEntity = _db.cases.Where(c => c.name == caseName).FirstOrDefault();
                var caseLockCollection = _db.caseGenericLocks.Where(caselock => caselock.CaseID == caseEntity.caseId && caselock.Type == ModuleType).ToList();
                if (caseLockCollection.Count == 0)
                {
                    return result;
                }
                else
                {
                    foreach (var caseLock in caseLockCollection)
                    {
                        var sessionLock = _db.genericObjectLocks.Where(genericObj => genericObj.LockId == caseLock.LockID).FirstOrDefault();
                        if (sessionLock != null && lockingObject != null)
                        {
                            var session = lockingObject as Session;
                            if (IsSessionFoundinGenericLock(sessionLock.Data, session))
                            {
                                result.IsLocked = true;
                                var userObj = _db.userDetails.Where(u => u.UserID == caseLock.UserID).FirstOrDefault();
                                UserDetails _user = new UserDetails()
                                {
                                    EmailId = userObj.EmailID,
                                    FirstName = userObj.FirstName,
                                    IsActive = userObj.IsActive,
                                    LastAccessed = userObj.LastAccessed,
                                    LastName = userObj.LastName,
                                    MachineName = userObj.MachineName,
                                    UserID = userObj.UserID,
                                    Name = userObj.FirstName + " " + userObj.LastName
                                };
                                result.User = _user;
                                result.LockType = LockType.ObjectLock;
                                return result;
                            }
                        }
                    }
                    return result;
                }
            }
            else
            {
                result.IsLocked = true;
                result.LockType = LockType.FileNotFound;
                return result;
            }
        }

        /// <summary>
        /// xml string to generate session data lock
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        private string CreateSessionLock(Session session)
        {
            XDocument doc = new XDocument(new XElement("ScanArea", new XAttribute("Name", session.ScanAreaId)));
            XElement sessionTag = doc.Element("ScanArea");
            sessionTag.Add(new XElement("Session", session.Name));
            return doc.ToString();
        }

        /// <summary>
        /// iterate session xml to check session lock exists
        /// </summary>
        /// <param name="xmlData">session lock xml</param>
        /// <param name="session">selected session</param>
        /// <returns></returns>
        private bool IsSessionFoundinGenericLock(string xmlData, Session session)
        {
            XDocument doc = XDocument.Parse(xmlData);
            if (doc.Descendants("ScanArea") != null && doc.Descendants("ScanArea").FirstOrDefault().Attribute("Name").Value == session.ScanAreaId)
            {
                if (doc.Descendants("Session").FirstOrDefault().Value == session.Name)
                    return true;
            }
            return false;
        }



        #endregion
        #region Reprocessing

        /// <summary>
        /// Get the folders to to reprocessed
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public IEnumerable<string> GetSpotFolderNamesForReprocess(Session session, string frameId = null)
        {
            var forScanArea = GetSpotFoldersForScanArea(session, frameId);
            List<string> spotFolders = new List<string>();
            foreach (string folderName in forScanArea)
            {
                string path = Path.GetFileName(folderName);
                spotFolders.Add(path);
            }
            return spotFolders;
        }

        /// <summary>
        /// This function will remove HasScores.CVS files from spot folders and launch the PCVScanner
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public bool StartReprocessing(Session session, IEnumerable<string> spotFolders)
        {

            bool isReprocessStarted = false;
            try
            {
                var locator = GetLocator(session);
                List<Frame> frames = GetFrames(session).ToList();
                string dir = Path.Combine(GetSlidePath(session.Slide));
                if (spotFolders != null)
                {
                    foreach (var spotFolder in spotFolders)
                    {
                        string SpotFolderPath = Path.Combine(dir, spotFolder);
                        Frame frame = null;
                        var frameIdFile = Path.Combine(SpotFolderPath, "frame.Id");
                        if (File.Exists(frameIdFile))
                        {
                            var frameId = File.ReadAllText(frameIdFile);
                            frame = frames.Where(a => a.Id.ToString().ToLower() == frameId.ToLower()).FirstOrDefault();
                        }
                        if (frame == null)
                            continue;
                        //update the assay file
                        File.WriteAllLines(Path.Combine(SpotFolderPath, "assay.name"), new string[] { session.AssayName, session.ScanAreaId });

                        File.Delete(Path.Combine(SpotFolderPath, ".scores"));
                        File.Delete(Path.Combine(SpotFolderPath, "HasScores.CVS"));
                        File.Delete(Path.Combine(SpotFolderPath, "PROC.CVS"));


                    }
                    //launch PCV Scanner
                    //Get the first frame width and height to launch
                    var launcher = new PcvScannerController(_casebaseFolder,
                                                                         session.Slide.CaseId,
                                                                         session.Slide.Id,
                                                                         session.ScanAreaId,
                                                                         locator.AssayFile.Name,
                                                                         20.0, // from pcv, I guess we always analyse 20X
                                                                         frames[0].XScale,
                                                                         frames[0].YScale);
                    launcher.StartPcvScanner();

                    isReprocessStarted = true;
                }

            }
            catch (Exception ex)
            {
                throw new DALException("Unable to Reprocess error", ex, DALException.DALErrorType.Unknown);
            }
            return isReprocessStarted;
        }


        /// <summary>
        /// Polling the scores from reprocess
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public ReprocessScores GetScoresFromSpotFoldersProcessed(Session session, IEnumerable<string> spotFolders)
        {
            ReprocessScores reprocessScores = new ReprocessScores();
            List<Score> probeScores = new List<Score>();
            List<string> PendingSpotFolders = new List<string>();
            try
            {
                string dir = Path.Combine(GetSlidePath(session.Slide));
                if (spotFolders != null)
                {
                    foreach (var spotFolder in spotFolders)
                    {
                       
                        string SpotFolderPath = Path.Combine(dir, spotFolder);
                        if(Directory.Exists(SpotFolderPath))
                        {
                            string hasScoreFile = Path.Combine(SpotFolderPath) + @"\HasScores.CVS";
                            if (File.Exists(hasScoreFile))
                            {
                                string scoreFile = Path.Combine(SpotFolderPath) + @"\.scores";
                                if (File.Exists(scoreFile))
                                {
                                    var xml = XElement.Load(scoreFile);
                                    var scores = (from cellNode in xml.Descendants("ScoredCell")
                                                  select ParseScoreFromXml(cellNode)).ToArray();
                                    if (scores != null)
                                    {
                                        foreach (Score score in scores)
                                        {
                                            probeScores.Add(score);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //If Spot folders is not analysed by PCVScanner sending back.
                                PendingSpotFolders.Add(spotFolder);
                            }
                        }
                                   
                       
                    }
                    reprocessScores.Scores = probeScores;
                    reprocessScores.SpotFolders = PendingSpotFolders;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {

                throw new DALException("Get scores from spot Folder error", ex, DALException.DALErrorType.Unknown);
            }
            return reprocessScores;
        }

        /// <summary>
        /// Can create new session - gets the updated scan area and checks for session count and checks for session if its already exists.
        /// </summary>
        /// <param name="sessionName">new session name</param>
        /// <param name="slide">selected slide</param>
        /// <param name="scanAreaName">selected scanarea name</param>
        /// <returns></returns>
        public bool CanCreateNewSession(string sessionName, Slide slide, string scanAreaName)
        {
            Session session = new Session(slide, sessionName, sessionName, scanAreaName);
            var updatedScanArea = UpdatedScanArea(session);
            return updatedScanArea.Sessions != null && updatedScanArea.Sessions.Count < 2 && !updatedScanArea.Sessions.Any(s => s.Name == session.Name);
        }
        #endregion


    }
}






