﻿using System;
using System.Runtime.Serialization;

namespace ProbeCaseViewDataAccessLocal
{
    // Using SerializationBinder to support the region file created by Legacy PCV.
    // Since the legacy pcv is using Binary Formatter
    // If you serialize an object in one assembly and try to deserialize it from another assembly, 
    // you will get an error saying that the assembly in which the serialized object was declared cannot be found. 
    // You will get the same error even if both assemblies contain the class definition for the specified object.
    //REFACTOR, REVISIT - change the hardcoded strings. Need to Support Legacy PCV as well, regions created from CytoApps should be able to open using legacy pcv. 
    public class SerializiblePointTypeNameConverter : SerializationBinder
    {
        private readonly Type _type;

        public SerializiblePointTypeNameConverter()
        {
        }

        public SerializiblePointTypeNameConverter(Type type)
        {
            _type = type;
        }

        // Since we are saving assembly name "ProbeCaseView" and namespace AI.ProbeScore+SerializiblePoint while serializing the .region file,
        // and when deserializing we have to deserialize as our type, ie CytoApps namespace and assembly.
        public override Type BindToType(string assemblyName, string typeName)
        {
            return _type;
        }

        // While Serializing we are changing the assembly name and typeName to support legacy PCV.
        // the legacy PCV expect assembly name "ProbeCaseView" and namespace "AI.ProbeScore+SerializiblePoint" while deserializing the .region file.
        public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = "ProbeCaseView";
            typeName = "AI.ProbeScore+SerializiblePoint";
        }
    }
}
