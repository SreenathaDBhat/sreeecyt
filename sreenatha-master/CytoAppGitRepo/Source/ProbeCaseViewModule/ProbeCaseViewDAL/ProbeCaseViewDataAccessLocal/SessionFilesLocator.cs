﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProbeCaseViewDataAccessLocal
{
    // handy class to locate files, referenced off the scores filename
    public class SessionFilesLocator
    {
        public static string SessionDelimiter { get { return "$="; } }
        public static string ImageFramesExtension { get { return ".imageframes"; } }

        public Guid SlideId { get; set; }
        private FileInfo scoresFile;


        public SessionFilesLocator(string scoresFilePath)
        {
            ScoresFile = new FileInfo(scoresFilePath);
        }

        public static string GetScoresFileName(string scanareaName, string sessionName)
        {
            return scanareaName + SessionDelimiter + sessionName + ".scores";
        }

        public static string GetZeroScoresFileName(string scanareaName)
        {
            return scanareaName + ".scores";
        }

        public DirectoryInfo SlideFolder
        {
            get
            {
                return ScoresFile.Directory;
            }
        }

        public FileInfo ScoresFile
        {
            get
            {
                return scoresFile;
            }
            set
            {
                scoresFile = value;

                // parse for session name
                if (scoresFile != null)
                {
                    var name = Path.GetFileNameWithoutExtension(scoresFile.FullName);
                    var splits = name.Split(new string[] { SessionDelimiter }, StringSplitOptions.None);
                    SessionName = (splits.Count() > 1) ? SessionName = splits[1] : "";
                }
            }
        }

        //private FileInfo ScoresFile
        //{
        //    get
        //    {
        //        return scoresFile;
        //    }
        //    set
        //    {
        //        scores = value;

        //        var folder = scoresFile.Directory;
        //        string slash = folder.FullName.EndsWith("\\") ? "" : "\\";
        //        FileInfo nameFile = new FileInfo(folder.FullName + slash + ".name");
        //        if (nameFile.Exists)
        //        {
        //            using (var reader = new StreamReader(nameFile.FullName, Encoding.Default))
        //                SlideName = reader.ReadToEnd();
        //        }
        //        else
        //            SlideName = folder.Name;

        //        var sArea =
        //        ScanAreaName = Path.GetFileNameWithoutExtension(imageFramesFile.FullName);
        //        SessionDisplayName = SlideName + ": " + ScanAreaName;

        //        // files coming out of manual capture are called imageframes.imageframes. not useful.
        //        if (string.Compare(ScanAreaName, "imageframes", StringComparison.CurrentCultureIgnoreCase) == 0)
        //        {
        //            ScanAreaName = string.Empty;
        //            SessionDisplayName = SlideName;
        //        }
        //    }
        //}


        //}

        public FileInfo AutoScoresAreModifiedFile
        {
            get { return new FileInfo(ScoresFile.FullName.Replace(".scores", ".autoscoresaremodified")); }
        }

        public FileInfo MultiSessionsBlockedFile
        {
            get { return new FileInfo(ScoresFile.FullName.Replace(".scores", ".multisessionblock")); }
        }

        public FileInfo ResultsAssayFile
        {
            get { return new FileInfo(ScoresFile.FullName.Replace(".scores", ".scoresassay")); }
        }

        public FileInfo FrameDataFile
        {
            get { return new FileInfo(ScoresFile.FullName.Replace(".scores", ".frameinfo")); }
        }

        public FileInfo OverlaysFile
        {
            get { return new FileInfo(ScoresFile.FullName.Replace(".scores", ".overlays")); }
        }

        public FileInfo AssayFile
        {
            get { return new FileInfo(ScoresFile.FullName.Replace(".scores", ".assay")); }
        }

        public FileInfo SessionInfo
        {
            get { return new FileInfo(ScoresFile.FullName.Replace(".scores", ".sessioninfo")); }
        }

        public FileInfo MeasurementsFile
        {
            get { return new FileInfo(ScoresFile.FullName.Replace(".scores", ".txt")); }
        }

        /// <summary>
        /// .selectframes file for dual score session.
        /// </summary>
        public FileInfo SelectFramesFile
        {
            get { return new FileInfo(ScoresFile.FullName.Replace(".scores", ".selectframes")); }
        }

        // flag folder to request pcvscanner.exe to exit
        public FileInfo RequestEndFile
        {
            get { return new FileInfo(Path.Combine(SlideFolder.FullName, PcvScannerLauncher.GetSessionId(SlideFolder.Parent.Name, SlideFolder.Name, AssayFile.Name) + ".END")); }
        }

        public string SessionName { get; private set; }
        public string SlideName { get; private set; }
        public string ScanAreaName { get; private set; }
        public string SessionDisplayName { get; private set; }
    }
}
