﻿using ProbeCaseViewDataAccess;
using System;
using System.Collections.Generic;
using ProbeCaseViewDataAccess.Models;
using PCVImageRenderers.Models;
using CytoApps.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml.Linq;

namespace ProbeCaseViewDataAccessRemote
{
    public class ProbeCaseViewDataAccessRemote : IProbeCaseViewDataAccess, ILocking
    {
        private string _baseUrl;

        public ProbeCaseViewDataAccessRemote(string baseUrl)
        {
            _baseUrl = baseUrl;
        }

        public bool CanAnalyseSlide(Slide slide)
        {
            using (var client = ConfigureClient())
            {
                string restUrl = string.Format("api/PCV/Cases/{0}/Slides/{1}/CanAnalyse",
                    slide.CaseId, slide.Id);

                HttpResponseMessage response = client.GetAsync(restUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var canAnalyse = response.Content.ReadAsAsync<bool>().Result;
                    return canAnalyse;
                }
            }
            return false;
        }

        public IEnumerable<Frame> GetFrames(ScanArea scanArea)
        {
            using (var client = ConfigureClient())
            {
                string restUrl = string.Format("api/PCV/Cases/{0}/Slides/{1}/ScanAreas/{2}/Frames",
                    scanArea.Slide.CaseId, scanArea.Slide.Id, scanArea.Id);

                HttpResponseMessage response = client.GetAsync(restUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var frames = response.Content.ReadAsAsync<IEnumerable<Frame>>().Result;
                    return frames;
                }
            }
            return null;
        }

        public byte[] GetOverlayData(Session session, Score score, Overlay overlay)
        {
            using (var client = ConfigureClient())
            {
                string restUrl = string.Format("api/PCV/Cases/{0}/Slides/{1}/ScanAreas/{2}/Sessions/{3}/Scores/{4}/Overlays/{5}",
                    session.Slide.CaseId, session.Slide.Id, session.ScanAreaId, session.Id, score.Id.ToString(), overlay.ID);

                HttpResponseMessage response = client.GetAsync(restUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var imageData = response.Content.ReadAsAsync<byte[]>().Result;
                    return imageData;
                }
            }
            return null;
        }

        public List<ScanArea> GetScanAreas(Slide slide)
        {
            using (var client = ConfigureClient())
            {
                string restUrl = string.Format("api/PCV/Cases/{0}/Slides/{1}/ScanAreas", slide.CaseId, slide.Id);
                HttpResponseMessage response = client.GetAsync(restUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var scanAreas = response.Content.ReadAsAsync<List<ScanArea>>().Result;
                    return scanAreas;
                }
            }
            return null;
        }

        public SessionScores GetScores(Session session)
        {
            using (var client = ConfigureClient())
            {
                string restUrl = string.Format("api/PCV/Cases/{0}/Slides/{1}/ScanAreas/{2}/Sessions/{3}/Scores",
                                            session.Slide.CaseId, session.Slide.Id, session.ScanAreaId, session.Id);
                HttpResponseMessage response = client.GetAsync(restUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var sessionScores = response.Content.ReadAsAsync<SessionScores>().Result;
                    return sessionScores;
                }
            }
            return null;
        }

        public byte[] GetThumbnailData(Session session, Score score)
        {
            using (var client = ConfigureClient())
            {
                string restUrl = string.Format("api/PCV/Cases/{0}/Slides/{1}/ScanAreas/{2}/Sessions/{3}/Scores/{4}/Thumbnail",
                    session.Slide.CaseId, session.Slide.Id, session.ScanAreaId, session.Id, score.Id.ToString());

                HttpResponseMessage response = client.GetAsync(restUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var imageData = response.Content.ReadAsAsync<byte[]>().Result;
                    return imageData;
                }
            }
            return null;
        }

        public byte[] GetAssayData(Session session)
        {
            using (var client = ConfigureClient())
            {
                string restUrl = string.Format("api/PCV/Cases/{0}/Slides/{1}/ScanAreas/{2}/Sessions/{3}/AssayData",
                    session.Slide.CaseId, session.Slide.Id, session.ScanAreaId, session.Id);

                HttpResponseMessage response = client.GetAsync(restUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var assayData = response.Content.ReadAsAsync<byte[]>().Result;
                    return assayData;
                }
            }
            return null;
        }

        public AssayInfoWrapper GetClassifierNamesAndScripts(Session session = null)
        {
            throw new NotImplementedException();
        }

        public Score[] GetScoreStats(Session session)
        {
            using (var client = ConfigureClient())
            {
                string restUrl = string.Format("api/PCV/Cases/{0}/Slides/{1}/ScanAreas/{2}/Sessions/{3}/Scores",
                                            session.Slide.CaseId, session.Slide.Id, session.ScanAreaId, session.Id);
                HttpResponseMessage response = client.GetAsync(restUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var sessionScores = response.Content.ReadAsAsync<Score[]>().Result;
                    return sessionScores;
                }
            }
            return null;
        }

        public byte[] GetSpotScriptData(string scriptName)
        {
            throw new NotImplementedException();
        }

        public byte[] GetFrameComponentData(Slide slide, string frameId, string componentId)
        {
            throw new NotImplementedException();
        }

        public byte[] GetFrameBitmap(Slide slide, string scanAreaId, Frame frame, IEnumerable<ComponentLevels> existingLevels, int requestedZ, FrameRenderSettings options)
        {
            throw new NotImplementedException();
        }

        private HttpClient ConfigureClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(_baseUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
            //                             BearerToken.AccessToken);

            return client;
        }

        public string GetRenderInstructionCellInfo(Session selectedSession, string scoreID)
        {
            throw new NotImplementedException();
        }

        public string GetEnhancedCellImageInfo(Session selectedSession, string scoreID)
        {
            throw new NotImplementedException();
        }

        public bool WriteRenderInstructionToEnhancedCell(Session selectedSession, byte[] bmp, string scoreID, XElement renderInstructionForBmp)
        {
            throw new NotImplementedException();
        }

        public void SaveSession(Session session, List<ScoreSettings> scoreSettingsList, SessionScores scores)
        {
            throw new NotImplementedException();
        }

        public void DeleteScores(Session session, IEnumerable<string> scores)
        {
            throw new NotImplementedException();
        }

        public List<ScoreSettings> LoadScoredCellsInfo(Session session, string scanAreaName)
        {
            throw new NotImplementedException();
        }

        public byte[] GetThumbnailBitmap(Session session, Score score, int stackIndex, IEnumerable<ChannelDisplay> channelDisplay, bool attenuateCounterstain, bool invertCounterstain, bool isTemporarySaved)
        {
            throw new NotImplementedException();
        }

        //public bool SaveThumbnailData(Session session, Score score, byte[] data)
        //{
        //    throw new NotImplementedException();
        //}

        public void ClearTemporarySavedThumb(Session session)
        {
            throw new NotImplementedException();
        }

        public byte[] CreateThumbnailBitmap(ScoreThumbnail scoreThumbnail, Session session)
        {
            throw new NotImplementedException();
        }

        public Session CreateNewSession(ScanArea scanArea, string sessionName, bool useSpotScores, bool isCreatedWithSelectedFrames = false, List<string> selectedFrameIdList = null)
        {
            throw new NotImplementedException();
        }

        public void DeleteSession(Session session)
        {
            throw new NotImplementedException();
        }

        public byte[] GetCachedFrameBitmap(string caseId, string slideId, string scanAreaId, string frameId)
        {
            throw new NotImplementedException();
        }

        public List<string> GetSelectedFramesForSession(Session session)
        {
            throw new NotImplementedException();
        }

        public long GetFrameCellCount(ScanArea scanArea, string frameId)
        {
            throw new NotImplementedException();
        }

        public void UpdateScores(Session session, List<string> frameIdList)
        {
            throw new NotImplementedException();
        }

        public byte[] GetSlideOverviewImage(Slide slide, string etchImage)
        {
            throw new NotImplementedException();
        }

        public List<FrameOutline> GetFrameOutlines(Slide slide)
        {
            throw new NotImplementedException();
        }
        public List<string> GetCompatibleAssays(FrameComponent[] component)
        {
            throw new NotImplementedException();
        }

        public byte[] GetDefaultAssayData(string assayName)
        {
            throw new NotImplementedException();
        }

        public void SaveAsDefaultAssay(byte[] AssayData, string assayName)
        {
            throw new NotImplementedException();
        }

        public List<string> GetAllDefaultAssays()
        {
            throw new NotImplementedException();
        }

        public void QueueRegionForAnalysis(AnalysisRegion analysisRegion)
        {
            throw new NotImplementedException();
        }

        public void DeleteRegions(Session session, IEnumerable<string> regions)
        {
            throw new NotImplementedException();
        }

        public List<AnnotationIdentifier> GetAllFramesAnnotation(Session session, List<Frame> frames)
        {
            throw new NotImplementedException();
        }

        public List<Score> GetScoreByRegionAnalysis(IEnumerable<AnalysisRegion> analysisRegions)
        {
            throw new NotImplementedException();
        }

        public bool SaveAnnotation(Session session, List<AnnotationIdentifier> frameAnnotations)
        {
            throw new NotImplementedException();
        }

        public List<string> GetStoredCommonAnnotations()
        {
            throw new NotImplementedException();
        }

        public void SaveCommonStoredAnnotation(List<string> annotationTexts)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AnalysisRegion> GetAllAnalysisRegions(Session session)
        {
            throw new NotImplementedException();
        }

        public void SaveAssayData(byte[] assayData, Session session)
        {
            throw new NotImplementedException();
        }

        public byte[] GetAssayDataAndCreateBackup(Session session)
        {
            throw new NotImplementedException();
        }

        public void RestoreAssayData(Session session)
        {
            throw new NotImplementedException();
        }

        public bool Initialize(DataSource dataSource)
        {
            throw new NotImplementedException();
        }

        public LockResult CheckKLockExists(UserDetails userDetails, string caseName, object lockingObject)
        {
            throw new NotImplementedException();
        }

        public void LockFeatureorObject(UserDetails userDetails, string caseName, object lockingObject)
        {
            throw new NotImplementedException();
        }

        public void RemoveLock(UserDetails userDetails, string caseName)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<string> GetSpotFolderNamesForReprocess(Session session, string frameId = null)
        {
            throw new NotImplementedException();
        }

        public bool StartReprocessing(Session session, IEnumerable<string> spotFolders)
        {
            throw new NotImplementedException();
        }

        public ReprocessScores GetScoresFromSpotFoldersProcessed(Session session, IEnumerable<string> spotFolders)
        {
            throw new NotImplementedException();
        }

        public bool CanCreateNewSession(string sessionName, Slide slide, string scanAreaName)
        {
            throw new NotImplementedException();
        }

        public void RestoreSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions)
        {
            throw new NotImplementedException();
        }
        public void DeleteNewScoresAndRegionsFromSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions)
        {
            throw new NotImplementedException();
        }
    }
}
