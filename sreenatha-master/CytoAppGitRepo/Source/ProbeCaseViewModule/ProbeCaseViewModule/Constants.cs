﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProbeCaseViewModule
{
    public static class Constants
    {
        public const string AllProbes = "All Probes";
        public const string Uninformative = "uninformative";
        public const string Fusions = "Fusions";
        public const int RequiredRAMMemory = 2000000000;
        public const int MinDecodeWidth = 128;
        public const int MaxDecodeWidth = 512;
        public const int DefaultDecodeWidth = 256;
        public const double CircleDiameterPixels = 100;
    }
}
