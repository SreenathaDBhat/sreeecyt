﻿using EnhancementControl.Converters;
using ProbeCaseViewModule.Logic;
using ProbeCaseViewModule.Models;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ProbeCaseViewModule.Controls
{
    /// <summary>
    /// Annotation Control used for describing/Commenting
    /// Interaction logic for AnnotationControl.xaml
    /// </summary>
    public partial class AnnotationControl : UserControl
    {
        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(AnnotationControl));
        public static readonly DependencyProperty AnnotationProperty = DependencyProperty.Register("Annotation", typeof(FrameAnnotation), typeof(AnnotationControl));
        public static readonly DependencyProperty AnnotationContainerProperty = DependencyProperty.Register("AnnotationContainer", typeof(IAnnotationContainer), typeof(AnnotationControl));
        public static readonly DependencyProperty StalkRootPanScaleFactorProperty = DependencyProperty.Register("StalkRootPanScaleFactor", typeof(double), typeof(AnnotationControl));

        private Point? drag;
        private Window rootHack;

        public AnnotationControl()
        {
            InitializeComponent();
            rootHack = VisualTreeWalker.FindParentOfType<Window>(this);
        }

        public bool IsReadOnly
        {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }

        public FrameAnnotation Annotation
        {
            get { return (FrameAnnotation)GetValue(AnnotationProperty); }
            set { SetValue(AnnotationProperty, value); }
        }

        public IAnnotationContainer AnnotationContainer
        {
            get { return (IAnnotationContainer)GetValue(AnnotationContainerProperty); }
            set { SetValue(AnnotationContainerProperty, value); }
        }

        public double StalkRootPanScaleFactor
        {
            get { return (double)GetValue(StalkRootPanScaleFactorProperty); }
            set { SetValue(StalkRootPanScaleFactorProperty, value); }
        }

        private void OnAnnotationLoadedFocusMe(object sender, RoutedEventArgs e)
        {
            // Either leave this out, or, put an event in to get the annotation control
            // to give focus to the text box...

            //TextBox text = (TextBox)sender;
            //Keyboard.Focus(text);
            //text.SelectAll();
        }

        private void OnDeleteAnnotation(object sender, RoutedEventArgs e)
        {
            AnnotationContainer.DeleteAnnotation((FrameAnnotation)((FrameworkElement)sender).Tag);
        }

        private void OnScaleMouseDown(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement s = (FrameworkElement)sender;
            if (!s.IsMouseCaptured)
            {
                s.CaptureMouse();
                drag = e.GetPosition(rootHack);
            }
        }

        private void OnScaleDrag(object sender, MouseEventArgs e)
        {
            if (drag != null)
            {
                FrameworkElement s = (FrameworkElement)sender;
                Point pos = e.GetPosition(rootHack);
                double delta = pos.Y - drag.Value.Y;
                double scale = Annotation.Scale + delta * -0.03;
                scale = Math.Max(0.5, Math.Min(4, scale));
                Annotation.Scale = scale;
                drag = pos;
            }
        }

        private void OnScaleMouseUp(object sender, MouseButtonEventArgs e)
        {
            ((FrameworkElement)sender).ReleaseMouseCapture();
            drag = null;
        }

        private void OnPanDrag(object sender, MouseEventArgs e)
        {
            if (drag != null)
            {
                FrameworkElement s = (FrameworkElement)sender;
                Point pos = e.GetPosition(rootHack);
                double deltaY = pos.Y - drag.Value.Y;
                double deltaX = pos.X - drag.Value.X;
                Annotation.ContentOffsetX += deltaX;
                Annotation.ContentOffsetY += deltaY;
                drag = pos;
            }
        }

        private void OnPanStalkRootDrag(object sender, MouseEventArgs e)
        {
            if (drag != null)
            {
                FrameworkElement s = (FrameworkElement)sender;
                Point pos = e.GetPosition(rootHack);
                double deltaY = (pos.Y - drag.Value.Y) / StalkRootPanScaleFactor;
                double deltaX = (pos.X - drag.Value.X) / StalkRootPanScaleFactor;
                Annotation.X += deltaX;
                Annotation.Y += deltaY;
                drag = pos;
            }
        }

        private void OnPanStalkRootMouseDown(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement s = (FrameworkElement)sender;
            if (!s.IsMouseCaptured)
            {
                s.CaptureMouse();
                drag = e.GetPosition(rootHack);
            }
        }

        private void OnPanStalkRootMouseUp(object sender, MouseButtonEventArgs e)
        {
            ((FrameworkElement)sender).ReleaseMouseCapture();
            drag = null;
        }
    }
}
