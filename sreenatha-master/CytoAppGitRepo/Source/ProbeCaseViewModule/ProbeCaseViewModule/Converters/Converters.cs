﻿using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.UI.Utilities;
using PCVImageRenderers.Models;
using ProbeCaseViewDataAccess.Models;
using ProbeCaseViewModule.Models;
using ProbeCaseViewModule.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ProbeCaseViewModule.Converters
{
    /// <summary>
    /// This converter class is used to remove Uniformative cell from main collection
    /// </summary>
    public class UninformativeToInformativeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ObservableCollection<ClassFilter> informativeClassFilters = new ObservableCollection<ClassFilter>();
            if (value != null)
            {
                foreach (ClassFilter ClassFilter in (ObservableCollection<ClassFilter>)value)
                {
                    if (!ClassFilter.ClassName.Equals(Constants.Uninformative, StringComparison.OrdinalIgnoreCase))
                        informativeClassFilters.Add(ClassFilter);
                }
            }
            return informativeClassFilters;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// This converter class is used to add All Probes as a godchannel in channelCollection
    /// </summary>
    public class GodChannelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ObservableCollection<ChannelDisplayInfo> godChannels = new ObservableCollection<ChannelDisplayInfo>();
            if (value != null)
            {
                var channels = (IEnumerable<ChannelDisplayInfo>)value;
                godChannels.Add(new ChannelDisplayInfo(new PCVImageRenderers.Models.ChannelDisplay() { Name = Constants.AllProbes }));
                foreach (ChannelDisplayInfo channel in channels)
                {
                    if (!channel.IsCounterstain)
                        godChannels.Add(channel);
                }
            }
            return godChannels;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// This converter class is used convert radio button checked value to Respective grid size value
    /// </summary>
    public class GridSizeRadioConverter : IValueConverter
    {
        public int GridSize
        {
            get; set;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int gridSize = (int)value;
            return gridSize == GridSize;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isChecked = (bool)value;
            return isChecked ? GridSize : 0;
        }
    }

    /// <summary>
    /// This converter class is used for Double Constant Multipling
    /// </summary>
    public class DoubleConstantMultiplierConverter : IValueConverter
    {
        public double Multiplier
        {
            get; set;
        }

        public DoubleConstantMultiplierConverter()
        {
            Multiplier = 1;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
            {
                return ((int)value) * Multiplier;
            }
            else if (value is float)
            {
                return ((float)value) * Multiplier;
            }
            else
            {
                return ((double)value) * Multiplier;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// This converter class is used to convert MouseEventargs to HistogramMouseEventArgs
    /// </summary>
    public class HistogramMouseEventArgsConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            var args = (MouseEventArgs)value;
            var element = (FrameworkElement)parameter;
            var point = args.GetPosition(element);
            HistogramMouseEventArgs histogramArgs = new HistogramMouseEventArgs
            {
                Point = point,
                ComponentLevels = element.Tag as ComponentLevels

            };
            return histogramArgs;
        }
    }

    public class MouseClickEventArgsConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            var args = (MouseButtonEventArgs)value;
            var element = (FrameworkElement)parameter;
            var point = args.GetPosition(element);
            MouseClickEventArgs mouseClickEventArgs = new MouseClickEventArgs
            {
                ClickCount = args.ClickCount,
                Points = point
            };
            return mouseClickEventArgs;
        }
    }

    //RVCMT-HA - add description / comment. 
    //RVCMT-HA - Clarify with Rizwan for MousePO\oints. ( there is property MousePosition in FrameViwModel. )
    public class AddAnnotationEventConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            AnnotationEventArgs annotationEventAgrs;
            var args = (MouseButtonEventArgs)value;
            var element = (FrameworkElement)parameter;
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                var controlPosition = args.GetPosition(element);
                var windowPosition = new Point();
                var window = Window.GetWindow(element);
                if (window != null)
                {
                    windowPosition = args.GetPosition(window);
                }
                element.CaptureMouse();
                annotationEventAgrs = new AnnotationEventArgs
                {
                    CurrentMousePosition = controlPosition,
                    IsMouseCapture = element.IsMouseCaptureWithin,
                    WindowMousePosition = windowPosition,
                    IsValid = true
                };
            }
            else
            {
                annotationEventAgrs = new AnnotationEventArgs
                {
                    IsValid = false,
                    CurrentMousePosition = new Point(),
                    WindowMousePosition = new Point(),
                    IsMouseCapture = false
                };
                Keyboard.Focus(element);
            }
            return annotationEventAgrs;
        }
    }

    public class MoveAnnotationEventConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            var args = (MouseEventArgs)value;
            var element = (FrameworkElement)parameter;
            var controlPosition = args.GetPosition(element);
            var windowPosition = new Point();
            var window = Window.GetWindow(element);
            if (window != null)
            {
                windowPosition = args.GetPosition(window);
            }
            AnnotationEventArgs annotationEventAgrs = new AnnotationEventArgs
            {
                CurrentMousePosition = controlPosition,
                IsMouseCapture = element.IsMouseCaptured,
                WindowMousePosition = windowPosition
            };
            return annotationEventAgrs;
        }
    }

    public class AnnotationMouseUpConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            var args = (MouseButtonEventArgs)value;
            var element = (FrameworkElement)parameter;
            element.ReleaseMouseCapture();
            return args;
        }
    }

    public class CurrentKeyPressedConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            var keyArgs = (KeyEventArgs)value;
            return keyArgs.Key;
        }
    }

    public class MouseWheelToDeltaConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            var mouseWheelArgs = (MouseWheelEventArgs)value;
            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                mouseWheelArgs.Handled = true;
            }
            return Math.Sign(mouseWheelArgs.Delta);
        }
    }

    /// <summary>
    /// This converter class is used to Show Frames Display
    /// </summary>
    public class ShowFramesDisplay : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool a = values[0] == DependencyProperty.UnsetValue ? false : (bool)values[0];
            bool b = values[1] == DependencyProperty.UnsetValue ? false : (bool)values[1];

            return a || b ? Visibility.Visible : Visibility.Hidden;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// This converter class is used in Frame Scoring
    /// </summary>
    public class FrameScoreConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue)
            {
                return null;
            }

            IEnumerable<Score> results = (IEnumerable<Score>)values[0];
            FrameInfo frame = (FrameInfo)values[1];

            if (null == results || null == frame)
            {
                return null;
            }

            var all = results.Where(r => r.ImageFrameId == frame.Image.Id).ToList().ConvertAll(r => (Color)ColorConverter.ConvertFromString(r.ColourString));
            var distinct = all.Distinct().ToList();
            return distinct;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// This converter class is used to convert key to string
    /// </summary>
    public class KeyToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value.ToString() == "None")
                return string.Empty;
            return ((Key)value).ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// This Behavior class is used set first item in the combobox as selected item when itemsource set through some converter 
    /// </summary>
    public static class ItemSelectionBehavior
    {
        public static bool GetIsFirstItemSelectProperty(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsFirstItemSelectProperty);
        }

        public static void SetIsFirstItemSelectProperty(DependencyObject obj, bool value)
        {
            obj.SetValue(IsFirstItemSelectProperty, value);
        }

        // Only Applicable to ComboBox Control 
        public static readonly DependencyProperty IsFirstItemSelectProperty =
            DependencyProperty.RegisterAttached("IsFirstItemSelect", typeof(bool), typeof(ItemSelectionBehavior),
                new UIPropertyMetadata(false, OnComboBoxItemSourceChanged));

        private static void OnComboBoxItemSourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as ComboBox;
            if (control == null)
                return;

            if (e.NewValue is bool == false)
                return;

            if ((bool)e.NewValue)
                control.Loaded += OnComboBoxLoad;
        }

        private static void OnComboBoxLoad(object sender, RoutedEventArgs e)
        {
            // Only react to the ComboBox Onload Event
            // And Check wheather it hasItems then set selection to first item. 
            if (!Object.ReferenceEquals(sender, e.OriginalSource))
                return;
            ComboBox combo = e.OriginalSource as ComboBox;
            if (combo != null)
                if (combo.HasItems)
                    combo.SelectedIndex = 0;
        }
    }

    public class BrushAlternator : IValueConverter
    {
        private int n = 0;
        private SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(32, 255, 255, 255));

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return n++ % 2 == 0 ? Brushes.Transparent : brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ScoreOpacityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool showAll = values[0] == DependencyProperty.UnsetValue ? false : (bool)values[0];
            bool mouseOver = values[1] == DependencyProperty.UnsetValue ? false : (bool)values[1];
            bool highlighted = values[2] == DependencyProperty.UnsetValue ? false : (bool)values[2];

            return (showAll || mouseOver || (highlighted && showAll)) ? 1.0 : 0.0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class FrameOutlineVisibility : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length < 2)
                return Visibility.Visible;

            var zoom = (double)values[0];
            var frameOutline = (FrameOutlineInfo)values[1];

            if (frameOutline == null)
                return Visibility.Visible;

            return (zoom > 16.0) ? Visibility.Hidden : Visibility.Visible;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ScorePositionX : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double x = 0.0;
            var score = (Score)values[0];
            var controller = (SlideOverviewViewModel)values[1];
            if (score != null && controller != null)
            {
                var frameinfo = controller.SlideOverViewFrames.Where(f => f.Frame.Id == score.ImageFrameId).FirstOrDefault();
                var leftside = frameinfo.Frame.IdealX + ((frameinfo.Frame.Width * frameinfo.Frame.XScale) / 2);
                x = (leftside - (score.X * frameinfo.Frame.XScale)) * controller.ScaleX;
                x -= (controller.ScoresDiameter / 2);
            }
            return x;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ScorePositionY : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double y = 0.0;
            var score = (Score)values[0];
            var controller = (SlideOverviewViewModel)values[1];
            if (score != null && controller != null)
            {
                var frameinfo = controller.SlideOverViewFrames.Where(f => f.Frame.Id == score.ImageFrameId).FirstOrDefault();
                var topside = frameinfo.Frame.IdealY + ((frameinfo.Frame.Height * frameinfo.Frame.YScale) / 2);
                y = (topside - (score.Y * frameinfo.Frame.YScale)) * controller.ScaleY;
                y -= (controller.ScoresDiameter / 2); // centre
            }
            return y;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MakeColorOpaque : IValueConverter
    {
        public double Amount
        {
            get; set;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var color = ColorParse.HexStringToColor(value.ToString());
            color.A = (byte)(255 * Amount);
            return color;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class RotateTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var angle = (double)value;

            return angle == 180 ? 0 : 180;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BlindScoredFrameVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var s = values[0] as PCVImageRenderers.Models.Frame;
            var b = values[1] as IEnumerable<ScoredCell>; //values[1] as IEnumerable<BlindScoreInfo>;
            if (s == null || b == null)
                return Visibility.Hidden;

            var visible = b.Where(a => a.Score.ImageFrameId == s.Id).Count() > 0;
            return (visible) ? Visibility.Visible : Visibility.Hidden;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class AnnotationControlControlsVisible : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool a = values[0] == DependencyProperty.UnsetValue ? false : (bool)values[0];
            bool b = values[1] == DependencyProperty.UnsetValue ? false : (bool)values[1];
            bool c = values[2] == DependencyProperty.UnsetValue ? false : (bool)values[2];

            return a || b || c ? Visibility.Visible : Visibility.Hidden;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public class RegionMouseDownEventConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            RegionEventArgs regionEventArgs = null;
            var args = (MouseButtonEventArgs)value;
            var element = (FrameworkElement)parameter;
            var controlPosition = args.GetPosition(element);

            if (!Keyboard.IsKeyDown(Key.RightCtrl) && !Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                element.CaptureMouse();
                regionEventArgs = new RegionEventArgs()
                {
                    CurrentMousePosition = controlPosition,
                    IsMouseCapture = element.IsMouseCaptureWithin,
                    isDrawing = true
                };
            }
            return regionEventArgs;
        }
    }

    public class RegionMouseMoveEventConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            RegionEventArgs regionEventArgs;
            var args = (MouseEventArgs)value;
            var element = (FrameworkElement)parameter;
            var controlPosition = args.GetPosition(element);

            regionEventArgs = new RegionEventArgs()
            {
                CurrentMousePosition = controlPosition,
                IsMouseCapture = element.IsMouseCaptureWithin,
                isDrawing = true,
                Width = element.ActualWidth,
                Height = element.ActualHeight
            };

            return regionEventArgs;
        }
    }

    public class RegionMouseUpEventConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            bool isShiftPressed = false;

            RegionEventArgs regionEventArgs;
            var args = (MouseButtonEventArgs)value;
            var element = (FrameworkElement)parameter;
            var controlPosition = args.GetPosition(element);

            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                isShiftPressed = true;

                if (controlPosition.X > element.ActualWidth)
                    isShiftPressed = false;
                else if (controlPosition.Y > element.ActualHeight)
                    isShiftPressed = false;
                if (controlPosition.X < 0)
                    isShiftPressed = false;
                else if (controlPosition.Y < 0)
                    isShiftPressed = false;
            }
            element.ReleaseMouseCapture();
            regionEventArgs = new RegionEventArgs()
            {
                CurrentMousePosition = controlPosition,
                isDrawing = true,
                IsShiftPressed = isShiftPressed
            };
            return regionEventArgs;
        }
    }

    /// <summary>
    /// Converts a Visual to BitmapSource. ( Every element that we place in WPF is inherited from a Visual )
    /// </summary>
    public class VisualToBitmapSourceConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            Border printVisual = parameter as Border;
            if (null == printVisual)
            {
                return null;
            }

            RenderTargetBitmap render = new RenderTargetBitmap(
                   (int)(printVisual.ActualWidth),
                   (int)(printVisual.ActualHeight),
                   96, 96, PixelFormats.Pbgra32);

            Border background = new Border();
            background.Background = Brushes.White;
            background.Width = printVisual.ActualWidth;
            background.Height = printVisual.ActualHeight;
            background.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            background.Arrange(new Rect(new Point(0, 0), background.DesiredSize));

            render.Render(background);
            render.Render(printVisual);
            render.Freeze();
            return render;
        }
    }
}