﻿using CytoApps.Infrastructure.UI.Utilities;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace ProbeCaseViewModule.Converters
{
    /// <summary>
    /// This class is used set the focus to UIelemnt after button click
    /// ButtonClickFocusAttachment.ElementToFocus="{Binding ElementName="UIElement"}"
    /// </summary>
    public class ButtonClickFocusAttachment
    {
        public static Control GetElementToFocus(Button button)
        {
            return (Control)button.GetValue(ElementToFocusProperty);
        }

        public static void SetElementToFocus(Button button, Control value)
        {
            button.SetValue(ElementToFocusProperty, value);
        }

        public static readonly DependencyProperty ElementToFocusProperty =
            DependencyProperty.RegisterAttached("ElementToFocus", typeof(Control),
            typeof(ButtonClickFocusAttachment), new UIPropertyMetadata(null, ElementToFocusPropertyChanged));

        public static void ElementToFocusPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                button.Click += (s, args) =>
                {
                    Control control = GetElementToFocus(button);
                    if (control != null)
                    {
                        control.Focus();
                    }
                };
            }
        }
    }

    /// <summary>
    /// This class is used set the focus to UIelemnt after slider value changed
    /// SliderFocusAttachment.ElementToFocus="{Binding ElementName="UIElement"}"
    /// </summary>
    public class SliderFocusAttachment
    {
        public static Control GetElementToFocus(Slider slider)
        {
            return (Control)slider.GetValue(ElementToFocusProperty);
        }

        public static void SetElementToFocus(Slider slider, Control value)
        {
            slider.SetValue(ElementToFocusProperty, value);
        }

        public static readonly DependencyProperty ElementToFocusProperty =
            DependencyProperty.RegisterAttached("ElementToFocus", typeof(Control),
            typeof(SliderFocusAttachment), new UIPropertyMetadata(null, ElementToFocusPropertyChanged));

        public static void ElementToFocusPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var slider = sender as Slider;
            if (slider != null)
            {
                slider.ValueChanged += (s, args) =>
                {
                    Control control = GetElementToFocus(slider);
                    if (control != null)
                    {
                        control.Focus();
                    }
                };
            }
        }
    }

    public static class InitialFocusBehavior
    {
        public static bool GetFocus(DependencyObject element)
        {
            return (bool)element.GetValue(FocusProperty);
        }

        public static void SetFocus(DependencyObject element, bool value)
        {
            element.SetValue(FocusProperty, value);
        }

        public static readonly DependencyProperty FocusProperty =
            DependencyProperty.RegisterAttached(
            "Focus",
            typeof(bool),
            typeof(InitialFocusBehavior),
            new UIPropertyMetadata(false, OnElementFocused));

        static void OnElementFocused(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            ItemsControl itemsControl = depObj as ItemsControl;
            if (itemsControl == null)
                return;
            itemsControl.Loaded += (object sender, RoutedEventArgs args) =>
            {
                // get the content presented for the first listbox element
                var contentPresenter = (ContentPresenter)itemsControl.ItemContainerGenerator.ContainerFromIndex(0);

                if (contentPresenter != null)
                {
                    // get the textbox and give it focus
                    var textbox = contentPresenter.ContentTemplate.FindName("editableTextBox", contentPresenter) as TextBox;
                    textbox.Focus();
                }
            };
        }
    }

    public class TextBoxBehavior
    {
        public static bool GetSelectAllTextOnFocus(TextBox textBox)
        {
            return (bool)textBox.GetValue(SelectAllTextOnFocusProperty);
        }

        public static void SetSelectAllTextOnFocus(TextBox textBox, bool value)
        {
            textBox.SetValue(SelectAllTextOnFocusProperty, value);
        }

        public static readonly DependencyProperty SelectAllTextOnFocusProperty =
            DependencyProperty.RegisterAttached(
                "SelectAllTextOnFocus",
                typeof(bool),
                typeof(TextBoxBehavior),
                new UIPropertyMetadata(false, OnSelectAllTextOnFocusChanged));

        private static void OnSelectAllTextOnFocusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBox = d as TextBox;
            if (textBox == null) return;

            if (e.NewValue is bool == false) return;

            if ((bool)e.NewValue)
            {
                textBox.GotFocus += SelectAll;
                textBox.PreviewMouseDown += IgnoreMouseButton;
            }
            else
            {
                textBox.GotFocus -= SelectAll;
                textBox.PreviewMouseDown -= IgnoreMouseButton;
            }
        }

        private static void SelectAll(object sender, RoutedEventArgs e)
        {
            var textBox = e.OriginalSource as TextBox;
            if (textBox == null) return;
            textBox.SelectAll();
        }

        private static void IgnoreMouseButton(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null || textBox.IsKeyboardFocusWithin) return;

            e.Handled = true;
            textBox.Focus();
        }
    }
       
    public class SliderDragBehaviors
    {
        public static readonly DependencyProperty DragCompletedCommandProperty =
            DependencyProperty.RegisterAttached("DragCompletedCommand", typeof(ICommand), typeof(SliderDragBehaviors),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(DragCompleted)));

        private static void DragCompleted(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var slider = (Slider)d;
            var thumb = GetThumbFromSlider(slider);

            thumb.DragCompleted += thumb_DragCompleted;
        }

        private static void thumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;
            element.Dispatcher.Invoke(() =>
            {
                var command = GetDragCompletedCommand(element);
                var slider = FindParentControl<Slider>(element) as Slider;
                command.Execute(slider.Value);
            });
        }

        public static void SetDragCompletedCommand(UIElement element, ICommand value)
        {
            element.SetValue(DragCompletedCommandProperty, value);
        }

        public static ICommand GetDragCompletedCommand(FrameworkElement element)
        {
            var slider = FindParentControl<Slider>(element);
            return (ICommand)slider.GetValue(DragCompletedCommandProperty);
        }

        private static Thumb GetThumbFromSlider(Slider slider)
        {
            var track = slider.Template.FindName("PART_Track", slider) as Track;
            return track == null ? null : track.Thumb;
        }

        private static DependencyObject FindParentControl<T>(DependencyObject control)
        {
            var parent = VisualTreeHelper.GetParent(control);
            while (parent != null && !(parent is T))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }
            return parent;
        }
    }
}
