﻿using ProbeCaseViewModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ProbeCaseViewModule.Converters
{

    /// <summary>
    /// Data Template selection for scored cell based on region present or not
    /// </summary>
    public sealed class ScoreDataTemplateSelector : DataTemplateSelector
    {

        public DataTemplate RegionScoreDataTemplate { get; set; }
        public DataTemplate ManualScoreDataTemplate { get; set; }      
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            ScoredCell selectedScoredCell = item as ScoredCell;
            if (selectedScoredCell != null)

            {
                if (selectedScoredCell.Region != null)
                {
                    return RegionScoreDataTemplate;
                }
                else
                {
                    return ManualScoreDataTemplate;
                }              
            }
            return null;
        }
    }
}
