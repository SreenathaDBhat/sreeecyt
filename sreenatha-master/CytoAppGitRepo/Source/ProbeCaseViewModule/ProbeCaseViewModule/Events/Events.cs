﻿using Prism.Events;
using ProbeCaseViewDataAccess.Models;
using ProbeCaseViewModule.Models;
using ProbeCaseViewModule.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace ProbeCaseViewModule.Events
{
    /// <summary>
    /// Create session event
    /// </summary>
    public class CreateSessionEvent : PubSubEvent<ScanAreaViewModel> { }

    /// <summary>
    /// Open session event
    /// </summary>
    public class OpenSessionEvent : PubSubEvent<Session> { }

    /// <summary>
    /// close session event
    /// </summary>
    public class CloseSessionEvent : PubSubEvent<object /* Unused */> { }

    /// <summary>
    /// close session event
    /// </summary>
    public class DeleteSessionEvent : PubSubEvent<ScanAreaViewModel> { }

    /// <summary>
    /// Overlay Selection change notofication
    /// </summary>
    public class RenderOverlaysEvent : PubSubEvent<object> { }

    /// <summary>
    /// Channel Display selection change notification
    /// </summary>
    public class ChannelDisplayEvent : PubSubEvent<object> { }

    /// <summary>
    /// grid has resized event
    /// </summary>
    public class GridResizeEvent : PubSubEvent<object> { }

    /// <summary>
    /// Review status cell event
    /// </summary>
    public class ReviewScoredCellEvent : PubSubEvent<bool> { }

    /// <summary>
    /// Render cell on Grid scroll event
    /// </summary>
    public class GridScrollValueChangeEvent : PubSubEvent<bool> { }

    /// <summary>
    ///  open frame window on click of frames button event
    /// </summary>
    public class ScoringWindowOpenEvent : PubSubEvent<FrameInfo> { }

    /// <summary>
    /// Render current channel value event
    /// </summary>
    public class CurrentChannelDisplayEvent : PubSubEvent<object> { }

    /// <summary>
    /// Render scored cell based on control panel actions event
    /// </summary>
    public class RefreshGridDisplayEvent : PubSubEvent<object> { }

    /// <summary>
    /// Apply image thumbnail Settings to Grid
    /// </summary>
    public class ApplyImageSettingsToGridEvent : PubSubEvent<bool> { }

    /// <summary>
    /// Says the frame needs to be rendered. Histogram(Levels) changes, Render Options changes etc 
    /// </summary>
    public class RenderFrameEvent : PubSubEvent<object>
    {
    }

    /// <summary>
    /// A single event to update invertCounterstain, AttenuateCounterstain, IsMaxprojection and CurrentStack Index value
    /// </summary>
    public class UpdateScoredCellEvent : PubSubEvent<object> { }

    /// <summary>
    /// To open current cell in Enhancement viewer
    /// </summary>
    public class EnhanceCurrentCellEvent : PubSubEvent<Dictionary<ScoredCell, EnhanceViewOptions>> { }

    /// <summary>
    /// To re-classify selected cell
    /// </summary>
    public class ReclassifyEvent : PubSubEvent<ProbeScoringClass> { }

    /// <summary>
    /// To Rescore signal information of selected cell event  
    /// </summary>
    public class ReScoreEvent : PubSubEvent<ScoredCell> { }

    /// <summary>
    /// Reclassify selected class to other using user input class name
    /// </summary>
    public class ReClassifyUsingNewClassEvent : PubSubEvent<string> { }

    /// <summary>
    /// To save all PCV related data like enhanced image information, reviewed information etc on click of 'OK' button
    /// </summary>
    public class SaveAllDataEvent : PubSubEvent<Session> { }

    /// <summary>
    /// Notify Scores Added/Deleted 
    /// False  indicates the dataservcie call to delete the thumbnail 
    /// </summary>
    public class ScoresUpdatedEvent : PubSubEvent<bool>
    {
    }
    /// <summary>
    /// Score new cells using function key assigned
    /// </summary>
    public class ScoringUsingKeyEvent : PubSubEvent<System.Windows.Input.Key> { }

    /// <summary>
    /// Send newly created score to Grid View
    /// </summary>
    public class NewScoreEvent : PubSubEvent<Score> { }

    public class PieChartChangeEvent : PubSubEvent<SlideViewModel> //REFACTOR - change to concrete type
    {
    }

    public class AdjustGridImageEvent : PubSubEvent<ScoredCell> { }

    /// <summary>
    /// Update Pie Chart when you navigate back from Grid View to Case browser workspaceview
    /// </summary>
    public class UpdateNavigatedPieChartEvent : PubSubEvent<Dictionary<Session, SessionSummary>> { }

    // TODO - Both SelectedFrameScoreEvent and SelectedGridScoreEvent are used to synchronize the selected scores between Grid view and Frame view
    // TODO - Need to REVISIT here during the shared view model REFACTOR

    /// <summary>
    /// To send the selected cell from Frame view to Grid View
    /// </summary>
    public class SelectedFrameScoreEvent : PubSubEvent<ScoredCell> { }

    /// <summary>
    /// To send the selected cell from Grid view to Frame view
    /// </summary>
    public class SelectedGridScoreEvent : PubSubEvent<ScoredCell> { }

    ///<summary>
    /// function key assigned for gridview
    /// </summary>
    public class GridViewKeyEvent : PubSubEvent<System.Windows.Input.Key> { }

    /// <summary>

    /// <summary>
    /// Clear Session data on Ok button click
    /// </summary>
    public class ClearSessionEvent : PubSubEvent<object> { };

    /// <summary>
    /// Send delta value of mouse scroll to presenter
    /// </summary>
    public class MouseScrollEvent : PubSubEvent<int> { };

    /// <summary>
    /// Displays Frame Gallery on Create Session with selected Frames
    /// </summary>
    public class CreateSessionWithSelectedFramesEvent : PubSubEvent<ScanAreaViewModel>
    {
    }

    /// <summary>
    /// Creates session with selected frames
    /// </summary>
    public class DisplayScorePageEvent : PubSubEvent<ObservableCollection<FrameInfo>>
    {
    }

    /// <summary>
    /// Closes Frame Gallery
    /// </summary>
    public class CloseDualReaderFramesGalleryEvent : PubSubEvent<object>
    {
    }

    /// <summary>
    /// Calculates total cell count of the selected frames
    /// </summary>
    public class CalculateTotalCellsEvent : PubSubEvent<object>
    {
    }

    /// <summary>
    /// To Add Cells from Scores page
    /// </summary>
    public class AddCellsEvent : PubSubEvent<Session>
    {
    }

    /// <summary>
    /// Navigates from Score Page to Frame Gallery
    /// </summary>
    public class NavigateBackToFramesGalleryEvent : PubSubEvent<Session>
    {
    }

    ///<summary>
    ///Open Spot Configuartion window
    /// </summary>
    public class OpenSpotConfigWindowEvent : PubSubEvent<bool>
    {
    }

    ///<summary>
    ///Create new Asay event
    /// </summary>
    public class CreateNewAssayEvent : PubSubEvent<string>
    {

    }

    ///<summary>
    ///Notify the gridViewpresenter on creating new assay
    /// </summary>
    public class NotifyGridViewPresenterEvent : PubSubEvent<object>
    {

    }


    ///<summary>
    /// Get the Comptible assay name event
    /// </summary>
    /// 
    public class GetComptibleAssaysEvent : PubSubEvent<object>
    {

    }

    ///<summary>
    ///Load a Assay from shared folder event
    /// </summary>
    public class LoadAssayEvent : PubSubEvent<string>
    {

    }

    ///<summary>
    ///Save assay to shared folder event
    /// </summary>
    public class SaveAsAssayEvent : PubSubEvent<string>
    {

    }

    /// <summary>
    /// Fit all frames in rectangular region of a slide
    /// </summary>
    public class FitAllFramesInSlideEvent : PubSubEvent<object>
    {

    }

    /// <summary>
    /// Zoom and Pan to selected Frame in Slide Overview View
    /// </summary>
    public class ZoomAndPanToCurrentFrameEvent : PubSubEvent<FrameInfo>
    {

    }

    /// <summary>
    /// Display stored annotation view
    /// </summary>
    public class DisplayStoredAnnotationViewEvent : PubSubEvent<object>
    {

    }

    /// <summary>
    /// Save region Event
    /// </summary>
    public class SaveRegionEvent : PubSubEvent<AnalysisRegionInfo>
    {

    }


    ///<summary>
    ///Close FrameWindow
    /// </summary>
    public class CloseFrameWindowEvent : PubSubEvent<object /* Unused */> { }


    // To Call Activated event when new user control loads
    public class ActivatedEvent : PubSubEvent<object>
    {

    }
    /// <summary>
    /// Reprocess Event execute when reprocess initiated.
    /// Boolean value true indicates reprocess all the frames 
    /// Boolean value false indicates reprocess the current frame
    /// </summary>
    public class ReprocessEvent : PubSubEvent<bool>
    {

    }
}
