﻿using System.Windows;
using System.Xml.Linq;
using System.Windows.Media;
using System.Globalization;

namespace EnhancementControl.Converters
{
    /// <summary>
    /// Safe parsing for XML objects.
    /// </summary>
    public static class AIXML
    {
        // **************************
        // STRING
        public static string AttributeValue(XElement node, string attributeName, string defaultValue)
        {
            if (node == null)
            {
                return defaultValue;
            }

            XAttribute a = node.Attribute(attributeName);
            if (a == null)
            {
                return defaultValue;
            }

            return a.Value;
        }


        // **************************
        // BOOL
        public static bool AttributeBoolValue(XElement node, string attributeName, bool defaultValue)
        {
            if (node == null)
            {
                return defaultValue;
            }

            XAttribute a = node.Attribute(attributeName);
            if (a == null)
            {
                return defaultValue;
            }

            bool b;
            if (!bool.TryParse(a.Value, out b))
            {
                return defaultValue;
            }

            return b;
        }

        // **************************
        // WPF Color
        public static Color AttributeColorValue(XElement node, string attributeName, Color defaultValue)
        {
            if (node == null)
            {
                return defaultValue;
            }

            XAttribute a = node.Attribute(attributeName);
            if (a == null)
            {
                return defaultValue;
            }

            Color c = Convert.WPFColorWithHTMLString(a.Value);
            return c;
        }

        // **************************
        // INT
        public static int AttributeIntValue(XElement node, string attributeName, int defaultValue)
        {
            return AttributeIntValue(node, attributeName, defaultValue, NumberStyles.Any, CultureInfo.InvariantCulture);
        }
        public static int AttributeIntValue(XElement node, string attributeName, int defaultValue, NumberStyles numberstyle, CultureInfo culture)
        {
            if (node == null)
            {
                return defaultValue;
            }

            XAttribute a = node.Attribute(attributeName);
            if (a == null)
            {
                return defaultValue;
            }

            int x;
            if (!int.TryParse(a.Value, numberstyle, culture, out x))
            {
                return defaultValue;
            }

            return x;
        }

        // **************************
        // DOUBLE
        public static double AttributeDoubleValue(XElement node, string attributeName, double defaultValue)
        {
            return AttributeDoubleValue(node, attributeName, defaultValue, NumberStyles.Any, CultureInfo.InvariantCulture);
        }
        public static double AttributeDoubleValue(XElement node, string attributeName, double defaultValue, NumberStyles numberstyle, CultureInfo culture)
        {
            if (node == null)
            {
                return defaultValue;
            }

            XAttribute a = node.Attribute(attributeName);
            if (a == null)
            {
                return defaultValue;
            }

            double x;
            if (!double.TryParse(a.Value, numberstyle, culture, out x))
            {
                return defaultValue;
            }

            return x;
        }

        // **************************
        // FLOAT
        public static float AttributeFloatValue(XElement node, string attributeName, float defaultValue)
        {
            return AttributeFloatValue(node, attributeName, defaultValue, NumberStyles.Any, CultureInfo.InvariantCulture);
        }
        public static float AttributeFloatValue(XElement node, string attributeName, float defaultValue, NumberStyles numberstyle, CultureInfo culture)
        {
            if (node == null)
            {
                return defaultValue;
            }

            XAttribute a = node.Attribute(attributeName);
            if (a == null)
            {
                return defaultValue;
            }

            float x;
            if (!float.TryParse(a.Value, numberstyle, culture, out x))
            {
                return defaultValue;
            }

            return x;
        }

        /// <summary>
        /// NOTE: Splits strings on commas - make sure you only EVER pass in an invariant culture string
        /// </summary>
        public static Rect AttributeRectValue(XElement node, string attributeName, Rect defaultValue)
        {
            if (node == null)
            {
                return defaultValue;
            }

            XAttribute a = node.Attribute(attributeName);
            if (a == null)
            {
                return defaultValue;
            }

            string str = a.Value;
            return Convert.RectWithString(str, defaultValue);
        }
    }
}
