﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Media;
using System.Collections.Generic;
using EnhancementControl.Models;

namespace EnhancementControl.Converters
{
    public class IEImageFromIndexConverter : IValueConverter
    {
        public IEnumerable<IEImageAccessory> Images { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string imageName = value as string;
            if (imageName == null)
            {
                return null;
            }

            foreach (var i in Images)
            {
                if (i.Name == imageName)
                {
                    return i.Bitmap;
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IENullToVisibilityConverter : IValueConverter
    {
        public Visibility ValueWhenNull { get; set; }
        public Visibility ValueWhenNotNull { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == null ? ValueWhenNull : ValueWhenNotNull;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IEBoolToVisibilityConverter : IValueConverter
    {
        public Visibility ValueWhenTrue { get; set; }
        public Visibility ValueWhenFalse { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is bool))
            {
                return ValueWhenFalse;
            }

            bool b = (bool)value;
            return b ? ValueWhenTrue : ValueWhenFalse;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IESystemDrawingColortoWPFBrush : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            if (value is System.Drawing.Color)
            {
                System.Drawing.Color c = (System.Drawing.Color)value;
                return new SolidColorBrush(Color.FromArgb(c.A, c.R, c.G, c.B));
            }
            else if (value is Color)
            {
                return new SolidColorBrush((Color)value);
            }
            else {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IEDoubleConstantAdditionConverter : IValueConverter
    {
        public double C { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double d = 0;

            if (value is double)
            {
                d = (double)value;
            }
            else if (value is float)
            {
                d = (double)(float)value;
            }
            else if (value is short)
            {
                d = (double)(short)value;
            }
            else if (value is int)
            {
                d = (double)(int)value;
            }

            return d + this.C;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IEGreaterThanToVisibiltyConverter : IValueConverter
    {
        public Visibility ValueWhenTrue { get; set; }
        public Visibility ValueWhenFalse { get; set; }
        public double GreaterThan { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double v = 0;
            if (value is double)
            {
                v = (double)value;
            }
            else if (value is float)
            {
                v = (float)value;
            }
            else if (value is int)
            {
                v = (int)value;
            }

            return v > GreaterThan ? ValueWhenTrue : ValueWhenFalse;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IETypeToVisibilityConverter : IValueConverter
    {
        public Type Type { get; set; }
        public Visibility ValueWhenTrue { get; set; }
        public Visibility ValueWhenFalse { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value.GetType() == this.Type)
            {
                return ValueWhenTrue;
            }
            else {
                return ValueWhenFalse;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IEPointToColorConverter : IValueConverter
    {
        public Brush ValueWhenAtOrigin { get; set; }
        public Brush ValueWhenNotAtOrigin { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is IEPoint))
            {
                return ValueWhenNotAtOrigin;
            }

            IEPoint p = (IEPoint)value;
            if (Math.Abs(p.x) > 0.01 || Math.Abs(p.y) > 0.01)
            {
                return ValueWhenNotAtOrigin;
            }
            else {
                return ValueWhenAtOrigin;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IEStringToVisibilityConverter : IValueConverter
    {
        public Visibility ValueWhenEmpty { get; set; }
        public Visibility ValueWhenNotEmpty { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !(value is string))
            {
                return ValueWhenEmpty;
            }

            if (((string)value).Length > 0)
            {
                return ValueWhenNotEmpty;
            }
            else {
                return ValueWhenEmpty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IEIntToDoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)(int)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IEBoolInvertConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
            {
                return false;
            }
            return !((bool)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
