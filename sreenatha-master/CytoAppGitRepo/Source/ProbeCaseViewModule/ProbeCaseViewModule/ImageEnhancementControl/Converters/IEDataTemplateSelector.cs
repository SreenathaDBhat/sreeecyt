﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;

namespace EnhancementControl.Converters
{
    public sealed class IETemplateSelectorItem<T>
    {
        public T Template { get; private set; }
        public Type Class { get; private set; }

        public IETemplateSelectorItem(Type classtype, T template)
        {
            Template = template;
            Class = classtype;
        }
    }

    public sealed class IEDataTemplateSelector : DataTemplateSelector
    {
        private Dictionary<Type, DataTemplate> _items;

        public IEDataTemplateSelector(IEnumerable<IETemplateSelectorItem<DataTemplate>> templates)
        {
            _items = new Dictionary<Type, DataTemplate>();

            foreach (var i in templates)
            {
                _items[i.Class] = i.Template;
            }
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            Type t = item.GetType();

            if (!_items.ContainsKey(t))
            {
                return null;
            }
            else {
                return _items[t];
            }
        }
    }

    public sealed class IEStyleSelector : StyleSelector
    {
        private Dictionary<Type, Style> _items;
        public IEStyleSelector(IEnumerable<IETemplateSelectorItem<Style>> templates)
        {
            _items = new Dictionary<Type, Style>();

            foreach (var i in templates)
            {
                _items[i.Class] = i.Template;
            }
        }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            Type t = item.GetType();

            if (!_items.ContainsKey(t))
            {
                return null;
            }
            else {
                return _items[t];
            }
        }
    }
}
