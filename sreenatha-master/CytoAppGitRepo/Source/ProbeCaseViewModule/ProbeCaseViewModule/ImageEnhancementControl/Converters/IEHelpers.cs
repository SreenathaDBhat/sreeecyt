﻿using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace EnhancementControl.Converters
{
    public static class Convert
    {
        public static Color WPFColorWithHTMLString(string htmlColorString)
        {
            System.Drawing.Color dc = System.Drawing.ColorTranslator.FromHtml(htmlColorString);
            return WPFColorWithDrawingColor(dc);
        }

        public static string HTMLColorStringWithWPFColor(Color color)
        {
            System.Drawing.Color dc = DrawingColorWithWPFColor(color);
            return System.Drawing.ColorTranslator.ToHtml(dc);
        }

        public static System.Drawing.Color DrawingColorWithWPFColor(Color color)
        {
            return System.Drawing.Color.FromArgb(255, color.R, color.G, color.B);
        }

        public static Color WPFColorWithDrawingColor(System.Drawing.Color color)
        {
            return Color.FromArgb(255, (byte)color.R, (byte)color.G, (byte)color.B);
        }

        public static Rect RectWithString(string str, Rect defaultValue)
        {
            string[] parts = str.Split(',');
            if (parts.Length != 4)
            {
                return defaultValue;
            }

            double l, t, w, h;
            if (!double.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out l))
            {
                return defaultValue;
            }
            if (!double.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out t))
            {
                return defaultValue;
            }
            if (!double.TryParse(parts[2], NumberStyles.Float, CultureInfo.InvariantCulture, out w))
            {
                return defaultValue;
            }
            if (!double.TryParse(parts[3], NumberStyles.Float, CultureInfo.InvariantCulture, out h))
            {
                return defaultValue;
            }

            return new Rect(l, t, w, h);
        }

        public static string StringWithRect(Rect r)
        {
            string s = string.Format("{0},{1},{2},{3}", r.Left, r.Top, r.Width, r.Height);
            return s;
        }
    }

    public sealed class VisualTreeWalker
    {
        private VisualTreeWalker()
        {
        }

        public static T FindChildOfType<T>(DependencyObject parent) where T : DependencyObject
        {
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; ++i)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (child.GetType() == typeof(T))
                    return (T)child;

                T foundChild = FindChildOfType<T>(child);
                if (foundChild != null)
                    return foundChild;
            }

            return default(T);
        }

        public static T FindChildOfType<T>(DependencyObject parent, string elementName) where T : FrameworkElement
        {
            return FindChildOfType<T>(parent, elementName, null);
        }

        public static T FindChildOfType<T>(DependencyObject parent, string elementName, object tag) where T : FrameworkElement
        {
            int count = VisualTreeHelper.GetChildrenCount(parent);
            var type = typeof(T);

            for (int i = 0; i < count; ++i)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                T ch = child as T;
                if (ch != null)
                {

                    bool ismatch = true;
                    if (tag != null)
                    {
                        if (tag != ch.Tag)
                            ismatch = false;
                    }

                    string name = (string)ch.GetValue(Control.NameProperty);
                    if (name != elementName)
                        ismatch = false;

                    if (ismatch)
                        return ch;
                }

                T foundChild = FindChildOfType<T>(child, elementName, tag);
                if (foundChild != null)
                    return foundChild;
            }

            return default(T);
        }

        public static T FindParentOfType<T>(DependencyObject obj) where T : DependencyObject
        {
            DependencyObject parent = VisualTreeHelper.GetParent(obj);
            while (parent != null)
            {
                if (parent.GetType() == typeof(T) || parent.GetType().IsSubclassOf(typeof(T)))
                    return (T)parent;
                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return null;
        }

        public static T FindParentOfType<T>(DependencyObject obj, string elementName) where T : DependencyObject
        {
            if (obj == null)
            {
                return null;
            }

            DependencyObject parent = VisualTreeHelper.GetParent(obj);
            if (parent is T && (string)parent.GetValue(Control.NameProperty) == elementName)
                return (T)parent;

            while (parent != null)
            {
                string name = (string)parent.GetValue(Control.NameProperty);

                if ((parent.GetType() == typeof(T) || parent.GetType().IsSubclassOf(typeof(T))) && name == elementName)
                {
                    return (T)parent;
                }
                else
                {
                    parent = VisualTreeHelper.GetParent(parent);
                }
            }

            return null;
        }
    }
}
