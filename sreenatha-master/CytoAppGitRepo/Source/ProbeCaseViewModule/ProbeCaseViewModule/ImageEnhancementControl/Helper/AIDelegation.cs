﻿using System.ComponentModel;
using System.Diagnostics;

namespace EnhancementControl.Helper
{
    public delegate void AIVoidDelegate();
    public delegate void AIPropertyMasterChangeDelegate(object property, object owner);

    /// <summary>
    /// WARNING: EXPERIMENTAL
    /// 
    /// AIProperty tries to inject some ability to work coherently in amongst the
    /// spaghetti of INotifyPropertyChanged and DataBinding notifications.
    /// 
    /// NOTE: you'll likely want to provide readonly access to AutoNotifyProperty objects
    /// you expose in your objects. Readonly access to the your instance of AutoNotifyProperty
    /// does not imply that the value it represents is readonly.
    ///
    /// eg:
    ///     public AIProperty&lt;bool&gt; SomeProperty { get { return _someProperty; } }
    /// 
    /// constructed like so:
    ///     _someProperty = new AutoNotifyProperty("SomeProperty", this, false);
    /// 
    /// client calls:
    ///     obj.SomeProperty.Value = true;
    /// and we're good.
    /// 
    /// MOTIVATION:
    /// Your classes needn't implement INotifyPropertyChanged directly anymore.
    /// You get rich will/did control.
    /// You can attach a single unmodifiable handler to any property changes (via master
    /// notification handler in the constructor) to make sure you always get the notification
    /// even if someone else removes your will/did handlers.
    /// 
    /// Downsides? You have to remember to bind your UI to [propname].Value instead of just [propname].
    /// Setting a breakpoint is a pain (property names help somewhat but it's not great).
    /// You're better off attaching to Value[Will|Did]Change and put the breakpoint in there.
    /// </summary>
    public sealed class AIProperty<TValue> : INotifyPropertyChanged
    {
        private TValue _value;
        private object _owner;
        private AIPropertyMasterChangeDelegate _parentNotify;

        public delegate void ValueDidChangeDelegate(object ownwer, TValue newValue);
        public delegate TValue ValueWillChangeDelegate(object owner, TValue oldValue, TValue potentialNewValue);

        public event PropertyChangedEventHandler PropertyChanged; // just for WPF Data Binding

        public string PropertyName { get; private set; }
        public ValueWillChangeDelegate ValueWillChange { get; set; }
        public ValueDidChangeDelegate ValueDidChange { get; set; }

        /// <summary>
        /// Create an AutoNotifyProperty object.
        /// </summary>
        /// <param name="propertyname">Name - for your benefit while debugging.</param>
        /// <param name="owner">Object this property applies to.</param>
        /// <param name="defaultValue">Default..</param>
        public AIProperty(string propertyname, object owner, TValue defaultValue)
            : this(propertyname, (o, p) => { }, owner, defaultValue)
        {
        }

        /// <summary>
        /// Create an AutoNotifyProperty object.
        /// </summary>
        /// <param name="propertyname">Name - for your benefit while debugging.</param>
        /// <param name="masterNotifier">Master notification callback - gives the client a hook,
        /// outside of the normal ValueDidChange/ValueWillChange mechanics.
        /// Theory is simple. It's just a second hook for being notified when the value changes.
        /// Nothing special. In fact, the only difference to ValueWill/DidChange is
        /// that the client can only set it via the constructor - providing extra resilience to 
        /// the API.
        /// </param>
        /// <param name="owner">Object this property applies to.</param>
        /// <param name="defaultValue">Default..</param>
        public AIProperty(string propertyname, AIPropertyMasterChangeDelegate masterNotifier, object owner, TValue defaultValue)
        {
            this.PropertyName = propertyname;

            _parentNotify = masterNotifier;

            if (_parentNotify == null)
            {
                Debug.Assert(false, "provide a master notification delegate or use the other constructor");
            }

            _value = defaultValue;
            _owner = owner;
        }

        /// <summary>
        /// Update the value.
        /// Fires <see cref="ValueWillChange"/> notification giving the client an opportunity
        /// to sense check the new potential value and substitute a correction. If the client 
        /// DOES provide a correction, <see cref="ValueDidChange"/> will be fired if, and only
        /// if, the *corrected* value differs from the current value.
        /// </summary>
        public TValue Value
        {
            get { return _value; }
            set
            {
                TValue oldValue = _value;
                TValue newValue = value;


                // we don't fire notifications if someone does a self assignment
                if (oldValue == null && newValue == null)
                {
                    return;
                }
                if (oldValue != null && oldValue.Equals(newValue))
                {
                    return;
                }

                if (ValueWillChange != null)
                {
                    newValue = ValueWillChange(_owner, oldValue, value);
                    if (newValue.Equals(oldValue))
                    {
                        return;
                    }
                }

                _value = newValue;

                if (_parentNotify != null)
                {
                    _parentNotify(this, _owner);
                }

                if (ValueDidChange != null)
                {
                    ValueDidChange(_owner, _value);
                }

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Value"));
                }
            }
        }
    }
}
