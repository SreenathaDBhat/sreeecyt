﻿using System;
using System.Windows;
using System.Windows.Input;

namespace EnhancementControl.IEInternalFunctions
{
    /// <summary>
    /// Convenience for simple drag operations only.
    /// The constructor attaches MouseMove and MouseUp event handlers, and captures the mouse
    /// to the ui element provided. Capture is released on mouse up or when you call <see cref="Stop"/>.
    /// </summary>
    /// <example>
    /// IEDragController d;
    /// d.DragMoved = (drag, pos) => { ... };
    /// d.DragEnded = (drag, pos) => { ... };
    /// </example>
    public sealed class AIDragController
    {
        public delegate void IEDragControllerDelegate(AIDragController drag, Point eventPosition);
        public IEDragControllerDelegate DragMoved;
        public IEDragControllerDelegate DragEnded;

        private const double IEDeadzoneSize = 5;
        private IInputElement _uiroot;
        private Point _initialPoint;
        private Point _lastPoint;
        private double _deltaWidth, _deltaHeight;
        private bool _stillInsideDeadzone;

        public AIDragController(IInputElement uiroot, MouseButtonEventArgs mouseDownEvent)
        {
            _stillInsideDeadzone = true;
            _initialPoint = mouseDownEvent.GetPosition(uiroot);
            _uiroot = uiroot;
            _uiroot.MouseMove += OnMouseMove;
            _uiroot.MouseLeftButtonUp += OnMouseUp;
            _uiroot.CaptureMouse();
            _lastPoint = _initialPoint;
            _deltaHeight = 0;
            _deltaWidth = 0;
        }

        /// <summary>
        /// If we move outside deadzone, then back in, still classed as 'outside'.
        /// </summary>
        public bool HasLeftDeadzone
        {
            get { return !_stillInsideDeadzone; }
        }
        public void Stop()
        {
            _uiroot.ReleaseMouseCapture();
        }


        /// <summary>
        /// Gets the point the drag started at, relative to uiroot
        /// </summary>
        public Point StartPoint
        {
            get { return _initialPoint; }
        }
        /// <summary>
        /// Gets the x amount moved since the last DragMoved event
        /// </summary>
        public double XDistanceMovedSinceLastMoveEvent
        {
            get { return _deltaWidth; }
        }
        /// <summary>
        /// Gets the y amount moved since the last DragMoved event
        /// </summary>
        public double YDistanceMovedSinceLastMoveEvent
        {
            get { return _deltaHeight; }
        }


        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            Point pos = e.GetPosition(_uiroot);
            _deltaWidth = pos.X - _lastPoint.X;
            _deltaHeight = pos.Y - _lastPoint.Y;
            _lastPoint = pos;

            if (_stillInsideDeadzone)
            {
                double distance = Math.Sqrt(((pos.X - _initialPoint.X) * (pos.X - _initialPoint.X)) + ((pos.Y - _initialPoint.Y) * (pos.Y - _initialPoint.Y)));
                if (distance < IEDeadzoneSize)
                {
                    return;
                }
            }

            _stillInsideDeadzone = false;
            if (DragMoved != null)
            {
                e.Handled = true;
                DragMoved(this, pos);
            }
        }

        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            Point pos = e.GetPosition(_uiroot);

            if (DragEnded != null)
            {
                e.Handled = true;
                DragEnded(this, pos);
            }

            _uiroot.MouseMove -= OnMouseMove;
            _uiroot.MouseLeftButtonUp -= OnMouseUp;
            _uiroot.ReleaseMouseCapture();
        }
    }
}
