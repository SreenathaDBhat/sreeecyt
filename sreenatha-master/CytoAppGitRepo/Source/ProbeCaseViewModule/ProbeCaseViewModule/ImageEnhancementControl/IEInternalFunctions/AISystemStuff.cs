﻿using System;
using System.IO;
using System.Windows;
using System.Xml.Linq;
using System.Collections.Generic;

namespace EnhancementControl.IEInternalFunctions
{
    /// <summary>
    /// Simple StringTable. Point it at a ResourceDictionary and it will pull
    /// out all the strings, indexed by their x:Key, and make them available
    /// to your code.
    /// </summary>
    public sealed class AIStringTable
    {
        private Dictionary<object, string> _strings;

        public AIStringTable(ResourceDictionary resources)
        {
            _strings = new Dictionary<object, string>();

            SuckStringsFromResources(resources);
        }

        /// <summary>
        /// You're not limited to 1 resource dictionary - you can add strings as
        /// you go. Duplicate keys will be overwritten, mind.
        /// </summary>
        public void SuckStringsFromResources(ResourceDictionary resources)
        {
            foreach (var k in resources.Keys)
            {
                var s = resources[k];
                if (s is string)
                {
                    _strings[k] = (string)s;
                }
            }

            foreach (var m in resources.MergedDictionaries)
            {
                SuckStringsFromResources(m);
            }
        }

        public string StringWithKey(object key)
        {
            return _strings[key];
        }
    }


    /// <summary>
    /// All our apps save stuff. All our apps since 1994 save stuff to disk, not the registry.
    /// AIAppDirectories aims to give you some one liners that will return the folders you need.
    /// Any folder that is returned by this class will certainly exist (folders are created as
    /// required).
    /// 
    /// AppId is really just a string identifying your app. "PCV" for example.
    /// Folder structures, as of Win7, are like so:
    /// C:\Users\james\AppData\Local\ai\{AppId}\        <- This is what AppRoot returns.
    /// </summary>
    public static class AIAppDirectories
    {
        private static string CompanyName = "ai";

        private static string airoot()
        {
            var folder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            folder = Path.Combine(folder, CompanyName);
            return folder;
        }

        public static string AIRoot()
        {
            var folder = airoot();
            CreateDirectoryTree(folder);
            return folder;
        }

        public static string AppRoot(string appid)
        {
            var ai = Path.Combine(airoot(), appid);
            CreateDirectoryTree(ai);
            return ai;
        }

        public static void CreateDirectoryTree(string folder)
        {
            if (!Directory.Exists(folder))
            {
                string path = Path.GetDirectoryName(folder);
                CreateDirectoryTree(path);
                Directory.CreateDirectory(folder);
            }
        }
    }

    /// <summary>
    /// Simple crash log
    /// </summary>
    public sealed class AICrashLog
    {
        private string _folder;

        public AICrashLog(string folderForXMLLogFiles)
        {
            _folder = folderForXMLLogFiles;
        }

        public string CrashLogFolder
        {
            get { return _folder; }
        }

        public void Log(object exceptionobject)
        {
            var folder = Path.Combine(_folder, "Crashes");
            AIAppDirectories.CreateDirectoryTree(folder);

            string filename = string.Format("crash.{0:yyyy}.{0:MM}.{0:dd}-{0:hh}.{0:mm}.{0:ss}.log", DateTime.Now);
            filename = Path.Combine(folder, filename);
            File.WriteAllText(filename, exceptionobject.ToString());
        }
    }

    /// <summary>
    /// Key value store, string to string, persisted to disk automatically.
    /// </summary>
    public sealed class AIKeyValueUserDefaults
    {
        private string _filename;
        private Dictionary<string, string> _map;

        public AIKeyValueUserDefaults(string filename)
        {
            _filename = filename;
            Load();
        }

        public void Set(string key, string value)
        {
            _map[key] = value;
            Save();
        }
        public string Get(string key)
        {
            return Get(key, null);
        }
        public string Get(string key, string fallbackDefault)
        {
            if (!_map.ContainsKey(key))
            {
                return fallbackDefault;
            }
            return _map[key];
        }

        private void Load()
        {
            _map = new Dictionary<string, string>();

            if (!File.Exists(_filename))
            {
                return;
            }

            XElement xml = XElement.Load(_filename);
            foreach (var valueNode in xml.Elements("value"))
            {
                var a = valueNode.Attribute("key");
                if (a == null)
                {
                    continue;
                }

                _map[a.Value] = valueNode.Value;
            }
        }

        private void Save()
        {
            var xml = new XElement("defaults");
            foreach (var k in _map.Keys)
            {
                xml.Add(new XElement("value", new XAttribute("key", k), _map[k]));
            }

            xml.Save(_filename);
        }
    }
}
