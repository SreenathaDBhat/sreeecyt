﻿using System;
using System.Linq;
using System.Windows;
using System.Xml.Linq;
using System.Windows.Media;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using EnhancementControl.Converters;
using EnhancementControl.Helper;

namespace EnhancementControl.IEInternalFunctions
{
    /// <summary>
    /// Annotation. The common part is pretty narrow.
    /// </summary>
    public abstract class IEAnnotation
    {
        private AIProperty<string> _name;
        private AIProperty<Point> _position;

        public abstract XElement Serialize();
        public AIProperty<string> Name { get { return _name; } }

        public IEAnnotation(string name, double x, double y)
        {
            _name = new AIProperty<string>("Name", this, name);
            _position = new AIProperty<Point>("Pos", this, new Point(x, y));
        }

        public AIProperty<Point> Position
        {
            get { return _position; }
        }
    }

    public sealed class IEImageAnnotation : IEAnnotation
    {
        private AIProperty<string> _image;
        private AIProperty<bool> _visible;

        public IEImageAnnotation(string annotationImageName, double x, double y) : base(annotationImageName, x, y)
        {
            _image = new AIProperty<string>("Image", this, annotationImageName);
            _visible = new AIProperty<bool>("ImageAnnoVisible", this, false);
        }

        public AIProperty<bool> Visible
        {
            get { return _visible; }
        }

        public AIProperty<string> ImageName
        {
            get { return _image; }
        }

        public override XElement Serialize()
        {
            var xml = new XElement("Image",
                new XAttribute("Name", _image.Value),
                new XAttribute("TopLeftX", Position.Value.X),
                new XAttribute("TopLeftY", Position.Value.Y),
                new XAttribute("On", _visible.Value)
            );

            return xml;
        }

        internal static IEAnnotation FromXML(XElement textAnnotationNode)
        {
            double x = AIXML.AttributeDoubleValue(textAnnotationNode, "TopLeftX", double.NaN);
            double y = AIXML.AttributeDoubleValue(textAnnotationNode, "TopLeftY", double.NaN);
            string text = AIXML.AttributeValue(textAnnotationNode, "Name", null);
            bool on = AIXML.AttributeBoolValue(textAnnotationNode, "On", false);

            IEImageAnnotation lbl = new IEImageAnnotation(text, x, y);
            lbl.Visible.Value = on;
            return lbl;
        }
    }

    /// <summary>
    /// Text Annotaiton.
    /// </summary>
    public sealed class IELabelAnnotation : IEAnnotation, INotifyPropertyChanged
    {
        private AIProperty<string> _text;

        private AIProperty<double> _fontSize;
        private AIProperty<Color> _fontColor;
        private AIProperty<bool> _editing;

        public event PropertyChangedEventHandler PropertyChanged;


        public IELabelAnnotation() : this(0, 0, "")
        {
        }
        private IELabelAnnotation(double x, double y, string text) : base(text, x, y)
        {
            _text = new AIProperty<string>("Text", this, text);
            _fontSize = new AIProperty<double>("FontSize", this, 16);
            _fontColor = new AIProperty<Color>("FontColor", this, Colors.White);
            _editing = new AIProperty<bool>("IsEditing", this, false);

            _fontSize.ValueWillChange = (o, s, v) =>
            {
                return Math.Max(6, v);
            };
            _text.ValueDidChange = (p, v) =>
            {
                Name.Value = v;

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsEmpty"));
                }
            };
        }

        public bool IsEmpty
        {
            get { return _text.Value == null || _text.Value.Trim().Length <= 0; }
        }
        public AIProperty<string> Text
        {
            get { return _text; }
        }
        public AIProperty<double> FontSize
        {
            get { return _fontSize; }
        }
        public AIProperty<Color> FontColor
        {
            get { return _fontColor; }
        }
        public AIProperty<bool> IsEditing
        {
            get { return _editing; }
        }

        public override XElement Serialize()
        {
            var xml = new XElement("Label",
                new XAttribute("Text", _text.Value),
                new XAttribute("TopLeftX", Position.Value.X),
                new XAttribute("TopLeftY", Position.Value.Y),
                new XAttribute("Size", _fontSize.Value),
                new XAttribute("Color", Converters.Convert.HTMLColorStringWithWPFColor(_fontColor.Value))
            );

            return xml;
        }

        internal static IEAnnotation FromXML(XElement textAnnotationNode)
        {
            double x = AIXML.AttributeDoubleValue(textAnnotationNode, "TopLeftX", double.NaN);
            double y = AIXML.AttributeDoubleValue(textAnnotationNode, "TopLeftY", double.NaN);
            string text = AIXML.AttributeValue(textAnnotationNode, "Text", "");

            IELabelAnnotation lbl = new IELabelAnnotation(x, y, text);
            lbl.FontColor.Value = AIXML.AttributeColorValue(textAnnotationNode, "Color", Colors.Black);
            lbl.FontSize.Value = AIXML.AttributeDoubleValue(textAnnotationNode, "Size", 12);
            return lbl;
        }
    }


    /// <summary>
    /// Polygon Annotation Point.
    /// </summary>
    public sealed class IEPolygonPoint
    {
        private AIProperty<double> _x;
        private AIProperty<double> _y;

        internal IEPolygonPoint(AIPropertyMasterChangeDelegate masterNotify, double x, double y)
        {
            _x = new AIProperty<double>("X", masterNotify, this, x);
            _y = new AIProperty<double>("Y", masterNotify, this, y);
        }

        public AIProperty<double> X { get { return _x; } }
        public AIProperty<double> Y { get { return _y; } }
    }


    /// <summary>
    /// Polygon Annotation; list of points really
    /// </summary>
    public sealed class IEPolygonAnnotation : IEAnnotation, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<IEPolygonPoint> _points;
        private AIProperty<Color> _color;

        public IEPolygonAnnotation(string name) : this(name, new Point(0, 0), new Point[0])
        { }

        public IEPolygonAnnotation(string name, Point offset, IEnumerable<Point> points) : base(name, offset.X, offset.Y)
        {
            _color = new AIProperty<Color>("Color", this, Colors.Yellow);
            _points = new ObservableCollection<IEPolygonPoint>(from p in points
                                                               select new IEPolygonPoint(IEPolygonPoint_Changed, p.X, p.Y));
        }

        public override XElement Serialize()
        {
            var points = from x in _points
                         select new XElement("Point",
                             new XAttribute("X", x.X.Value),
                             new XAttribute("Y", x.Y.Value));

            var xml = new XElement("PolygonAnnotation",
                    new XAttribute("Color", Converters.Convert.HTMLColorStringWithWPFColor(_color.Value)),
                    new XAttribute("OffsetX", Position.Value.X),
                    new XAttribute("OffsetY", Position.Value.Y),
                    new XAttribute("Name", Name.Value), points);

            return xml;
        }

        /// <summary>
        /// Create a new annotation instance from a xml serialization
        /// </summary>
        public static IEAnnotation FromXML(XElement anode)
        {
            List<IEPolygonPoint> points = new List<IEPolygonPoint>();
            IEPolygonAnnotation anno = new IEPolygonAnnotation(AIXML.AttributeValue(anode, "Name", "ERROR"));
            anno.Color.Value = AIXML.AttributeColorValue(anode, "Color", Colors.White);

            double offsetX = AIXML.AttributeDoubleValue(anode, "OffsetX", 0);
            double offsetY = AIXML.AttributeDoubleValue(anode, "OffsetY", 0);
            anno.Position.Value = new Point(offsetX, offsetY);

            var nval = anode.Attribute("Name");
            if (nval != null)
            {
                anno.Name.Value = nval.Value;
            }

            foreach (var ptnode in anode.Descendants("Point"))
            {
                double x = AIXML.AttributeDoubleValue(ptnode, "X", 0.0);
                double y = AIXML.AttributeDoubleValue(ptnode, "Y", 0.0);

                IEPolygonPoint pt = new IEPolygonPoint(anno.IEPolygonPoint_Changed, x, y);
                points.Add(pt);
            }

            anno._points = new ObservableCollection<IEPolygonPoint>(points);
            return anno;
        }

        public AIProperty<Color> Color
        {
            get { return _color; }
        }

        public IEnumerable<IEPolygonPoint> Points
        {
            get { return _points; }
        }

        /// <summary>
        /// A bunch of WPF controls (Polygon PolyLine...) all store their points as PointCollection.
        /// Provide a easy, bindable PointCollection from our real points.
        /// Notifications are provided via INotifyPropertyChanged when points are moved, added, or removed.
        /// </summary>
        public PointCollection WPFPoints
        {
            get
            {
                var wpfpoints = from p in _points select new Point(p.X.Value, p.Y.Value);
                return new PointCollection(wpfpoints);
            }
        }

        private void NotifyWPFPoints()
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("WPFPoints"));
            }
        }
        private void IEPolygonPoint_Changed(object property, object owner)
        {
            NotifyWPFPoints();
        }
    }


    /// <summary>
    /// Annotation Collection.
    /// Use is optional.
    /// </summary>
    public sealed class IEAnnotationCollection
    {
        private ObservableCollection<IEAnnotation> _annotations;
        private ObservableCollection<IEImageAnnotation> _imageAnnotations;

        public IEAnnotationCollection()
        {
            _annotations = new ObservableCollection<IEAnnotation>();
            _imageAnnotations = new ObservableCollection<IEImageAnnotation>();
        }

        public IEnumerable<IEAnnotation> Annotations
        {
            get { return _annotations; }
        }

        public IEnumerable<IEImageAnnotation> ImageAnnotations
        {
            get { return _imageAnnotations; }
        }

        public void Add(IEAnnotation annotation)
        {
            if (annotation is IEImageAnnotation)
            {
                _imageAnnotations.Add((IEImageAnnotation)annotation);
            }
            else {
                _annotations.Add(annotation);
            }
        }

        public void Remove(IEAnnotation annotation)
        {
            if (annotation is IEImageAnnotation)
            {
                _imageAnnotations.Remove((IEImageAnnotation)annotation);
            }
            else {
                _annotations.Remove(annotation);
            }
        }

        public XElement Serialize()
        {
            var xml = new XElement("Annotations",
                new XElement("Image", from a in _imageAnnotations
                                      select a.Serialize()),
                new XElement("Others", from a in _annotations
                                       select a.Serialize()));
            return xml;
        }

        public void DeserializeFrom(XElement xml)
        {
            _annotations.Clear();
            _imageAnnotations.Clear();

            var imageOnes = xml.Element("Image");
            var otherOnes = xml.Element("Others");

            foreach (var anode in otherOnes.Elements())
            {
                IEAnnotation newAnnotation = null;

                if (anode.Name.LocalName == "Label")
                {
                    newAnnotation = IELabelAnnotation.FromXML(anode);
                }
                else if (anode.Name.LocalName == "PolygonAnnotation")
                {
                    newAnnotation = IEPolygonAnnotation.FromXML(anode);
                }

                Add(newAnnotation);
            }

            foreach (var anode in imageOnes.Elements())
            {
                var newAnnotation = IEImageAnnotation.FromXML(anode);
                Add(newAnnotation);
            }
        }
    }
}
