﻿using System.Collections.Generic;

namespace EnhancementControl.IEInternalFunctions
{
    /// <summary>
    /// IEDataJournal. It's a baby, specialised undo list.
    /// What IEDataJournal does is provide a history of application state that the client can step through, forward and back.
    /// To use, you simply pass in copies of your state (T) to <see cref="LogApplicationState"/> after something worthy of undoing happens.
    /// Call stepBackwards() and you will be given the 'last' state...you get the idea.
    /// You are responsible for actually reflecting the new (or old!) state in your application.
    /// </summary>
    /// <typeparam name="T">Application State. XElement is a good choice.</typeparam>
    public sealed class IEDataJournal<T>
    {
        private List<T> _list;
        private int _lastChangeId;

        // _index
        // State at index '_index' is considered the 'current' state.
        private int _index;


        public IEDataJournal()
        {
            _list = new List<T>();
            _index = 0;
            _lastChangeId = -1;
        }

        public bool CanStepBackwards
        {
            get { return _index > 0; }
        }

        public bool CanStepForwards
        {
            get { return _index < _list.Count - 1; }
        }

        public T Top
        {
            get
            {
                if (_index <= 0)
                {
                    return default(T);
                }
                else {
                    return _list[_index];
                }
            }
        }

        /// <summary>
        /// Add application state to the history.
        /// </summary>
        /// <param name="state">Some object used to represent your state - this will be passed back to you via Undo/Redo</param>
        /// <param name="changeIdentifier">
        /// Some number used to identify the type of change.
        /// Sometimes, repeated changes to the same property can be ignored. Setting x = 2, then x = 3, and finally x = 4 may be redundant in your application. May aswel go strait back to 2 from 4.
        /// If this is true for your app, pass the same identifier to this call for each change and IEIEDataJournal will try to be efficient.
        /// To disable all cleverness, just pass -1.
        /// </param>
        public void LogApplicationState(T state, int changeIdentifier)
        {
            if (_index < _list.Count - 1)
            {
                _list.RemoveRange(_index + 1, (_list.Count - _index) - 1);
                _lastChangeId = -1;
            }

            if (changeIdentifier >= 0 && _lastChangeId == changeIdentifier && _index > 0)
            {
                _list[_index] = state;
            }
            else {
                _list.Add(state);
                _index = _list.Count - 1;
            }

            _lastChangeId = changeIdentifier;
        }

        /// <summary>
        /// Move to the previous state in the journal.
        /// </summary>
        /// <returns>Previous application state, or default(T) if <see cref="CanStepBackwards" /> returns FALSE</returns>
        public T StepBackwards()
        {
            if (!CanStepBackwards)
            {
                return default(T);
            }

            _index = _index - 1;
            return _list[_index];
        }

        /// <summary>
        /// Move to the next state in the journal.
        /// </summary>
        /// <returns>Next application state, or default(T) if <see cref="CanStepForwards" /> returns FALSE</returns>
        public T StepForwards()
        {
            if (!CanStepForwards)
            {
                return default(T);
            }

            _index = _index + 1;
            return _list[_index];
        }

        public void Clear()
        {
            _list.Clear();
            _index = 0;
            _lastChangeId = -1;
        }
    }
}
