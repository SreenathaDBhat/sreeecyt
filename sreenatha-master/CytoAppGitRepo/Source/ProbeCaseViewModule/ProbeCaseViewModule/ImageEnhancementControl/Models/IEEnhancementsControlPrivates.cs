﻿using EnhancementControl.Helper;
using EnhancementControl.IEInternalFunctions;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Media;

namespace EnhancementControl.Models
{
    internal sealed class IEEnhancementsControlDataContext
    {
        private AIProperty<IEImageEnhancementsRenderInstruction> _components;
        private AIProperty<IEAnnotationCollection> _annotations;
        private AIProperty<Thickness> _padding;
        private AIProperty<int> _zoom;
        private AIProperty<bool> _showCropper;

        public AIProperty<IEImageEnhancementsRenderInstruction> Render { get { return _components; } }
        public AIProperty<IEAnnotationCollection> Annotations { get { return _annotations; } }
        public AIProperty<Thickness> Padding { get { return _padding; } }
        public AIProperty<int> Zoom { get { return _zoom; } }
        public AIProperty<bool> ShowCropper { get { return _showCropper; } }

        public IEEnhancementsControlDataContext()
        {
            _components = new AIProperty<IEImageEnhancementsRenderInstruction>("Components", this, null);
            _annotations = new AIProperty<IEAnnotationCollection>("Annotations", this, null);
            _padding = new AIProperty<Thickness>("Padding", this, new Thickness(0));
            _zoom = new AIProperty<int>("Zoom", this, 1);
            _showCropper = new AIProperty<bool>("ShowCropper", this, true);
        }
    }


    /// <summary>
    /// Convieniece wrapper around AIKeyValueUserDefaults for common user prefs in the enhancements control.
    /// </summary>
    internal sealed class IEEnhancementsControlDefaults
    {
        private AIKeyValueUserDefaults _defaults;
        private static readonly string _kCanvasColor = "canvas.color";
        private static readonly string _kFontColor = "text.font.color";
        private static readonly string _kFontSize = "text.fontsize";
        private static readonly string _kPolyStrokeColor = "poly.stroke.color";
        public static readonly string AppId = "PCV";

        public IEEnhancementsControlDefaults()
        {
            _defaults = new AIKeyValueUserDefaults(Path.Combine(AIAppDirectories.AppRoot(AppId), "grid.enhancements.prefs"));
        }

        public Color CanvasColor
        {
            get
            {
                string col = _defaults.Get(_kCanvasColor, "#000000");
                return Converters.Convert.WPFColorWithHTMLString(col);
            }
            set
            {
                string col = Converters.Convert.HTMLColorStringWithWPFColor(value);
                _defaults.Set(_kCanvasColor, col);
            }
        }

        public Color TextAnnotationFontColor
        {
            get
            {
                string col = _defaults.Get(_kFontColor, "#ffffff");
                return Converters.Convert.WPFColorWithHTMLString(col);
            }
            set
            {
                string col = Converters.Convert.HTMLColorStringWithWPFColor(value);
                _defaults.Set(_kFontColor, col);
            }
        }

        public double TextAnnotationFontSize
        {
            get
            {
                string s = _defaults.Get(_kFontSize, "12");
                double d = 0;
                if (double.TryParse(s, out d))
                {
                    return d;
                }
                else {
                    return 12;
                }
            }
            set
            {
                _defaults.Set(_kFontSize, value.ToString(CultureInfo.InvariantCulture));
            }
        }

        public Color PolygonAnnotationStrokeColor
        {
            get
            {
                string col = _defaults.Get(_kPolyStrokeColor, "#ffff00");
                return Converters.Convert.WPFColorWithHTMLString(col);
            }
            set
            {
                string col = Converters.Convert.HTMLColorStringWithWPFColor(value);
                _defaults.Set(_kPolyStrokeColor, col);
            }
        }
    }
}
