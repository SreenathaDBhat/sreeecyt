﻿using EnhancementControl.Converters;
using EnhancementControl.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

namespace EnhancementControl.Models
{
    /// <summary>
    /// DataSource
    /// At a bare minimum, in order to use the IEEnhancementControl you must provide
    /// a data source.
    /// The datasource provides a blueprint of the image that IEEnhancementControl uses
    /// to provide a UI for composing an image from those parts.
    /// DataSource also provides bitmap data for each component, on demand.
    /// </summary>
    public interface IEImageEnhancementsDataSource
    {
        /// <summary>
        /// Provide the blueprint for your image.
        /// </summary>
        /// <returns></returns>
        IEImageStructure IEDataSource_GetImageStructure();

        /// <summary>
        /// When the enhancement control needs a new image slice, it will ask for it. On demand.
        /// The call will come in on an undefined thread, but care is taken to assure it's *never*
        /// the main (ui) thread.
        /// </summary>
        /// <returns>
        /// Raw, uncompressed greys for the whole image. You'll get an exception if the result is not *exactly* imageWidth*imageHeight*bpp bytes in length.
        /// </returns>
        byte[] IEDataSource_MonochromeImageDataForComponent(IEImageStructureComponent component, int zplane);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageComponent"></param>
        /// <returns></returns>
        byte[] IEDataSource_RGBImageDataForOverlay(IEImageStructureOverlay overlay);
    }

    /// <summary>
    /// Description of a multi-component image.
    /// </summary>
    public sealed class IEImageStructure
    {
        public IEImageStructureComponent[] Components { get; private set; }
        public IEImageStructureOverlay[] Overlays { get; private set; }
        public int PixelWidth { get; private set; }
        public int PixelHeight { get; private set; }

        public IEImageStructure(IEImageStructureComponent[] components, IEImageStructureOverlay[] overlays, int width, int height)
        {
            this.Components = components;
            this.Overlays = overlays;
            this.PixelWidth = width;
            this.PixelHeight = height;
        }
    }

    /// <summary>
    /// IEImageStructureComponent describes a single component of a composite image.
    /// For instance, if your image has 2 channels, "DAPI" and "Spectrum Orange" you'd
    /// create an IEImageStructureComponent for each.
    /// </summary>
    public sealed class IEImageStructureComponent
    {
        public string Name { get; private set; }
        public int NumberOfPlanes { get; private set; }
        public int NumberOfPlanesMinusOne { get { return NumberOfPlanes - 1; } }
        public bool IsCounterstain { get; private set; }
        public int BitsPerPixel { get; private set; }
        public Color Color { get; private set; }
        public bool HasMaxProjection { get; private set; }

        /// <param name="name">Name of the component - usually the same as the fluorochrome name.</param>
        /// <param name="planes">Number of Z planes available. Minimum: 1. No maximum.</param>
        /// <param name="counterstain">True if this component is the counterstain.</param>
        /// <param name="bpp">BITS per pixel. Send 8 for a standard RGB component.</param>
        /// <param name="color">Render color</param>
        /// <param name="hasoverlay">Component has an overlay available.</param>
        /// <param name="hasMaxProjection">If true, the slice at z=0 is interpreted as the special maximum projection</param>
        public IEImageStructureComponent(string name, int planes, bool counterstain, int bpp, Color color, bool hasMaxProjection)
        {
            Debug.Assert(name != null, "Need a name - eg: DAPI");
            Debug.Assert(name.Length > 0, "Need a name - eg: DAPI");
            Debug.Assert(planes > 0, "No Z Stacks? Not even 1?");
            Debug.Assert(bpp > 0, "BitsPerPixel cannot be zero - eg: 8,10,12...");
            Debug.Assert(color != null, "Need a color");

            this.Name = name;
            this.NumberOfPlanes = planes;
            this.IsCounterstain = counterstain;
            this.BitsPerPixel = bpp;
            this.Color = color;
            this.HasMaxProjection = hasMaxProjection;
        }

        public bool HasZStack
        {
            get { return NumberOfPlanes > 1; }
        }
    }

    /// <summary>
    /// Overlay.
    /// Overlays differ from image annotations in that they can be attached to a parent component, allowing
    /// the UI to group them together. If no parent component is specified, the overlay is still available
    /// but a different UI is used. Standard Image annotations can be moved around by the user; overlays
    /// cannot.
    /// </summary>
    public sealed class IEImageStructureOverlay
    {
        public string Name { get; private set; }
        public IEImageStructureComponent Parent { get; private set; }
        public Color Color { get; private set; }

        public IEImageStructureOverlay(string name, Color color) : this(name, color, null)
        {
        }

        public IEImageStructureOverlay(string name, Color color, IEImageStructureComponent parent)
        {
            Name = name;
            Parent = parent;
            Color = color;
        }
    }

    /// <summary>
    /// Integer based Point structure.
    /// Nothing interesting here.
    /// </summary>
    public struct IEPoint
    {
        public int x, y;

        public IEPoint(int xx, int yy)
        {
            x = xx;
            y = yy;
        }
    }

    /// <summary>
    /// Parameters that affect the rendering of a single image component.
    /// IEEnhancementsControl creates a bunch of these - one for each IEImageStructureComponent your
    /// IEImageEnhancementsDataSource provides.
    /// </summary>
    public sealed class IEImageComponentRenderInstructions
    {
        #region Privates
        private AIPropertyMasterChangeDelegate _masterChange;
        private IEImageStructureComponent _component;
        private IEImageOverlayRenderInstructions _attachedOverlay;
        private float _minHistoGap;

        // User modifiable properties.
        // MAKE SURE: when adding properties, that Clone, Serialise, and DeserializeFrom are all updated.
        private AIProperty<IEPoint> _offset;
        private AIProperty<float> _histoBottom;
        private AIProperty<float> _histoTop;
        private AIProperty<float> _gamma;
        private AIProperty<Color> _color;
        private AIProperty<bool> _attenuate;
        private AIProperty<bool> _invert;
        private AIProperty<bool> _visible;
        private AIProperty<int> _z;
        private AIProperty<bool> _useMaxProjection;

        private IEImageComponentRenderInstructions()
        {
            throw new NotSupportedException("Use the other constructor");
        }

        #endregion

        public IEImageComponentRenderInstructions(IEImageStructureComponent cmp, IEImageOverlayRenderInstructions attachedOverlay)
        {
            _histoTop = new AIProperty<float>("HistoTop", MasterNotify_ChildPropertyDidChange, this, 1);
            _histoBottom = new AIProperty<float>("HistoBottom", MasterNotify_ChildPropertyDidChange, this, 0);
            _gamma = new AIProperty<float>("Gamma", MasterNotify_ChildPropertyDidChange, this, 1.0f);
            _color = new AIProperty<Color>("Color", MasterNotify_ChildPropertyDidChange, this, Colors.PeachPuff);
            _z = new AIProperty<int>("Z", MasterNotify_ChildPropertyDidChange, this, 0);
            _visible = new AIProperty<bool>("Visible", MasterNotify_ChildPropertyDidChange, this, true);
            _attenuate = new AIProperty<bool>("Attenuate", MasterNotify_ChildPropertyDidChange, this, false);
            _invert = new AIProperty<bool>("Invert", MasterNotify_ChildPropertyDidChange, this, false);
            _offset = new AIProperty<IEPoint>("Offset", MasterNotify_ChildPropertyDidChange, this, new IEPoint(0, 0));
            _useMaxProjection = new AIProperty<bool>("MaxProj", MasterNotify_ChildPropertyDidChange, this, false);
            _attachedOverlay = attachedOverlay;

            if (cmp != null)
            {
                _minHistoGap = 10.0f / (float)Math.Pow(2.0, (double)cmp.BitsPerPixel);
                _component = cmp;
                _color.Value = cmp.Color;
                _useMaxProjection.Value = cmp.HasMaxProjection;

                _histoTop.ValueWillChange = (owner, oldVal, newVal) => {
                    return Math.Max(_histoBottom.Value + _minHistoGap, Math.Min(1.0f, newVal));
                };
                _histoBottom.ValueWillChange = (owner, oldVal, newVal) => {
                    return Math.Max(0, Math.Min(_histoTop.Value - _minHistoGap, newVal));
                };
                _useMaxProjection.ValueWillChange = (owner, oldVal, newVal) => {
                    if (!_component.HasMaxProjection)
                    {
                        return false;
                    }
                    else {
                        return newVal;
                    }
                };
            }
        }

        public string Name
        {
            get
            {
                return _component.Name;
            }
        }

        public IEImageComponentRenderInstructions Clone()
        {
            IEImageComponentRenderInstructions clone = new IEImageComponentRenderInstructions(_component, _attachedOverlay);
            clone.CopyFrom(this);
            return clone;
        }

        public void CopyFrom(IEImageComponentRenderInstructions other)
        {
            // may seem silly but it's not that slow, not in a perf hotspot, and most importantly, NO DUPs
            var xml = IEImageComponentRenderInstructions.Serialize(other);
            DeserializeFrom(xml);
        }

        public static XElement Serialize(IEImageComponentRenderInstructions c)
        {
            return new XElement("Component",
                new XAttribute("Name", c.Name),
                new XAttribute("Color", Converters.Convert.HTMLColorStringWithWPFColor(c.Color.Value)),
                new XAttribute("Gamma", c.Gamma.Value),
                new XAttribute("HistBottom", c.HistogramBottom.Value),
                new XAttribute("HistTop", c.HistogramTop.Value),
                new XAttribute("Visible", c.Visible.Value),
                new XAttribute("Z", c.Z.Value),
                new XAttribute("UseMaxProjection", c.UseMaxProjection.Value),
                new XAttribute("Invert", c.Invert.Value),
                new XAttribute("Attenuate", c.Attenuate.Value),
                new XAttribute("OffsetX", c.Offset.Value.x),
                new XAttribute("OffsetY", c.Offset.Value.y)
            );
        }

        public void DeserializeFrom(XElement serializedComponent)
        {
            try
            {
                this.Color.Value = AIXML.AttributeColorValue(serializedComponent, "Color", this.Color.Value);
                this.Gamma.Value = AIXML.AttributeFloatValue(serializedComponent, "Gamma", this.Gamma.Value);
                this.HistogramBottom.Value = AIXML.AttributeFloatValue(serializedComponent, "HistBottom", this.HistogramBottom.Value);
                this.HistogramTop.Value = AIXML.AttributeFloatValue(serializedComponent, "HistTop", this.HistogramTop.Value);
                this.Visible.Value = AIXML.AttributeBoolValue(serializedComponent, "Visible", true);
                this._z.Value = AIXML.AttributeIntValue(serializedComponent, "Z", 0);
                this._attenuate.Value = AIXML.AttributeBoolValue(serializedComponent, "Attenuate", false);
                this._invert.Value = AIXML.AttributeBoolValue(serializedComponent, "Invert", false);
                this._useMaxProjection.Value = AIXML.AttributeBoolValue(serializedComponent, "UseMaxProjection", true);

                int offsetX = AIXML.AttributeIntValue(serializedComponent, "OffsetX", 0);
                int offsetY = AIXML.AttributeIntValue(serializedComponent, "OffsetY", 0);
                this._offset.Value = new IEPoint(offsetX, offsetY);
            }
            finally
            {
            }
        }

        private void MasterNotify_ChildPropertyDidChange(object property, object owner)
        {
            if (_masterChange != null)
            {
                _masterChange(property, owner);
            }
        }

        public AIPropertyMasterChangeDelegate MasterNotifier
        {
            get { return _masterChange; }
            set { _masterChange = value; }
        }

        public IEImageOverlayRenderInstructions AssosiatedOverlay { get { return _attachedOverlay; } }
        public IEImageStructureComponent Component { get { return _component; } }

        public AIProperty<float> HistogramBottom { get { return _histoBottom; } }
        public AIProperty<float> HistogramTop { get { return _histoTop; } }
        public AIProperty<float> Gamma { get { return _gamma; } }
        public AIProperty<Color> Color { get { return _color; } }
        public AIProperty<bool> Visible { get { return _visible; } }
        public AIProperty<bool> Invert { get { return _invert; } }
        public AIProperty<bool> Attenuate { get { return _attenuate; } }
        public AIProperty<int> Z { get { return _z; } }
        public AIProperty<IEPoint> Offset { get { return _offset; } }
        public AIProperty<bool> UseMaxProjection { get { return _useMaxProjection; } }

        public int ZValueCorrectedForMaximumProjection()
        {
            if (Component.HasMaxProjection)
            {
                return UseMaxProjection.Value ? -1 : Z.Value;
            }
            else {
                return Z.Value;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public sealed class IEImageOverlayRenderInstructions
    {
        private AIPropertyMasterChangeDelegate _masterChange;

        public IEImageOverlayRenderInstructions(IEImageStructureOverlay overlay)
        {
            Visible = new AIProperty<bool>("Visible", MasterNotify_ChildPropertyDidChange, this, false);
            Overlay = overlay;
        }

        public AIProperty<bool> Visible { get; private set; }
        public IEImageStructureOverlay Overlay { get; private set; }
        public AIPropertyMasterChangeDelegate MasterNotifier { get { return _masterChange; } set { _masterChange = value; } }
        public string Name { get { return Overlay.Name; } }

        public IEImageOverlayRenderInstructions Clone()
        {
            IEImageOverlayRenderInstructions i = new IEImageOverlayRenderInstructions(Overlay);
            i.Visible.Value = Visible.Value;
            return i;
        }

        public void CopyFrom(IEImageOverlayRenderInstructions other)
        {
            var xml = Serialize(other);
            DeserializeFrom(xml);
        }
        public void DeserializeFrom(XElement serializedOverlay)
        {
            Visible.Value = AIXML.AttributeBoolValue(serializedOverlay, "Visible", false);
        }
        public static XElement Serialize(IEImageOverlayRenderInstructions overlay)
        {
            return new XElement("Overlay",
                new XAttribute("Name", overlay.Name),
                new XAttribute("Visible", overlay.Visible.Value));
        }

        private void MasterNotify_ChildPropertyDidChange(object property, object owner)
        {
            if (_masterChange != null)
            {
                _masterChange(property, owner);
            }
        }
    }


    /// <summary>
    /// </summary>
    public sealed class IEImageEnhancementsRenderInstruction
    {
        private IEImageComponentRenderInstructions[] _components;
        private IEImageOverlayRenderInstructions[] _overlays;
        private IEImageOverlayRenderInstructions[] _orphanOverlays;

        private IEImageEnhancementsRenderInstruction()
        {
        }

        public IEImageEnhancementsRenderInstruction(IEnumerable<IEImageStructureComponent> componentRenderInstructions, IEnumerable<IEImageStructureOverlay> overlayRenderInstructions)
        {
            //overlays really need their own type...soon :(
            _overlays = (from o in overlayRenderInstructions
                         select new IEImageOverlayRenderInstructions(o)).ToArray();
            _components = (from c in componentRenderInstructions
                           select new IEImageComponentRenderInstructions(c, OverlayRenderInstructionsForComponent(c))).ToArray();

            _orphanOverlays = (from o in _overlays
                               where o.Overlay.Parent == null
                               select o).ToArray();

            foreach (var i in this.ComponentInstructions)
            {
                i.MasterNotifier = ChildComponentMasterNotificationDidFire;
            }
            foreach (var i in this.Overlays)
            {
                i.MasterNotifier = ChildComponentMasterNotificationDidFire;
            }
        }

        private void ChildComponentMasterNotificationDidFire(object prop, object owner)
        {
            if (this.MasterNotifier != null)
            {
                this.MasterNotifier(prop, owner);
            }
        }

        /// <summary>
        /// MasterNotifier fires any time one of our child properties fires
        /// </summary>
        public AIPropertyMasterChangeDelegate MasterNotifier
        {
            get;
            set;
        }

        public IEnumerable<IEImageComponentRenderInstructions> ComponentInstructions
        {
            get { return _components; }
        }

        public IEnumerable<IEImageOverlayRenderInstructions> Overlays
        {
            get { return _overlays; }
        }
        public IEnumerable<IEImageOverlayRenderInstructions> OrphanOverlays
        {
            get { return _orphanOverlays; }
        }

        public IEImageEnhancementsRenderInstruction Clone()
        {
            var clones = from c in _components select c.Clone();
            var overlayClones = from o in _overlays select o.Clone();
            var collection = new IEImageEnhancementsRenderInstruction();
            collection._overlays = overlayClones.ToArray();
            collection._components = clones.ToArray();
            return collection;
        }

        public static XElement Serialise(IEImageEnhancementsRenderInstruction renderInstruction)
        {
            return new XElement("Render",
                new XElement("Components", from c in renderInstruction.ComponentInstructions select IEImageComponentRenderInstructions.Serialize(c)),
                new XElement("Overlays", from c in renderInstruction.Overlays select IEImageOverlayRenderInstructions.Serialize(c)));
        }

        public void DeserializeFrom(XElement serializedRenderInstructions)
        {
            Debug.Assert(serializedRenderInstructions != null, "Can't deserialize nothing");

            foreach (var serializedComponent in serializedRenderInstructions.Descendants("Component"))
            {
                var name = serializedComponent.Attribute("Name").Value;

                var c = _components.Where(component => component.Component.Name == name).First();
                c.DeserializeFrom(serializedComponent);
            }
            foreach (var serializedOverlay in serializedRenderInstructions.Descendants("Overlay"))
            {
                var name = serializedOverlay.Attribute("Name").Value;

                var c = _overlays.Where(overlay => overlay.Overlay.Name == name).First();
                c.DeserializeFrom(serializedOverlay);
            }
        }

        public IEImageOverlayRenderInstructions OverlayRenderInstructionsForComponent(IEImageStructureComponent component)
        {
            foreach (IEImageOverlayRenderInstructions overlay in _overlays)
            {
                if (overlay.Overlay.Parent == component)
                {
                    return overlay;
                }
            }

            return null;
        }
    }



    // /////////////////////////////////////////////////////////////////////////////////////////////////
    // CLIENT PROVIDED ACCESSORIES
    // Clients of the Image Enhancement control can provide some simple accessories that are made
    // available for the user to drop onto their composition.
    // When selected in the UI, we create enhancement-control-compatible Annotations from these
    // accessories so think of IE*Accessory objects as nothing more than blueprints for annotations.
    // /////////////////////////////////////////////////////////////////////////////////////////////////

    public enum IEAccessoryHorizontalPlacement
    {
        Left, Center, Right, DontCare
    }
    public enum IEAccessoryVerticallPlacement
    {
        Top, Center, Bottom, DontCare
    }

    public sealed class IETextAccessory
    {
        public IETextAccessory(string text)
        {
            this.Text = text;
        }

        public string Text
        {
            get;
            private set;
        }
    }

    public sealed class IEImageAccessory
    {

        public IEImageAccessory(string description, BitmapSource bmp, bool onByDefault, IEAccessoryHorizontalPlacement h, IEAccessoryVerticallPlacement v)
        {
            this.Bitmap = bmp;
            this.Name = description;
            this.HorizontalPlacement = h;
            this.VerticalPlacement = v;
            this.OnByDefault = onByDefault;
        }

        public BitmapSource Bitmap
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public IEAccessoryHorizontalPlacement HorizontalPlacement
        {
            get;
            private set;
        }

        public IEAccessoryVerticallPlacement VerticalPlacement
        {
            get;
            private set;
        }

        public bool OnByDefault
        {
            get;
            private set;
        }
    }
}
