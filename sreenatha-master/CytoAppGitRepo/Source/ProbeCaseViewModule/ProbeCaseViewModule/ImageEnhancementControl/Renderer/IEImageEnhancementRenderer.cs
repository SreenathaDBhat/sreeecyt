﻿using System;
using System.Linq;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;
using EnhancementControl.Models;
using EnhancementControl.Converters;

namespace EnhancementControl.Renderer
{
    public static class IndexEnumerator
    {
        public static int[] IndicesOfItemsWhere<T>(this IEnumerable<T> src, Func<T, bool> condition)
        {
            int n = src.Count();
            List<int> indices = new List<int>();

            for (int i = 0; i < n; ++i)
            {
                if (condition(src.ElementAt(i)))
                {
                    indices.Add(i);
                }
            }

            return indices.ToArray();
        }
    }


    public sealed class IEImageEnhancementRenderer
    {
        /// <summary>
        /// renderInstructions and imageData are index aligned - imageData[0] is the pixel data for renderInstructions[0]
        /// </summary>
        public unsafe void Render(IEImageComponentRenderInstructions[] renderInstructions, byte[][] imageData, byte* destinationImageBuffer, int width, int height)
        {
            int[] regularComponentsIndices = renderInstructions.IndicesOfItemsWhere(x => !x.Attenuate.Value);
            int[] attenuatedComponentsIndices = renderInstructions.IndicesOfItemsWhere(x => x.Attenuate.Value);

            AddToRender(regularComponentsIndices, renderInstructions, imageData, destinationImageBuffer, width, height);
            AddToRender(attenuatedComponentsIndices, renderInstructions, imageData, destinationImageBuffer, width, height);
        }

        public unsafe void RenderOverlays(IEImageOverlayRenderInstructions[] renderInstructions, byte[][] imageData, byte* destinationImageBuffer, int width, int height)
        {
            for (int i = 0; i < renderInstructions.Length; ++i)
            {
                AddOverlayToRender(renderInstructions[i], imageData[i], destinationImageBuffer, width, height);
            }
        }

        private unsafe void AddToRender(int[] indices, IEImageComponentRenderInstructions[] renderInstructions, byte[][] imageData, byte* destinationImageBuffer, int width, int height)
        {
            foreach (var i in indices)
            {
                IEImageComponentRenderInstructions component = renderInstructions[i];
                byte[] bitmap = imageData[i];
                AddToRender(component, bitmap, destinationImageBuffer, width, height);
            }
        }

        private unsafe void AddToRender(IEImageComponentRenderInstructions ri, byte[] channelBytes, byte* destinationImageBuffer, int width, int height)
        {
            double range = (ri.HistogramTop.Value - ri.HistogramBottom.Value) * 255;
            double lo = ri.HistogramBottom.Value * 255;
            bool invert = ri.Invert.Value;
            bool attenuate = ri.Attenuate.Value;
            Color renderColor = Converters.Convert.DrawingColorWithWPFColor(ri.Color.Value);

            if (invert && ri.Component.IsCounterstain)
            {
                renderColor = Color.White;
            }

            // if gamma is +/- 0.001 from the zero value (which is 1), use a lut
            byte[] gammaLut = null;
            if (Math.Abs(ri.Gamma.Value - 1) > 0.001)
            {
                gammaLut = GammaLUT(ri.Gamma.Value);
            }

            fixed (byte* pIn = &channelBytes[0])
            {
                int numPixels = width * height;
                byte* d = destinationImageBuffer;
                byte* s = pIn;
                float col_r = renderColor.R / 255.0f;
                float col_g = renderColor.G / 255.0f;
                float col_b = renderColor.B / 255.0f;

                int offset = 0;

                if (ri.Offset.Value.x != 0)
                {
                    offset = ri.Offset.Value.x;
                }
                if (ri.Offset.Value.y != 0)
                {
                    offset += (ri.Offset.Value.y * width);
                }

                if (offset > 0)
                {
                    d += offset * 4;
                }
                else if (offset < 0)
                {
                    s += -offset;
                }

                numPixels -= Math.Abs(offset);


                for (int i = 0; i < numPixels; ++i)
                {
                    byte v = *s++;
                    double dv = 255 * Math.Min(1.0, Math.Max(0, (v - lo)) / range);
                    double attenuationFactor = 1;

                    if (invert)
                    {
                        dv = 255 - dv;
                    }

                    if (attenuate)
                    {
                        attenuationFactor = *d;
                        attenuationFactor = attenuationFactor > *(d + 1) ? attenuationFactor : *(d + 1);
                        attenuationFactor = attenuationFactor > *(d + 2) ? attenuationFactor : *(d + 2);
                        attenuationFactor = 1 - (attenuationFactor / 255.0);
                        dv *= attenuationFactor;
                    }


                    *(d + 0) = (byte)Math.Min(255, *(d + 0) + (byte)(col_b * dv));
                    *(d + 1) = (byte)Math.Min(255, *(d + 1) + (byte)(col_g * dv));
                    *(d + 2) = (byte)Math.Min(255, *(d + 2) + (byte)(col_r * dv));
                    *(d + 3) = 255;

                    if (gammaLut != null)
                    {
                        *(d + 0) = gammaLut[*(d + 0)];
                        *(d + 1) = gammaLut[*(d + 1)];
                        *(d + 2) = gammaLut[*(d + 2)];
                    }

                    d += 4;
                }
            }
        }

        private unsafe void AddOverlayToRender(IEImageOverlayRenderInstructions ri, byte[] overlayRGBBytes, byte* destinationImageBuffer, int width, int height)
        {
            Debug.Assert(overlayRGBBytes.Length == 4 * width * height);

            fixed (byte* pIn = &overlayRGBBytes[0])
            {
                int numPixels = width * height;
                byte* d = destinationImageBuffer;
                byte* s = pIn;
                byte* end = s + (width * height * 4);

                while (s < end)
                {
                    if (*(s + 3) > 10)
                    {
                        *(d + 0) = *(s + 0);
                        *(d + 1) = *(s + 1);
                        *(d + 2) = *(s + 2);
                        *(d + 3) = 255;
                    }

                    d += 4;
                    s += 4;
                }
            }
        }

        private byte[] GammaLUT(double gamma)
        {
            var lut = new byte[256];
            for (int i = 0; i < 256; i++)
                lut[i] = (byte)Math.Min(255, (int)((255.0 * Math.Pow(i / 255.0, 1.0 * gamma)) + 0.5));
            return lut;
        }
    }
}
