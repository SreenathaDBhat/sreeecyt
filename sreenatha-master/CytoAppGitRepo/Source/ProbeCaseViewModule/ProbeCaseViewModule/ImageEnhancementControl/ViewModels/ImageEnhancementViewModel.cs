﻿using EnhancementControl.Models;
using EnhancementControl.Views;
using PCVImageRenderers;
using Prism.Commands;
using ProbeCaseViewModule.Models;
using ProbeCaseViewModule.Services;
using ProbeCaseViewModule.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using EnhancementControl.Converters;
using EnhancementControl.Helper;
using CytoApps.Localization;
using CytoApps.Infrastructure.UI.Controls;

namespace EnhancementControl.ViewModels
{
    public class ImageEnhancementViewModel
    {
        #region "Private Varaibles"
        private ImageEnhancementView _imageEnhancementView;
        private IEEnhancementsControl _enhancementControl;
        private bool _isSaveEnhancedImageInfo = false;
        //private bool _dismissedCleanly;
        #endregion "Private Variables"

        #region "Constructor"
        public ImageEnhancementViewModel(IEImageEnhancementsDataSource _iEImageEnhancementsDataSource)
        {
            // _dismissedCleanly = false;
            OnOkCommand = new DelegateCommand<object>(OnOkClick);
            OnCancelCommand = new DelegateCommand<object>(OnCancelClick);
            _enhancementControl = new IEEnhancementsControl(_iEImageEnhancementsDataSource);
            _imageEnhancementView = new ImageEnhancementView();
            _enhancementControl.ExtensionUI = new ContentControl()
            {
                ContentTemplate = _imageEnhancementView.Resources["ExtensionUI"] as DataTemplate
            };
            _imageEnhancementView.DataContext = this;
            _imageEnhancementView.Content = _enhancementControl;
        }
        #endregion "Constructor"

        private DelegateCommand _windowCloseCommand;
        public ICommand WindowCloseCommand
        {
            get
            {
                return _windowCloseCommand ?? (_windowCloseCommand = new DelegateCommand(() =>
                {
                    OnWindowClosed();
                }, () => true));
            }
        }


        #region "Properties"
        public AIVoidDelegate EnhancementsWindowDidDismissWithOK { get; set; }
        public ICommand OnOkCommand { get; set; }
        public ICommand OnCancelCommand { get; set; }
        public IEEnhancementsControl EnhancementsControl
        {
            get { return _enhancementControl; }
        }
        #endregion "Properties"

        #region "Methods"
        private void OnCancelClick(object window)
        {
            _isSaveEnhancedImageInfo = false;
            ((Window)window).Close();
        }

        public void DisplayEnhancementWindow()
        {
            if (_imageEnhancementView != null)
            {
                _imageEnhancementView.ShowDialog();
            }
        }

        private void OnOkClick(object window)
        {
            _isSaveEnhancedImageInfo = true;
            ((Window)window).Close();
        }

        private void OnWindowClosed()
        {
            if (!_isSaveEnhancedImageInfo)
            {
                MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-CloseEnhancedImageWindowMessage"), Literal.Strings.Lookup("cbStr-CloseEnhancedImageWindowTitle"), MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        _isSaveEnhancedImageInfo = true;
                        //_dismissedCleanly = true;
                        EnhancementsWindowDidDismissWithOK();
                        _enhancementControl.ClearUndoList();
                        break;
                    case MessageBoxResult.Cancel:
                        _isSaveEnhancedImageInfo = false;
                        break;
                }
            }
            else
            {
                //_dismissedCleanly = true;
                EnhancementsWindowDidDismissWithOK();
                _enhancementControl.ClearUndoList();
            }
        }
        #endregion "Methods"
    }

    /// <summary>
    /// This class used set datasource to ImageEnhancement control and it should implement IEImageEnhancementsDataSource interface
    /// </summary>
    public sealed class CurrentCellEnhancementDataSource : IEImageEnhancementsDataSource
    {
        #region "Private Varaibles"
        private ScoredCell _cell;
        private StackedThumbnailRenderer _gridRenderer; // only used to load the image data - not for rendering (must refactor sometime..)
        private IDataService _dataservice;
        private OverlayInfo[] _gridOverlays;
        private IEImageStructureComponent[] _components;
        private IEImageStructureOverlay[] _overlays;
        private SharedViewModel _sharedViewModel;
        private FrameInfo _currentframeInfo;
        private byte[] _data;
        #endregion "Private Variables"

        #region "Constructor"
        public CurrentCellEnhancementDataSource(IDataService dataservice, ScoredCell cell, IEnumerable<OverlayInfo> overlays)
        {
            // _sharedViewModel =new SharedViewModel.Instance;
            _gridOverlays = overlays == null ? new OverlayInfo[0] : overlays.ToArray();
            _dataservice = dataservice;
            _cell = cell;
            _gridRenderer = cell.ThumbRenderer;
            _data = _dataservice.GetThumbnailData(_sharedViewModel.SelectedSession, _cell.Score);
            _currentframeInfo = _sharedViewModel.Frames.Where(x => x.Image.Id == _cell.FrameId).FirstOrDefault();
            _components = (from f in _currentframeInfo.Image.Components
                           select new IEImageStructureComponent(
                               f.Name,
                               f.NumberOfSlices - (f.HasZStack ? 1 : 0),
                               f.IsCounterstain,
                               8,
                               f.RenderColor,
                               f.HasZStack // if there's a stack, ASSume there's a maxproj...
                           )).ToArray();

            List<IEImageStructureOverlay> overlaysList = new List<IEImageStructureOverlay>();
            if (cell.HasSpotMeasurements)
            {
                foreach (var o in _gridOverlays)
                {
                    IEImageStructureComponent parentComponent = _components.Where(c => string.Compare(c.Name, o.Name, true) == 0).FirstOrDefault();
                    overlaysList.Add(new IEImageStructureOverlay(o.Name, parentComponent == null ? Converters.Convert.WPFColorWithHTMLString(o.RenderColor) : parentComponent.Color, parentComponent));
                }
            }

            _overlays = overlaysList.ToArray();
        }
        #endregion "Constructor"

        #region "Methods"
        public IEImageStructure IEDataSource_GetImageStructure()
        {
            var structure = new IEImageStructure(_components, _overlays, _cell.ThumbRenderer.Width, _cell.ThumbRenderer.Height);
            return structure;
        }

        public byte[] IEDataSource_MonochromeImageDataForComponent(IEImageStructureComponent component, int zplane)
        {
            var frameComponent = _currentframeInfo.Image.Components.Where(c => c.Name == component.Name).FirstOrDefault();
            if (frameComponent == null)
            {
                return null;
            }

            int z = (int)zplane;
            if (component.HasMaxProjection)
            {
                z += 1;
            }
            MemoryStream stream = new MemoryStream(_data);
            var bytes = _gridRenderer.ReadImage(frameComponent, z, stream);
            return bytes;
        }

        public byte[] IEDataSource_RGBImageDataForOverlay(IEImageStructureOverlay overlay)
        {
            OverlayInfo gridOverlay = _gridOverlays.Where(o => string.Compare(o.Name, overlay.Name, true) == 0).FirstOrDefault();
            if (gridOverlay == null)
            {
                return null;
            }

            // Extract image data from bitmap and make background transparent
            int w = 0, h = 0;
            var bmp = _dataservice.GetOverlayData(_sharedViewModel.SelectedSession, _cell.Score, gridOverlay.Overlay);
            using (var ms = new MemoryStream(bmp))
            {
                var pixels = ThumbnailOverlayRenderer.GetImageDataFromBitmap(ms, out w, out h);
                pixels = ThumbnailOverlayRenderer.MakeBackgroundTransparent(pixels);

                //ScoredCell.(Path.Combine(_slideFolder, filename), out w, out h);
                return pixels;
            }
        }
        #endregion "Methods"
    }
}
