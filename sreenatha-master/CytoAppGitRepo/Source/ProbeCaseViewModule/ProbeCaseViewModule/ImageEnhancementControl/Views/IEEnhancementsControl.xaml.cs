﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Xml.Linq;
using System.Threading;
using System.Diagnostics;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Collections.ObjectModel;
using EnhancementControl.Models;
using EnhancementControl.IEInternalFunctions;
using EnhancementControl.Converters;
using EnhancementControl.Renderer;

namespace EnhancementControl.Views
{
    /// <summary>
    /// Interaction logic for IEEnhancementsControl.xaml
    /// </summary>
    public partial class IEEnhancementsControl : UserControl
    {
        private IEEnhancementsControl()
        {
            throw new NotSupportedException("Need a data source. Can't add me directly to XAML? Tough. Convenience < api integrity.");
        }

        public IEEnhancementsControl(IEImageEnhancementsDataSource dataSource)
        {
            _defaults = new IEEnhancementsControlDefaults();
            _dataSource = dataSource;
            _imageStructure = _dataSource.IEDataSource_GetImageStructure();
            _displayImage = new WriteableBitmap((int)_imageStructure.PixelWidth, (int)_imageStructure.PixelHeight, 96, 96, PixelFormats.Bgra32, null);
            _ignoreRenderCalls = false;
            _renderer = new IEImageEnhancementRenderer();
            _renderInstructions = new IEImageEnhancementsRenderInstruction(_imageStructure.Components, _imageStructure.Overlays);
            _undoList = new IEDataJournal<XElement>();
            _annotations = new IEAnnotationCollection();
            _imageAccessories = new ObservableCollection<IEImageAccessory>();
            _textAccessories = new ObservableCollection<IETextAccessory>();
            _dataContext = new IEEnhancementsControlDataContext();
            _dataContext.Render.Value = _renderInstructions;
            _dataContext.Annotations.Value = _annotations;

            DataContext = _dataContext;
            InitializeComponent();
            Focusable = true;

            this.Background = new SolidColorBrush(_defaults.CanvasColor);

            _strings = new AIStringTable(this.Resources);

            _imageControl.Source = _displayImage;
            _imageControl.Width = _imageStructure.PixelWidth;
            _imageControl.Height = _imageStructure.PixelHeight;

            var annotationItemTemplateSelector = new IEStyleSelector(new IETemplateSelectorItem<Style>[] {
                new IETemplateSelectorItem<Style>(typeof(IELabelAnnotation), (Style)_annotationsDisplay.FindResource("annotationTemplate_text")),
                new IETemplateSelectorItem<Style>(typeof(IEPolygonAnnotation), (Style)_annotationsDisplay.FindResource("annotationTemplate_poly")),
            });
            _annotationsDisplay.ItemContainerStyleSelector = annotationItemTemplateSelector;

            var annoPropsTemplateSelector = new IEDataTemplateSelector(new IETemplateSelectorItem<DataTemplate>[] {
                new IETemplateSelectorItem<DataTemplate>(typeof(IELabelAnnotation), (DataTemplate)_annotationsPanel.FindResource("annotationPropertiesTextLabel")),
                new IETemplateSelectorItem<DataTemplate>(typeof(IEPolygonAnnotation), (DataTemplate)_annotationsPanel.FindResource("annotationPropertiesPoly")),
            });
            _annotationsListbox.ItemTemplateSelector = annoPropsTemplateSelector;

            _renderInstructions.MasterNotifier = (changedProperty, owner) =>
            {
                Render(true, changedProperty.GetHashCode());
            };
        }

        /// <summary>
        /// EnhancementsControl provides an extension area that you can insert your own app UI into.
        /// It's at the bottom right, in the bottom toolbar and is intended for stuff like OK/Cancel
        /// buttons, though you can provide anything you want.
        /// </summary>
        public UIElement ExtensionUI
        {
            get { return _extensionArea.Child; }
            set { _extensionArea.Child = value; }
        }

        /// <summary>
        /// If true, killing the control without saving would result in data loss.
        /// </summary>
        public bool IsModified
        {
            get { return _undoList.CanStepBackwards; }
        }

        public void ClearUndoList()
        {
            _undoList.Clear();
        }

        /// <summary>
        /// Returns a COPY of the current render instruction - that is, the image component
        /// setup not the annotations.
        /// </summary>
        public IEImageEnhancementsRenderInstruction CopyOfCurrentRenderInstruction
        {
            get
            {
                var collection = _renderInstructions.Clone();
                return collection;
            }
        }

        /// <summary>
        /// Serializes the current state into an XElement.
        /// You are able to completely restore the state of the ImageEnhancements control with this.
        /// </summary>
        public XElement ControlState
        {
            get { return GenerateXMLSerialization(_renderInstructions.Clone()); }
            set { ReadStateFromXML(value); Render(true, -1); }
        }

        /// <summary>
        /// Gets or sets the list of text accessories the user can choose from.
        /// </summary>
        public IEnumerable<IETextAccessory> TextAccessories
        {
            get { return _textAccessories; }
            set
            {
                _textAccessories.Clear();
                foreach (var a in value)
                {
                    _textAccessories.Add(a);
                }

                _accessoryButton.ContextMenu = BuildAccessoryMenu();
            }
        }

        /// <summary>
        /// Gets or sets the list of image accessories the user can choose from.
        /// </summary>
        public IEnumerable<IEImageAccessory> ImageAccessories
        {
            get { return _imageAccessories; }
            set
            {
                _imageAccessories.Clear();
                foreach (var a in value)
                {
                    _imageAccessories.Add(a);

                    IEImageAnnotation anno = new IEImageAnnotation(a.Name, 0, 0);
                    _annotations.Add(anno);
                    anno.Visible.Value = a.OnByDefault;
                    anno.Position.Value = InitialPositionForAccessory(a);
                }

                _accessoryButton.ContextMenu = BuildAccessoryMenu();

                var imageConverter = FindResource("_accessoryImageFromIndex") as IEImageFromIndexConverter;
                Debug.Assert(imageConverter != null);
                imageConverter.Images = value;
            }
        }

        /// <summary>
        /// Renders the current image into a BGRA32 bitmap.
        /// </summary>
        public BitmapSource GenerateBitmap()
        {
            _cropControls.Visibility = Visibility.Collapsed;

            if (_editBox != null)
            {
                KillFloatingAnnotationEditBox();
            }
            _annotationsListbox.SelectedItem = null;

            var element = _SNAP_ME_;
            double w = element.ActualWidth;
            double h = element.ActualHeight;
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)w, (int)h, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(element);
            bmp.Freeze();

            _cropControls.Visibility = System.Windows.Visibility.Visible;
            return bmp;
        }

        public void CopyRenderInstruction(IEImageEnhancementsRenderInstruction renderInstruction)
        {
            try
            {
                _ignoreRenderCalls = true;

                foreach (var i in renderInstruction.ComponentInstructions)
                {
                    var ourVersion = _renderInstructions.ComponentInstructions.Where(c => string.Compare(c.Component.Name, i.Component.Name) == 0).FirstOrDefault();
                    if (ourVersion != null)
                    {
                        ourVersion.CopyFrom(i);
                        continue;
                    }
                }
                foreach (var i in renderInstruction.Overlays)
                {
                    var ourVersion = _renderInstructions.Overlays.Where(c => string.Compare(c.Name, i.Name) == 0).FirstOrDefault();
                    if (ourVersion != null)
                    {
                        ourVersion.CopyFrom(i);
                        continue;
                    }
                }
            }
            finally
            {
                _ignoreRenderCalls = false;
                Render(true, -1);
            }
        }




        #region PRIVATE - Implementation
        #region RoutedUICommands
        public static readonly RoutedUICommand ComandChangeRenderColor = new RoutedUICommand();
        public static readonly RoutedUICommand ComandAnnotationAddPoly = new RoutedUICommand();
        public static readonly RoutedUICommand ComandAnnotationAddText = new RoutedUICommand();
        public static readonly RoutedUICommand ComandAnnotationAddAccessory = new RoutedUICommand();
        public static readonly RoutedUICommand ComandComponentOffset = new RoutedUICommand();
        public static readonly RoutedUICommand ComandChooseCanvasBackground = new RoutedUICommand();
        public static readonly RoutedUICommand ComandIncreaseAnnotationFontSize = new RoutedUICommand();
        public static readonly RoutedUICommand ComandDecreaseAnnotationFontSize = new RoutedUICommand();
        public static readonly RoutedUICommand ComandChooseAnnotationFontColor = new RoutedUICommand();
        public static readonly RoutedUICommand ComandChooseAnnotationStrokeColor = new RoutedUICommand();
        #endregion
        #region Privates
        private IEEnhancementsControlDataContext _dataContext;
        private IEImageEnhancementRenderer _renderer;
        private IEImageEnhancementsRenderInstruction _renderInstructions;
        private long _renderId;
        private IEImageEnhancementsDataSource _dataSource;
        private IEImageStructure _imageStructure;
        private WriteableBitmap _displayImage;
        private IEDataJournal<XElement> _undoList;
        private IEAnnotationCollection _annotations;
        private TextBox _editBox;
        private ObservableCollection<IEImageAccessory> _imageAccessories;
        private ObservableCollection<IETextAccessory> _textAccessories;
        private AIStringTable _strings;
        private IEEnhancementsControlDefaults _defaults;

        // Set when there are going to be a lot of quick-fire updates (like an UNDO)
        // When true, no new renders will be permitted and the undo list will not be populated
        // in response to changes.
        private bool _ignoreRenderCalls;

        // Rendering...We don't actually render a bitmap each and every time someone calls Render.
        // That would be rediculous: consider dragging a slider - each pixel moved would result in a render.
        // Instead we simply defer any render requests that come in WHILE the last render is still happening 
        // and just wait till it's done.
        // If, while rendering, multiple render requests are received, only the most recent will actually be
        // rendered.
        private IEImageEnhancementsRenderInstruction _nextRenderInstructions;
        private bool _rendering;

        private DateTime _lastMouseDownTime = DateTime.MinValue;
        #endregion

        private XElement GenerateXMLSerialization(IEImageEnhancementsRenderInstruction ri)
        {
            Color bgCol = Colors.Black;
            if (Background is SolidColorBrush)
            {
                bgCol = ((SolidColorBrush)Background).Color;
            }

            var componentXml = IEImageEnhancementsRenderInstruction.Serialise(ri);
            var annotationXml = _annotations.Serialize();
            var xml = new XElement("State",
                componentXml,
                annotationXml,
                new XElement("UI",
                    new XAttribute("Background", Converters.Convert.HTMLColorStringWithWPFColor(bgCol)),
                    new XAttribute("L", _dataContext.Padding.Value.Left),
                    new XAttribute("T", _dataContext.Padding.Value.Top),
                    new XAttribute("R", _dataContext.Padding.Value.Right),
                    new XAttribute("B", _dataContext.Padding.Value.Bottom),
                    new XAttribute("Scale", _dataContext.Zoom.Value)));

            return xml;
        }

        /// <summary>
        /// Call after an undo or redo, with the new renderinstruction state.
        /// Render will NOT be called but UI notifications WILL fire.
        /// </summary>
        private void ReadStateFromXML(XElement serializedRenderInstructions)
        {
            try
            {
                var componentXml = serializedRenderInstructions.Element("Render");
                var annotationXml = serializedRenderInstructions.Element("Annotations");

                _ignoreRenderCalls = true;
                _renderInstructions.DeserializeFrom(componentXml);
                _annotations.DeserializeFrom(annotationXml);

                var uiNode = serializedRenderInstructions.Element("UI");
                if (uiNode != null)
                {
                    var bgAttr = uiNode.Attribute("Background");
                    if (bgAttr != null)
                    {
                        Background = new SolidColorBrush(Converters.Convert.WPFColorWithHTMLString(bgAttr.Value));
                    }

                    double bl = AIXML.AttributeIntValue(uiNode, "L", (int)_dataContext.Padding.Value.Left);
                    double bt = AIXML.AttributeIntValue(uiNode, "T", (int)_dataContext.Padding.Value.Top);
                    double br = AIXML.AttributeIntValue(uiNode, "R", (int)_dataContext.Padding.Value.Right);
                    double bb = AIXML.AttributeIntValue(uiNode, "B", (int)_dataContext.Padding.Value.Bottom);
                    _dataContext.Padding.Value = new Thickness(bl, bt, br, bb);

                    ZoomTo(AIXML.AttributeIntValue(uiNode, "Scale", 1));
                }

            }
            finally
            {
                _ignoreRenderCalls = false;
            }
        }

        // //////////////////////////////////////////////////////////////////
        // UNDO / REDO
        // //////////////////////////////////////////////////////////////////

        private void DropUndoMarker(int changedPropertyHash)
        {
            DropUndoMarker(_renderInstructions, changedPropertyHash);
        }
        private void DropUndoMarker(IEImageEnhancementsRenderInstruction ri, int changedPropertyHash)
        {
            var xml = GenerateXMLSerialization(ri);
            _undoList.LogApplicationState(xml, changedPropertyHash);
        }

        private void CanUndoExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _undoList == null ? false : _undoList.CanStepBackwards;
        }
        private void ExecuteUndo(object sender, ExecutedRoutedEventArgs e)
        {
            var ri = _undoList.StepBackwards();
            if (ri != null)
            {
                ReadStateFromXML(ri);
                Render(false, -1);
            }
        }

        private void CanRedoExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _undoList == null ? false : _undoList.CanStepForwards;
        }
        private void ExecuteRedo(object sender, ExecutedRoutedEventArgs e)
        {
            var ri = _undoList.StepForwards();
            if (ri != null)
            {
                ReadStateFromXML(ri);
                Render(false, -1);
            }
        }

        // //////////////////////////////////////////////////////////////////
        // RENDERING
        // //////////////////////////////////////////////////////////////////

        // ONLY CALL FROM MAIN THREAD
        private void Render(bool addToUndo, int changedPropertyHash)
        {
            Debug.Assert(Thread.CurrentThread == Dispatcher.Thread, "Call Render from UI thread");

            if (_ignoreRenderCalls)
            {
                return;
            }

            if (_rendering)
            {
                _nextRenderInstructions = _renderInstructions.Clone();
                return;
            }

            _rendering = true;

            // Take a clone of the render instructions - we don't want to use the objects that may be bound to UI
            // on some render thread...
            IEImageEnhancementsRenderInstruction renderInstruction = _renderInstructions.Clone();

            if (addToUndo)
            {
                DropUndoMarker(renderInstruction, changedPropertyHash);
            }

            RenderComponents(renderInstruction);
        }

        private unsafe void RenderComponents(IEImageEnhancementsRenderInstruction renderInstruction)
        {
            List<Task<byte[]>> imageDataLoadTasks = new List<Task<byte[]>>();
            List<Task<byte[]>> overlayDataLoadTasks = new List<Task<byte[]>>();
            List<IEImageComponentRenderInstructions> components = new List<IEImageComponentRenderInstructions>();
            List<IEImageOverlayRenderInstructions> overlays = new List<IEImageOverlayRenderInstructions>();

            foreach (var component in renderInstruction.ComponentInstructions)
            {
                if (component.Visible.Value)
                {
                    var threadSafeComponent = component; // make sure we don't use a iterator value in the task below - things get well fucked
                    components.Add(threadSafeComponent);
                    imageDataLoadTasks.Add(Task.Factory.StartNew(() =>
                    {
                        return _dataSource.IEDataSource_MonochromeImageDataForComponent(threadSafeComponent.Component, threadSafeComponent.ZValueCorrectedForMaximumProjection());
                    }));
                }
            }
            foreach (var overlay in renderInstruction.Overlays)
            {
                if (overlay.Visible.Value)
                {
                    var threadSafeOverlay = overlay; // make sure we don't use a iterator value in the task below - things get well fucked
                    overlays.Add(threadSafeOverlay);
                    overlayDataLoadTasks.Add(Task.Factory.StartNew(() =>
                    {
                        return _dataSource.IEDataSource_RGBImageDataForOverlay(threadSafeOverlay.Overlay);
                    }));
                }
            }

            TaskScheduler uiThreadtaskScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            byte[] imageBuffer = new byte[_imageStructure.PixelWidth * _imageStructure.PixelHeight * 4];
            long rid = ++_renderId;

            var allLoadTasks = imageDataLoadTasks.Union(overlayDataLoadTasks).ToArray();
            if (allLoadTasks.Length == 0)
            {
                allLoadTasks = new Task<byte[]>[] {
                    Task.Factory.StartNew(() => {
                        return (byte[])null;
                    })
                };
            }

            Task.Factory.ContinueWhenAll(allLoadTasks, loadResults =>
            {
                IEImageComponentRenderInstructions[] componentsArray = components.ToArray();
                IEImageOverlayRenderInstructions[] overlaysArray = overlays.ToArray();

                fixed (byte* finalImageBytesPtr = &imageBuffer[0])
                {
                    if (componentsArray.Length > 0)
                    {
                        var loadedBytes = (from r in imageDataLoadTasks select r.Result).ToArray();
                        _renderer.Render(componentsArray, loadedBytes, finalImageBytesPtr, _imageStructure.PixelWidth, _imageStructure.PixelHeight);

                        loadedBytes = (from r in overlayDataLoadTasks select r.Result).ToArray();
                        _renderer.RenderOverlays(overlaysArray, loadedBytes, finalImageBytesPtr, _imageStructure.PixelWidth, _imageStructure.PixelHeight);
                    }
                }
            }).ContinueWith(t =>
            {
                // the locally cached rid will differ from the ivar _renderId if another render has started
                // while this one was running. Cheap but effective.
                if (rid == _renderId)
                {
                    _displayImage.WritePixels(new Int32Rect(0, 0, (int)_imageStructure.PixelWidth, (int)_imageStructure.PixelHeight), imageBuffer, (int)_imageStructure.PixelWidth * 4, 0);
                }

                if (_nextRenderInstructions != null)
                {
                    var nr = _nextRenderInstructions;
                    _nextRenderInstructions = null;
                    RenderComponents(nr);
                }
                else {
                    _rendering = false;
                }
            }, uiThreadtaskScheduler);
        }




        // //////////////////////////////////////////////////////////////////
        // ZOOM
        // //////////////////////////////////////////////////////////////////

        private int ParseIntFromZoomCommandParameter(object parameter)
        {
            int n;
            if (int.TryParse(parameter as string, out n))
            {
                return n;
            }
            else {
                return 1;
            }
        }
        private void ExecuteZoom(object sender, ExecutedRoutedEventArgs e)
        {
            RadioButton check = e.OriginalSource as RadioButton;
            if (check != null && check.IsChecked.GetValueOrDefault(false))
            {
                int n = ParseIntFromZoomCommandParameter(e.Parameter);
                ZoomTo(n);
                _dataContext.Padding.Value = new Thickness(0);
                DropUndoMarker(-1);
            }
        }

        private void ZoomTo(int z)
        {
            _dataContext.Zoom.Value = z;
            _imageControl.Width = _imageStructure.PixelWidth * z;
            _imageControl.Height = _imageStructure.PixelHeight * z;

            switch (z)
            {
                case 1:
                    imageSizeRadio0.IsChecked = true;
                    break;
                case 2:
                    imageSizeRadio1.IsChecked = true;
                    break;
                case 3:
                    imageSizeRadio2.IsChecked = true;
                    break;
            }
        }

        // //////////////////////////////////////////////////////////////////
        // Routine events...
        // //////////////////////////////////////////////////////////////////

        private void OnMouseUpSplitter(object sender, MouseButtonEventArgs e)
        {
            // TODO: remember the position of the splitter
        }

        private void ExecuteChangeColor(object sender, ExecutedRoutedEventArgs e)
        {
            IEImageComponentRenderInstructions component = e.Parameter as IEImageComponentRenderInstructions;
            if (component == null)
            {
                return;
            }

            var dlg = new System.Windows.Forms.ColorDialog();
            dlg.Color = Converters.Convert.DrawingColorWithWPFColor(component.Color.Value);
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                component.Color.Value = Converters.Convert.WPFColorWithDrawingColor(dlg.Color);
            }
        }

        private void ExecuteComponentOffset(object sender, ExecutedRoutedEventArgs e)
        {
            FrameworkElement ui = (FrameworkElement)e.OriginalSource;
            IEImageComponentRenderInstructions component = (IEImageComponentRenderInstructions)ui.Tag;
            IEPoint offset = component.Offset.Value;

            switch ((string)e.Parameter)
            {
                case "0":
                    offset.y--;
                    break;
                case "1":
                    offset.x--;
                    break;
                case "2":
                    offset.x++;
                    break;
                case "3":
                    offset.y++;
                    break;
                case "-1":
                    offset.x = 0;
                    offset.y = 0;
                    break;
            }

            component.Offset.Value = offset;
        }

        private void ExecuteChooseCanvasBackground(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.Background is SolidColorBrush)
            {
                var b = (SolidColorBrush)this.Background;
                if (b.Color.R > 128)
                {
                    this.Background = Brushes.Black;
                }
                else {
                    this.Background = Brushes.White;
                }
            }
            else {
                this.Background = Brushes.Black;
            }

            _defaults.CanvasColor = ((SolidColorBrush)this.Background).Color;
            DropUndoMarker(-1);
        }

        private void OnCatchAllMouseUp(object sender, MouseButtonEventArgs e)
        {
            KillFloatingAnnotationEditBox();
            DeselectAll();
        }

        // //////////////////////////////////////////////////////////////////
        // DRAG DROP
        // //////////////////////////////////////////////////////////////////

        private void SelectAnnotation(IEAnnotation annotation)
        {
            if (_annotationsListbox.SelectedItem != annotation)
            {
                _annotationsListbox.SelectedItem = annotation;
            }
        }
        private void DeselectAll()
        {
            _annotationsListbox.SelectedItem = null;
        }

        private void KillFloatingAnnotationEditBox()
        {
            if (_editBox == null)
            {
                return;
            }

            IELabelAnnotation anno = (IELabelAnnotation)_editBox.Tag;
            anno.Text.Value = _editBox.Text;

            var scope = FocusManager.GetFocusScope(_editBox);
            FocusManager.SetFocusedElement(scope, null);
            Keyboard.ClearFocus();

            foreach (var a in _annotations.Annotations)
            {
                if (a is IELabelAnnotation)
                {
                    ((IELabelAnnotation)a).IsEditing.Value = false;
                }
            }

            if (_editBox != null)
            {
                Grid p = VisualTreeHelper.GetParent(_editBox) as Grid;
                p.Children.Remove(_editBox);
                _editBox = null;
            }
        }

        private void CreateFloatingEditBoxForAnnotation(IELabelAnnotation anno)
        {
            var item = _annotationsDisplay.ItemContainerGenerator.ContainerFromItem(anno);
            Grid annoroot = VisualTreeWalker.FindChildOfType<Grid>(item, "annoroot");
            Grid editBoxHousing = VisualTreeWalker.FindChildOfType<Grid>(item, "controlEditBoxGetsAddedTo");
            Debug.Assert(editBoxHousing != null, "We depend on finding a Grid named 'controlEditBoxGetsAddedTo' above us in the xaml.");

            TextBlock displayLabel = VisualTreeWalker.FindChildOfType<TextBlock>(item, "textDisplay");
            Debug.Assert(displayLabel != null, "Couldn't find the display label - need it to copy text appearance.");

            if (_editBox != null)
            {
                KillFloatingAnnotationEditBox();
            }

            _editBox = new TextBox();
            _editBox.TextWrapping = TextWrapping.Wrap;
            _editBox.Text = anno.Text.Value;
            _editBox.BorderBrush = null;
            _editBox.SetBinding(TextBox.FontFamilyProperty, new Binding("FontFamily") { Source = displayLabel });
            _editBox.SetBinding(TextBox.FontSizeProperty, new Binding("FontSize") { Source = displayLabel });
            _editBox.Foreground = displayLabel.Foreground;
            _editBox.BorderThickness = new Thickness(0);
            _editBox.AcceptsReturn = true;
            _editBox.Tag = anno;

            if (this.Background is SolidColorBrush)
            {
                Color c = (this.Background as SolidColorBrush).Color;
                _editBox.Background = new SolidColorBrush(Color.FromArgb(64, c.R, c.G, c.B));
            }
            else {
                _editBox.Background = null;
            }

            _editBox.PreviewKeyDown += (s, e) =>
            {
                if (e.Key == Key.Escape)
                {
                    e.Handled = true;
                    KillFloatingAnnotationEditBox();
                }
            };

            _editBox.Loaded += (s, e) =>
            {
                _editBox.Focus();
                Keyboard.Focus(_editBox);
            };

            _editBox.LostFocus += (s, e) =>
            {
                KillFloatingAnnotationEditBox();
                DropUndoMarker(-1);
            };
            _editBox.GotFocus += (s, e) =>
            {
                _editBox.SelectAll();
            };

            annoroot.Children.Add(_editBox);
        }

        private void OnBeginDrag_TextAnnotation(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            FrameworkElement element = (FrameworkElement)sender;
            IEAnnotation anno = (IEAnnotation)element.Tag;

            Point pointerOffset = e.GetPosition(_imageAndAnnotationsGrid);
            pointerOffset = new Point(pointerOffset.X - anno.Position.Value.X, pointerOffset.Y - anno.Position.Value.Y);

            AIDragController drag = new AIDragController(_imageAndAnnotationsGrid, e);
            drag.DragMoved = (d, p) =>
            {
                anno.Position.Value = new Point(p.X - pointerOffset.X, p.Y - pointerOffset.Y);
                SelectAnnotation(anno);
            };
            drag.DragEnded = (d, p) =>
            {
                if (d.HasLeftDeadzone)
                {
                    DropUndoMarker(-1);
                }
                else {
                    // DOUBLE CLICK!!!!
                    var label = anno as IELabelAnnotation;
                    if (label != null && label.Text != null && (DateTime.Now.Subtract(_lastMouseDownTime)).TotalSeconds < 0.5)
                    {
                        CreateFloatingEditBoxForAnnotation(label);
                        label.IsEditing.Value = true;
                        _lastMouseDownTime = DateTime.MinValue;
                    }

                    _lastMouseDownTime = DateTime.Now;
                    SelectAnnotation(anno);
                }
            };
        }

        private void OnBeginDrag_PolyAnnotationEndpoint(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            FrameworkElement element = (FrameworkElement)sender;
            IEPolygonPoint polyPoint = (IEPolygonPoint)element.Tag;

            // start hacking...
            Grid polyRoot = VisualTreeWalker.FindParentOfType<Grid>((DependencyObject)sender, "polyAnnotationRoot");
            IEPolygonAnnotation poly = polyRoot == null ? null : polyRoot.Tag as IEPolygonAnnotation;
            Debug.Assert(poly != null, "THIS DEPENDS ON THE XAML STRUCTURE");

            Point offset = new Point(poly.Position.Value.X, poly.Position.Value.Y);

            AIDragController drag = new AIDragController(_imageAndAnnotationsGrid, e);
            drag.DragMoved = (d, p) =>
            {
                if (d.HasLeftDeadzone)
                {
                    SelectAnnotation(poly);
                    polyPoint.X.Value = p.X - offset.X;
                    polyPoint.Y.Value = p.Y - offset.Y;
                }
            };
            drag.DragEnded = (d, p) =>
            {
                if (d.HasLeftDeadzone)
                {
                    DropUndoMarker(-1);
                }
                else {
                    SelectAnnotation(poly);
                }
            };
        }

        private void OnBeginDrag_PolyAnnotationLine(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            FrameworkElement element = (FrameworkElement)sender;
            IEPolygonAnnotation poly = (IEPolygonAnnotation)element.Tag;

            Point pointerOffset = e.GetPosition(_imageAndAnnotationsGrid);
            pointerOffset = new Point(pointerOffset.X - poly.Position.Value.X, pointerOffset.Y - poly.Position.Value.Y);

            AIDragController drag = new AIDragController(_imageAndAnnotationsGrid, e);
            drag.DragMoved = (d, p) =>
            {
                poly.Position.Value = new Point(p.X - pointerOffset.X, p.Y - pointerOffset.Y);
                SelectAnnotation(poly);
            };
            drag.DragEnded = (d, p) =>
            {
                if (d.HasLeftDeadzone)
                {
                    DropUndoMarker(-1);
                }
                else {
                    SelectAnnotation(poly);
                }
            };
        }



        // //////////////////////////////////////////////////////////////////
        // ACCESSORIES and ANNOTATIONS
        // //////////////////////////////////////////////////////////////////

        private void ExecuteAnnotationAddText(object sender, ExecutedRoutedEventArgs e)
        {
            double y = (_imageStructure.PixelHeight * _dataContext.Zoom.Value / 2.0) - 15;
            var a = new IELabelAnnotation();
            a.Position.Value = new Point(10 - _dataContext.Padding.Value.Left, y);
            a.Text.Value = "";
            a.FontSize.Value = _defaults.TextAnnotationFontSize;
            a.FontColor.Value = _defaults.TextAnnotationFontColor;
            _annotations.Add(a);
            _annotationsListbox.SelectedItem = a;
            DropUndoMarker(-1);
        }

        private void ExecuteAnnotationAddPoly(object sender, ExecutedRoutedEventArgs e)
        {
            var a = new IEPolygonAnnotation("Line", new Point(0, 0), new Point[] {
                new Point(10 - _dataContext.Padding.Value.Left, 10 - _dataContext.Padding.Value.Top),
                new Point(100 - _dataContext.Padding.Value.Left, 100 - _dataContext.Padding.Value.Top)
            });

            a.Color.Value = _defaults.PolygonAnnotationStrokeColor;

            _annotations.Add(a);
            _annotationsListbox.SelectedItem = a;
            DropUndoMarker(-1);
        }

        private void CanExecuteAnnotationAddAccessory(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _imageAccessories.Count > 0 || _textAccessories.Count > 0;
        }
        private void ExecuteAnnotationAddAccessory(object sender, ExecutedRoutedEventArgs e)
        {
            ContextMenu menu = _accessoryButton.ContextMenu;
            if (menu != null)
            {
                menu.IsOpen = true;
            }
        }

        private Point InitialPositionForAccessory(IEImageAccessory accessory)
        {
            if (accessory.Bitmap == null)
            {
                return new Point(0, 0);
            }

            double s = _dataContext.Zoom.Value;
            double imgW = _imageStructure.PixelWidth * s;
            double imgH = _imageStructure.PixelHeight * s;
            double aW = accessory.Bitmap.PixelWidth;
            double aH = accessory.Bitmap.PixelWidth;
            double margin = 10;
            double x = 0;
            double y = 0;

            switch (accessory.HorizontalPlacement)
            {
                case IEAccessoryHorizontalPlacement.Left:
                case IEAccessoryHorizontalPlacement.DontCare:
                    x = margin;
                    break;

                case IEAccessoryHorizontalPlacement.Center:
                    x = ((imgW / 2) - (aW / 2));
                    break;

                case IEAccessoryHorizontalPlacement.Right:
                    x = imgW - (aW + margin);
                    break;
            }

            switch (accessory.VerticalPlacement)
            {
                case IEAccessoryVerticallPlacement.Top:
                case IEAccessoryVerticallPlacement.DontCare:
                    y = margin;
                    break;

                case IEAccessoryVerticallPlacement.Center:
                    y = ((imgH / 2) - (aH / 2));
                    break;

                case IEAccessoryVerticallPlacement.Bottom:
                    y = imgH - (aH + margin);
                    break;
            }

            return new Point(x, y);
        }

        private void OnTextAccessorySelected(object sender, EventArgs e)
        {
            IETextAccessory textAccessory = ((MenuItem)sender).Tag as IETextAccessory;
            if (textAccessory != null)
            {
                double y = (_imageStructure.PixelHeight * _dataContext.Zoom.Value / 2.0) - 15;
                var a = new IELabelAnnotation();
                a.Position.Value = new Point(10 - _dataContext.Padding.Value.Left, y);
                a.Text.Value = textAccessory.Text;
                a.FontSize.Value = _defaults.TextAnnotationFontSize;
                a.FontColor.Value = _defaults.TextAnnotationFontColor;
                _annotations.Add(a);
                DropUndoMarker(-1);
            }
        }

        private ContextMenu BuildAccessoryMenu()
        {
            if (_textAccessories.Count == 0)
            {
                return null;
            }

            ContextMenu menu = new ContextMenu();
            menu.ItemsSource = (from i in _textAccessories
                                select new MenuItem()
                                {
                                    Header = i.Text.Length > 30 ? i.Text.Substring(0, 30) : i.Text,
                                    Tag = i
                                }).ToArray();

            foreach (var i in menu.ItemsSource)
            {
                ((MenuItem)i).Click += OnTextAccessorySelected;
            }

            return menu;
        }

        private void ExecuteDeleteAnnotation(object sender, ExecutedRoutedEventArgs e)
        {
            IEAnnotation annotation = (IEAnnotation)e.Parameter;
            _annotations.Remove(annotation);
            DropUndoMarker(-1);
        }

        private void ExecuteIncreaseFontsize(object sender, ExecutedRoutedEventArgs e)
        {
            IELabelAnnotation a = e.Parameter as IELabelAnnotation;
            if (a != null)
            {
                a.FontSize.Value = a.FontSize.Value + 1;
                _defaults.TextAnnotationFontSize = a.FontSize.Value;
                DropUndoMarker(a.FontSize.GetHashCode());
            }
        }

        private void ExecuteDecreaseFontsize(object sender, ExecutedRoutedEventArgs e)
        {
            IELabelAnnotation a = e.Parameter as IELabelAnnotation;
            if (a != null)
            {
                a.FontSize.Value = a.FontSize.Value - 1;
                _defaults.TextAnnotationFontSize = a.FontSize.Value;
                DropUndoMarker(a.FontSize.GetHashCode());
            }
        }

        private void ExecuteChangeAnnotationColor(object sender, ExecutedRoutedEventArgs e)
        {
            IELabelAnnotation a = e.Parameter as IELabelAnnotation;
            if (a != null)
            {
                System.Drawing.Color dc = Converters.Convert.DrawingColorWithWPFColor(a.FontColor.Value);
                System.Windows.Forms.ColorDialog cd = new System.Windows.Forms.ColorDialog();
                cd.Color = dc;

                if (cd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    a.FontColor.Value = Converters.Convert.WPFColorWithDrawingColor(cd.Color);
                    DropUndoMarker(-1);

                    _defaults.TextAnnotationFontColor = a.FontColor.Value;
                }
            }
        }

        private void ExecuteChangeAnnotationStrokeColor(object sender, ExecutedRoutedEventArgs e)
        {
            IEPolygonAnnotation a = e.Parameter as IEPolygonAnnotation;
            if (a != null)
            {
                System.Windows.Forms.ColorDialog cd = new System.Windows.Forms.ColorDialog();
                cd.Color = Converters.Convert.DrawingColorWithWPFColor(a.Color.Value);

                if (cd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    a.Color.Value = Converters.Convert.WPFColorWithDrawingColor(cd.Color);
                    DropUndoMarker(-1);

                    _defaults.PolygonAnnotationStrokeColor = a.Color.Value;
                }
            }
        }

        private void OnUncheckImageAnnotation(object sender, RoutedEventArgs e)
        {
            IEImageAnnotation a = (IEImageAnnotation)((FrameworkElement)sender).Tag;
            a.Visible.Value = false;
            DropUndoMarker(-1);
        }

        private void OnCheckImageAnnotation(object sender, RoutedEventArgs e)
        {
            IEImageAnnotation a = (IEImageAnnotation)((FrameworkElement)sender).Tag;
            a.Visible.Value = true;

            foreach (var i in _imageAccessories)
            {
                if (i.Name == a.ImageName.Value)
                {
                    Point p = InitialPositionForAccessory(i);
                    a.Position.Value = p;
                }
            }

            DropUndoMarker(-1);
        }

        // //////////////////////////////////////////////////////////////////
        // DRAG DROP CROP RECT
        // //////////////////////////////////////////////////////////////////

        private void DragClipWithMultipliers(FrameworkElement element, MouseButtonEventArgs e, double lmul, double tmul, double rmul, double bmul)
        {
            AIDragController drag = new AIDragController(this, e);
            drag.DragMoved = (d, p) =>
            {
                if (d.HasLeftDeadzone)
                {
                    var t = _dataContext.Padding.Value;
                    var dl = d.XDistanceMovedSinceLastMoveEvent * 2 * lmul; // WTF is the *2 about?
                    var dt = d.YDistanceMovedSinceLastMoveEvent * 2 * tmul; // It's a shitty workaround for bug #7565.
                    var dr = d.XDistanceMovedSinceLastMoveEvent * 2 * rmul; // Because everything is centred on the screen, resizing also re-centres creating the effect that 
                    var db = d.YDistanceMovedSinceLastMoveEvent * 2 * bmul; // the mouse pointer detaches from the thing it's dragging. Compensate by doubling the resize delta.
                    t.Left += dl;
                    t.Right += dr;
                    t.Top += dt;
                    t.Bottom += db;
                    _dataContext.Padding.Value = t;
                }
            };
            drag.DragEnded = (d, p) =>
            {
                if (d.HasLeftDeadzone)
                {
                    DropUndoMarker(-1);
                }
            };
        }

        private void OnCropDragTop(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;
            DragClipWithMultipliers(element, e, 0, -1, 0, 0);
        }

        private void OnCropDragBottom(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;
            DragClipWithMultipliers(element, e, 0, 0, 0, 1);
        }

        private void OnCropDragLeft(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;
            DragClipWithMultipliers(element, e, -1, 0, 0, 0);
        }

        private void OnCropDragRight(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;
            DragClipWithMultipliers(element, e, 0, 0, 1, 0);
        }

        // //////////////////////////////////////////////////////////////////
        // KEYBOARD SHORTCUTS
        // //////////////////////////////////////////////////////////////////

        private void OnToplevelKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.FocusedElement is TextBox)
            {
                return;
            }

            // p = toggle max projection
            if (e.Key == Key.P)
            {
                ToggleAllMaxProjections();
            }
            else if (e.Key == Key.Delete || e.Key == Key.Back)
            {
                if (_annotationsListbox.SelectedItem != null)
                {
                    IEAnnotation a = (IEAnnotation)_annotationsListbox.SelectedItem;
                    _annotations.Remove(a);
                    DropUndoMarker(-1);
                }
            }
            else if (e.Key >= Key.NumPad1 && e.Key < Key.NumPad9)
            {
                int index = ((int)e.Key) - ((int)Key.NumPad1);

                if (index < _renderInstructions.ComponentInstructions.Count())
                { // key represents a regular image component
                    var componentRenderInstruction = _renderInstructions.ComponentInstructions.ElementAt(index);
                    if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
                    {
                        IEImageOverlayRenderInstructions overlay = _renderInstructions.OverlayRenderInstructionsForComponent(componentRenderInstruction.Component);
                        if (overlay != null)
                        {
                            overlay.Visible.Value = !overlay.Visible.Value;
                        }
                    }
                    else {
                        componentRenderInstruction.Visible.Value = !componentRenderInstruction.Visible.Value;
                    }
                }
                else { // key represents a orphan overlay (fusions etc)
                    index -= _renderInstructions.ComponentInstructions.Count();
                    if (index >= 0 && index < _renderInstructions.OrphanOverlays.Count())
                    {
                        var componentRenderInstruction = _renderInstructions.OrphanOverlays.ElementAt(index);
                        componentRenderInstruction.Visible.Value = !componentRenderInstruction.Visible.Value;
                    }
                }
            }
        }

        private void ToggleAllMaxProjections()
        {
            var components = _renderInstructions.ComponentInstructions.Where(c => c.Component.HasZStack);
            if (components.Count() == 0)
            {
                return;
            }

            bool on = false;

            foreach (var c in components)
            {
                if (c.UseMaxProjection.Value)
                {
                    on = true;
                    break;
                }
            }

            on = !on;

            foreach (var c in components)
            {
                c.UseMaxProjection.Value = on;
            }

            DropUndoMarker(-1);
        }

        private void OnMouseDownImageCanvas(object sender, MouseButtonEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                e.Handled = true;

                KillFloatingAnnotationEditBox();

                var pt = e.GetPosition(_imageAndAnnotationsGrid);
                IELabelAnnotation txt = new IELabelAnnotation();
                txt.Position.Value = pt;
                txt.FontSize.Value = _defaults.TextAnnotationFontSize;
                txt.FontColor.Value = _defaults.TextAnnotationFontColor;
                txt.IsEditing.Value = true;

                IEPolygonAnnotation line = new IEPolygonAnnotation("Line", new Point(0, 0), new Point[] { pt, pt });
                line.Color.Value = _defaults.PolygonAnnotationStrokeColor;

                _annotations.Add(line);
                _annotations.Add(txt);

                AIDragController drag = new AIDragController(_imageAndAnnotationsGrid, e);
                drag.DragMoved = (d, p) =>
                {
                    line.Points.ElementAt(0).X.Value = p.X;
                    line.Points.ElementAt(0).Y.Value = p.Y;
                    txt.Position.Value = p;
                };
                drag.DragEnded = (d, p) =>
                {
                    DropUndoMarker(-1);
                    CreateFloatingEditBoxForAnnotation(txt);
                    _annotationsListbox.SelectedItem = txt;
                };
            }
            else {
                e.Handled = false;
            }
        }

        #endregion
    }
}