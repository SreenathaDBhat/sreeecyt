﻿using System;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ProbeCaseViewModule.Logic
{
    public class FrameImageToolsLogic
    {
        public static void PrintFrame(BitmapSource bitmapImage, string CaseName, string SlideName)
        {
            System.Windows.Controls.PrintDialog printer = new System.Windows.Controls.PrintDialog();

            if ((bool)printer.ShowDialog().GetValueOrDefault())
            {
                double aspect = bitmapImage.Height / bitmapImage.Width;
              
                Image img = new Image();
                img.Source = bitmapImage;

                TextBlock text = new TextBlock();
                text.FontSize = 18;
                text.FontWeight = FontWeights.Bold;
                text.Foreground = Brushes.Black;
                text.Text = "Case: " + string.Format(CultureInfo.CurrentCulture, "{0}", CaseName) + 
                    "    Slide: " + string.Format(CultureInfo.CurrentCulture, "{0}", SlideName);
                text.Margin = new Thickness(0, 0, 0, 20);
                text.TextWrapping = TextWrapping.Wrap;
                DockPanel.SetDock(text, Dock.Left);

                TextBlock dateText = new TextBlock();
                dateText.FontSize = 18;
                dateText.FontWeight = FontWeights.Bold;
                dateText.Foreground = Brushes.Black;
                dateText.Text = DateTime.Now.ToLongDateString();
                dateText.Margin = new Thickness(10, 0, 0, 20);
                DockPanel.SetDock(dateText, Dock.Right);

                DockPanel topPanel = new DockPanel();
                topPanel.Children.Add(dateText);
                topPanel.Children.Add(text);
                DockPanel.SetDock(topPanel, Dock.Top);

                DockPanel dock = new DockPanel();
                dock.Children.Add(topPanel);
                dock.Children.Add(img);
                dock.Margin = new Thickness(50);
                dock.Width = printer.PrintableAreaWidth - (dock.Margin.Left + dock.Margin.Right);
                dock.Height = dock.Width * aspect;

                dock.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                dock.Arrange(new Rect(new Point(0, 0), dock.DesiredSize));

                printer.PrintVisual(dock, "Probe Case View Print");
            }
        }

        public static Boolean SaveAs(BitmapSource image)
        {
            Boolean done = false;

            SaveFileDialog fd = new SaveFileDialog();
            fd.AddExtension = true;
            fd.RestoreDirectory = true;
            fd.Title = "Export Image";
            fd.Filter = "JPEG Image|*.jpg;*.jpeg|PNG Image|*.png";

            fd.ShowDialog();
            if (!string.IsNullOrWhiteSpace( fd.FileName ))
            {
               done = SaveAs(image, fd.FileName);
            }
            return done;
        }

        public static Boolean SaveAs(BitmapSource image, string filename)
        {
            Boolean done = false;
            string imageFormat = Path.GetExtension(filename).ToUpper();
            BitmapEncoder encoder = null;

            switch (imageFormat)
            {
                case ".JPG":
                    encoder = new JpegBitmapEncoder();
                    break;
                case ".PNG":
                    encoder = new PngBitmapEncoder();
                    break;
                case ".BMP":
                    encoder = new BmpBitmapEncoder();
                    break;
                case ".TIF":
                    encoder = new TiffBitmapEncoder();
                    break;
            }

            try
            {
                using (var stream = File.OpenWrite(filename))
                {
                    encoder.Frames.Add(BitmapFrame.Create(image));
                    encoder.Save(stream);
                    stream.Flush();
                    done = true;
                }
            }
            catch
            {
                done = false;
            }
            return done;
        }
    }
}
