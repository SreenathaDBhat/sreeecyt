﻿using ProbeCaseViewModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ProbeCaseViewModule.Logic
{
    public interface IAnnotationContainer
    {
        FrameAnnotation NewAnnotation(Point position, string annotationText);
        void DeleteAnnotation(FrameAnnotation annotation);
        IEnumerable<FrameAnnotation> Annotations { get; }
    }
}
