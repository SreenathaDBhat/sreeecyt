﻿using CytoApps.Models;
using Microsoft.Practices.ServiceLocation;
using PCVImageRenderers.Models;
using ProbeCaseViewDataAccess.Models;
using ProbeCaseViewModule.Services;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace ProbeCaseViewModule.Logic
{
    public class SlideOverViewLogic
    {
        private IDataService _dataService;

        public SlideOverViewLogic()
        {
            _dataService = ServiceLocator.Current.TryResolve<IDataService>();
        }

        public IEnumerable<FrameOutline> GetFrameOutlines(Slide slide)
        {
            return _dataService.GetFrameOutlines(slide);
        }

        public BitmapImage GetCachedFrameBitmap(Slide slide, string scanAreaId, Frame frame, int decodeWidth = 0)
        {
            return _dataService.GetCachedFrameBitmap(slide, scanAreaId, frame, decodeWidth);
        }

        public BitmapImage GetSlideOverviewImage(Slide slide, string etchImage, int decodeWidth)
        {
            return _dataService.GetSlideOverviewImage(slide, etchImage, decodeWidth);
        }
    }
}
