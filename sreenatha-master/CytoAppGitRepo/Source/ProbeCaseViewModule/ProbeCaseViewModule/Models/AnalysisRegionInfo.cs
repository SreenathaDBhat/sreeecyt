﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows;
namespace ProbeCaseViewModule.Models
{
    /// <summary>
    ///This class is used to draw a region on the fraemwindow
    /// </summary>
    public class AnalysisRegionInfo : BindableBase
    {

        private ObservableCollection<Point> _points = new ObservableCollection<Point>();
        private bool _isFilled;
        private string _title;
        private bool _allowDelete = false;
        private Guid _id;
        private string _frameId;
        private bool _isVisible = true;
        private Color _color = Colors.YellowGreen;

        public AnalysisRegionInfo(Guid id, PointCollection pc = null)
        {
            this._id = id;          
            if (pc != null)
                SetPoints(pc);
        }

        public AnalysisRegionInfo() : this(Guid.NewGuid())
        {
        }

        public Guid ID
        {
            get
            {
                return _id;
            }
        }

        public string FrameId
        {
            get
            {
               return _frameId;
            }
            set
            {
                _frameId = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<Point> Points
        {
            get
            {
                return _points;
            }
        }

        public void SetPoints(PointCollection pts)
        {
            _points = new ObservableCollection<Point>(pts);
            SetLabelPosition();
            OnPropertyChanged("DisplayShape");
            OnPropertyChanged("LabelX");
            OnPropertyChanged("LabelY");
        }

        public PointCollection DisplayShape
        {
            get
            {
                return new PointCollection(from p in _points select new Point(p.X, p.Y));
            }
        }

        internal void SetLabelPosition()
        {
            if (DisplayShape != null)
            {
                Point max = new Point(0, 0);
                foreach (var p in DisplayShape)
                {
                    if (max.Y < p.Y)
                        max = p;
                }
                _labelX = max.X;
                _labelY = max.Y;
            }
        }

        private double _labelX;
        public double LabelX
        {
            get
            {
                return _labelX;
            }
            set
            {
                _labelX = value;
                OnPropertyChanged("LabelX");
            }
        }

        private double _labelY;
        public double LabelY
        {
            get
            {
                return _labelY;
            }
            set
            {
                _labelY = value;
                OnPropertyChanged("LabelY");
            }
        }

        public void AdjustLabelToFit(int fitWidth, int fitHeight)
        {
            var limit = fitHeight - 30;
            // only worry about going off the bottom really
            if (_labelY < limit)
                return;

            if (DisplayShape != null)
            {
                Point max = new Point(int.MaxValue, int.MaxValue);
                foreach (var p in DisplayShape)
                {
                    if (p.Y > limit && p.Y < max.Y)
                        max = p;
                }
                if (max.Y != int.MaxValue)
                {
                    _labelX = max.X;
                    _labelY = max.Y;
                }
            }
        }

        public bool IsVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                _isVisible = value;
                OnPropertyChanged("IsVisible");
            }
        }

        public void AddPoint(Point pt)
        {
            _points.Add(pt);
            OnPropertyChanged("DisplayShape");
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }

        public bool AllowDelete
        {
            get
            {
                return _allowDelete;
            }
            set
            {
                _allowDelete = value;
                OnPropertyChanged("AllowDelete");
                OnPropertyChanged("LabelX");
                OnPropertyChanged("LabelY");
            }
        }

        public Color Colour
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
                OnPropertyChanged("LineColour");
            }
        }

        public Brush LineColour
        {
            get
            {
                return new SolidColorBrush(_color);
            }
        }

        public bool IsFilled
        {
            get
            {
                return _isFilled;
            }
            set
            {
                _isFilled = value;
                OnPropertyChanged("IsFilled");
            }
        }


        private bool _isRegionScored = true;
        public bool IsRegionScored

        {
            get
            {
                return _isRegionScored;
            }
            set
            {
                _isRegionScored = value;
                OnPropertyChanged();
            }
        }

        private double _lineThickness = 3 / Math.Pow(1, 0.95);
        public double LineThickness
        {
            get
            {
                return _lineThickness;
            }
            set
            {
                _lineThickness = value;
                OnPropertyChanged("LineThickness");
            }
        }

        //TODO add comment
        private bool _canDelete;
        public bool CanDelete
        {
            get
            {
                return _canDelete;
            }
            set
            {
                _canDelete = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Identify the newly created region
        /// </summary>
        private bool _isNewRegion;
        public bool IsNewRegion
        {
            get
            {
                return _isNewRegion;
            }
            set
            {
                _isNewRegion = value;
                OnPropertyChanged();
            }
        }
    }
}
