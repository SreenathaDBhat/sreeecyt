﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.IO;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows;
using System.Globalization;
using CytoApps.Infrastructure.UI.Utilities;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// Annotation class to creating new annotations, Saving and Reading from xml
    /// </summary>
    public class Annotation : INotifyPropertyChanged
    {
        private const int defaultContentOffset = 10;

        private string content;
        private double x;
        private double y;
        private double scale = 1;
        private double contentOffsetX = defaultContentOffset;
        private double contentOffsetY = defaultContentOffset;
        private bool showArrowLine = true;

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        public string Content
        {
            get { return content; }
            set { content = value; Notify("Content"); }
        }

        public double X
        {
            get { return x; }
            set { x = value; Notify("X"); }
        }

        public double Y
        {
            get { return y; }
            set { y = value; Notify("Y"); }
        }

        public double Scale
        {
            get { return scale; }
            set { scale = value; Notify("Scale"); }
        }

        public double ContentOffsetX
        {
            get { return contentOffsetX; }
            set { contentOffsetX = value; Notify("ContentOffsetX"); Notify("ArrowLinePoints"); }
        }

        public double ContentOffsetY
        {
            get { return contentOffsetY; }
            set { contentOffsetY = value; Notify("ContentOffsetY"); Notify("ArrowLinePoints"); }
        }

        public bool ShowArrowLine
        {
            get { return showArrowLine; }
            set { showArrowLine = value; Notify("ShowArrowLine"); }
        }

        public PointCollection ArrowLinePoints
        {
            get
            {
                PointCollection points = new PointCollection();
                points.Add(new Point(0, 0));
                points.Add(new Point(contentOffsetX + 5, contentOffsetY + 5));
                return points;
            }
        }

        public static void SaveAnnotationsXml(FileInfo file, IEnumerable<Annotation> annotations)
        {
            var xml = new XElement("Annotations", annotations.ToList().ConvertAll<XElement>(a => new XElement("Annotation",
                                                                                                    new XAttribute("X", a.X),
                                                                                                    new XAttribute("Y", a.Y),
                                                                                                    new XAttribute("Scale", a.Scale),
                                                                                                    new XAttribute("ContentOffsetX", a.ContentOffsetX),
                                                                                                    new XAttribute("ContentOffsetY", a.ContentOffsetY),
                                                                                                    new XAttribute("ShowArrowLine", a.ShowArrowLine),
                                                                                                    new XCData(a.Content))));
            file.Refresh();
            if (file.Exists)
            {
                file.Delete();
            }

            xml.Save(file.FullName);
        }

        public static ObservableCollection<Annotation> LoadXml(FileInfo file)
        {
            if (!file.Exists)
                return new ObservableCollection<Annotation>();

            XElement xml = XElement.Load(file.FullName);

            ObservableCollection<Annotation> list = new ObservableCollection<Annotation>(from a in xml.Elements("Annotation")
                                                                                         select AnnotationFromNode(a));
            return list;
        }

        private static Annotation AnnotationFromNode(XElement xml)
        {
            Annotation a = new Annotation();

            a.X = Double.Parse(Parsing.ValueOrDefault(xml.Attribute("X"), "0"), CultureInfo.InvariantCulture);
            a.Y = Double.Parse(Parsing.ValueOrDefault(xml.Attribute("Y"), "0"), CultureInfo.InvariantCulture);
            a.ContentOffsetX = Double.Parse(Parsing.ValueOrDefault(xml.Attribute("ContentOffsetX"), "20"), CultureInfo.InvariantCulture);
            a.ContentOffsetY = Double.Parse(Parsing.ValueOrDefault(xml.Attribute("ContentOffsetY"), "20"), CultureInfo.InvariantCulture);
            a.Scale = Double.Parse(Parsing.ValueOrDefault(xml.Attribute("Scale"), "1"), CultureInfo.InvariantCulture);
            a.ShowArrowLine = bool.Parse(Parsing.ValueOrDefault(xml.Attribute("ShowArrowLine"), "true"));
            a.Content = xml.Value;
            return a;
        }
    }


    public class AnnotationsController
    {
        private string _annotationsFilePath;
        private ObservableCollection<string> _allAnnotations;

        public AnnotationsController()
        {
            string localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            //REFACTOR
            _annotationsFilePath = Path.Combine(localAppData + "\\Leica\\PCV", "pcv.annotations");
            _allAnnotations = new ObservableCollection<string>();
            LoadAllAnnotations();
        }

        public IEnumerable<string> AllAnnotations
        {
            get { return _allAnnotations; }
        }

        public void Save()
        {
            SaveAllAnnotations();
        }

        private void LoadAllAnnotations()
        {
            FileInfo annotationsFile = new FileInfo(_annotationsFilePath);

            if (annotationsFile.Exists)
            {
                using (var stream = annotationsFile.OpenText())
                {
                    while (!stream.EndOfStream)
                    {
                        var annotation = stream.ReadLine();
                        _allAnnotations.Add(annotation);
                    }
                }
            }

            SortAllAnnotations();
        }

        internal void AddAnnotation(string annotation)
        {
            if (!_allAnnotations.Contains(annotation))
            {
                _allAnnotations.Add(annotation);
                SortAllAnnotations();
                SaveAllAnnotations();
            }
        }

        private void SortAllAnnotations()
        {
            List<string> temp = _allAnnotations.ToList();
            temp.Sort();

            _allAnnotations.Clear();
            foreach (var s in temp)
                _allAnnotations.Add(s);
        }

        private void SaveAllAnnotations()
        {
            FileInfo annotationsFile = new FileInfo(_annotationsFilePath);
            if (annotationsFile.Exists)
            {
                annotationsFile.Delete();
            }


            using (var stream = annotationsFile.Create())
            {
                StreamWriter sw = new StreamWriter(stream);
                foreach (var annotation in _allAnnotations)
                    sw.WriteLine(annotation);

                sw.Close();
            }
        }

        internal void RemoveAnnotation(string annotation)
        {
            _allAnnotations.Remove(annotation);
            SaveAllAnnotations();
        }
    }
}
