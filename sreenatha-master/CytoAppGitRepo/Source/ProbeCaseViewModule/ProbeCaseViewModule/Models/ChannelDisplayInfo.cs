﻿using PCVImageRenderers.Models;
using Prism.Mvvm;
using System;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// Wrapper class for Channel Display
    /// </summary>
    public class ChannelDisplayInfo : BindableBase
    {
        public ChannelDisplayInfo(ChannelDisplay channelDisplay)
        {
            _channelDisplay = channelDisplay;
         
        }

        private ChannelDisplay _channelDisplay;

        public ChannelDisplay ChannelDisplay
        {
            get
            {
                return _channelDisplay;
            }
            set
            {
                _channelDisplay = value;
            }
        }

        public string Name
        {
            get
            {
                return _channelDisplay.Name;
            }
        }

        public string ChannelColour
        {
            get
            {
                return _channelDisplay.ColourString;
            }
            set
            {
                _channelDisplay.ColourString = value;
                OnPropertyChanged();
            }
        }

        private bool _isChecked;
        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                _isChecked = value;
                OnPropertyChanged();
            }
        }

        public double HistogramLow
        {
            get
            {
                return _channelDisplay.HistogramLow;
            }
            set
            {
                _channelDisplay.HistogramLow = Math.Min(_channelDisplay.HistogramHigh - 0.05, value);
                OnPropertyChanged();              
            }
        }

        public double HistogramHigh
        {
            get
            {
                return _channelDisplay.HistogramHigh;

            }
            set
            {
                _channelDisplay.HistogramHigh = Math.Max(_channelDisplay.HistogramLow + 0.05, value);
                OnPropertyChanged();                

            }
        }

        public double Gamma
        {
            get { return _channelDisplay.Gamma; }
            set
            {
                _channelDisplay.Gamma = Math.Min(2.0, Math.Max(0.0, value));
                OnPropertyChanged();
            }
        }

        public bool IsCounterstain
        {
            get
            {
                return _channelDisplay.IsCounterstain;
            }
        }
    }
}
