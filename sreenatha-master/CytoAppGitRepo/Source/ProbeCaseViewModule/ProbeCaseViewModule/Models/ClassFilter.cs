﻿using Microsoft.Practices.ServiceLocation;
using Prism.Events;
using Prism.Mvvm;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// This class is used filtering scored cell based on Cell Class
    /// </summary>
    public class ClassFilter : BindableBase
    {
        IEventAggregator _eventAggregator;
        public ClassFilter()
        {
            _eventAggregator = ServiceLocator.Current.TryResolve<IEventAggregator>();
        }
        #region Properties

        private string _className;
        public string ClassName
        {
            get { return _className; }
            set
            {
                _className = value;
                this.OnPropertyChanged();
            }
        }

        private bool _isChecked;
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;
                this.OnPropertyChanged();
            }
        }

        private int _classCount;
        public int ClassCount
        {
            get { return _classCount; }
            set
            {
                _classCount = value;
                this.OnPropertyChanged();
            }
        }

        private double _classPercentage;
        public double ClassPercentage
        {
            get { return _classPercentage; }
            set
            {
                _classPercentage = value;
                this.OnPropertyChanged();
            }
        }

        private bool _classFilterVisibility = true;
        public bool ClassFilterVisibility
        {
            get { return _classFilterVisibility; }
            set
            {
                _classFilterVisibility = value;
                this.OnPropertyChanged();
            }
        }
        private string _color;
        public string Color
        {
            get { return _color; }
            set
            {
                _color = value;
                this.OnPropertyChanged();
            }
        }

        public Key ShortcutKey
        {
            get;
            set;
        }

        public bool IsUninformativeClass { get { return ClassName.Equals("Uninformative", StringComparison.OrdinalIgnoreCase); } }

        #endregion

    }
}
