﻿using System.Collections.Generic;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    ///  Current Grid View Settings
    ///  To render instructions to match the grid View as closely as possible in Enhance Viewer
    /// </summary>
    public class EnhanceViewOptions
    {
        public HashSet<OverlayInfo> Overlays
        {
            get;
            set;
        }
        public HashSet<ChannelDisplayInfo> ChannelDisplays
        {
            get;
            set;
        }

        public int CurrentStackIndex
        {
            get;
            set;
        }

        public bool AttenuateCounterstain
        {
            get;
            set;
        }

        public bool InvertCounterstain
        {
            get;
            set;
        }

        public bool DisplayIndividualCounts
        {
            get;
            set;
        }

        public bool DisplayCellIDs
        {
            get;
            set;
        }
    }
}
