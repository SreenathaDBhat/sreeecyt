﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// Wrapper class for PCV.Models Annotation, Can be used in generic annotations
    /// </summary>
    public class FrameAnnotation : BindableBase
    {
        private const int defaultContentOffset = 10;
        private ProbeCaseViewDataAccess.Models.Annotation _annotation;

        public FrameAnnotation(ProbeCaseViewDataAccess.Models.Annotation annotation)
        {
            _annotation = annotation;
        }

        public ProbeCaseViewDataAccess.Models.Annotation Annotation
        {
            get
            {
                return _annotation;
            }
        }

        public string Content
        {
            get { return _annotation.Content; }
            set
            {
                _annotation.Content = value;
                OnPropertyChanged("Content");
            }
        }

        public double X
        {
            get { return _annotation.X; }
            set
            {
                _annotation.X = value;
                OnPropertyChanged("X");
            }
        }

        public double Y
        {
            get { return _annotation.Y; }
            set
            {
                _annotation.Y = value;
                OnPropertyChanged("Y");
            }
        }

        public double Scale
        {
            get
            { return _annotation.Scale; }
            set
            {
                _annotation.Scale = value;
                OnPropertyChanged("Scale");
            }
        }

        public double ContentOffsetX
        {
            get { return _annotation.ContentOffsetX; }
            set
            {
                _annotation.ContentOffsetX = value;
                OnPropertyChanged("ContentOffsetX");
                OnPropertyChanged("ArrowLinePoints");
            }
        }

        public double ContentOffsetY
        {
            get { return _annotation.ContentOffsetY; }
            set
            {
                _annotation.ContentOffsetY = value;
                OnPropertyChanged("ContentOffsetY");
                OnPropertyChanged("ArrowLinePoints");
            }
        }

        public bool ShowArrowLine
        {
            get { return _annotation.ShowArrowLine; }
            set
            {
                _annotation.ShowArrowLine = value;
                OnPropertyChanged("ShowArrowLine");
            }
        }

        public PointCollection ArrowLinePoints
        {
            get
            {
                PointCollection points = new PointCollection();
                points.Add(new Point(0, 0));
                points.Add(new Point(ContentOffsetX + 5, ContentOffsetY + 5));
                return points;
            }
        }

        private bool _isDeleted;
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                OnPropertyChanged();
            }
        }

        private Color _foregroundBrush;
        public Color ForegroundBrush
        {
            get { return _foregroundBrush; }
            set { _foregroundBrush = value; }
        }
    }

}
