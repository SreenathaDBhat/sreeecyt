﻿using PCVImageRenderers.Models;
using Prism.Mvvm;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// Wrapper class for PCVImageRenderers.Models.Frame
    /// </summary>
    public class FrameInfo : BindableBase
    {
        private bool _starred;
        private Frame _imageFrame;

        public FrameInfo(int identifier)
        {
            Identifier = identifier;
        }

        public int Identifier
        {
            get;
            private set;
        }

        public Frame Image
        {
            get
            {
                return _imageFrame;
            }
            set
            {
                _imageFrame = value;
                OnPropertyChanged();
                OnPropertyChanged("FrameHeight");
                OnPropertyChanged("FrameWidth");
            }
        }

        public int FrameHeight
        {
            get
            {
                return _imageFrame.Height;
            }
            set
            {
                _imageFrame.Height = value;
                OnPropertyChanged();
            }
        }

        public int FrameWidth
        {
            get
            {
                return _imageFrame.Width;
            }
            set
            {
                _imageFrame.Width = value;
                OnPropertyChanged();
            }
        }

        public bool IsStarred
        {
            get
            {
                return _starred;
            }
            set
            {
                _starred = value;
                OnPropertyChanged();
            }
        }
    }
}
