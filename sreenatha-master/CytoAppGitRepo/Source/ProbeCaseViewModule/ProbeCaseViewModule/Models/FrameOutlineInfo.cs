﻿using Prism.Mvvm;
using ProbeCaseViewDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ProbeCaseViewModule.Models
{
    public class FrameOutlineInfo : BindableBase
    {
        public double PositionX
        {
            get; private set;
        }
        public double PositionY
        {
            get; private set;
        }
        public double Width
        {
            get; private set;
        }
        public double Height
        {
            get; private set;
        }
        public double CentreX
        {
            get; private set;
        }
        public double CentreY
        {
            get; private set;
        }
        public string ThumbnailPath { get; set; }
        public bool IsOverviewImage { get; set; }
        public string CentrePos { get; set; }

        private ImageSource thumbnail;
        public ImageSource Thumbnail
        {
            get { return thumbnail; }
            set
            {
                thumbnail = value;
                OnPropertyChanged("Thumbnail");
            }
        }

        public PCVImageRenderers.Models.Frame Frame { get; set; }

        public FrameOutlineInfo(PCVImageRenderers.Models.Frame frame, double scalex, double scaley)
        {
            Frame = frame;
            CentreX = frame.IdealX * scalex;
            CentreY = frame.IdealY * scaley;
            Width = frame.Width * frame.XScale * scalex;
            Height = frame.Height * frame.YScale * scaley;
            PositionX = CentreX - (Width / 2);
            PositionY = CentreY - (Height / 2);
            ThumbnailPath = System.IO.Path.Combine(Frame.ImageFramesFileLocation, Frame.Id.ToString() + ".thumbnail.jpg");
            CentrePos = ((int)frame.IdealX).ToString() + "," + ((int)frame.IdealY).ToString();

            OnPropertyChanged("PositionX"); OnPropertyChanged("PositionY"); OnPropertyChanged("Width"); OnPropertyChanged("Height"); OnPropertyChanged("CentreX"); OnPropertyChanged("CentreY");
            OnPropertyChanged("CentrePos");
        }

        public FrameOutlineInfo(double scalex, double scaley, FrameOutline frameOutline)
        {
            PositionX = frameOutline.PositionX * scalex;
            PositionY = frameOutline.PositionY * scaley;
            Width = frameOutline.Width * scalex;
            Height = frameOutline.Height * scaley;
            CentreX = PositionX + (Height / 2);
            CentreY = PositionY + (Width / 2);
            ThumbnailPath = frameOutline.ThumbnailPath;
            CentrePos = ((int)CentreX).ToString() + "," + ((int)CentreY).ToString();

            OnPropertyChanged("PositionX"); OnPropertyChanged("PositionY"); OnPropertyChanged("Width"); OnPropertyChanged("Height"); OnPropertyChanged("CentreX"); OnPropertyChanged("CentreY");
            OnPropertyChanged("CentrePos");
        }
    }
}
