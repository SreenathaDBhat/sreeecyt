﻿using PCVImageRenderers.Models;
using System.Windows;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// This Class is used for converting MouseEventsArgs to  PCVImageRenderers.Models.ComponentLevels and MousePoint(X,Y)
    /// </summary>
    public class HistogramMouseEventArgs
    {
        public ComponentLevels ComponentLevels
        {
            get; set;
        }

        public Point Point
        {
            get; set;
        }
    }
}
