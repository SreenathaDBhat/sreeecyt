﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ProbeCaseViewModule.Models
{
    public class MouseClickEventArgs
    {
        public Point Points
        {
            get;
            set;
        }

        public int ClickCount
        {
            get;
            set;
        }
    }

    public class AnnotationEventArgs
    {
        public Point CurrentMousePosition
        {
            get;
            set;
        }
        public bool IsMouseCapture
        {
            get;
            set;
        }

        public Point WindowMousePosition
        {
            get;
            set;
        }

        public bool IsValid
        {
            get;
            set;
        }

    }

    public class RegionEventArgs
    {
        public Point CurrentMousePosition
        {
            get;
            set;
        }
        public bool isDrawing
        {
            get;
            set;
        }
        public bool IsShiftPressed
        {
            get;
            set;
        }
        public bool IsMouseCapture
        {
            get;
            set;
        }

        public double Height
        {
            get;
            set;
        }
        public double Width
        {
            get;
            set;
        }

    }
}
