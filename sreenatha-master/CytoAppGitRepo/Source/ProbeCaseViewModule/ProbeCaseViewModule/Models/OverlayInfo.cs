﻿using Prism.Mvvm;
using ProbeCaseViewDataAccess.Models;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// Wrapper class for PCVDAL.Models.Overlay 
    /// </summary>
    public class OverlayInfo : BindableBase
    {
        public OverlayInfo(Overlay overlay)
        {
            _overlay = overlay;
        }

        private Overlay _overlay;

        public Overlay Overlay
        {
            get
            {
                return _overlay;
            }
        }

        public string Name
        {
            get
            {
                return _overlay.Name;
            }
        }

        public string RenderColor
        {
            get
            {
                return _overlay.RenderColor;
            }
        }

        private bool _isChecked;
        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                _isChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }
    }
}
