﻿using CytoApps.Infrastructure.UI.Utilities;
using CytoApps.SpotConfiguration;
using Prism.Mvvm;
using ProbeCaseViewDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// This class is used for Assay configurations
    /// </summary>
    public class ProbeScoringAssay : BindableBase
    {
        private enum MatchComparison { Less, LessEqual, Equal, GreaterEqual, Greater, NotEqual };
        private Guid _id;
        private ObservableCollection<ProbeScoringClass> _classes;
        private string _name;
        //private bool duplicateClassNames = false;

        public CytoApps.SpotConfiguration.SpotConfigController SpotConfigController
        {
            get; private set;
        }

        public ProbeScoringAssay()
        {
            _id = Guid.NewGuid();
            _name = "Unnamed";
            _classes = new ObservableCollection<ProbeScoringClass>();
        }

        public ProbeScoringAssay(IEnumerable<ProbeScoringClass> classesToUse)
        {
            _id = Guid.NewGuid();
            _name = "Unnamed";
            _classes = new ObservableCollection<ProbeScoringClass>(classesToUse);
        }

        public ProbeScoringAssay(CytoApps.SpotConfiguration.SpotConfigController spotController)
        {
            _id = Guid.NewGuid();
            _name = spotController.AssayName;
            _classes = new ObservableCollection<ProbeScoringClass>();

            foreach (var c in spotController.ClassList)
            {
                // Only add the class if there is not already a class with the same name.
                // Class names must be unique within the class list (the 'ProbeScoringAssay'),
                // but don't have to be within the (actual) assay (multiple classes with the same name map to a single class in the class list,
                // their classification criteria merging with a logical OR).
                if (!ContainsClass(c.Name))
                    _classes.Add(new ProbeScoringClass { Text = c.Name, Color = c.DisplayColour, ShortcutKey = NextAvailableShortcut(this) });
            }

            _classes.Add(new ProbeScoringClass { Text = "Uninformative", Color = Color.FromRgb(160, 160, 160), ShortcutKey = Key.F11 });

            SpotConfigController = spotController;
        }

        // To Create ProbeScoringAssay object, By inferring the class list from SessionScores.     
        public ProbeScoringAssay(SessionScores sessionScores, string assayName = "")
        {
            _id = Guid.NewGuid();
            _name = assayName;
            _classes = new ObservableCollection<ProbeScoringClass>();
            var distinctScores = sessionScores.Scores.GroupBy(s => s.Class).Distinct().Select(sc => sc.First());
            foreach (var score in distinctScores)
            {
                if (string.Compare(score.Class, "uninformative", true) == 0)
                {
                    _classes.Add(new ProbeScoringClass { Text = "Uninformative", Color = Color.FromRgb(160, 160, 160), ShortcutKey = Key.F11 });
                }
                else
                {
                    _classes.Add(new ProbeScoringClass { Text = score.Class, Color = ColorParse.HexStringToColor(score.ColourString), ShortcutKey = NextAvailableShortcut(this) });
                }
            }
            _classes = new ObservableCollection<ProbeScoringClass>(_classes.OrderBy(o => o.ShortcutKey));
        }

        public bool ContainsClass(string name)
        {
            if (_classes.Where(c => c.Text.Equals(name, StringComparison.OrdinalIgnoreCase)).Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Guid Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (_name != value)
                {
                    _name = value;

                    if (SpotConfigController != null)
                    {
                        SpotConfigController.AssayName = _name;
                        //TODO Save();
                    }

                    OnPropertyChanged();
                }
            }
        }

        public IEnumerable<ProbeScoringClass> Classes
        {
            get
            {
                return _classes;
            }
            set
            {
                _classes = new ObservableCollection<ProbeScoringClass>(value);


                // TODO
                //foreach (ProbeScoringClass p in _classes)
                //{
                //    p.PropertyChanged += newClass_PropertyChanged;
                //}
            }
        }

        public bool IsEditable
        {
            get
            {
                return !AssayInfo.IsCEPXYAssayName(Name);
            }
        }

        public bool HasRatioConstraints
        {
            get
            {
                return SpotConfigController != null && SpotConfigController.RatioList != null && SpotConfigController.RatioList.Count() > 0;
            }
        }

        public const int MAXCLASSES = 20; // This should match the constant with the same name in AI_CODE_TREE\Common\include\SpotConfig.h
                                          // Note this is the maximum number of classes NOT including the "Uninformative" class.

        public bool AddClass(ProbeScoringClass newClass)
        {
            // Only add the class if there is not already a class with the same name.
            // Class names must be unique within the class list (the 'ProbeScoringAssay').
            if (!ContainsClass(newClass.Text))
            {
                if (_classes.Count() > 0)
                    _classes.Insert(Classes.Count() - 1, newClass); // add before uninformative
                else
                    _classes.Add(newClass);

                if (SpotConfigController != null)
                {
                    SpotConfigController.AddClass(new SpotClass { Name = newClass.Text, DisplayColour = newClass.Color });
                    Save();
                }
                ForceShortcutsReassign();
                return true;
            }

            return false; // Not added.
        }

        public Key NextAvailableShortcut(ProbeScoringAssay scoringAssay)
        {
            // Shortcut 'hot' keys are allocated in the folowing order:
            // F1 to F10, then 1 to 9 (numbers from the main keyboard, not the numeric keypad),
            // then 0 (the integer value for the 0 key is smaller than that for the 1 key, so
            // it must be treated separately in order for it to appear after the 9 key).
            Key key = Key.F1;
            while (true)
            {
                if (scoringAssay._classes.Where(c => c.ShortcutKey == key).Count() == 0)
                {
                    return key;
                }
                else
                {
                    if ((key >= Key.F1 && key < Key.F10)
                        || (key >= Key.D1 && key < Key.D9))
                        key++;
                    else if (key >= Key.F10)
                        key = Key.D1;
                    else if (key >= Key.D9)
                        key = Key.D0;
                }
            }
        }

        public SpotClass Classify(SignalCount[] classifiableCounts)
        {
            if (SpotConfigController == null || SpotConfigController.ClassList.Count() == 0)
            {
                return null;
            }

            var probeCounts = classifiableCounts.Where(c => c.SignalInfo.SignalName != "Fusions").ToArray();
            var fusionCounts = classifiableCounts.Where(c => c.SignalInfo.SignalName == "Fusions").ToArray();
            System.Diagnostics.Debug.Assert(fusionCounts.Count() <= 1, "0 or 1 fusion categories supported");
            var fusionCount = fusionCounts.FirstOrDefault();

            var firstClass = SpotConfigController.ClassList.First();
            if (probeCounts.Length != firstClass.ProbeCounts.Count || fusionCounts.Length != firstClass.FusionCounts.Count)
            {
                return null;
            }

            foreach (var potentialClass in SpotConfigController.ClassList)
            {
                bool allRulesPassed = true;

                // check fusion
                if (fusionCount != null)
                {
                    if (!CountSatisfiesFormula(fusionCount.Count, potentialClass.FusionCounts[0].Formula))
                    {
                        allRulesPassed = false;
                        continue;
                    }
                }

                // check probes
                if (allRulesPassed)
                {
                    for (int i = 0; i < potentialClass.ProbeCounts.Count; ++i)
                    {
                        SignalCount count = probeCounts[i];
                        CountOrRatio condition = potentialClass.ProbeCounts[i];

                        if (!CountSatisfiesFormula(count.Count, condition.Formula))
                        {
                            allRulesPassed = false;
                            continue;
                        }
                    }
                }

                // finally, the ratios
                if (allRulesPassed)
                {
                    for (int i = 0; i < potentialClass.Ratios.Count(); ++i)
                    {
                        Ratio ratioInfo = SpotConfigController.RatioList.ElementAt(i);
                        CountOrRatio ratioCondition = potentialClass.Ratios.ElementAt(i);
                        SignalCount numerator = CountForChannel(classifiableCounts, ratioInfo.NumeratorProbeName);
                        SignalCount denominator = CountForChannel(classifiableCounts, ratioInfo.DenominatorProbeName);
                        if (numerator != null && denominator != null)
                        {
                            double ratio = numerator.Count / (double)denominator.Count;
                            if (!CountSatisfiesFormula(ratio, ratioCondition.Formula))
                            {
                                allRulesPassed = false;
                                continue;
                            }
                        }
                    }
                }

                if (allRulesPassed)
                {
                    return potentialClass;
                }
            }
            return null;
        }

        private SignalCount CountForChannel(SignalCount[] classifiableCounts, string channelName)
        {
            for (int i = 0; i < classifiableCounts.Length; ++i)
            {
                var c = classifiableCounts[i];
                if (string.Compare(c.SignalInfo.SignalName, channelName, true) == 0)
                {
                    return c;
                }
            }
            return null;
        }

        private bool CountSatisfiesFormula(double count, string matchFormula)
        {
            if (matchFormula == "*")
            {
                return true;
            }

            string processedFormulaString = matchFormula;
            processedFormulaString = processedFormulaString.Replace("\t", "");
            processedFormulaString = processedFormulaString.Replace("\n", "");
            processedFormulaString = processedFormulaString.Replace(" ", "");

            string[] parts = processedFormulaString.Split('&');
            foreach (var formula in parts)
            {
                MatchComparison comparison = MatchComparison.Equal;
                string number = formula;

                if (number.StartsWith("<="))
                {
                    comparison = MatchComparison.LessEqual;
                    number = number.Substring(2);
                }
                else if (number.StartsWith(">="))
                {
                    comparison = MatchComparison.GreaterEqual;
                    number = number.Substring(2);
                }
                else if (number.StartsWith("<"))
                {
                    comparison = MatchComparison.Less;
                    number = number.Substring(1);
                }
                else if (number.StartsWith(">"))
                {
                    comparison = MatchComparison.Greater;
                    number = number.Substring(1);
                }

                double conditionValue;
                if (double.TryParse(number, out conditionValue))
                {
                    if (comparison == MatchComparison.Equal)
                    {
                        if (Math.Abs(count - conditionValue) > 0.00001)
                        {
                            return false;
                        }
                    }
                    else if (comparison == MatchComparison.NotEqual)
                    {
                        if (Math.Abs(count - conditionValue) < 0.00001)
                        {
                            return false;
                        }
                    }
                    else if (comparison == MatchComparison.Greater)
                    {
                        if (count <= conditionValue)
                        {
                            return false;
                        }
                    }
                    else if (comparison == MatchComparison.GreaterEqual)
                    {
                        if (count < conditionValue)
                        {
                            return false;
                        }
                    }
                    else if (comparison == MatchComparison.Less)
                    {
                        if (count >= conditionValue)
                        {
                            return false;
                        }
                    }
                    else if (comparison == MatchComparison.LessEqual)
                    {
                        if (count > conditionValue)
                        {
                            return false;
                        }
                    }
                }
                else {
                    return false;
                }
            }
            return true;
        }

        public double CaluclateRatio(IEnumerable<SignalCount> counts)
        {
            if (!HasRatioConstraints)
            {
                return 0;
            }

            var ratio = SpotConfigController.RatioList.First();
            var numeratorCount = counts.Where(c => string.Compare(c.SignalInfo.SignalName, ratio.NumeratorProbeName, true) == 0).FirstOrDefault();
            var denominatorCount = counts.Where(c => string.Compare(c.SignalInfo.SignalName, ratio.DenominatorProbeName, true) == 0).FirstOrDefault();

            if (numeratorCount == null || denominatorCount == null)
            {
                return 0;
            }
            else {
                return numeratorCount.Count / (double)denominatorCount.Count;
            }
        }

        public int CountInformativeClasses()
        {
            int count = 0;
            foreach (ProbeScoringClass c in _classes)
            {
                if (!c.Text.Equals("Uninformative", StringComparison.OrdinalIgnoreCase))
                    count++;
            }
            return count;
        }

        public void Save()
        {
            if (SpotConfigController != null)
                SpotConfigController.SaveAssay();
        }

        public void ForceShortcutsReassign()
        {
            foreach (var c in _classes)
                c.ShortcutKey = Key.None;

            foreach (var c in _classes)
            {
                if (c.Text.Equals("Uninformative", StringComparison.OrdinalIgnoreCase))
                    c.ShortcutKey = Key.F11;
                else
                    c.ShortcutKey = NextAvailableShortcut(this);
            }
        }

    }
}
