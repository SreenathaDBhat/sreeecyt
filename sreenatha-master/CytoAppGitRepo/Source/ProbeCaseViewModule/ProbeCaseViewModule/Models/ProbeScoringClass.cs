﻿using CytoApps.Infrastructure.UI.Utilities;
using Prism.Mvvm;
using System;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Linq;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// This class is defining PCVcell's Class such as 2O2G, 1O2G 's etc.
    /// </summary>
    public class ProbeScoringClass : BindableBase
    {
        private string _text;

        public ProbeScoringClass()
        {
            ShortcutKey = Key.None;
            Text = string.Empty;
            Color = FixedColorForShortcutKey(ShortcutKey);
        }

        public Color Color
        {
            get;
            set;
        }

        public Key ShortcutKey
        {
            get;
            set;
        }

        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
                OnPropertyChanged();
            }
        }

        public static Color FixedColorForShortcutKey(Key shortcut)
        {
            if (shortcut == Key.F1)
                return Colors.Green;
            else if (shortcut == Key.F2)
                return Colors.Red;
            else if (shortcut == Key.F3)
                return Colors.Blue;
            else if (shortcut == Key.F4)
                return Colors.Cyan;
            else if (shortcut == Key.F5)
                return Colors.Magenta;
            else if (shortcut == Key.F6)
                return Colors.Yellow;
            else if (shortcut == Key.F7)
                return Colors.Orange;
            else if (shortcut == Key.F8)
                return Colors.LimeGreen;
            else if (shortcut == Key.F9)
                return Colors.Beige;
            else if (shortcut == Key.F10)
                return Colors.Plum;
            else if (shortcut == Key.D1)
                return Colors.Crimson;
            else if (shortcut == Key.D2)
                return Colors.Olive;
            else if (shortcut == Key.D3)
                return Colors.Purple;
            else if (shortcut == Key.D4)
                return Colors.Maroon;
            else if (shortcut == Key.D5)
                return Colors.Lime;
            else if (shortcut == Key.D6)
                return Colors.BurlyWood;
            else if (shortcut == Key.D7)
                return Colors.SaddleBrown;
            else if (shortcut == Key.D8)
                return Colors.Teal;
            else if (shortcut == Key.D9)
                return Colors.Orchid;
            else if (shortcut == Key.D0)
                return Colors.DarkGoldenrod;
            else if (shortcut == Key.Back)
                return Colors.Black;
            else
                return Colors.CornflowerBlue; // LOL
        }
               
      
    }
}
