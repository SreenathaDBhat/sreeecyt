﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// This class is used for displaying Scored cell statistics such as signalAverages, FusionAverages and Ratios
    /// </summary>
    public class ScoreStat
    {
        public string Name { get; set; }
        public Color Color { get; set; }
        public Color ControlSignalColor { get; set; }
        public double DoubleValue { get; set; }
        public double Numerator { get; set; }
        public double Denominator { get; set; }
        public bool IsControl { get; set; }

        public Brush Brush
        {
            get
            {
                return new SolidColorBrush(Color);
            }
        }

        public Brush ControlSignalBrush
        {
            get
            {
                return new SolidColorBrush(ControlSignalColor);
            }
        }
    }

    public class ScoreStats
    {
        private ObservableCollection<ScoreStat> signalAverages;
        private ObservableCollection<ScoreStat> ratios;
        private ObservableCollection<ScoreStat> fusionAverages;

        public ScoreStats()
        {
            signalAverages = new ObservableCollection<ScoreStat>();
            ratios = new ObservableCollection<ScoreStat>();
            fusionAverages = new ObservableCollection<ScoreStat>();
        }

        public ObservableCollection<ScoreStat> SignalAverages
        {
            get { return signalAverages; }
        }

        public ObservableCollection<ScoreStat> Ratios
        {
            get { return ratios; }
        }

        public ScoreStat Ratio
        {
            get { return ratios.FirstOrDefault(); }
        }

        public ObservableCollection<ScoreStat> FusionAverages
        {
            get { return fusionAverages; }
        }

        public bool IsFusions
        {
            get { return fusionAverages.Count() > 0; }
        }

        public bool IsSignalAverages
        {
            get { return signalAverages.Count() > 0; }
        }

        public bool IsRatios
        {
            get { return ratios.Count() > 0; }
        }



        public void CalculateStats(ProbeScoringAssay assay, IEnumerable<ScoredCell> scores)
        {
            signalAverages.Clear();
            ratios.Clear();
            fusionAverages.Clear();

            if (assay == null || assay.SpotConfigController == null)
                return;

            var assayControl = assay.SpotConfigController;
            var signals = assayControl.ProbeList.ToArray();

            //Only use scores that have been auto scored, using number of spot measurements for this, > 2 if generated from the script
            var validScores = scores.Where(s => s.Class != "Uninformative" && (s.Score.SpotMeasurements != null && s.Score.SpotMeasurements.Count() > 2));
            var nCells = validScores.Count();

            var list = new List<ScoreStat>();
            foreach (var sg in signals)
            {
                var cnt = (from s in validScores
                           select s.Counts.Where(x => x.SignalInfo.SignalName == sg.Name).Sum(a => a.Count)).Sum();

                var average = nCells == 0 ? 0.0 : (double)cnt / (double)nCells;
                list.Add(new ScoreStat
                {
                    Name = sg.Name,
                    DoubleValue = average,
                    Color = sg.PseudoColour,
                    ControlSignalColor = Colors.Transparent,
                    IsControl = sg.Name == ControlSignalName(assayControl.RatioList),
                    Numerator = cnt,
                    Denominator = nCells
                });
            }

            foreach (var s in list.OrderBy(a => a.IsControl))
                signalAverages.Add(s);

            var ratioList = assayControl.RatioList.ToArray();

            foreach (var r in ratioList)
            {
                var cntDenominator = (from s in validScores
                                      select s.Counts.Where(x => x.SignalInfo.SignalName == r.DenominatorProbeName).Sum(a => a.Count)).Sum();
                var cntNumerator = (from s in validScores
                                    select s.Counts.Where(x => x.SignalInfo.SignalName == r.NumeratorProbeName).Sum(a => a.Count)).Sum();
                double ratio = cntDenominator == 0 ? 0.0 : (double)cntNumerator / (double)cntDenominator;
                var testColour = (from s in signals where s.Name == r.NumeratorProbeName select s.PseudoColour).FirstOrDefault();
                var controlColour = (from s in signals where s.Name == r.DenominatorProbeName select s.PseudoColour).FirstOrDefault();

                ratios.Add(new ScoreStat { Name = r.Name, DoubleValue = ratio, Color = testColour, ControlSignalColor = controlColour, Numerator = cntNumerator, Denominator = cntDenominator });
            }

            var fusionList = assayControl.FusionList.ToArray();

            //should be as above but name is wrong in signalinfo, e.g fusions rather tahn fusion, do this for now
            if (fusionList.Count() > 0)
            {
                var cnt = (from s in validScores
                           select s.Counts.Where(x => x.SignalInfo.SignalName == "Fusions").Sum(a => a.Count)).Sum();
                var average = nCells == 0 ? 0.0 : (double)cnt / (double)nCells;
                fusionAverages.Add(new ScoreStat { Name = "Fusions", DoubleValue = average, Color = Colors.LightGray, Numerator = cnt, Denominator = nCells });
            }
        }

        private string ControlSignalName(IEnumerable<CytoApps.SpotConfiguration.Ratio> ratioList)
        {
            if (ratioList == null)
                return null;

            //actually only ever one ratio so
            var ratio = ratioList.FirstOrDefault();
            if (ratio != null)
                return ratio.DenominatorProbeName;
            return null;
        }

        public BitmapSource TwoToneRectangle(SolidColorBrush left, SolidColorBrush right)
        {
            const int w = 16;
            const int h = 16;

            var s = new StackPanel();
            s.Orientation = Orientation.Horizontal;

            var l = new System.Windows.Shapes.Rectangle()
            {
                Width = w / 2,
                Height = h,
                Fill = left
            };

            var r = new System.Windows.Shapes.Rectangle()
            {
                Width = w / 2,
                Height = h,
                Fill = right
            };
            s.Children.Add(l);
            s.Children.Add(r);

            s.Measure(new System.Windows.Size(w, h));
            s.Arrange(new Rect(0, 0, w, h));

            RenderTargetBitmap bmp = new RenderTargetBitmap(w, h, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(s);
            bmp.Freeze();
            return bmp;
        }

        private BitmapSource GetColourShape(ScoreStat s)
        {
            if (s.Name == "Ratio")
                return TwoToneRectangle(new SolidColorBrush(s.Color), new SolidColorBrush(s.ControlSignalColor));
            else
                return EllipseImageForColor(s.Color);
        }

        public BitmapSource EllipseImageForColor(Color c)
        {
            return EllipseImageForColor(new SolidColorBrush(c));
        }

        public BitmapSource EllipseImageForColor(Brush fill)
        {
            var e = new System.Windows.Shapes.Ellipse()
            {
                Width = 16,
                Height = 16,
                Fill = fill
            };

            e.Measure(new Size(16, 16));
            e.Arrange(new Rect(1, 1, 16, 16));

            var r = new System.Windows.Shapes.Rectangle()
            {
                Width = 18,
                Height = 18,
                Fill = Brushes.White
            };

            r.Measure(new Size(18, 18));
            r.Arrange(new Rect(0, 0, 18, 18));

            RenderTargetBitmap bmp = new RenderTargetBitmap(18, 18, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(r);
            bmp.Render(e);
            bmp.Freeze();
            return bmp;
        }


        public ScoreStats Average(IEnumerable<ScoreStats> set)
        {
            set = from s in set where s != null select s;

            if (set.Count() < 1)
            {
                return null;
            }
            else if (set.Count() == 1)
            {
                return set.First();
            }
            else {
                //  sense check the inputs
                var firstStat = set.First();
                foreach (var stat in set.Skip(1))
                {
                    if (stat == null || firstStat == null)
                    { // bugfix
                        continue;
                    }

                    if (stat.SignalAverages.Count != firstStat.SignalAverages.Count || stat.Ratios.Count != firstStat.Ratios.Count || stat.FusionAverages.Count != firstStat.FusionAverages.Count)
                    {
                        return null;
                    }
                    for (int i = 0; i < stat.SignalAverages.Count; ++i)
                    {
                        if (firstStat.SignalAverages[i].Name != stat.SignalAverages[i].Name)
                        {
                            return null;
                        }
                    }
                    for (int i = 0; i < stat.FusionAverages.Count; ++i)
                    {
                        if (firstStat.FusionAverages[i].Name != stat.FusionAverages[i].Name)
                        {
                            return null;
                        }
                    }
                    for (int i = 0; i < stat.Ratios.Count; ++i)
                    {
                        if (firstStat.Ratios[i].Name != stat.Ratios[i].Name)
                        {
                            return null;
                        }
                    }
                }

                var newOne = new ScoreStats();
                newOne.signalAverages = new ObservableCollection<ScoreStat>(from s in firstStat.SignalAverages
                                                                            select Average(Flatten(from x in set select (IEnumerable<ScoreStat>)x.SignalAverages), s.Name));
                newOne.ratios = new ObservableCollection<ScoreStat>(from s in firstStat.Ratios
                                                                    select Average(Flatten(from x in set select (IEnumerable<ScoreStat>)x.Ratios), s.Name));
                newOne.fusionAverages = new ObservableCollection<ScoreStat>(from s in firstStat.FusionAverages
                                                                            select Average(Flatten(from x in set select (IEnumerable<ScoreStat>)x.FusionAverages), s.Name));


                return newOne;
            }
        }

        private ScoreStat Average(IEnumerable<ScoreStat> values, string name)
        {
            var signals = from s in values
                          where s.Name == name
                          select s;

            double numerator = signals.Sum(s => s.Numerator);
            double denominator = signals.Sum(s => s.Denominator);

            return new ScoreStat()
            {
                Name = name,
                Color = signals.First().Color,
                DoubleValue = denominator == 0.0 ? 0.0 : numerator / denominator,
                Numerator = numerator,
                Denominator = denominator,
                ControlSignalColor = signals.First().ControlSignalColor
            };
        }

        private IEnumerable<T> Flatten<T>(IEnumerable<IEnumerable<T>> bumpy)
        {
            List<T> list = new List<T>();
            foreach (var x in bumpy)
            {
                list.AddRange(x);
            }

            return list;
        }
    }
}
