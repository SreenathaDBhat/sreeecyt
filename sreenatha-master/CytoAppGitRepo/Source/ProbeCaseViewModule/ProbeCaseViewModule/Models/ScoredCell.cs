﻿using CytoApps.Models;
using PCVImageRenderers;
using PCVImageRenderers.Models;
using Prism.Mvvm;
using ProbeCaseViewDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// Wrapper class for Score class with ImageRendering
    /// </summary>
    public class ScoredCell : BindableBase
    {
        private StackedThumbnailRenderer _thumbnailRenderer;

        public ScoredCell(Score score, StackedThumbnailRenderer thumbnailRenderer)
        {
            _counts = new ObservableCollection<SignalCount>();
            this._score = score;
            this._thumbnailRenderer = thumbnailRenderer;
        }

        public ScoredCell(Score score)
        {
            _counts = new ObservableCollection<SignalCount>();
            this._score = score;
        }

        public ScoredCell(Score score, PointCollection points)
        {
            _counts = new ObservableCollection<SignalCount>();
            this._score = score;
            Region = points;
        }

        public string Class
        {
            get
            {
                return _score.Class;
            }
            set
            {
                _score.Class = value;
                OnPropertyChanged();
            }
        }


        public string Color
        {
            get { return _score.ColourString; }
            set
            {
                _score.ColourString = value;
                OnPropertyChanged();
            }
        }


        public double X
        {
            get
            {
                return _score.X;
            }
            set
            {
                _score.X = value;
                OnPropertyChanged();
            }
        }
        public double Y
        {
            get
            {
                return _score.Y;
            }
            set
            {
                _score.Y = value;
                OnPropertyChanged();
            }
        }

        public List<Measurement> SpotMeasurements
        {
            get
            {
                return _score.SpotMeasurements;
            }
            set
            {
                _score.SpotMeasurements = value;
                OnPropertyChanged();
            }
        }

        private Score _score;
        public Score Score
        {
            get
            {
                return _score;
            }
        }

        public Guid FrameId
        {
            get
            {
                return _score.ImageFrameId;
            }
        }

        public static readonly string ScoredCellIdNumberNone = "n/a";
        public string ScoredCellId
        {
            get
            {
                var na = ScoredCellIdNumberNone;

                if (Score.SpotMeasurements == null)
                    return na;

                var m = Score.SpotMeasurements.Where(s => s.OriginalName == "MEAS_Id").FirstOrDefault();
                return m == null ? na : m.Value;
            }
        }

        private bool _reviewed;
        public bool Reviewed
        {
            get { return _reviewed; }
            set
            {
                _reviewed = value;
                OnPropertyChanged();
            }
        }

        private bool _selected;
        public bool Selected
        {
            get { return _selected; }
            set
            {
                _selected = value;
                OnPropertyChanged();
            }
        }

        private bool _isHighlighted;
        public bool IsHighlighted
        {
            get
            {
                return _isHighlighted;
            }
            set
            {
                _isHighlighted = value;
                OnPropertyChanged();
            }
        }

        private bool _isDeleted;
        public bool IsDeleted
        {
            get
            {
                return _isDeleted;
            }
            set
            {
                _isDeleted = value;
                OnPropertyChanged();
            }
        }

        #region Overlays Properties commented
        //private IEnumerable<OverlayInfo> _overlays;
        //public IEnumerable<OverlayInfo> Overlays
        //{
        //    get { return _overlays; }
        //}
        #endregion

        private BitmapSource _thumbnail;
        public BitmapSource Thumbnail
        {
            get
            {
                return _thumbnail;
            }
            set
            {
                _thumbnail = value;
                RenderTime = DateTime.Now;
                OnPropertyChanged();
            }
        }

        private ImageSource _thumbnailOverlay;
        public ImageSource ThumbnailOverlay
        {
            get
            {
                return _thumbnailOverlay;
            }
            set
            {
                _thumbnailOverlay = value;
                OnPropertyChanged();
            }
        }

        public DateTime RenderTime
        {
            get;
            private set;
        }

        public StackedThumbnailRenderer ThumbRenderer
        {
            get { return _thumbnailRenderer; }
        }

        private bool _hasEnhancements;
        public bool HasEnhancements
        {
            get { return _hasEnhancements; }
            set
            {
                _hasEnhancements = value;
                OnPropertyChanged();
            }
        }

        private byte[] _cacheThumbnailData;
        public byte[] CacheThumbnailData
        {
            get { return _cacheThumbnailData; }
            set { _cacheThumbnailData = value; }
        }

        private bool _isNewlyCreated;
        public bool IsNewlyCreated
        {
            get { return _isNewlyCreated; }
            set { _isNewlyCreated = value; }
        }

        private bool _isTemporarySaved;
        public bool IsTemporarySaved
        {
            get { return _isTemporarySaved; }
            set { _isTemporarySaved = value; }
        }

        private bool _isRescored;
        public bool IsRescored
        {
            get
            {
                return _isRescored;
            }
            set
            {
                _isRescored = value;
                OnPropertyChanged();
            }
        }

        public bool HasSpotMeasurements
        {
            get
            {
                return _score.SpotMeasurements != null && _score.SpotMeasurements.Count() > 0;
            }
        }

        public void SetSpotMeasurement(string valuename, string value)
        {
            if (HasSpotMeasurements)
            {
                var measurement = _score.SpotMeasurements.Where(m => string.Compare(m.UnescapedName, valuename, true) == 0).FirstOrDefault();
                if (measurement != null)
                {
                    measurement.Value = value;
                }
            }
        }

        private PointCollection _region;
        public PointCollection Region
        {
            get
            {
                return _region;
            }
            set
            {
                _region = value;
                SetLabelPosition();
                OnPropertyChanged("Region");
            }
        }

        private double labelX;
        public double LabelX
        {
            get { return labelX; }
            set
            {
                labelX = value;
                OnPropertyChanged();
            }
        }

        private double labelY;
        public double LabelY
        {
            get { return labelY; }
            set
            {
                labelY = value;
                OnPropertyChanged();
            }
        }

        internal void SetLabelPosition()
        {
            if (Region != null)
            {
                Point max = new Point(0, 0);
                foreach (var p in Region)
                {
                    if (max.Y < p.Y)
                        max = p;
                }
                labelX = max.X;
                labelY = max.Y;
            }
        }




        public void AdjustLabelToFit(int fitWidth, int fitHeight)
        {
            var limit = fitHeight - 30;
            // only worry about going off the bottom really
            if (labelY < limit)
                return;

            if (Region != null)
            {
                Point max = new Point(int.MaxValue, int.MaxValue);
                foreach (var p in Region)
                {
                    if (p.Y > limit && p.Y < max.Y)
                        max = p;
                }
                if (max.Y != int.MaxValue)
                {
                    labelX = max.X;
                    labelY = max.Y;
                }
            }
        }

        private ObservableCollection<SignalCount> _counts;
        public ObservableCollection<SignalCount> Counts
        {
            get { return _counts; }
        }

        public void ParseMeasurements(IEnumerable<SignalInfo> signalOverlays)
        {
            _counts.Clear();
            foreach (var overlay in signalOverlays)
            {
                // Add a count for each signal overlay (signal type e.g. probe count, fusions etc.).
                // If there is no measurement for a signal, add a count of zero.
                int measurementValue = 0;
                var moddedSignalName = overlay.SignalName.Replace(" ", "_");
                moddedSignalName = moddedSignalName.Replace("&", "_");

                if (_score.SpotMeasurements != null)
                {
                    foreach (var measurement in _score.SpotMeasurements)
                    {
                        if (measurement.SignalName == moddedSignalName)
                        {
                            int value = int.Parse(measurement.Value);
                            if (value > 0)
                            {
                                measurementValue = value;
                                break;
                            }
                        }
                    }
                }
                _counts.Add(new SignalCount() { SignalInfo = overlay, Count = measurementValue });
            }
        }

        public void Render(byte[] thumbnailData, int stackIndex, IEnumerable<ChannelDisplay> channelDisplay, bool attenuateCounterstain, bool invertCounterstain)
        {
            // Update the RenderTime first, in case the cell is invalidated during this render (e.g. because its properties have changed), which
            // would cause the RenderTime to be reset so that it would be subsequently rendered again with the new properties. If the render time
            // was set at the end of the current render, this would hide the fact that the cell had been invalidated during the render and so it
            // wouldn't then be re-rendered with the latest settings. This would cause the display to get out of sync with the settings.
            RenderTime = DateTime.Now;

            try
            {
                var lastRenderedThumb = _thumbnailRenderer.Render(thumbnailData, stackIndex, channelDisplay, attenuateCounterstain, invertCounterstain);

                if (lastRenderedThumb == null)
                {
                    RenderTime = DateTime.MinValue;
                    return;
                }

                Thumbnail = lastRenderedThumb;
            }
            catch (Exception)
            {
                RenderTime = DateTime.MinValue;
                throw;
            }
        }

        public BitmapSource RenderOverlay(byte[] bmpData, Color color)
        {
            try
            {
                return ThumbnailOverlayRenderer.Render(bmpData, color);
            }
            catch (Exception)
            {
                RenderTime = DateTime.MinValue;
                throw;
            }
        }

        public void Dispose()
        {
            _thumbnail = null;
            _thumbnailRenderer = null;          
            SpotMeasurements = null;
            Thumbnail = null;
            _counts.Clear();
            _counts = null;
        }
    }
}
