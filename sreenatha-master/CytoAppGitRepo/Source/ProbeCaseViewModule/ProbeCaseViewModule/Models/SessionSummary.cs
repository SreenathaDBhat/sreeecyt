﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// This class holds summary of the session such as Class count, signal information, Total Informative cells
    /// </summary>
    public class SessionSummary : BindableBase
    {
        private IEnumerable<ClassFilter> _classes;
        public IEnumerable<ClassFilter> Classes
        {
            get
            {
                return _classes;
            }
            set
            {
                _classes = value;
                OnPropertyChanged();
            }
        }

        private int _totalInformativeCell;
        public int TotalInformativeCell
        {
            get
            {
                return _totalInformativeCell;
            }
            set
            {
                _totalInformativeCell = value;
                OnPropertyChanged();
            }
        }

        private ScoreStats _scoreStats;
        public ScoreStats ScoreStats
        {
            get
            {
                return _scoreStats;
            }
            set
            {
                _scoreStats = value;
                OnPropertyChanged();
            }
        }
    }
}
