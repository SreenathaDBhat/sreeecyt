﻿using Prism.Mvvm;
using System.Globalization;
using System.Windows.Media;

namespace ProbeCaseViewModule.Models
{
    /// <summary>
    /// This class is used for Displaying signal count
    /// </summary>
    public class SignalInfo
    {
        public string SignalName { get; set; }
        public Color Color { get; set; }
    }

    public class SignalCount : BindableBase
    {
        public SignalInfo SignalInfo
        {
            get;
            set;
        }

        private int _count;
        public int Count
        {
            get { return _count; }
            set { _count = value; OnPropertyChanged(); ResetEdit(); }
        }

        private string _editedCount;
        public string UserEditableScore
        {
            get { return _editedCount; }
            set
            {
                _editedCount = value; OnPropertyChanged();
                OnPropertyChanged("UserEditableScoreIsValid");
            }
        }

        public void ResetEdit()
        {
            _editedCount = _count.ToString(CultureInfo.CurrentCulture);
            OnPropertyChanged("UserEditableScoreIsValid");
        }

        public bool UserEditableScoreIsValid
        {
            get
            {
                int n;
                if (!int.TryParse(_editedCount, NumberStyles.Integer, CultureInfo.CurrentCulture, out n))
                {
                    return false;
                }

                return n >= 0;
            }
        }
        public bool CommitUserEdit()
        {
            if (!UserEditableScoreIsValid)
            {
                return false;
            }
            else {
                int n;
                if (int.TryParse(_editedCount, NumberStyles.Integer, CultureInfo.CurrentCulture, out n))
                {
                    _count = n;
                    OnPropertyChanged("Count");
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        /// <summary>
        /// This is a dirty flag - if the user has provided a new value, this will return true UNTIL
        /// commit or revert is called. Then it'll go back to false.
        /// </summary>
        public bool HasUserEditInFlight
        {
            get
            {
                if (_editedCount == null)
                {
                    return false;
                }

                int n = 0;
                if (int.TryParse(_editedCount, NumberStyles.Integer, CultureInfo.CurrentCulture, out n))
                {
                    return n != _count;
                }
                else {
                    return false;
                }
            }
        }
    }
}
