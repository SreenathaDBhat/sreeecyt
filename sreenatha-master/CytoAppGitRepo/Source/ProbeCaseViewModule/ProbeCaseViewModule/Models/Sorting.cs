﻿using System;
using System.Linq;
using System.Collections.Generic;
using ProbeCaseViewModule.Models;
using ProbeCaseViewDataAccess.Models;

namespace AI
{
    /// <summary>
    /// This class is used for sorting Scored cell collection based on class,frames and signalInformation
    /// </summary>
    public abstract class ScoreSorter : IComparer<ScoredCell>
    {
        public abstract string Name { get; }
        public ScoreSorter ConflictSorter { get; set; }
        public bool IsSecondarySortAllowed { get; protected set; }

        private int AbsoluteFallback(ScoredCell a, ScoredCell b)
        {
            return a.Score.Id.CompareTo(b.Score.Id); // Sort by cell GUID.
        }

        public virtual int Compare(ScoredCell a, ScoredCell b)
        {
            if (ConflictSorter == null)
                return AbsoluteFallback(a, b);
            else
                return ConflictSorter.Compare(a, b);
        }
    }
    
    public class SignalCountSorter : ScoreSorter
    {
        private string name;

        public SignalCountSorter(string signalName)
        {
            name = signalName;
            IsSecondarySortAllowed = true;
        }

        public override string Name
        {
            get { return name; }
        }

        public override int Compare(ScoredCell a, ScoredCell b)
        {
            var signalA = a.Counts.Where(n => n.SignalInfo.SignalName == name).FirstOrDefault();
            var signalB = b.Counts.Where(n => n.SignalInfo.SignalName == name).FirstOrDefault();
            if (signalA == null || signalB == null)
                return 0;
            var c = -signalA.Count.CompareTo(signalB.Count); // NOTE THE NEGATIVE! we want big numbers first
            if (c == 0)
                return base.Compare(a, b);
            return c;
        }
    }

    public class ClassSorter : ScoreSorter
    {
        private Dictionary<string, int> indices;

        public ClassSorter(IEnumerable<ClassFilter> classes)
        {
            RefreshFromClassList(classes);
            IsSecondarySortAllowed = true;
        }

        public override string Name
        {
            get { return "Class"; }
        }

        public override int Compare(ScoredCell a, ScoredCell b)
        {
            var ai = indices[a.Class];
            var bi = indices[b.Class];
            var c = ai.CompareTo(bi);
            if (c == 0)
                return base.Compare(a, b);
            return c;
        }

        public void RefreshFromClassList(IEnumerable<ClassFilter> classes)
        {
            indices = new Dictionary<string, int>();
            int i = 0;
            if (classes != null)
            {
                foreach (var c in classes)
                    indices.Add(c.ClassName, i++);
            }
        }
    }
    
    public delegate int GetFrameSequenceNumber(Guid frameGUID);

    public class FrameSorter : ScoreSorter
    {
        private GetFrameSequenceNumber GetFrameNumber;

        public FrameSorter(GetFrameSequenceNumber gfsn)
        {
            GetFrameNumber = gfsn;
            IsSecondarySortAllowed = false; // The purpose of frame sort is to always give the same order, regardless of other criteria.
        }

        public void Tidy()
        {
            GetFrameNumber = null;
        }

        public override string Name
        {
            get { return "Frame"; }
        }

        public override int Compare(ScoredCell a, ScoredCell b)
        {
            // This sort should give the same ordering of cells if a scan is reprocessed.
            // The cell MEAS_Id values alone cannot be relied upon for doing this (and they will be
            // different after reprocessing), but they should always have the same ordering within each frame.
            // So sort by frame first, then by cell.
            // Use the frame number, not the frame ID, so that the cells in 'frame 1' come first, etc..
            var c = GetFrameNumber(a.FrameId).CompareTo(GetFrameNumber(b.FrameId));
            if (c != 0)
                return c;
            else
            {
                if (a.HasSpotMeasurements && b.HasSpotMeasurements)
                {
                    Measurement aCellID = a.Score.SpotMeasurements.Where(cell => cell.OriginalName == "MEAS_Id").FirstOrDefault();
                    Measurement bCellID = b.Score.SpotMeasurements.Where(cell => cell.OriginalName == "MEAS_Id").FirstOrDefault();

                    int aID, bID; // Must do a numerical rather than a string comparison.
                    if (aCellID != null && int.TryParse(aCellID.Value, out aID)
                        && bCellID != null && int.TryParse(bCellID.Value, out bID))
                        return aID.CompareTo(bID);
                }
                return base.Compare(a, b);
            }
        }
    }

}

