﻿using Prism.Events;
using Prism.Regions;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Services;
using ProbeCaseViewModule.ViewModels;
using ProbeCaseViewModule.Views;
using System.ComponentModel.Composition;
using System;
using ProbeCaseViewDataAccess.Models;
using System.IO;
using CytoApps.SpotConfiguration;
using ProbeCaseViewModule.Models;

namespace ProbeCaseViewModule.Presenters
{
    /// <summary>
    /// REFACTOR WH Comment required
    /// </summary>
  
    public class AssayPresenter
    {
        private IEventAggregator _eventAggregator;
        private IDataService _dataService;
        private IRegionManager _regionManager;

     //   [ImportingConstructor]
        public AssayPresenter(IEventAggregator eventAggregator, IRegionManager regionManager, IDataService dataService)
        {
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _dataService = dataService;
        }
    }
}
