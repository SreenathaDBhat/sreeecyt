﻿using Prism.Events;
using Prism.Regions;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Services;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using ProbeCaseViewModule.ViewModels;
using CytoApps.Infrastructure.UI;
using ProbeCaseViewModule.Views;
using System.Collections.ObjectModel;
using ProbeCaseViewModule.Models;
using System;
using ProbeCaseViewDataAccess.Models;
using System.Threading;
using Microsoft.Practices.ServiceLocation;
using CytoApps.Infrastructure.Helpers;
using System.Windows;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Localization;

namespace ProbeCaseViewModule.Presenters
{
    class DualReaderFramesGalleryPresenter : IDisposable
    {
        #region Fields

        private IEventAggregator _eventAggregator;
        private IDataService _dataService;
        private IRegionManager _regionManager;
        private ScanAreaViewModel _scanAreaViewModel;
        private DualReaderFramesGalleryViewModel _dualReaderFramesGalleryViewModel;
        private bool _isFrameGalleryDisplayedFirstTime;
        private Session _currentSession;
        private CancellationTokenSource _thumbnailCancelToken;
        private DualReaderFramesGalleryView _dualReaderFramesGalleryView;

        #endregion

        #region Constructor

        public DualReaderFramesGalleryPresenter()
        {
            _eventAggregator = ServiceLocator.Current.TryResolve<IEventAggregator>();
            _regionManager = ServiceLocator.Current.TryResolve<IRegionManager>();
            _dataService = ServiceLocator.Current.TryResolve<IDataService>();
            _dualReaderFramesGalleryView = new DualReaderFramesGalleryView();
            SubscribeEvents();
        }
        #endregion

        #region Event Subscribtion

        private void SubscribeEvents()
        {
            _eventAggregator.GetEvent<DisplayScorePageEvent>().Subscribe(OnDisplayScorePage);
            _eventAggregator.GetEvent<CloseDualReaderFramesGalleryEvent>().Subscribe(CloseDualReaderFramesGallery);
            _eventAggregator.GetEvent<CalculateTotalCellsEvent>().Subscribe(CalculateTotalCells);
            _eventAggregator.GetEvent<NavigateBackToFramesGalleryEvent>().Subscribe(DisplayFrameGallery);
        }

        #endregion

        #region Methods
        public void Show(ScanAreaViewModel scanAreaViewModel)
        {
            _isFrameGalleryDisplayedFirstTime = true;
            _dualReaderFramesGalleryView.DataContext = CreateDualReaderFramesGalleryViewModel(scanAreaViewModel);
            OpenDualReaderFramesGalleryView();
            RestoreFrameSelection(scanAreaViewModel);
        }

        private bool CheckProbeCaseWorkspaceViewExists()
        {
            bool viewExists = false;
            IList<object> Views = new List<object>(_regionManager.Regions[RegionNames.MainRegion].Views);
            foreach (var view in Views)
            {
                if (view is ProbeCaseWorkspaceView) //*TODO
                {
                    viewExists = true;
                    return viewExists;
                }
            }
            return viewExists;
        }

        private void OpenDualReaderFramesGalleryView()
        {
            _regionManager.AddToRegion(RegionNames.MainRegion, _dualReaderFramesGalleryView);
            _regionManager.Regions[RegionNames.MainRegion].Activate(_dualReaderFramesGalleryView);
          //  _regionManager.RegisterViewWithRegion(PcvRegionNames.ScoresViewRegion, () => _dualReaderFramesGalleryView);

        }
       

       

        /// <summary>
        /// Create DualReaderFramesGalleryViewModel for Frame Gallery Data context
        /// </summary>
        /// <param name="scanAreaViewModel"></param>
        /// <returns></returns>
        private DualReaderFramesGalleryViewModel CreateDualReaderFramesGalleryViewModel(ScanAreaViewModel scanAreaViewModel)
        {
            if (_dualReaderFramesGalleryViewModel != null) _dualReaderFramesGalleryViewModel = null;
            _dualReaderFramesGalleryViewModel = new DualReaderFramesGalleryViewModel();
            if (scanAreaViewModel != null)
            {
                _scanAreaViewModel = scanAreaViewModel;
                var frames = _dataService.GetFrames(_scanAreaViewModel.SelectedScanArea);
                int n = 1;
                _dualReaderFramesGalleryViewModel.FrameList = new ObservableCollection<DualReaderFrameDetailsViewModel>(from frame in frames select new DualReaderFrameDetailsViewModel(_eventAggregator) { Frame = new FrameInfo(n++) { Image = frame } });
                _dualReaderFramesGalleryViewModel.CaseName = _scanAreaViewModel.SelectedScanArea.Slide.CaseId;
                _dualReaderFramesGalleryViewModel.SlideName = _scanAreaViewModel.SelectedScanArea.Slide.Name;
                _dualReaderFramesGalleryViewModel.ScanAreaName = _scanAreaViewModel.SelectedScanArea.Name;
                _dualReaderFramesGalleryViewModel.ScoresName = _scanAreaViewModel.NewSessionName;

                _thumbnailCancelToken = new CancellationTokenSource();
                Task.Run(() =>
                Parallel.ForEach(_dualReaderFramesGalleryViewModel.FrameList, (item) =>
                                 {
                                     item.Thumbnail = _dataService.GetCachedFrameBitmap(_scanAreaViewModel.SelectedScanArea.Slide, _scanAreaViewModel.SelectedScanArea.Id, item.Frame.Image);
                                     item.CellCount = _dataService.GetFrameCellCount(_scanAreaViewModel.SelectedScanArea, item.Frame.Image.Id.ToString());
                                 }), _thumbnailCancelToken.Token);
            }
            return _dualReaderFramesGalleryViewModel;
        }

        /// <summary>
        /// Restores selection of already selected frames and diables frames selected in another session
        /// </summary>
        /// <param name="scanAreaViewModel"></param>
        private void RestoreFrameSelection(ScanAreaViewModel scanAreaViewModel)
        {
            if (scanAreaViewModel != null)
            {
                //Check for previous session selected frames and making them disabled when create a new session.
                if (scanAreaViewModel.SessionViewModelCollection.Count() > 0)
                {
                    Session previousSession = scanAreaViewModel.SelectedScanArea.Sessions.Where(x => x.Name != scanAreaViewModel.NewSessionName).FirstOrDefault();
                    if (previousSession != null)
                    {
                        List<string> selectedFramesofPreviousSession = _dataService.GetSelectedFramesForSession(previousSession);
                        foreach (var frameValue in _dualReaderFramesGalleryViewModel.FrameList)
                        {
                            if (selectedFramesofPreviousSession.Contains(frameValue.Frame.Image.Id.ToString()))
                            {
                                frameValue.IsAlreadySelected = true;
                            }
                        }
                    }

                    //Check for current session selected frames and making them enabled.
                    Session currentSession = scanAreaViewModel.SelectedScanArea.Sessions.Where(x => x.Name == scanAreaViewModel.NewSessionName).FirstOrDefault();
                    if (currentSession != null)
                    {
                        List<string> selectedFramesofCurrentSession = _dataService.GetSelectedFramesForSession(currentSession);
                        foreach (var frameValue in _dualReaderFramesGalleryViewModel.FrameList)
                        {
                            if (selectedFramesofCurrentSession.Contains(frameValue.Frame.Image.Id.ToString()))
                            {
                                frameValue.IsSelected = true;
                                _dualReaderFramesGalleryViewModel.SelectedFrames.Add(frameValue.Frame);
                            }
                            else
                            {
                                frameValue.IsSelected = false;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates Session with selected frames
        /// </summary>
        /// <param name="selectedFrames"></param>
        private void OnDisplayScorePage(ObservableCollection<FrameInfo> selectedFrames)
        {
            List<string> selectedFrameIdList = selectedFrames.Select(x => x.Image.Id.ToString()).ToList();

            if (_scanAreaViewModel.SelectedScanArea != null)
            {
                if (_isFrameGalleryDisplayedFirstTime)
                {
                    Session _newSession = _dataService.CreateNewSession(_scanAreaViewModel.SelectedScanArea, _scanAreaViewModel.NewSessionName, _scanAreaViewModel.UseSpotScores, true, selectedFrameIdList);
                    if (_newSession != null)
                    {
                        _currentSession = _newSession;
                        SessionViewModel sessionviewModel = new SessionViewModel(_newSession, _eventAggregator);
                        _scanAreaViewModel.SessionViewModelCollection.Add(sessionviewModel);
                    }
                }
                else
                {
                    _dataService.UpdateScores(_currentSession, selectedFrameIdList);
                }

                if (_currentSession != null)
                {
                    _isFrameGalleryDisplayedFirstTime = false;
                    _dataService.RemoveLock(LoginHelper.CurrentUser, _currentSession.Slide.CaseId);
                    _regionManager.Regions[RegionNames.MainRegion].Deactivate(_dualReaderFramesGalleryView);
                    _regionManager.Regions[RegionNames.MainRegion].Remove(_dualReaderFramesGalleryView);
                    _dualReaderFramesGalleryViewModel.Dispose();
                    //Open Score Page in GridViewPresenter
                    _eventAggregator.GetEvent<OpenSessionEvent>().Publish(_currentSession);
                }
                else
                {
                    CloseDualReaderFramesGallery(null);
                    MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-SessionCreationFailedMessage"), Literal.Strings.Lookup("cbStr-SessionCreationFailedTitle"), MessageBoxButton.OK, MessageBoxImage.Error, true);
                    if (result == MessageBoxResult.OK)
                    {
                        _eventAggregator.GetEvent<CytoApps.Infrastructure.UI.Events.RefreshSelectedCaseEvent>().Publish(null);
                    }
                }
            }
        }

        /// <summary>
        /// Closes Frame Gallery
        /// </summary>
        /// <param name="obj"></param>
        private void CloseDualReaderFramesGallery(object unused)
        {
            if (_thumbnailCancelToken != null)
            {
                _thumbnailCancelToken.Cancel();
                _thumbnailCancelToken.Dispose();
            }

            _regionManager.Regions[RegionNames.MainRegion].Deactivate(_dualReaderFramesGalleryView);
            _regionManager.Regions[RegionNames.MainRegion].Remove(_dualReaderFramesGalleryView);
            if (_isFrameGalleryDisplayedFirstTime)
            {             
                _regionManager.RequestNavigate(RegionNames.MainRegion, "CaseBrowserWorkspaceView");
            }
            else
            {
                _dataService.RemoveLock(LoginHelper.CurrentUser, _currentSession.Slide.CaseId);
                //If Frame gallery displayed from Review Score Page
                _eventAggregator.GetEvent<OpenSessionEvent>().Publish(_currentSession);
            }
            _dualReaderFramesGalleryViewModel.Dispose();
        }

        /// <summary>
        /// Calculate total selected cells
        /// </summary>
        /// <param name="selectedFrames"></param>
        private void CalculateTotalCells(Object unused)
        {
            _dualReaderFramesGalleryViewModel.TotalCells = 0;

            var selectedFrameList = _dualReaderFramesGalleryViewModel.FrameList.Where(x => _dualReaderFramesGalleryViewModel.SelectedFrames.Contains(x.Frame)).ToList();//Extract DualReaderFrameDetailsViewModel list to get cell count
            foreach (var item in selectedFrameList)
            {
                _dualReaderFramesGalleryViewModel.TotalCells += _dataService.GetFrameCellCount(_scanAreaViewModel.SelectedScanArea, item.Frame.Image.Id.ToString());//Need to call DAL service instead of cell count, as task will be in process of filling cell count
            }
        }

        /// <summary>
        /// Display Frame Gallery from Scores Page
        /// </summary>
        /// <param name="obj"></param>
        public void DisplayFrameGallery(Session session)
        {
            _currentSession = session;
            _scanAreaViewModel = CreateScanAreaViewModel(session);
            if (_scanAreaViewModel != null)
            {
                _scanAreaViewModel.NewSessionName = session.Name;
                _dualReaderFramesGalleryView.DataContext = CreateDualReaderFramesGalleryViewModel(_scanAreaViewModel);
                OpenDualReaderFramesGalleryView();
                RestoreFrameSelection(_scanAreaViewModel);
            }
        }

        /// <summary>
        /// ---------------------------------------------------HACK ----------Needs Review
        /// Need to check if this data service is ok to extract scan area to build _scanAreaViewModel!!
        /// Creates ScanArea View Model
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        private ScanAreaViewModel CreateScanAreaViewModel(Session session)
        {
            if (session != null)
            {
                var scanArea = _dataService.GetScanAreas(session.Slide).Where(x => x.Id == session.ScanAreaId).First();
                ScanAreaViewModel scanAreaViewModel = new ScanAreaViewModel(scanArea, _eventAggregator);
                scanAreaViewModel.SelectedScanArea = scanArea;
                foreach (var item in scanAreaViewModel.SelectedScanArea.Sessions)
                {
                    SessionViewModel sessionviewModel = new SessionViewModel(item, _eventAggregator);
                    scanAreaViewModel.SessionViewModelCollection.Add(sessionviewModel);
                }
                return scanAreaViewModel;
            }
            return null;
        }

        public void Dispose()
        {
            _eventAggregator.GetEvent<DisplayScorePageEvent>().Unsubscribe(OnDisplayScorePage);
            _eventAggregator.GetEvent<CloseDualReaderFramesGalleryEvent>().Unsubscribe(CloseDualReaderFramesGallery);
            _eventAggregator.GetEvent<CalculateTotalCellsEvent>().Unsubscribe(CalculateTotalCells);
            _eventAggregator.GetEvent<NavigateBackToFramesGalleryEvent>().Unsubscribe(DisplayFrameGallery);
            if(_dualReaderFramesGalleryViewModel!= null)
            {
                _dualReaderFramesGalleryViewModel.Dispose();
            }
            _eventAggregator = null;
            _dataService = null;
            _regionManager = null;
            _scanAreaViewModel = null;
            _dualReaderFramesGalleryViewModel = null;
            _currentSession = null;
            _thumbnailCancelToken = null;
            _dualReaderFramesGalleryView = null;
        }

        #endregion
    }
}