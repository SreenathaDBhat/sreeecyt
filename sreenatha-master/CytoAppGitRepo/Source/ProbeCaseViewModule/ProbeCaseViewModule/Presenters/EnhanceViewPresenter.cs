﻿using CytoApps.Infrastructure.Helpers;
using EnhancementControl.Converters;
using EnhancementControl.Models;
using EnhancementControl.ViewModels;
using EnhancementControl.Views;
using Prism.Events;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Models;
using ProbeCaseViewModule.Services;
using ProbeCaseViewModule.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

namespace ProbeCaseViewModule.Presenters
{
    /// <summary>
    /// This class acts as presenter for ImageEnhance View 
    /// Contains methods to view current cell in Enhance Viewer Window
    /// Load Previous and Save Image Enhance View Instruction to dataSource
    /// and Notifies cell that enhanceCell is avaliable
    /// </summary>
    public class EnhanceViewPresenter
    {
        #region "Private Properties"
        private IEventAggregator _eventAggregator;
        private IDataService _dataService;
        private SharedViewModel _sharedViewModel;
        private ScoredCell _scoredCell;
        private EnhanceViewOptions _options;
#pragma warning disable 0649, 0169

        private Views.GridView _gridView;
#pragma warning restore 0649, 0169
        #endregion "Private Properties"

        #region "Constructor"
       // [ImportingConstructor]
        public EnhanceViewPresenter(IEventAggregator eventAggregator, IDataService dataService)
        {
            _eventAggregator = eventAggregator;
            _dataService = dataService;
            SubscribeEvents();
        }
        #endregion "Constructor"

        #region methods
        private void SubscribeEvents()
        {
            _eventAggregator.GetEvent<EnhanceCurrentCellEvent>().Subscribe(GetCellandGridSettings);
        }

        private void GetCellandGridSettings(Dictionary<ScoredCell, EnhanceViewOptions> currentGridViewSettings)
        {
            if (currentGridViewSettings != null)
            {
                _scoredCell = currentGridViewSettings.FirstOrDefault().Key;
                _options = currentGridViewSettings.FirstOrDefault().Value;
                OpenEnhanceControl(_scoredCell, _options.Overlays);
            }
        }

        private void OpenEnhanceControl(ScoredCell currentCell, IEnumerable<OverlayInfo> overlays)
        {
            try
            {
                //Create a datasource to bind to Enhancement Control
                CurrentCellEnhancementDataSource source = new CurrentCellEnhancementDataSource(_dataService, currentCell, overlays);
                ImageEnhancementViewModel ImageEnhancementVM = new ImageEnhancementViewModel(source);
                LoadPrimeEnhnceControlWithAccessories(ImageEnhancementVM.EnhancementsControl, currentCell);
                ImageEnhancementVM.EnhancementsWindowDidDismissWithOK = () =>
                {
                    var bmp = ImageEnhancementVM.EnhancementsControl.GenerateBitmap();
                    var bmpData = ConvertBitmapSourceToByteArray(bmp);
                    currentCell.HasEnhancements = _dataService.WriteRenderInstructionToEnhancedCell(_sharedViewModel.SelectedSession, bmpData, currentCell.Score.Id.ToString(), ImageEnhancementVM.EnhancementsControl.ControlState);
                };
                ImageEnhancementVM.DisplayEnhancementWindow();
            }
            catch (UnauthorizedAccessException exception)
            {
                currentCell.HasEnhancements = false;
                LogManager.Error("Error: ", exception.InnerException);
            }
            catch (Exception exception)
            {
                LogManager.Error("Error: ", exception.InnerException);
            }
        }

        private byte[] ConvertBitmapSourceToByteArray(BitmapSource bmp)
        {
            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bmp));
            using (MemoryStream stream = new MemoryStream())
            {
                encoder.Save(stream);
                return stream.ToArray();
            }
        }

        /// <summary>
        /// Load Enhance View instructions if avaliable or create new and Loads annotations such as CellId and Signal Counts
        /// </summary>
        /// <param name="enhancementsControl">Control</param>
        /// <param name="currentCell">selected cell</param>
        private void LoadPrimeEnhnceControlWithAccessories(IEEnhancementsControl enhancementsControl, ScoredCell currentCell)
        {

            AnnotationsController annotations = new AnnotationsController();
            enhancementsControl.TextAccessories = from a in annotations.AllAnnotations select new IETextAccessory(a);

            //// Render all (visible) accessory overlays into bitmaps the enhance control can understand and use
            FrameworkElement selectedUIElement = (FrameworkElement)_gridView.mainList.ItemContainerGenerator.ContainerFromItem(currentCell);
            if (currentCell.HasSpotMeasurements && selectedUIElement != null)
            {
                var countsUI = VisualTreeWalker.FindChildOfType<Border>(selectedUIElement, "countsUI");
                var idUI = VisualTreeWalker.FindChildOfType<TextBlock>(selectedUIElement, "idnumberUI");
                Brush background = null;
                List<IEImageAccessory> imageAccessories = new List<IEImageAccessory>();
                if (countsUI != null && currentCell.HasSpotMeasurements)
                {
                    var imageAccessory = new IEImageAccessory("Signal Counts", NabScreenshot(countsUI, background), _options.DisplayIndividualCounts, IEAccessoryHorizontalPlacement.Left, IEAccessoryVerticallPlacement.Top);
                    imageAccessories.Add(imageAccessory);
                }
                if (idUI != null)
                {
                    var imageAccessory = new IEImageAccessory("Cell Identifier", NabScreenshot(idUI, background), _options.DisplayCellIDs, IEAccessoryHorizontalPlacement.Right, IEAccessoryVerticallPlacement.Top);
                    imageAccessories.Add(imageAccessory);
                }
                enhancementsControl.ImageAccessories = imageAccessories;
            }
            bool loadedPreviousSettings = false;
            var loadExisting = currentCell.HasEnhancements;
            var renderInstructionsInfo = _dataService.GetRenderInstructionCellInfo(_sharedViewModel.SelectedSession, currentCell.Score.Id.ToString());
            if (renderInstructionsInfo != null)
            {
                XElement renderInstructionsXML = XElement.Parse(renderInstructionsInfo);
                enhancementsControl.ClearUndoList();
                enhancementsControl.ControlState = renderInstructionsXML;
                loadedPreviousSettings = true;
            }

            if (!loadedPreviousSettings)
            {
                // initialize the render instructions to match the grid as closely as possible
                IEImageEnhancementsRenderInstruction renderInstruction = enhancementsControl.CopyOfCurrentRenderInstruction;

                foreach (var componentParams in renderInstruction.ComponentInstructions)
                {
                    var gridChannel = _options.ChannelDisplays.Where(c => c.Name == componentParams.Component.Name).FirstOrDefault();
                    if (gridChannel != null)
                    {
                        componentParams.HistogramBottom.Value = (float)gridChannel.HistogramLow;
                        componentParams.HistogramTop.Value = (float)gridChannel.HistogramHigh;
                        componentParams.Visible.Value = gridChannel.IsChecked;
                        componentParams.Gamma.Value = (float)gridChannel.Gamma;

                        if (componentParams.Component.HasZStack)
                        {
                            componentParams.Z.Value = _options.CurrentStackIndex;
                            componentParams.UseMaxProjection.Value = true;
                        }
                    }

                    componentParams.Attenuate.Value = componentParams.Component.IsCounterstain && false;
                    componentParams.Invert.Value = componentParams.Component.IsCounterstain && false;
                }

                foreach (var overlay in renderInstruction.Overlays)
                {
                    var gridOverlay = _options.Overlays.Where(o => o.Name == overlay.Name).FirstOrDefault();
                    overlay.Visible.Value = gridOverlay == null ? false : gridOverlay.IsChecked;
                }

                enhancementsControl.ClearUndoList();
                enhancementsControl.CopyRenderInstruction(renderInstruction);
            }

        }

        public RenderTargetBitmap NabScreenshot(FrameworkElement ctrl, System.Windows.Media.Brush background)
        {
            return NabScreenshot(ctrl, (int)ctrl.ActualWidth, (int)ctrl.ActualHeight, background);
        }

        private RenderTargetBitmap NabScreenshot(FrameworkElement ctrl, int w, int h, System.Windows.Media.Brush background)
        {
            if (ctrl == null || ctrl.ActualWidth <= double.Epsilon || ctrl.ActualHeight <= double.Epsilon)
            {
                return null;
            }
            RenderTargetBitmap render = new RenderTargetBitmap(w, h, 96, 96, PixelFormats.Pbgra32);

            if (background != null)
            {
                Border b = new Border()
                {
                    Background = background,
                    Width = w,
                    Height = h
                };
                b.Measure(new System.Windows.Size(w, h));
                b.Arrange(new Rect(0, 0, w, h));
                render.Render(b);
            }

            render.Render(ctrl);
            render.Freeze();

            return render;
        }
        #endregion
    }
}
