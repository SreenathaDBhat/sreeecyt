﻿using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Localization;
using CytoApps.SpotConfiguration;
using Microsoft.Practices.ServiceLocation;
using PCVImageRenderers.Models;
using Prism.Events;
using Prism.Regions;
using ProbeCaseViewDataAccess.Models;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Models;
using ProbeCaseViewModule.Properties;
using ProbeCaseViewModule.Services;
using ProbeCaseViewModule.ViewModels;
using ProbeCaseViewModule.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using System.ComponentModel;

namespace ProbeCaseViewModule.Presenters
{
    /// <summary>
    /// Frame Window Presenter holds all logical stuffs for displaying Frame
    /// </summary>
    class FrameWindowPresenter : IDisposable
    {
        private FrameWindowView _frameWindow;
        private StoredAnnotationEditView _storedAnnotationEditView;
        private FrameWindowViewModel _frameWindowViewModel;
        private CancellationTokenSource _renderCancelToken;
        private List<AnalysisRegion> _regions;
        // Worker thread for polling regions.
        private System.ComponentModel.BackgroundWorker _worker = null;
        // Worker thread to reprocess frames
        private System.ComponentModel.BackgroundWorker _readReprocessResultWorker = null;
        // Status flag for starting Region Polling on worker thread
        private bool _isProcessRegionsStarted = false;
        // Delegate wrapper for calling target method asynchronously
        private Action<AnalysisRegionInfo> AsyncSendRegionForAnalysis = null;
        private int _cellCount;
        private List<string> _spotFolders;

        private List<Task> _tasksList;
#pragma warning disable 0649, 0169
        private IEventAggregator _eventAggregator;
        private IDataService _dataService;

#pragma warning restore 0649, 0169
        public FrameWindowPresenter()
        {
            _eventAggregator = ServiceLocator.Current.TryResolve<IEventAggregator>();
            _dataService = ServiceLocator.Current.TryResolve<IDataService>();
            _tasksList = new List<Task>();
            // Initialize delegate wrapper to call target method asynchronously.
            AsyncSendRegionForAnalysis = new Action<AnalysisRegionInfo>(SendRegionForAnalysis);
            SubscribeEvents();
        }

        public void InitializeFrameViewModel(SharedViewModel sharedVM)
        {
            if (_frameWindowViewModel != null)
            {
                _frameWindowViewModel.SharedVM.FrameChangeEvent -= CurrentFrameChanged;
                _frameWindowViewModel = null;
            }
            _frameWindowViewModel = new FrameWindowViewModel(sharedVM);
            _tasksList.Add(Task.Factory.StartNew(() => LoadAllFrameAnnotations())); //RVCMT-HA - handle cancellation, Kill if the window closes in between.
            //get all the regions for session
            _tasksList.Add(Task.Factory.StartNew(() => LoadAllRegions()));
            //  getAllRegions.Wait();
            _frameWindowViewModel.SharedVM.FrameChangeEvent += CurrentFrameChanged;
            _regions = new List<AnalysisRegion>();
            // Create Background worker instance for polling regions
            _worker = new System.ComponentModel.BackgroundWorker();
            _worker.DoWork += ProcessRegions;
            _worker.RunWorkerCompleted += ProcessingRegionsCompleted;
            _worker.WorkerSupportsCancellation = true;
            _worker.WorkerReportsProgress = true;
            _isProcessRegionsStarted = false;
            _cellCount = GetScoreCount();

            //Create the reporcess worker thread 
            _readReprocessResultWorker = new System.ComponentModel.BackgroundWorker();
            _readReprocessResultWorker.DoWork += ReadReprocessResult;
            _readReprocessResultWorker.RunWorkerCompleted += ReadReprocessResultCompleted;

            //_timer = new System.Timers.Timer();
            //_timer.Interval = 2000;
            //_timer.Elapsed += new ElapsedEventHandler(CheckRegionProcessd);
        }



        private void LoadAllFrameAnnotations()
        {
            var allframesAnnotation = _dataService.GetAllFramesAnnotation(_frameWindowViewModel.SharedVM.SelectedSession, _frameWindowViewModel.SharedVM.Frames.Select(frame => frame.Image).ToList());
            if (allframesAnnotation != null)
            {
                foreach (var frAnnotation in allframesAnnotation)
                {
                    FrameAnnotationViewModel frameAnnotation = new FrameAnnotationViewModel(frAnnotation.Identifier, frAnnotation.Annotations);
                    _frameWindowViewModel.SharedVM.AllFrameAnnotations.Add(frameAnnotation);
                }
            }
        }

        private void CurrentFrameChanged()
        {
            _frameWindowViewModel.RefreshCanExecute();
            _frameWindowViewModel.SlideOverviewVM.CurrentFrameChanged();
            _frameWindowViewModel.DisplayImage = null;
            Render();
            GetRegionsForCurrentFrame();

        }

        private void SubscribeEvents()
        {
            _eventAggregator.GetEvent<ScoringWindowOpenEvent>().Subscribe(OnScoringWindow);
            _eventAggregator.GetEvent<RenderFrameEvent>().Subscribe(OnRender);
            _eventAggregator.GetEvent<SelectedGridScoreEvent>().Subscribe(OnSelectedGridScoreChange);
            _eventAggregator.GetEvent<OpenSpotConfigWindowEvent>().Subscribe(OnOpenSpotConfiguartionAndReprocess);
            _eventAggregator.GetEvent<CreateNewAssayEvent>().Subscribe(OnCreateNewAssay);
            _eventAggregator.GetEvent<GetComptibleAssaysEvent>().Subscribe(GetCompatibleAssays);
            _eventAggregator.GetEvent<LoadAssayEvent>().Subscribe(LoadAssayFromSharedFolder);
            _eventAggregator.GetEvent<SaveAsAssayEvent>().Subscribe(OnSaveAsAssay);
            _eventAggregator.GetEvent<FitAllFramesInSlideEvent>().Subscribe(FitAllFramesInSlide);
            _eventAggregator.GetEvent<ZoomAndPanToCurrentFrameEvent>().Subscribe(ZoomAndPanTo);
            _eventAggregator.GetEvent<SaveRegionEvent>().Subscribe(AnalyseRegion);
            _eventAggregator.GetEvent<CloseFrameWindowEvent>().Subscribe(HideFrameWindow);
            _eventAggregator.GetEvent<DisplayStoredAnnotationViewEvent>().Subscribe(ToggleStoredAnnotationView);
            _eventAggregator.GetEvent<ReprocessEvent>().Subscribe(OnReprocessFrames);
            _eventAggregator.GetEvent<ScoresUpdatedEvent>().Subscribe(OnScoresUpdated);

        }


        private void ToggleStoredAnnotationView(object obj)
        {
            if (_storedAnnotationEditView == null)
            {
                _storedAnnotationEditView = new StoredAnnotationEditView();
                _storedAnnotationEditView.Closed += (a, b) => _storedAnnotationEditView = null;
                _storedAnnotationEditView.DataContext = _frameWindowViewModel;
                _storedAnnotationEditView.ShowDialog();
            }
        }

        private void OnSaveAsAssay(string assayName)
        {
            if (!string.IsNullOrWhiteSpace(assayName))
            {
                try
                {
                    List<string> existingAssays = _dataService.GetAllDefaultAssays().ToList();
                    if (existingAssays != null)
                    {
                        MessageBoxResult result = MessageBoxResult.Yes;
                        string existingAssay = existingAssays.Where(s => string.Equals(s, assayName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                        if (!string.IsNullOrWhiteSpace(existingAssay))
                        {
                            result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-SaveAssayExistingMessage"), Literal.Strings.Lookup("cbStr-SaveAssayTitle"), System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question, _frameWindow);
                        }
                        if (result == MessageBoxResult.Yes)
                        {
                            _dataService.SaveAsDefaultAssay(_frameWindowViewModel.SharedVM.CurrentAssay.SpotConfigController.SaveByteAssay(assayName), assayName);
                            _frameWindowViewModel.SharedVM.CurrentAssay.Name = assayName;
                        }
                    }
                }
                catch (Exception exception)
                {
                    LogManager.Error("Error: ", exception);
                    CustomPopUp.Show(Literal.Strings.Lookup("cbStr-SaveAssayMessage"), Literal.Strings.Lookup("cbStr-SaveAssayTitle"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error, _frameWindow);
                }
            }
        }

        private void LoadAssayFromSharedFolder(string assayName)
        {
            MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-CreateNewAssayMessage"), Literal.Strings.Lookup("cbStr-CreateNewAssayTitle"), System.Windows.MessageBoxButton.OKCancel, System.Windows.MessageBoxImage.Information, _frameWindow);
            if (result == MessageBoxResult.OK)
            {
                ClearRegionsForSession();
                SessionSummary sessionSummary = new SessionSummary();
                sessionSummary.TotalInformativeCell = 0;
                ProbeScoringAssay newAssay = null;

                var fluors = _frameWindowViewModel.SharedVM.CurrentFrame.Image.Components.Select(c => new CVFluoroChrome { Name = c.Name, colour = c.RenderColor, IsCounterstain = c.IsCounterstain }).ToArray();

                if (fluors != null)
                {
                    SpotConfigController spotController = null;
                    var assayData = _dataService.GetDefaultAssayData(assayName);
                    if (null != assayData)
                    {
                        AssayInfo _assayInfo = _dataService.GetClassifierNamesAndScripts(null);
                        if (_assayInfo != null)
                        {
                            spotController = SpotConfigController.FromStream(assayData, _assayInfo);
                            // spotController = new SpotConfigController(ms, _assayInfo, false , assayName);
                        }
                    }
                    if (spotController != null)
                    {
                        spotController.CounterStain = fluors.FirstOrDefault();
                        spotController.CounterStain.IsCounterstain = true;
                        var probes = fluors.Where(f => f.IsCounterstain == false);
                        foreach (var p in probes)
                            spotController.AddProbe(p);
                        newAssay = new ProbeScoringAssay(spotController);
                    }
                    if (newAssay != null)
                    {
                        UpdateGridAndFrameFromAssay(newAssay);
                        SavAssayFile();
                        _frameWindowViewModel.IsAssayExist = true;
                    }
                }
            }
        }
        /// <summary>
        /// Get the Comptible assay name from shared folder
        /// </summary>
        /// <param name="unUsed"></param>
        private void GetCompatibleAssays(object unUsed)
        {
            try
            {
                if (_frameWindowViewModel.SharedVM.Frames != null && _frameWindowViewModel.SharedVM.Frames.Count > 0)
                {
                    FrameInfo frame = _frameWindowViewModel.SharedVM.Frames.DefaultIfEmpty(null).FirstOrDefault();
                    if (frame != null && frame.Image.Components != null && frame.Image.Components.Count() > 0)
                    {
                        _frameWindowViewModel.AvailebleAssays = _dataService.GetCompatibleAssays(frame.Image.Components);
                        if (_frameWindowViewModel.AvailebleAssays != null && _frameWindowViewModel.AvailebleAssays.Count() > 0)
                        {
                            _frameWindowViewModel.SelectedAssay = _frameWindowViewModel.AvailebleAssays[0];
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                //TODO show any error message?
                LogManager.Error("Error: ", exception);
            }
        }

        /// <summary>
        /// This menthod will get the new assay and update the frame and gridview
        /// </summary>
        /// <param name="assayName"></param>
        private void OnCreateNewAssay(string assayName)
        {
            try
            {
                MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-CreateNewAssayMessage"), Literal.Strings.Lookup("cbStr-CreateNewAssayTitle"), System.Windows.MessageBoxButton.OKCancel, System.Windows.MessageBoxImage.Information, _frameWindow);
                if (result == MessageBoxResult.OK)
                {
                    ClearRegionsForSession();
                    ProbeScoringAssay newAssay = null;
                    var fluors = _frameWindowViewModel.SharedVM.CurrentFrame.Image.Components.Select(c => new CVFluoroChrome { Name = c.Name, colour = c.RenderColor, IsCounterstain = c.IsCounterstain }).ToArray();
                    // ProbeScoringAssay newAssay = FromSpotConfigAssay(fluors, assayName);
                    if (fluors != null)
                    {
                        SpotConfigController spotController = null;
                        var assayData = _dataService.GetAssayData(_frameWindowViewModel.SharedVM.SelectedSession);
                        if (null != assayData)
                        {
                            AssayInfo _assayinfo = _dataService.GetClassifierNamesAndScripts(null);
                            if (_assayinfo != null)
                            {
                                spotController = new SpotConfigController(assayData, _assayinfo, true, assayName);
                            }
                        }
                        else
                        {
                            AssayInfo _assayinfo = _dataService.GetClassifierNamesAndScripts(null);
                            if (_assayinfo != null)
                            {
                                //if no assay present then open spot config. for create new assay
                                spotController = new SpotConfigController(null, _assayinfo, true, assayName);
                            }
                        }
                        if (spotController != null)
                        {
                            spotController.CounterStain = fluors.FirstOrDefault();
                            spotController.CounterStain.IsCounterstain = true;
                            var probes = fluors.Where(f => f.IsCounterstain == false);
                            foreach (var p in probes)
                                spotController.AddProbe(p);
                            newAssay = OpenSpotConfiguration(spotController);
                        }
                    }

                    if (newAssay != null)
                    {
                        UpdateGridAndFrameFromAssay(newAssay);
                        _frameWindowViewModel.OpenSpotConfigurationCommand.RaiseCanExecuteChanged();
                        SavAssayFile();
                        _frameWindowViewModel.IsAssayExist = true;
                    }
                }
            }
            catch (Exception exception)
            {
                LogManager.Error("Error: ", exception);
            }
        }

        public void ClearRegionsForSession()
        {
            foreach (AnalysisRegionInfo region in _frameWindowViewModel.SharedVM.AllFrameRegions)
            {
                if (region.IsNewRegion == false)
                {
                    region.CanDelete = true;
                }
            }
            GetRegionsForCurrentFrame();
        }

        /// <summary>
        /// Functin will save the new assay or loaded assay in the session and we need to restart the pcv scanner and assay should be saved
        /// </summary>
        private void SavAssayFile()
        {
            _frameWindowViewModel.SharedVM.IsDirty = true;
            _frameWindowViewModel.SharedVM.SelectedSession.AssayName = _frameWindowViewModel.SharedVM.CurrentAssay.Name;
            var newByteAssay = _frameWindowViewModel.SharedVM.CurrentAssay.SpotConfigController.SaveByteAssay();
            _dataService.SaveAssayData(newByteAssay, _frameWindowViewModel.SharedVM.SelectedSession);
        }


        /// <summary>
        /// Get the probeScoring assay 
        /// </summary>
        /// <param name="fluoros"></param>
        /// <param name="spotController"></param>
        /// <returns></returns>
        private ProbeScoringAssay OpenSpotConfiguration(SpotConfigController spotController)
        {
            ProbeScoringAssay currentAssay = null;
            if (spotController != null)
            {
                var config = new CytoApps.SpotConfiguration.SpotConfigDialog(spotController);
                config.Owner = _frameWindow;
                if (config.ShowDialog() ?? false)
                {
                    currentAssay = new ProbeScoringAssay(spotController);
                }
            }
            return currentAssay;
        }

        private void UpdateGridAndFrameFromAssay(ProbeScoringAssay newAssay)
        {
            _frameWindowViewModel.SharedVM.CurrentAssay = newAssay;
            _frameWindowViewModel.SharedVM.ScoredCells.Clear();
            _frameWindowViewModel.SharedVM.SessionSummary = new SessionSummary();
            _frameWindowViewModel.SharedVM.SessionSummary.TotalInformativeCell = 0;
            _frameWindowViewModel.UpdateFrame();
            //Notify the gridView Presenter
            _eventAggregator.GetEvent<NotifyGridViewPresenterEvent>().Publish(null);
        }

        /// <summary>
        /// Open Spot Configuration Window from frame
        /// </summary>
        /// <param name="canReprocess">
        /// canReprcess is true indicates the reprocess can be done  and intiate the reprocess task
        /// </param>

        private void OnOpenSpotConfiguartionAndReprocess(bool canReprocess = false)
        {
            try
            {
                ProbeScoringAssay currentAssay = null;

                var spotController = _frameWindowViewModel.SharedVM.CurrentAssay.SpotConfigController;
                //Copy this for on cancel load the previous 
                var clonedSpotController = spotController.Clone();
                //Make the classes editable or not based on scores exist.
                if (!canReprocess)
                {
                    spotController.UndeleteableClassNames = _frameWindowViewModel.SharedVM.ScoredCells.Where(x => !x.IsDeleted).Select(x => x.Class).Distinct();

                    if (spotController.UndeleteableClassNames != null)
                    {
                        clonedSpotController.UndeleteableClassNames = spotController.UndeleteableClassNames.ToList();
                    }
                }

                currentAssay = OpenSpotConfiguration(clonedSpotController);
                if (currentAssay != null)
                {
                    _frameWindowViewModel.SharedVM.IsDirty = true;
                    _frameWindowViewModel.SharedVM.CurrentAssay = currentAssay;
                    _frameWindowViewModel.UpdateFrame();
                    SavAssayFile();
                    //Notify the gridView Presenter
                    _eventAggregator.GetEvent<NotifyGridViewPresenterEvent>().Publish(null);

                    //Reprocess the frame
                    if (canReprocess)
                    {
                        OnReProcessSession();
                    }
                }
                else
                {
                    //To  hide the progress bar on cancel click
                    if (canReprocess)
                        _frameWindowViewModel.CanReprocessCurrentFrame = _frameWindowViewModel.CanReprocessAllFrames = false;
                }
            }
            catch (Exception exception)
            {
                LogManager.Error("Error: ", exception);
            }
        }


        // TODO - Need to REVISIT here during the shared view model REFACTOR
        private void OnSelectedGridScoreChange(ScoredCell scoredCell)
        {
            Parallel.ForEach(_frameWindowViewModel.SharedVM.ScoredCells.ToList(), (score) =>
            {
                score.IsHighlighted = false;
            });

            if (scoredCell != null)
            {
                _frameWindowViewModel.SharedVM.CurrentFrame = _frameWindowViewModel.SharedVM.Frames.Where(frame => frame.Image.Id == scoredCell.FrameId).FirstOrDefault();
                scoredCell.IsHighlighted = true;
            }
        }

        private void OnScoringWindow(FrameInfo frameInfo)
        {
            if (_frameWindow == null)
            {
                _frameWindow = new FrameWindowView();
                _frameWindow.Closed += (a, b) => _frameWindow = null;
                _frameWindow.DataContext = _frameWindowViewModel;
                _frameWindowViewModel.ShowOverview = false;
                _frameWindowViewModel.AnalysisRegionSurfaceVM.CircleDiameterPixels = Constants.CircleDiameterPixels;
                _frameWindow.Show();
                if (_frameWindowViewModel.DisplayImage == null)
                {
                    CurrentFrameChanged();
                }
                FitToBoundingBox();
                GetRegionsForCurrentFrame();

            }
            else
            {
                _frameWindow.Show();
            }
        }

        private void Render()
        {
            if (_renderCancelToken != null)
            {
                _renderCancelToken.Cancel();
            }
            _renderCancelToken = new CancellationTokenSource();

            var dispatcher = Dispatcher.CurrentDispatcher;
            _tasksList.Add(Task.Run(() =>
            {
                // HACK, need an explanation from Jago on how this should work, its confusing me and looks a bit nasty
                // dealing with cache outsitde render
                var levels = new List<ComponentLevels>();
                foreach (var c in _frameWindowViewModel.SharedVM.CurrentFrame.Image.Components)
                {
                    var existing = (_frameWindowViewModel.ComponentDisplayOptionsVM.RendererLevels != null) ? _frameWindowViewModel.ComponentDisplayOptionsVM.RendererLevels.FirstOrDefault(e => e.Component == c) : null;

                    if (existing == null)
                    {
                        existing = new ComponentLevels(c, _frameWindowViewModel.ComponentDisplayOptionsVM.RendererOptions.ComponentOffsets(c.Name));
                        // HACK, so bad :( copy over visible settings for now from previous frame
                        var previousLevels = (_frameWindowViewModel.ComponentDisplayOptionsVM.RendererLevels != null) ? _frameWindowViewModel.ComponentDisplayOptionsVM.RendererLevels.FirstOrDefault(e => e.Component.Name == c.Name) : null;
                        if (previousLevels != null)
                        {
                            existing.IsVisible = previousLevels.IsVisible;
                        }
                    }
                    else
                        existing.Wake(_frameWindowViewModel.ComponentDisplayOptionsVM.RendererOptions.ComponentOffsets(c.Name));

                    levels.Add(existing);
                }
                _frameWindowViewModel.ComponentDisplayOptionsVM.RendererLevels = levels;

                // Note: render will also assign histogram data to the RenderLevels parameter items
                var bitmap = _dataService.GetFrameBitmap(_frameWindowViewModel.SelectedSession.Slide, _frameWindowViewModel.SelectedSession.ScanAreaId,
                                                    _frameWindowViewModel.SharedVM.CurrentFrame.Image,
                                                    _frameWindowViewModel.ComponentDisplayOptionsVM.RendererLevels, _frameWindowViewModel.ComponentDisplayOptionsVM.ZLevel,
                                                    _frameWindowViewModel.ComponentDisplayOptionsVM.RendererOptions);

                // update on UI thread
                dispatcher.BeginInvoke((Action)delegate
                {
                    if (!_renderCancelToken.IsCancellationRequested)
                    {
                        _frameWindowViewModel.DisplayImage = bitmap;

                        // foreach component generate display points from histogram data
                        foreach (var c in _frameWindowViewModel.ComponentDisplayOptionsVM.RendererLevels)
                        {
                            if (c.Histogram != null)
                            {
                                var pc = new PointCollection(c.Histogram.Points);
                                pc.Freeze(); // can now use Dep. Property across threads
                                c.HistogramPoints = pc;
                                c.SetLowHighValues(c.Histogram.RenderedLow, c.Histogram.RenderedHigh, true);
                            }
                            else
                            {
                                c.HistogramPoints = null;
                                c.SetLowHighValues(0, 0, true);
                            }
                        }
                        _frameWindowViewModel.UpdateFrame();
                    }
                }, DispatcherPriority.Normal);

            }, _renderCancelToken.Token));
        }

        public void CloseFrameWindowOnSessionClose(object unused)
        {
            if (null != _tasksList && _tasksList.Count > 0)
            {
                Task.WaitAll(_tasksList.ToArray());// wait all tasks to complete, load region, load annotation etc.
            }

            if (_frameWindow != null && _frameWindow.Visibility == System.Windows.Visibility.Visible)
            {
                ProbeCaseViewSettings.Default.Save();
                _frameWindow.Close();
            }
        }

        public void HideFrameWindow(object unUsed)
        {
            if (_frameWindow != null && _frameWindow.Visibility == System.Windows.Visibility.Visible)
            {
                ProbeCaseViewSettings.Default.Save();
                _frameWindow.Hide();
            }
        }
        private void OnRender(object obj)
        {
            Render();
        }

        #region "Analysis Region"
        /// <summary>
        /// Get all the regins for session
        /// </summary>
        private void LoadAllRegions()
        {
            var regions = _dataService.GetAllAnalysisRegions(_frameWindowViewModel.SharedVM.SelectedSession);
            if (regions != null)
            {
                foreach (AnalysisRegion region in regions)
                {
                    var pc = new PointCollection(from p in region.Points select new Point(p.X, p.Y));
                    AnalysisRegionInfo newregion = new AnalysisRegionInfo(region.Id, pc);
                    newregion.FrameId = region.Frame.Id.ToString();
                    _frameWindowViewModel.SharedVM.AllFrameRegions.Add(newregion);
                }
            }
        }
        ///<summary>
        ///Clear the regions
        /// </summary>
        private void ClearRegions()
        {
            _frameWindowViewModel.AnalysisRegionSurfaceVM.Regions.Clear();
        }

        private void GetRegionsForCurrentFrame()
        {
            ClearRegions();
            var regions = _frameWindowViewModel.SharedVM.AllFrameRegions.Where(region => region.FrameId == _frameWindowViewModel.SharedVM.CurrentFrame.Image.Id.ToString() && region.CanDelete == false);
            if (regions != null)
            {
                foreach (AnalysisRegionInfo region in regions)
                {
                    region.AllowDelete = true;
                    region.AdjustLabelToFit(_frameWindowViewModel.SharedVM.CurrentFrame.FrameHeight, _frameWindowViewModel.SharedVM.CurrentFrame.FrameWidth);
                    _frameWindowViewModel.AnalysisRegionSurfaceVM.AddRegion(region);
                    var cell = _frameWindowViewModel.ScoresForCurrentFrame.Where(x => x.Score.RegionId == region.ID).FirstOrDefault();
                    if (cell != null)
                    {
                        cell.Region = region.DisplayShape;
                        cell.AdjustLabelToFit(_frameWindowViewModel.SharedVM.CurrentFrame.Image.Width, _frameWindowViewModel.SharedVM.CurrentFrame.Image.Height);
                    }
                    _frameWindowViewModel.UpdateFrame();
                }
            }
        }

        /// <summary>
        /// Save Region 
        /// </summary>
        private void AnalyseRegion(AnalysisRegionInfo analysisRegionInfo)
        {
            if (analysisRegionInfo != null)
            {
                _frameWindowViewModel.SharedVM.IsDirty = true;
                //To delete the newly created region on Cancel click in gridview.
                _frameWindowViewModel.SharedVM.AllFrameRegions.Add(analysisRegionInfo);
                // Call SendRegionForAnalysis asynchronously.
                // Need it for keeping UI responsive.
                AsyncSendRegionForAnalysis.BeginInvoke(analysisRegionInfo, x => AsyncSendRegionForAnalysis.EndInvoke(x), null);
            }
        }

        private void SendRegionForAnalysis(AnalysisRegionInfo analysisRegionInfo)
        {
            try
            {
                AnalysisRegion analysisRegion = new AnalysisRegion()
                {
                    Id = analysisRegionInfo.ID,
                    Session = _frameWindowViewModel.SelectedSession,
                    Frame = _frameWindowViewModel.SharedVM.CurrentFrame.Image,
                    CellCount = _cellCount++,
                    Points = (from p in analysisRegionInfo.DisplayShape select new SerializiblePoint { X = p.X, Y = p.Y }).ToList(),
                };
                _dataService.QueueRegionForAnalysis(analysisRegion);
                _regions.Add(analysisRegion);
                // Check if worker thread is already running or not.
                // And start worker thread for processing regions.
                if ((_regions != null && _regions.Count > 0) && !_isProcessRegionsStarted)
                    StartProcessRegions();

            }
            catch (Exception createNewRegionError)
            {
                LogManager.Error("Create new region error: ", createNewRegionError);
            }
        }

        /// <summary>
        /// Get the last score count to get the unique id for the score
        /// </summary>
        /// <returns></returns>
        private int GetScoreCount()
        {
            int scoreCount = 0;
            var measIds = from s in _frameWindowViewModel.SharedVM.ScoredCells
                          where s.HasSpotMeasurements && s.SpotMeasurements.Where(m => m.OriginalName == "MEAS_Id").Count() > 0
                          select s.SpotMeasurements.Where(m => m.OriginalName == "MEAS_Id").First().Value;
            if (measIds.FirstOrDefault() != null)
            {
                // The object numbers are the last 3 digits of the MEAS_IDs
                scoreCount = (from m in measIds select Convert.ToInt32(m, CultureInfo.InvariantCulture) % 1000).Max();
            }
            return scoreCount;
        }

        #region Worker thread methods
        /// <summary>
        /// Start region processing asynchronously on worker thread.
        /// </summary>
        private void StartProcessRegions()
        {
            // Create new collection of regions and pass to worker thread. 
            // It just to keep collection thread safe.
            _worker.RunWorkerAsync(new List<AnalysisRegion>(_regions));
            // Set status flag to true. To indicate worker is processing regions.
            _isProcessRegionsStarted = true;
        }

        /// <summary>
        /// Asynchronous handler method for worker thread. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProcessRegions(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            // Get regions to process.
            var regions = (List<AnalysisRegion>)e.Argument;
            List<Score> scores = null;
            // Loop until we get single score.
            do
            {
                if (scores != null)
                {
                    // To realize CPU
                    Thread.Sleep(20);
                }
                // Get scores for processed regions.
                scores = _dataService.GetScoreByRegionAnalysis(regions);
            } while (scores == null || scores.Count <= 0);
            // pass scores to callback handler.
            e.Result = scores;
        }

        /// <summary>
        /// Callback Handler method for worker thread.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProcessingRegionsCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Error == null)
            {
                // Get scores of processed regions
                var scores = (List<Score>)e.Result;
                foreach (var score in scores)
                {
                    if (score != null)
                    {
                        // get region from regions, which match regionID of score.
                        AnalysisRegion region = _regions.Where(x => x.Id == score.RegionId).FirstOrDefault();
                        if (_regions.Contains(region))
                        {
                            // Remove processed Region from regions collection.
                            _regions.Remove(region);
                            //if Class present indicates the score is exist
                            if (score.Class != null)
                            {
                                // Get AnalysisRegionInfo of scored region. To set AllowDelete = true.
                                AnalysisRegionInfo analysisRegion = _frameWindowViewModel.AnalysisRegionSurfaceVM.Regions.Where(x => x.ID == score.RegionId).FirstOrDefault();
                                var newScore = new ScoredCell(score);
                                newScore.IsNewlyCreated = true;
                                Application.Current.Dispatcher.Invoke(new Action(() =>
                                {
                                    newScore.Region = new PointCollection(from p in region.Points select new Point(p.X, p.Y));
                                    // It will help to delete region when new assay is loaded or created.
                                    if (analysisRegion != null)
                                        analysisRegion.AllowDelete = true;
                                    newScore.AdjustLabelToFit(_frameWindowViewModel.SharedVM.CurrentFrame.FrameWidth, _frameWindowViewModel.SharedVM.CurrentFrame.FrameHeight);
                                    newScore.ParseMeasurements(_frameWindowViewModel.SharedVM.Signals);
                                    _frameWindowViewModel.SharedVM.ScoredCells.Add(newScore);
                                    _frameWindowViewModel.UpdateFrame();
                                    _eventAggregator.GetEvent<ScoresUpdatedEvent>().Publish(true);
                                }));
                            }
                            else
                            {
                                // Score with no class indicates the no score present
                                AnalysisRegionInfo analysisRegion = _frameWindowViewModel.AnalysisRegionSurfaceVM.Regions.Where(x => x.ID == score.RegionId).FirstOrDefault();
                                if (analysisRegion != null)
                                {
                                    _frameWindowViewModel.SharedVM.AllFrameRegions.Remove(analysisRegion);
                                    analysisRegion.IsRegionScored = false;
                                    analysisRegion.AllowDelete = false;
                                }
                            }
                        }
                        else
                        {
                            //Add region to regions collection for processing.
                            _regions.Add(region);
                        }
                    }
                }

                // check for other newly added regions
                if (_regions != null && _regions.Count > 0)
                {
                    //If true, Start worker thread again.
                    StartProcessRegions();
                }
                else
                {
                    // set status flag to false. So when new region is added.
                    //it will call StartProcessRegions to start processing.
                    _isProcessRegionsStarted = false;
                }
            }

        }
        #endregion

        #endregion

        #region "Slide Overview"

        private void FitAllFramesInSlide(object unasigned)
        {
            FitToBoundingBox();
        }

        public void FitToBoundingBox()
        {
            var r = GetRegionsBoundingBox();
            var newpos = new Point(r.Left + (r.Width / 2), r.Top + (r.Height / 2));
            var scaleToActual = _frameWindow.slideOverView.stretchSizeCanvas.ActualWidth / _frameWindowViewModel.SlideOverviewVM.SlideWidth;
            double desiredFit = r.Width * scaleToActual * 1.2;
            bool fitWidth = true;
            if (r.Height > r.Width)
            {
                desiredFit = r.Height;
                fitWidth = false;
            }
            _frameWindow.slideOverView.stretchSizeCanvas.ZoomAndPanTo(newpos.X, newpos.Y, desiredFit, fitWidth);
        }

        private void ZoomAndPanTo(FrameInfo Frame)
        {
            if (Frame != null && _frameWindowViewModel.SlideOverviewVM.IsViewable)
            {
                var frameOutline = GetFrameOutline(Frame.Image);
                if (frameOutline != null)
                {
                    var newpos = new Point(frameOutline.CentreX, frameOutline.CentreY);
                    var newzoom = (_frameWindowViewModel.SlideOverviewVM.Zoom < 128.0) ? 128.0 : _frameWindowViewModel.SlideOverviewVM.Zoom;

                    _frameWindow.slideOverView.stretchSizeCanvas.CentreOnDomainPoint(newpos, newzoom);
                }
            }
        }

        private FrameOutlineInfo GetFrameOutline(PCVImageRenderers.Models.Frame frame)
        {
            return _frameWindowViewModel.SlideOverviewVM.SlideOverViewFrames.Where(fr => fr.Frame == frame).FirstOrDefault();
        }

        private Rect GetRegionsBoundingBox()
        {
            double l = double.MaxValue, t = double.MaxValue, b = 0.0, r = 0.0;

            foreach (var f in _frameWindowViewModel.SlideOverviewVM.SlideOverViewFrames)
            {
                if (f.PositionX < l)
                    l = f.PositionX;
                if (f.PositionX + f.Width > r)
                    r = f.PositionX + f.Width;
                if (f.PositionY < t)
                    t = f.PositionY;
                if (f.PositionY + f.Height > b)
                    b = f.PositionY + f.Height;
            }
            if (Math.Abs(l + r) <= 2.0 && Math.Abs(t + b) <= 2.0)
                _frameWindowViewModel.SlideOverviewVM.OverViewEnabled = false;

            return new Rect(new Point(l, t), new Point(r, b));
        }


        #endregion "Slide Overview"

        #region Reprocess

        /// <summary>
        ///Method will get the SpotFolders for session to reprocess and if spotFolders present Open the Spot Configuartion window
        /// </summary>
        /// <param name="unUsed"></param>
        private void OnReprocessFrames(bool CanReprocessAll)
        {
            _spotFolders = new List<string>();
            if (CanReprocessAll)
            {

                _spotFolders = _dataService.GetSpotFolderNamesForReprocess(_frameWindowViewModel.SharedVM.SelectedSession).ToList();
            }
            else
            {
                _spotFolders = _dataService.GetSpotFolderNamesForReprocess(_frameWindowViewModel.SharedVM.SelectedSession, _frameWindowViewModel.SharedVM.CurrentFrame.Image.Id.ToString()).ToList();

            }
            //No spot Folders present to reprocess
            if (_spotFolders.Count() == 0)
            {
                _frameWindowViewModel.CanReprocessCurrentFrame = _frameWindowViewModel.CanReprocessAllFrames = false;
                CustomPopUp.Show(Literal.Strings.Lookup("cbStr-NoRegionToReprocessMessage"), Literal.Strings.Lookup("cbStr-ReprocessTitle"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information, _frameWindow);
                return;
            }
            else
            {
                MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-ReprocessMessage"), Literal.Strings.Lookup("cbStr-ReprocessTitle"), MessageBoxButton.YesNo, MessageBoxImage.Question, _frameWindow);
                if (result == MessageBoxResult.Yes)
                {
                    OnOpenSpotConfiguartionAndReprocess(true);
                }
                else
                {
                    _frameWindowViewModel.CanReprocessCurrentFrame = _frameWindowViewModel.CanReprocessAllFrames = false;
                }
            }
        }


        /// <summary>
        /// On Reprocess clear the scores and regions from the frames and start the worker thread
        /// </summary>
        private void OnReProcessSession()
        {
            _frameWindowViewModel.ClearScoresFromSession();
            _frameWindowViewModel.SharedVM.IsReprocessing = _frameWindowViewModel.SharedVM.IsDirty = _dataService.StartReprocessing(_frameWindowViewModel.SharedVM.SelectedSession, _spotFolders);
            _frameWindowViewModel.ReprocessCurrent = 0;
            _frameWindowViewModel.ReprocessMaximum = _spotFolders.Count();
            _frameWindowViewModel.IsAssayPopupOpen = true;
            if (_frameWindowViewModel.SharedVM.IsReprocessing)
            {
                StartReadReprocessWorker(_spotFolders);
            }
        }


        /// <summary>
        /// reprocess work complete method to get tehscores and intiate the reprocess if any spot folder exist
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReadReprocessResultCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            if (!e.Cancelled && e.Error == null)
            {
                ReprocessScores reprocessScore = (ReprocessScores)e.Result;
                if (reprocessScore != null)
                {
                    if (reprocessScore.SpotFolders.Count() > 0)
                    {
                        StartReadReprocessWorker(reprocessScore.SpotFolders);
                    }
                    if (reprocessScore.Scores.Count > 0)
                    {
                        Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new Action(() =>
                        {
                            _frameWindowViewModel.ReprocessCurrent = _frameWindowViewModel.ReprocessMaximum - reprocessScore.SpotFolders.Count();
                            _frameWindowViewModel.SharedVM.IsReprocessing = (_frameWindowViewModel.ReprocessCurrent != _frameWindowViewModel.ReprocessMaximum);
                            if (!_frameWindowViewModel.SharedVM.IsReprocessing)
                            {
                                _frameWindowViewModel.IsAssayPopupOpen = false;
                                _frameWindowViewModel.CanReprocessCurrentFrame = _frameWindowViewModel.CanReprocessAllFrames = _frameWindowViewModel.SharedVM.IsReprocessing;
                            }
                            foreach (Score score in reprocessScore.Scores)
                            {
                                var newScore = new ScoredCell(score);
                                newScore.IsNewlyCreated = true;
                                if (score.RegionId != Guid.Empty)
                                {
                                    AnalysisRegionInfo analysisRegion = _frameWindowViewModel.SharedVM.AllFrameRegions.Where(x => x.ID == score.RegionId).FirstOrDefault();
                                    if (analysisRegion != null)
                                    {
                                        analysisRegion.CanDelete = false;
                                        newScore.Region = new PointCollection(from p in analysisRegion.Points select new Point(p.X, p.Y));
                                    }
                                }
                                newScore.ParseMeasurements(_frameWindowViewModel.SharedVM.Signals);
                                _frameWindowViewModel.SharedVM.ScoredCells.Add(newScore);
                            }
                            _frameWindowViewModel.UpdateFrame();
                            _eventAggregator.GetEvent<ScoresUpdatedEvent>().Publish(true);
                        }));
                    }
                }
            }
        }


        /// <summary>
        /// Reprocess do work method to get the scores 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReadReprocessResult(object sender, DoWorkEventArgs e)
        {
            var spotFolders = (List<string>)e.Argument;
            e.Result = _dataService.GetScoresFromSpotFoldersProcessed(_frameWindowViewModel.SharedVM.SelectedSession, spotFolders);

        }

        /// <summary>
        /// intiate the thread for reprocess
        /// </summary>
        /// <param name="spotFolders"></param>
        private void StartReadReprocessWorker(List<string> spotFolders)
        {
            //Create the reporcess worker thread 

            _readReprocessResultWorker.RunWorkerAsync(spotFolders);
        }
        #endregion


        private void UnSubscribeEvents()
        {
            _eventAggregator.GetEvent<ScoringWindowOpenEvent>().Unsubscribe(OnScoringWindow);
            _eventAggregator.GetEvent<RenderFrameEvent>().Unsubscribe(OnRender);
            _eventAggregator.GetEvent<SelectedGridScoreEvent>().Unsubscribe(OnSelectedGridScoreChange);
            _eventAggregator.GetEvent<OpenSpotConfigWindowEvent>().Unsubscribe(OnOpenSpotConfiguartionAndReprocess);
            _eventAggregator.GetEvent<CreateNewAssayEvent>().Unsubscribe(OnCreateNewAssay);
            _eventAggregator.GetEvent<GetComptibleAssaysEvent>().Unsubscribe(GetCompatibleAssays);
            _eventAggregator.GetEvent<LoadAssayEvent>().Unsubscribe(LoadAssayFromSharedFolder);
            _eventAggregator.GetEvent<SaveAsAssayEvent>().Unsubscribe(OnSaveAsAssay);
            _eventAggregator.GetEvent<FitAllFramesInSlideEvent>().Unsubscribe(FitAllFramesInSlide);
            _eventAggregator.GetEvent<ZoomAndPanToCurrentFrameEvent>().Unsubscribe(ZoomAndPanTo);
            _eventAggregator.GetEvent<SaveRegionEvent>().Unsubscribe(AnalyseRegion);
            _eventAggregator.GetEvent<CloseFrameWindowEvent>().Unsubscribe(HideFrameWindow);
            _eventAggregator.GetEvent<DisplayStoredAnnotationViewEvent>().Unsubscribe(ToggleStoredAnnotationView);
            _eventAggregator.GetEvent<ReprocessEvent>().Unsubscribe(OnReprocessFrames);
            _eventAggregator.GetEvent<ScoresUpdatedEvent>().Unsubscribe(OnScoresUpdated);

        }

        private void OnScoresUpdated(bool canUpdateScore)
        {
            if (!canUpdateScore)
            {
                List<ScoreSettings> scoreCellSettingList = UpdateScoreSettingList();
                foreach (var score in _frameWindowViewModel.SharedVM.ScoredCells.ToList())
                {

                    if (score.IsNewlyCreated && score.IsDeleted)
                    {
                        _frameWindowViewModel.SharedVM.ScoredCells.Remove(score);
                        score.Dispose();
                    }
                }
                _dataService.DeleteNewScoresAndRegionsFromSession(_frameWindowViewModel.SharedVM.SelectedSession, scoreCellSettingList.Where(cell => cell.IsNewlyCreated == true && cell.IsDeleted == true).ToList(),
            _frameWindowViewModel.SharedVM.AllFrameRegions.Where(region => region.IsNewRegion == true && region.CanDelete == true).Select(x => x.ID.ToString()).ToList());
                foreach (AnalysisRegionInfo region in _frameWindowViewModel.SharedVM.AllFrameRegions.ToList())
                {
                    if (region.IsNewRegion == true && region.CanDelete == true)
                    {
                        _frameWindowViewModel.SharedVM.AllFrameRegions.Remove(region);
                    }
                }
            }

        }

        private List<ScoreSettings> UpdateScoreSettingList()
        {
            List<ScoreSettings> scoreCellSettingList = new List<ScoreSettings>();
            foreach (var cell in _frameWindowViewModel.SharedVM.ScoredCells)
            {
                if (cell.IsDeleted || cell.IsNewlyCreated)
                {
                    scoreCellSettingList.Add(new ScoreSettings()
                    {
                        scoreid = cell.Score.Id,
                        IsReviewed = cell.Reviewed,
                        IsRescored = cell.IsRescored,
                        IsEnhanced = cell.HasEnhancements,
                        IsDeleted = cell.IsDeleted,
                        IsNewlyCreated = cell.IsNewlyCreated
                    });
                }
            }
            return scoreCellSettingList;
        }

        public void Dispose()
        {
            _frameWindowViewModel.SharedVM.FrameChangeEvent -= CurrentFrameChanged;
            UnSubscribeEvents();

            if (_renderCancelToken != null)
            {
                _renderCancelToken.Cancel();
                _renderCancelToken.Dispose();
            }
            if (_frameWindow != null)
            {
                _frameWindow.stretchSizeCanvas.DataContext = null;
                _frameWindow.stretchSizeCanvas = null;
                _frameWindow.DataContext = null;
                _frameWindow.Close();
                _frameWindow = null;

            }
            _frameWindowViewModel.Dispose();
            _frameWindowViewModel = null;
            _eventAggregator = null;
            _dataService = null;
            // Dispose Worker Thread
            _worker.RunWorkerCompleted -= ProcessingRegionsCompleted;
            _worker.DoWork -= ProcessRegions;
            _worker.Dispose();
            //_worker = null;
            // Destroy delegate instance
            _readReprocessResultWorker.RunWorkerCompleted -= ReadReprocessResultCompleted;
            _readReprocessResultWorker.DoWork -= ReadReprocessResult;
            _readReprocessResultWorker.Dispose();
            AsyncSendRegionForAnalysis = null;

        }
    }
}
