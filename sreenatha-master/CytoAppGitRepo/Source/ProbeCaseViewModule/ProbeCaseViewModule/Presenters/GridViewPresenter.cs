﻿using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.UI.Controls;
using PCVImageRenderers;
using Prism.Events;
using Prism.Regions;
using ProbeCaseViewDataAccess.Models;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Models;
using ProbeCaseViewModule.Services;
using ProbeCaseViewModule.ViewModels;
using ProbeCaseViewModule.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using CytoApps.Localization;
using PCVImageRenderers.Models;
using System.Threading;
using System.Windows.Threading;
using CytoApps.Infrastructure.UI.Utilities;
using AI;
using System.Collections.Concurrent;
using CytoApps.Exceptions;
using Microsoft.Practices.ServiceLocation;
using System.Windows.Input;
using CytoApps.Infrastructure.UI.ApplicationCommands;
using Prism.Commands;
using System.ComponentModel;
using CytoApps.Infrastructure.UI.Events;

namespace ProbeCaseViewModule.Presenters
{
    /// <summary>
    /// Grid View Presenter holds all logical stuffs for displaying ScoredCells and Enhancing scoredcells
    /// </summary>
    /// 
    class GridViewPresenter : IDisposable
    {
        private IRegionManager _regionManager;
        private GridViewModel _gridViewModel;
        private CancellationTokenSource _thumbnailCancelToken;
        private BlockingCollection<ScoredCell> _autoSaveCellCollection;
        private CancellationTokenSource _autoSaveCancelToken;
        private List<Task> _newScoreThumbCreationTasks;
        private IEventAggregator _eventAggregator;
        private IDataService _dataService;
        public GridView _gridView;

        private DualReaderFramesGalleryPresenter _dualReaderFramesGalleryPresenter;
        public GridViewPresenter()
        {
            _regionManager = ServiceLocator.Current.TryResolve<IRegionManager>();
            _eventAggregator = ServiceLocator.Current.TryResolve<IEventAggregator>();
            _dataService = ServiceLocator.Current.TryResolve<IDataService>();
            _gridView = new GridView();
            _autoSaveCellCollection = new BlockingCollection<ScoredCell>(20);
            _autoSaveCancelToken = new CancellationTokenSource();
            _newScoreThumbCreationTasks = new List<Task>();
            SubscribeEvents();

            // Registering to global shutdown command, allowing the modules to set the Cancel property to true if the module wants to cancel the shutdown.
            //  HostCommands.ShutdownCommand.RegisterCommand(ShutdownCommand);
        }

        //public ICommand ShutdownCommand
        //{
        //    get
        //    {
        //        return new DelegateCommand<ShutDownEventArgs>((args) =>
        //        {
        //            args.CancelEventArgs.Cancel = _isDirty;
        //        });
        //    }
        //}

        public void InitializeGridViewModel(SharedViewModel sharedVm, DualReaderFramesGalleryPresenter dualReaderFramesGalleryPresenter)
        {
            _dualReaderFramesGalleryPresenter = dualReaderFramesGalleryPresenter;
            if (_gridViewModel != null) _gridViewModel = null;
            _gridViewModel = new GridViewModel(sharedVm);

            ////REVISIT - MERGE Conflict.. REFACTOR
            ////Dual Reader - Fill VM with selected frames if sesion is created with selected frames
            _gridViewModel.IsAddCellsVisible = sharedVm.SelectedSession.IsCreatedWithSelectedFrames;

            ParseMeasurements(_gridViewModel);
            // Image stacks, whats the max to handle slider intervals
            _gridViewModel.StackSize = _gridViewModel.SharedVM.Frames.Max(frame => frame.Image.ZLevels);
            // once data is all read in lets handle amy missing data with defaults
            HandleVersions();
            // Add scored cells to paginated source, this handles retrieving cell images when based on visibility
            _gridViewModel.PaginatedScores.Source = _gridViewModel.SharedVM.ScoredCells.ToArray();
            _gridView.DataContext = _gridViewModel;
            // Configure sorting and filtering classes
            _gridViewModel.GetFilteredClass();
            GetSortingOptions(_gridViewModel);
           OpenGridView();

            if (_gridViewModel.PaginatedScores.Source != null && _gridViewModel.PaginatedScores.Source.Count > 0)
            {
                _gridViewModel.CurrentCell = _gridViewModel.PaginatedScores.Source.ElementAt(0);
                _gridViewModel.CurrentCell.Selected = true;
            }

        }


        private void OpenGridView()
        {
            _regionManager.AddToRegion(RegionNames.MainRegion, _gridView);
            _regionManager.Regions[RegionNames.MainRegion].Activate(_gridView);
        }

        private void SubscribeEvents()
        {
            _eventAggregator.GetEvent<CloseSessionEvent>().Subscribe(CloseSession);
            _eventAggregator.GetEvent<RenderOverlaysEvent>().Subscribe(RenderOverlays);
            _eventAggregator.GetEvent<ChannelDisplayEvent>().Subscribe(RenderThumbnails);
            _eventAggregator.GetEvent<RefreshGridDisplayEvent>().Subscribe(RenderThumbnails);
            _eventAggregator.GetEvent<ApplyImageSettingsToGridEvent>().Subscribe(OnApplyImageSettingsToGrid);
            _eventAggregator.GetEvent<GridResizeEvent>().Subscribe(RenderAll);
            _eventAggregator.GetEvent<GridScrollValueChangeEvent>().Subscribe(GridScrollValueChanged);
            _eventAggregator.GetEvent<CurrentChannelDisplayEvent>().Subscribe(ChannelSettingsAdjusted);
            _eventAggregator.GetEvent<UpdateScoredCellEvent>().Subscribe(RenderThumbnails);
            _eventAggregator.GetEvent<ReclassifyEvent>().Subscribe(ReClassifySelectedCells);
            _eventAggregator.GetEvent<ReScoreEvent>().Subscribe(RescoreSignalCount);
            _eventAggregator.GetEvent<ReClassifyUsingNewClassEvent>().Subscribe(ReclassifyToOther);
            _eventAggregator.GetEvent<SaveAllDataEvent>().Subscribe(SaveAllDataOnClose);
            _eventAggregator.GetEvent<ScoresUpdatedEvent>().Subscribe(OnScoresUpdated);
            _eventAggregator.GetEvent<NewScoreEvent>().Subscribe(NewScore);
            _eventAggregator.GetEvent<AdjustGridImageEvent>().Subscribe(AdjustGridImage);
            _eventAggregator.GetEvent<SelectedFrameScoreEvent>().Subscribe(OnSelectedScoreOnFrameChange);
            _eventAggregator.GetEvent<NotifyGridViewPresenterEvent>().Subscribe(OnUpdateFromAssay);
        
            //Dual Reader Score Page display
            _eventAggregator.GetEvent<AddCellsEvent>().Subscribe(AddCells);
        }

     

        private void OnUpdateFromAssay(object unUsed)
        {
            if (_gridViewModel != null)
            {
                _gridViewModel.PaginatedScores.Source = _gridViewModel.SharedVM.ScoredCells.ToArray();
                _gridViewModel.GetFilteredClass();
                GetSortingOptions(_gridViewModel);
            }
        }

        //private void OpenGridView()
        //{
        //    if (!CheckProbeCaseWorkspaceViewExists())
        //    {
        //        _regionManager.RegisterViewWithRegion(RegionNames.MainRegion, typeof(ProbeCaseWorkspaceView));
        //    }
        //    else
        //    {
        //        _regionManager.RequestNavigate(RegionNames.MainRegion, "ProbeCaseWorkspaceView.xaml");
        //    }

        //    ClearViews();
        //    ActivateScoresView();
        //    _regionManager.RegisterViewWithRegion(PcvRegionNames.ScoresViewRegion, () => _gridView);
        //    //OpenFramesWindow();

        //}

        /// <summary>
        /// Take setting from the adjust setting VM and apply to grid display
        /// </summary>
        /// <param name="unused"></param>
        private void OnApplyImageSettingsToGrid(bool canApplyImageSettings)
        {
            if (canApplyImageSettings)
            {
                if (_gridViewModel.AdjustThumbnailVM == null)
                    return;

                // RFACTOR HashSet bit pointless as we aren't adding to it in this app, get rid of it
                _gridViewModel.ChannelDisplays = new HashSet<ChannelDisplayInfo>(_gridViewModel.AdjustThumbnailVM.ChannelDisplays);
                _gridViewModel.CurrentStackIndex = _gridViewModel.AdjustThumbnailVM.CurrentStackIndex;
                _gridViewModel.IsMaxProjection = _gridViewModel.AdjustThumbnailVM.IsMaxProjection;
                _gridViewModel.ClonedAdjustThumnailVM = (AdjustThumbnailViewModel)_gridViewModel.AdjustThumbnailVM.Clone();
                // refresh grid display
                _gridViewModel.InvalidateView();
                RenderThumbnails();
            }
            _gridViewModel.IsAdjustImagePopupOpen = false;
        }

        /// <summary>
        /// Updates the image display in the Adjust settings popup
        /// </summary>
        /// <param name="unused"></param>
        private void ChannelSettingsAdjusted(object unused)
        {
            _gridViewModel.AdjustThumbnailVM.RenderRequestTime = DateTime.Now;

            if (_gridViewModel.AdjustThumbnailVM.ChannelDisplays == null)
                return;

            if (_gridViewModel.AdjustThumbnailVM.RenderTask == null)
            {
                _gridViewModel.AdjustThumbnailVM.RenderTask = Task.Factory.StartNew(() =>
                {
                    try
                    {
                        while (_gridViewModel.AdjustThumbnailVM.RenderRequestTime > _gridViewModel.AdjustThumbnailVM.LastRenderTime)
                        {
                            int stackIndex = _gridViewModel.AdjustThumbnailVM.IsMaxProjection ? -1 : _gridViewModel.AdjustThumbnailVM.CurrentStackIndex;
                            var visible = _gridViewModel.AdjustThumbnailVM.ChannelDisplays.Where(channel => channel.IsChecked).Select(channelDisplay => channelDisplay.ChannelDisplay).ToList();

                            _gridViewModel.AdjustThumbnailVM.Thumbnail = _gridViewModel.AdjustThumbnailVM.ThumbRenderer.Render(_gridViewModel.AdjustThumbnailVM.ThumbnailData, stackIndex, visible, _gridViewModel.AttenuateCounterstain, _gridViewModel.InvertCounterstain);
                            _gridViewModel.AdjustThumbnailVM.LastRenderTime = DateTime.Now;
                        }
                    }
                    finally
                    {
                        _gridViewModel.AdjustThumbnailVM.RenderTask = null;
                    }
                });
            }
        }

        /// <summary>
        /// Initiates the Adjust settings popup for the display of the selected cell
        /// </summary>
        /// <param name="scoredCell"></param>
        private void AdjustGridImage(ScoredCell scoredCell)
        {
            var frame = _gridViewModel.SharedVM.Frames.DefaultIfEmpty(null).FirstOrDefault();

            _gridViewModel.SelectCellRange(scoredCell, scoredCell);

            // create temp set of channel settings
            _gridViewModel.AdjustThumbnailVM.ThumbnailData = null;
            _gridViewModel.AdjustThumbnailVM.ChannelDisplays = (from c in _gridViewModel.ChannelDisplays
                                                                select new ChannelDisplayInfo(c.ChannelDisplay) { IsChecked = c.IsChecked }).ToList();
            //_gridViewModel.AdjustThumbnailVM.Thumbnail = scoredCell.Thumbnail;
            _gridViewModel.AdjustThumbnailVM.ThumbRenderer = new StackedThumbnailRenderer(frame.Image.Components);
            _gridViewModel.AdjustThumbnailVM.StackSizeMinusOne = _gridViewModel.SharedVM.Frames.Max(f => f.Image.ZLevelsMinusOne);

            // Get the thumbnail data
            Task.Factory.StartNew(() =>
            {
                _gridViewModel.AdjustThumbnailVM.ThumbnailData = _dataService.GetThumbnailData(_gridViewModel.SharedVM.SelectedSession, scoredCell.Score);
            }).ContinueWith(t =>
            {
                // update display in popup
                ChannelSettingsAdjusted(null);
            });

            // show channel adjust popup
            _gridViewModel.IsAdjustImagePopupOpen = true;

        }

        // TODO - Need to REVISIT here during the shared view model REFACTOR
        private void OnSelectedScoreOnFrameChange(ScoredCell scoredCell)
        {
            Parallel.ForEach(_gridViewModel.SharedVM.ScoredCells, (cell) =>
            {
                cell.Selected = false;
            });

            int newIndex = 0;
            if (scoredCell != null)
            {
                newIndex = _gridViewModel.PaginatedScores.Source.ToList().IndexOf(scoredCell);
            }

            if (!(newIndex > _gridViewModel.PaginatedScores.FirstVisibleItemIndex && newIndex < _gridViewModel.PaginatedScores.LastVisibleItemIndex))
            {
                _gridViewModel.PaginatedScores.FirstVisibleItemIndex = newIndex;
            }
            scoredCell.Selected = true;
        }

        private void NewScore(Score probeScore)
        {
            var frame = _gridViewModel.SharedVM.CurrentFrame;
            ScoredCell cell = CreateScoredCell(probeScore, frame.Image.Components);
            AddToAutoSaveCollection(cell);
            cell.IsNewlyCreated = true;
            _gridViewModel.ClearHighlightedandSelectedCells();
            cell.IsHighlighted = true;
            cell.Selected = true;
            _gridViewModel.SharedVM.ScoredCells.Add(cell);
            OnScoresUpdated(true);
            _newScoreThumbCreationTasks.Add(Task.Run(() => SaveThumbTemporary(), _autoSaveCancelToken.Token));

            _gridViewModel.SharedVM.IsDirty = true;
        }

        private void SaveThumbTemporary()
        {
            try
            {
                foreach (var score in _autoSaveCellCollection.GetConsumingEnumerable())
                {
                    ScoreThumbnail thumbInfo = new ScoreThumbnail()
                    {
                        AttenuateCounterstain = _gridViewModel.AttenuateCounterstain,
                        Channels = _gridViewModel.ChannelDisplays.Where(channel => channel.IsChecked).Select(channelDisplay => channelDisplay.ChannelDisplay),
                        Coordinates = new Point(score.X, score.Y),
                        FrameID = score.FrameId,
                        InvertCounterstain = _gridViewModel.InvertCounterstain,
                        RegionId = score.Score.RegionId,
                        ScoreId = score.Score.Id,
                        StackIndex = _gridViewModel.IsMaxProjection ? -1 : _gridViewModel.CurrentStackIndex
                    };
                    score.Thumbnail = _dataService.CreateThumbnailBitmap(thumbInfo, _gridViewModel.SharedVM.SelectedSession);
                    score.IsTemporarySaved = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void AddToAutoSaveCollection(ScoredCell cell)
        {
            Task.Run(() =>
            {
                _autoSaveCellCollection.Add(cell);
            });
        }

        private ScoreInfo GetScoreInfo(Score probeScore, FrameInfo frameInfo)
        {
            return new ScoreInfo()
            {
                Region = new PointCollection(),
                RegionId = Guid.Empty,
                X = probeScore.X,
                Y = probeScore.Y
            };
        }

        public byte[] LoadFrameFromData(string componentId)
        {
            return _dataService.GetFrameComponentData(_gridViewModel.SharedVM.SelectedSession.Slide, string.Empty, componentId);
        }

        /// <summary>
        /// Reclasssify cell based on user input if class name exists reclassify else create new class
        /// </summary>
        /// <param name="newClassName">Class Name</param>
        private void ReclassifyToOther(string newClassName)
        {
            if (newClassName != null)
            {
                if (newClassName.Length == 0)
                    return;

                ProbeScoringClass existingClass = _gridViewModel.SharedVM.CurrentAssay.Classes.Where(c => c.Text == newClassName).FirstOrDefault();
                if (existingClass != null)
                {
                    ReClassifySelectedCells(existingClass);
                }
                else
                {
                    var newClass = AddClassToAssay(newClassName);
                    if (newClass != null)
                    {
                        ReClassifySelectedCells(newClass);
                    }
                }
                newClassName = string.Empty;

                _gridViewModel.SharedVM.IsDirty = true;
            }
        }

        public ProbeScoringClass AddClassToAssay(string newClassName)
        {
            if (_gridViewModel.SharedVM.CurrentAssay == null || _gridViewModel.SharedVM.CurrentAssay.CountInformativeClasses() >= ProbeScoringAssay.MAXCLASSES)
                return null;

            System.Windows.Input.Key nextShortcut = _gridViewModel.SharedVM.CurrentAssay.NextAvailableShortcut(_gridViewModel.SharedVM.CurrentAssay);
            Color newColour = ProbeScoringClass.FixedColorForShortcutKey(nextShortcut);
            ProbeScoringClass newClass = new ProbeScoringClass { Color = newColour, Text = newClassName, ShortcutKey = nextShortcut };
            _gridViewModel.SharedVM.CurrentAssay.AddClass(newClass);
            //TODO open CVScanner
            //RelaunchCVScannerRequired = true;
            // TODO:
            // classSorter.RefreshFromClassList(assay.Classes);
            return newClass;
        }

        /// <summary>
        /// Rescore signal count and re classify based on user input
        /// </summary>
        /// <param name="cell">current cell signal count</param>
        private void RescoreSignalCount(ScoredCell cell)
        {
            foreach (var c in cell.Counts)
            {
                if (c.UserEditableScoreIsValid == false)
                {
                    return;
                }
            }
            ReclassifyCellBasedOnUserEdits(cell);
            cell.IsRescored = true;
            _gridViewModel.IsReclassifyPopupOpen = false;

            _gridViewModel.SharedVM.IsDirty = true;
        }

        /// <summary>
        /// Take the user-typed data in cell.Counts and feed it into the assay
        /// for reclassification. Uninformative is the fallback when the new counts
        /// don't match a class.
        /// </summary>
        public void ReclassifyCellBasedOnUserEdits(ScoredCell cell)
        {
            var components = _gridViewModel.SharedVM.Frames.FirstOrDefault().Image.Components.Where(fl => fl.IsCounterstain == false).ToArray();
            var classificationCounts = cell.Counts;
            if (classificationCounts == null)
            {
                return;
            }

            var changedCounts = cell.Counts.Where(c => c.HasUserEditInFlight).ToArray();
            // mark it as rescored
            if (changedCounts.Count() > 0)
            {
                if (!(_gridViewModel.ScoreCellSettingList.Exists(scoreSetting => scoreSetting.scoreid == cell.Score.Id && scoreSetting.IsRescored == true)))
                {
                    cell.IsRescored = true;
                    _gridViewModel.ScoreCellSettingList.Add(new ScoreSettings
                    {
                        scoreid = cell.Score.Id,
                        IsEnhanced = cell.HasEnhancements,
                        IsRescored = cell.IsRescored,
                        IsReviewed = cell.Reviewed
                    });
                }
            }

            foreach (var c in cell.Counts)
            {
                c.CommitUserEdit();
            }

            var newClass = _gridViewModel.SharedVM.CurrentAssay.Classify(classificationCounts.ToArray());
            if (newClass != null)
            {
                var spotClass = _gridViewModel.SharedVM.CurrentAssay.Classes.Where(c => c.Text == newClass.Name).FirstOrDefault();
                _gridViewModel.Reclassify(cell, spotClass);
            }
            else
            {
                var uninformative = _gridViewModel.SharedVM.CurrentAssay.Classes.Where(n => n.Text.Equals("Uninformative", StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (uninformative != null)
                {
                    _gridViewModel.Reclassify(cell, uninformative);
                }
            }

            // Update the ratio measurement
            if (_gridViewModel.SharedVM.CurrentAssay.HasRatioConstraints)
            {
                double newRatio = _gridViewModel.SharedVM.CurrentAssay.CaluclateRatio(cell.Counts);
                cell.SetSpotMeasurement("Ratio", newRatio.ToString(System.Globalization.CultureInfo.InvariantCulture));
            }

            foreach (var signalCount in cell.Counts)
            {
                cell.SetSpotMeasurement(signalCount.SignalInfo.SignalName, signalCount.Count.ToString(System.Globalization.CultureInfo.InvariantCulture));
            }
            _gridViewModel.GetFilteredClass();
        }

        /// <summary>
        /// Reclassify selected cells to probe scoring class
        /// </summary>
        /// <param name="classs"></param>
        private void ReClassifySelectedCells(ProbeScoringClass classs)
        {
            _gridViewModel.Reclassify(_gridViewModel.GetSelectedCells(), classs);
            _gridViewModel.IsReclassifyPopupOpen = false;
            _gridViewModel.SharedVM.IsDirty = true;
        }

        /// <summary>
        /// Return collection of selected cells
        /// </summary>
        /// <returns>selected cells</returns>

        private void GridScrollValueChanged(bool valueChanged)
        {
            if (valueChanged)
            {
                RenderAll();
            }
        }

        private void RenderAll(object unused = null)
        {
            RenderOverlays();
            RenderThumbnails();
        }

        // REFACTOR WH, can we abstract some this into the regionManagement stuff
        private void CloseSession(object unused)
        {
            MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-CloseSessionMessage"), Literal.Strings.Lookup("cbStr-CloseSessionTitle"), MessageBoxButton.OKCancel, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.OK:
                    _gridViewModel.SharedVM.IsDirty = false;
                    _autoSaveCellCollection.CompleteAdding();
                    _autoSaveCancelToken.Cancel();
                    var scores = _gridViewModel.SharedVM.ScoredCells.Where(s => s.IsNewlyCreated).Select(s => s.Score).ToList();
                    var regions = _gridViewModel.SharedVM.AllFrameRegions.Where(x => x.IsNewRegion == true).ToList();
                    UpdateScoreSettingList();
                    _dataService.RestoreSession(_gridViewModel.SharedVM.SelectedSession,_gridViewModel.ScoreCellSettingList, regions.Select(x => x.ID.ToString()));
                    //_dataService.ClearTemporarySavedThumb(_gridViewModel.SharedVM.SelectedSession);
                    ////delete the newly added  regions and scores
                    //var regions = _gridViewModel.SharedVM.AllFrameRegions.Where(x => x.IsNewRegion == true).ToList();
                    //if (regions != null && regions.Count > 0)
                    //{
                    //    _dataService.DeleteRegions(_gridViewModel.SharedVM.SelectedSession, regions.Select(x => x.ID.ToString()));
                    //}
                    //_dataService.RestoreAssayData(_gridViewModel.SharedVM.SelectedSession);
                    CloseProbeCaseViewWindow();
                    break;
            }
        }

        private void HandleVersions()
        {
            // we need to do some extra assignments to handle the fact that data isn't stored sensible and
            // there are different versions of data out there
            if (_gridViewModel.SharedVM.Frames != null)
            {
                var fromFrame = (from channel in _gridViewModel.SharedVM.CurrentFrame.Image.Components
                                 select new ChannelDisplay
                                 {
                                     Name = channel.Name,
                                     IsVisible = true,
                                     HistogramLow = 0,
                                     HistogramHigh = 1,
                                     ColourString = channel.RenderColorString,
                                     IsCounterstain = channel.IsCounterstain
                                 }).ToArray();

                // However, the user can override some display settings, histogram, gamma etc in the session, so patch this together
                foreach (var channel in fromFrame)
                {
                    var fromSession = _gridViewModel.SharedVM.SessionScores.ChannelDisplays.Select(a => a).Where(a => a.Name == channel.Name).FirstOrDefault();
                    if (fromSession != null)
                    {
                        channel.HistogramHigh = fromSession.HistogramHigh;
                        channel.HistogramLow = fromSession.HistogramLow;
                        channel.Gamma = fromSession.Gamma;
                    }
                }

                _gridViewModel.SharedVM.SessionScores.ChannelDisplays = fromFrame.ToList();
                // TODO need this here as we patch the data up after the view model is created; REFACTOR
                _gridViewModel.ChannelDisplays = new HashSet<ChannelDisplayInfo>(from channelDisplay in _gridViewModel.SharedVM.SessionScores.ChannelDisplays select new ChannelDisplayInfo(channelDisplay) { IsChecked = true });
            }
        }

        private ScoredCell CreateScoredCell(Score score, IEnumerable<FrameComponent> components)
        {
            var thumbnailRenderer = new StackedThumbnailRenderer(components);
            return new ScoredCell(score, thumbnailRenderer);
        }

        private void GetSortingOptions(GridViewModel sVM)
        {
            if (sVM.allOtherSorters == null)
            {
                sVM.allOtherSorters = new ScoreSorter[0];
            }
            else
            {
                sVM.ClearSorterAndDelegateReferences();
            }
            List<ScoreSorter> sorters = new List<ScoreSorter>();
            //Class and Frame sorter
            ClassSorter classSorter = new ClassSorter(sVM.SharedVM.SessionSummary.Classes);
            FrameSorter frameSorter = new FrameSorter(sVM.FindFrameNumber);
            sVM.allOtherSorters = new ScoreSorter[] { classSorter, frameSorter };
            //signal sorter
            if (sVM.SharedVM.CurrentFrame != null && _gridViewModel.SharedVM.ScoredCells != null && _gridViewModel.SharedVM.ScoredCells.Count > 0)
            {
                sVM.signalSorters = (from signal in sVM.SharedVM.Signals select new SignalCountSorter(signal.SignalName)).ToArray();
            }
            else
            {
                sVM.signalSorters = new ScoreSorter[0];
            }

            // TODO - REVISIT - QuickFix for MAnualScoring Crash, Clearing PrimaryScoreSorters in GetSortingOptions().
            // because GetSortingOptions is adding multiple ClassSorters to PrimaryScoreSorters each time when we add a new score. 
            // REFACTOR           
            sVM.PrimaryScoreSorters.Clear();

            foreach (ScoreSorter scoreSorter in sVM.allOtherSorters)
            { sVM.PrimaryScoreSorters.Add(scoreSorter); }

            foreach (SignalCountSorter signalsorter in sVM.signalSorters)
            {
                sVM.PrimaryScoreSorters.Add(signalsorter); ;
            }
            //Set Default Primary sorter
            sVM.SelectedPrimarySorter = sVM.PrimaryScoreSorters.FirstOrDefault();
            sVM.SelectedSecondarySorter = sVM.SecondaryScoreSorters.FirstOrDefault();
            sVM.SelectedPrimarySorter.ConflictSorter = sVM.SelectedSecondarySorter;
        }

        private void OnCreateNewSession(ScanAreaViewModel ScanAreaViewModel)
        {
            try
            {
                ScanAreaViewModel scanAreaViewModel = ScanAreaViewModel as ScanAreaViewModel;
                if (scanAreaViewModel.SelectedScanArea != null)
                {
                    var newSession = _dataService.CreateNewSession(scanAreaViewModel.SelectedScanArea, scanAreaViewModel.NewSessionName, scanAreaViewModel.UseSpotScores);
                    if (newSession != null)
                    {
                        SessionViewModel sessionviewModel = new SessionViewModel(newSession, _eventAggregator);
                        scanAreaViewModel.SessionViewModelCollection.Add(sessionviewModel);
                    }
                }

            }
            catch (DALException createNewSessionError)
            {
                LogManager.Error("Create new Session Error: ", createNewSessionError);
                CustomPopUp.Show(Literal.Strings.Lookup("cbStr-CreateNewSessionExceptionMessage"), Literal.Strings.Lookup("cbStr-CreateNewSessionTitle"), MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        public void ParseMeasurements(GridViewModel sVM)
        {
            if (_gridViewModel.SharedVM.Frames != null && _gridViewModel.SharedVM.ScoredCells != null && _gridViewModel.SharedVM.ScoredCells.Count > 0)
            {
                // Get a SignalInfo for each overlay, except that corresponding with the counterstain channel.
                // Note that there are overlays not just for image channels but also for things like fusions (if fusion analysis has been done).
                var frame = _gridViewModel.SharedVM.Frames.DefaultIfEmpty(null).FirstOrDefault();
                List<SignalInfo> signals = new List<SignalInfo>();
                foreach (OverlayInfo overlay in sVM.Overlays)
                {
                    foreach (FrameComponent frameComponent in frame.Image.Components)
                    {
                        if (frameComponent.Name == overlay.Name && !frameComponent.IsCounterstain)
                        {
                            signals.Add(new SignalInfo() { SignalName = overlay.Name, Color = frameComponent.RenderColor });
                        }
                    }

                    if (overlay.Name.ToUpper() == "FUSIONS")
                    {
                        signals.Add(new SignalInfo() { SignalName = overlay.Name, Color = ColorParse.HexStringToColor(overlay.RenderColor) });
                    }
                }

                sVM.SharedVM.Signals = signals;
                Parallel.ForEach(_gridViewModel.SharedVM.ScoredCells, (score) =>
                {
                    score.ParseMeasurements(sVM.SharedVM.Signals);
                });
            }
        }

        //private bool CheckProbeCaseWorkspaceViewExists()
        //{
        //    bool viewExists = false;
        //    IList<object> Views = new List<object>(_regionManager.Regions[RegionNames.MainRegion].Views);
        //    foreach (var view in Views)
        //    {
        //        if (view is ProbeCaseWorkspaceView) //*TODO
        //        {
        //            viewExists = true;
        //            return viewExists;
        //        }
        //    }
        //    return viewExists;
        //}

        //private void ActivateScoresView()
        //{
        //    if (_regionManager.Regions[RegionNames.MainRegion] != null)
        //    {
        //        List<object> views = new List<object>(_regionManager.Regions[RegionNames.MainRegion].Views);
        //        foreach (var view in views)
        //        {
        //            if (view is ProbeCaseWorkspaceView) //*TODO
        //            {
        //                _regionManager.Regions[RegionNames.MainRegion].Activate(view);
        //            }
        //        }
        //    }
        //}

        //private void ClearViews()
        //{
        //    if (_regionManager.Regions[PcvRegionNames.ScoresViewRegion] != null)
        //    {
        //        List<object> views = new List<object>(_regionManager.Regions[PcvRegionNames.ScoresViewRegion].Views);
        //        foreach (var view in views)
        //        {
        //            _regionManager.Regions[PcvRegionNames.ScoresViewRegion].Remove(view);
        //        }
        //    }
        //}

        //*TODO  - Revisit for Performance and Synchronization 
        private void RenderThumbnails(object unused = null)
        {
            if (_gridViewModel == null)
                return;

            // Cancel render, may have to check Cancel to make more granular
            if (_thumbnailCancelToken != null && _dataService != null)
            {
                _thumbnailCancelToken.Cancel();
            }
            _thumbnailCancelToken = new CancellationTokenSource();

            int stackIndex = _gridViewModel.IsMaxProjection ? -1 : _gridViewModel.CurrentStackIndex;
            var visible = _gridViewModel.ChannelDisplays.Where(channel => channel.IsChecked).Select(channelDisplay => channelDisplay.ChannelDisplay);
            Task.Factory.StartNew(() =>
            {
                Parallel.ForEach(_gridViewModel.PaginatedScores.CurrentPage, (cell) =>
                {
                    // Get the raw thumbnail data for selected score
                    if (cell.RenderTime < _gridViewModel.InvalidateTime)
                    {
                        //var thumbnailData = _dataService.GetThumbnailData(_gridViewModel.SelectedSession, cell.Score);
                        //cell.Render(thumbnailData, stackIndex, visible, _gridViewModel.AttenuateCounterstain, _gridViewModel.InvertCounterstain);
                        cell.Thumbnail = _dataService.GetThumbnailBitmap(_gridViewModel.SharedVM.SelectedSession, cell.Score, stackIndex, visible,
                                                                     _gridViewModel.AttenuateCounterstain, _gridViewModel.InvertCounterstain, cell.IsTemporarySaved);
                    }
                });
            }, _thumbnailCancelToken.Token, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);
        }

        //*TODO  - Revisit for Performance and Synchronization 
        private void RenderOverlays(object unused = null)
        {
            var dispatcher = Dispatcher.CurrentDispatcher;
            Task.Factory.StartNew(() =>
            {
                Parallel.ForEach(_gridViewModel.PaginatedScores.CurrentPage, (scoredCell) =>
                {
                    var olist = new List<BitmapSource>();
                    foreach (var overlayInfo in _gridViewModel.Overlays.Where(overlay => overlay.IsChecked == true).ToList())
                    {
                        var thumbnailOverlayData = _dataService.GetOverlayData(_gridViewModel.SharedVM.SelectedSession, scoredCell.Score, overlayInfo.Overlay);

                        if (null != thumbnailOverlayData)
                            olist.Add(scoredCell.RenderOverlay(thumbnailOverlayData, Colors.Black));
                    }

                    // TODO change overlay render to be less WPF, render into a buffer, Blocks on UI thread,also need to create any UI elements on the UI thread
                    dispatcher.BeginInvoke((Action)delegate
                {
                    DrawingGroup group = new DrawingGroup();
                    foreach (var bm in olist)
                    {
                        ImageDrawing image = new ImageDrawing(bm, new Rect(0, 0, bm.Width, bm.Height));
                        group.Children.Add(image);
                    }
                    scoredCell.ThumbnailOverlay = new DrawingImage(group);
                }, DispatcherPriority.Background);
                });
            }, CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);
        }

        /// <summary>
        /// Saves session
        /// </summary>
        /// <param name="selectedSession"></param>
        private void SaveAllData(Session selectedSession)
        {
            // Before Saving, Stop adding new scores to the _autoSaveCellCollection.
            // Wait till all the ThumbnailCreation task completes.( The NewScore method will be spooling new tasks and run SaveThumbTemporary() )        
            _autoSaveCellCollection.CompleteAdding();
            Task.WaitAll(_newScoreThumbCreationTasks.ToArray());

            Task<bool> saveAnnotation = Task<bool>.Factory.StartNew(() =>
            {
                var annotations = _gridViewModel.SharedVM.AllFrameAnnotations.Where(x => x.Annotations.Count() > 0).ToList();
                List<AnnotationIdentifier> annotationList = new List<AnnotationIdentifier>();
                if (annotations != null && annotations.Count > 0)
                {
                    foreach (var frannotation in annotations)
                    {
                        annotationList.Add(new AnnotationIdentifier() { Identifier = frannotation.FrameId, Annotations = frannotation.Annotations.Where(s => !s.IsDeleted).Select(x => x.Annotation).ToList() });
                    }
                }
                return _dataService.SaveAnnotation(selectedSession, annotationList);
            });
            Task.WaitAll(saveAnnotation);
            _dataService.SaveCommonStoredAnnotation(_gridViewModel.SharedVM.CommonAnnotations.ToList());
            SessionScores scores = new SessionScores()
            {
                Scores = _gridViewModel.SharedVM.ScoredCells.Where(s => !s.IsDeleted).Select(s => s.Score).ToList(),
                ChannelDisplays = _gridViewModel.ChannelDisplays.Select(channel => channel.ChannelDisplay).ToList(),
                Overlays = _gridViewModel.Overlays.Select(overlayInfo => overlayInfo.Overlay).ToList(),
            };
            UpdateScoreSettingList();
            Task.Factory.StartNew(() =>
            {
                _dataService.SaveSession(selectedSession, _gridViewModel.ScoreCellSettingList, scores);
            });

            //delete the selected regions which is marked as delete
            var regions = _gridViewModel.SharedVM.AllFrameRegions.Where(x => x.CanDelete == true).ToList();
            if (regions != null && regions.Count > 0)
            {
                _dataService.DeleteRegions(selectedSession, regions.Select(x => x.ID.ToString()));
            }
        }

        private void SaveAllDataOnClose(Session selectedSession)
        {
            SaveAllData(selectedSession);
            CloseProbeCaseViewWindow();
        }

        private void UpdateScoreSettingList()
        {
            foreach (var cell in _gridViewModel.SharedVM.ScoredCells)
            {
                if (cell.HasEnhancements || cell.IsRescored || cell.Reviewed || cell.IsDeleted || cell.IsNewlyCreated)
                {
                    if (_gridViewModel.ScoreCellSettingList.Exists(scoreSetting => scoreSetting.scoreid == cell.Score.Id))
                    {
                        _gridViewModel.ScoreCellSettingList.Where(scoreSetting => scoreSetting.scoreid == cell.Score.Id).ForEach(scoreSetting =>
                        {
                            scoreSetting.scoreid = cell.Score.Id;
                            scoreSetting.IsReviewed = cell.Reviewed;
                            scoreSetting.IsRescored = cell.IsRescored;
                            scoreSetting.IsEnhanced = cell.HasEnhancements;
                            scoreSetting.IsDeleted = cell.IsDeleted;
                            scoreSetting.IsNewlyCreated = cell.IsNewlyCreated;                   
                        });
                    }
                    else
                    {
                        _gridViewModel.ScoreCellSettingList.Add(new ScoreSettings()
                        {
                            scoreid = cell.Score.Id,
                            IsReviewed = cell.Reviewed,
                            IsRescored = cell.IsRescored,
                            IsEnhanced = cell.HasEnhancements,
                            IsDeleted = cell.IsDeleted,
                            IsNewlyCreated = cell.IsNewlyCreated
                    });
                    }
                }
            }
        }

        private void CloseProbeCaseViewWindow()
        {
            // Set the changed assay name to Session AssayName property.
            //_gridViewModel.SharedVM.SelectedSession.AssayName = _gridViewModel.SharedVM.CurrentAssay.Name;
            ////Refactor sharedviewmodel add SessionSummary as shared property between GridView and Frame
            ////Below code block will update opened session pie chart on Ok button click
            //Dictionary<Session, SessionSummary> parameter = new Dictionary<Session, SessionSummary>();
            //parameter.Add(_gridViewModel.SharedVM.SelectedSession, _gridViewModel.SharedVM.SessionSummary);
            ////Call Activated event on Scan Area to update changes in Scan Area
            //_eventAggregator.GetEvent<ActivatedEvent>().Publish(parameter);

            //IList<object> Views = new List<object>(_regionManager.Regions[RegionNames.MainRegion].Views);
            //foreach (var view in Views)
            //{
            //    if (view is ProbeCaseWorkspaceView)
            //    {
            //        _regionManager.Regions[RegionNames.MainRegion].Deactivate(view);
            //    }
            //}
            _dataService.RemoveLock(LoginHelper.CurrentUser, _gridViewModel.SharedVM.SelectedSession.Slide.CaseId);
            _regionManager.Regions[RegionNames.MainRegion].Deactivate(_gridView);
            _regionManager.RequestNavigate(RegionNames.MainRegion, "CaseBrowserWorkspaceView");          
            _eventAggregator.GetEvent<ClearSessionEvent>().Publish(null);
        }

        private void OnScoresUpdated(bool unUsed)
        {            
            _gridViewModel.GetFilteredClass();
            GetSortingOptions(_gridViewModel);
            //_gridViewModel.UpdateScoreOrder();
        }


        private void UnSubscribeEvents()
        {
            _eventAggregator.GetEvent<CloseSessionEvent>().Unsubscribe(CloseSession);
            _eventAggregator.GetEvent<RenderOverlaysEvent>().Unsubscribe(RenderOverlays);
            _eventAggregator.GetEvent<ChannelDisplayEvent>().Unsubscribe(RenderThumbnails);
            _eventAggregator.GetEvent<RefreshGridDisplayEvent>().Unsubscribe(RenderThumbnails);
            _eventAggregator.GetEvent<ApplyImageSettingsToGridEvent>().Unsubscribe(OnApplyImageSettingsToGrid);
            _eventAggregator.GetEvent<GridResizeEvent>().Unsubscribe(RenderAll);
            //_eventAggregator.GetEvent<ReviewScoredCellEvent>().Unsubscribe(ReviewScoredCells);
            _eventAggregator.GetEvent<GridScrollValueChangeEvent>().Unsubscribe(GridScrollValueChanged);
            _eventAggregator.GetEvent<CurrentChannelDisplayEvent>().Unsubscribe(ChannelSettingsAdjusted);
            _eventAggregator.GetEvent<UpdateScoredCellEvent>().Unsubscribe(RenderThumbnails);
            _eventAggregator.GetEvent<ReclassifyEvent>().Unsubscribe(ReClassifySelectedCells);
            _eventAggregator.GetEvent<ReScoreEvent>().Unsubscribe(RescoreSignalCount);
            _eventAggregator.GetEvent<ReClassifyUsingNewClassEvent>().Unsubscribe(ReclassifyToOther);
            _eventAggregator.GetEvent<SaveAllDataEvent>().Unsubscribe(SaveAllDataOnClose);
            _eventAggregator.GetEvent<ScoresUpdatedEvent>().Unsubscribe(OnScoresUpdated);
            _eventAggregator.GetEvent<NewScoreEvent>().Unsubscribe(NewScore);
            _eventAggregator.GetEvent<AdjustGridImageEvent>().Unsubscribe(AdjustGridImage);
            _eventAggregator.GetEvent<SelectedFrameScoreEvent>().Unsubscribe(OnSelectedScoreOnFrameChange);
            //_eventAggregator.GetEvent<MouseScrollEvent>().Unsubscribe(GridScroll);
            //_eventAggregator.GetEvent<GridViewKeyEvent>().Unsubscribe(OnKeyDown);
            _eventAggregator.GetEvent<AddCellsEvent>().Unsubscribe(AddCells);
            _eventAggregator.GetEvent<NotifyGridViewPresenterEvent>().Unsubscribe(OnUpdateFromAssay);           
        }


        public void Dispose()
        {
            UnSubscribeEvents();
            _gridView.DataContext = null;
            _gridView = null;
            _gridViewModel.Dispose();
            _gridViewModel = null;
           if(_thumbnailCancelToken!=null)
            {
                _thumbnailCancelToken.Cancel();
            }
            _thumbnailCancelToken = null;
            _autoSaveCellCollection = null;
            _autoSaveCancelToken = null;
            _newScoreThumbCreationTasks = null;
            _eventAggregator = null;
            _dataService = null;
            _dualReaderFramesGalleryPresenter = null;
        }

        /// <summary>
        /// Dual reader feature - Navigates to Frame Gallery after confirmation, Add/Remove cells in existing Session
        /// </summary>
        /// <param name="session"></param>
        private void AddCells(Session session)
        {
            MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-SaveChanges"), Literal.Strings.Lookup("cbStr-AddCellsTitle"), MessageBoxButton.YesNoCancel, MessageBoxImage.Information, true);

            if (result == MessageBoxResult.Cancel)
            {
                return;
            }
            if (result == MessageBoxResult.Yes)
            {
                SaveAllData(session);
            }           
            if (_dualReaderFramesGalleryPresenter != null) _dualReaderFramesGalleryPresenter.DisplayFrameGallery(_gridViewModel.SharedVM.SelectedSession);
        }
    }
}
