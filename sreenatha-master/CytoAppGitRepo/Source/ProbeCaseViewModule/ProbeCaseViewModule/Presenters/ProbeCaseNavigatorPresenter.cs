﻿using CytoApps.Exceptions;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Infrastructure.UI.Events;
using CytoApps.Localization;
using CytoApps.Models;
using CytoApps.SpotConfiguration;
using Prism.Events;
using Prism.Regions;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Models;
using ProbeCaseViewModule.Services;
using ProbeCaseViewModule.ViewModels;
using ProbeCaseViewModule.Views;
using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace ProbeCaseViewModule.Presenters
{
    [Export]
    class ProbeCaseNavigatorPresenter
    {
        private IEventAggregator _eventAggregator;
        private IDataService _dataService;
        private IRegionManager _regionManager;

        [ImportingConstructor]
        public ProbeCaseNavigatorPresenter(IEventAggregator eventAggregator, IRegionManager regionManager, IDataService dataService)
        {
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _dataService = dataService;
            SubscribeEvents();
        }

        private void SubscribeEvents()
        {
            _eventAggregator.GetEvent<SelectedCaseSlidesEvent>().Subscribe(OnSlideLoad);
            _eventAggregator.GetEvent<PieChartChangeEvent>().Subscribe(OnPieChartChange);
        }

        private void OnPieChartChange(SlideViewModel slide)
        {
            if (slide != null)
            {
                foreach (var scanarea in slide.ScanAreaViewModelCollection)
                {
                    foreach (var session in scanarea.SessionViewModelCollection)
                    {
                        Task.Factory.StartNew(() =>
                        {
                            var summary = GetCurrentSessionSummary(session.Session);

                            session.SessionSummary = summary;
                        });
                    }
                    scanarea.IsAssaysEqual = scanarea.SessionViewModelCollection.Select(x => x.Session.AssayName).Distinct().Count() < 2;
                }
            }
        }

        /// <summary>
        /// Load Case Data of selected Case i.e. Slide, Scan Area and Session 
        /// </summary>
        /// <param name="slideCollection"></param>
        private void OnSlideLoad(CaseDataProxy plugginInfo)
        {
            try
            {
                if (plugginInfo.Slide != null)
                {
                    var slide = plugginInfo.Slide;

                    // If its not a PCV slide, return now, nothng we can do with it
                    if ((slide.KnownSlideTypes & (UInt32)KnownSlides.ProbeCaseViewSlide) == 0)
                        return;

                    _dataService.CreateDatasource(plugginInfo.DataSource);

                    var dispatcher = Dispatcher.CurrentDispatcher;

                    ProbeCaseNavigationViewModel probeCaseNavigationViewModel = new ProbeCaseNavigationViewModel(_eventAggregator);
                    probeCaseNavigationViewModel.SlideViewModel = new SlideViewModel(slide);
                    PopulateNavigationRegions(probeCaseNavigationViewModel, plugginInfo.PluginRegionName);

                    Task.Factory.StartNew(() =>
                    {
                        var scanAreas = _dataService.GetScanAreas(slide);
                        foreach (var scanArea in scanAreas)
                        {
                            ScanAreaViewModel scanAreaViewModel = new ScanAreaViewModel(scanArea, _eventAggregator);
                            dispatcher.BeginInvoke((Action)delegate
                            {
                                probeCaseNavigationViewModel.SlideViewModel.ScanAreaViewModelCollection.Add(scanAreaViewModel);
                            }, DispatcherPriority.Normal);

                            foreach (var session in scanAreaViewModel.SelectedScanArea.Sessions)
                            {
                                SessionViewModel sessionviewModel = new SessionViewModel(session, _eventAggregator);

                                // TODO - REFATOR - by default the Piechart will not be displayed. On Checkbox Checked PieChartChangeEvent will be published
                                // and handled in presenter method OnPieChartChange()
                                // var summary = GetCurrentSessionSummary(session);
                                // sessionviewModel.SessionSummary = summary;

                                scanAreaViewModel.SessionViewModelCollection.Add(sessionviewModel);
                            }
                        }
                    });
                }
            }

            // HACK - temp to handle that I haven't written a remote DAL at the momemt
            // issue is that if this throws an exception the Navi UI is broken for other modules
            // so probably have to handle exceptions in here
            catch (Exception ex)
            {
                LogManager.Debug("PCV: probably haven't written the DAL yet : " + ex.Message);
                return;
            }
        }

        /// <summary>
        /// Inject PCV navigation views to plugin region of respective slide
        /// </summary>
        /// <param name="_pcvNavigationViewCollection">PCV Navigation view</param>
        /// <param name="pluginRegionName">plugin region name</param>
        private void PopulateNavigationRegions(ProbeCaseNavigationViewModel pcvNavigationViewModeln, string pluginRegionName)
        {
            ProbeCaseNavigationView probeCaseNavigationView = new ProbeCaseNavigationView();

            //Add view to plugin region
            probeCaseNavigationView.DataContext = pcvNavigationViewModeln;
            _regionManager.AddToRegion(pluginRegionName, probeCaseNavigationView);
        }

        /// <summary>
        /// Returns the class and signal information of the selected session
        /// </summary>
        /// <param name="session">session</param>
        /// <returns>Returns the Session Summary</returns>
        private SessionSummary GetCurrentSessionSummary(ProbeCaseViewDataAccess.Models.Session session)
        {
            ProbeCaseViewDataAccess.Models.Score[] scores = _dataService.GetScoreStats(session);
            SessionSummary sessionSummary = new SessionSummary();
            if (scores != null && scores.Length > 0)
            {
                sessionSummary.TotalInformativeCell = scores.Count((cell => cell.Class.ToLower() != Constants.Uninformative));
                var scoredCellsbyGroup = scores.GroupBy(x => x.Class).ToList();
                var scoredClasses = (from scoredCellClass in scoredCellsbyGroup
                                     select new ClassFilter
                                     {
                                         ClassName = scoredCellClass.Key,
                                         ClassCount = scoredCellClass.Count(),
                                         IsChecked = true,
                                         ClassPercentage = (scoredCellClass.Count() / (double)sessionSummary.TotalInformativeCell) * 100,
                                         Color = scoredCellClass.Select(cell => cell.ColourString).FirstOrDefault()
                                     }).ToList();
                if (scoredClasses != null)
                {
                    sessionSummary.Classes = scoredClasses.OrderByDescending(classFilter => classFilter.ClassName).Where(x => x.ClassName.ToLower() != Constants.Uninformative).ToObservableCollection();
                }
                ScoreStats scoreStats = null;
                var assayData = _dataService.GetAssayData(session);
                if (null != assayData)
                {
                    //var hackyPathToAiishared = Path.Combine(@"\\localhost\Casebase\cases\", @"..\aii_shared");
                    AssayInfo _assayinfo = _dataService.GetClassifierNamesAndScripts(null);
                    if (_assayinfo != null)
                    {
                        var spotController = SpotConfigController.FromStream(assayData, _assayinfo);

                        var assay = new ProbeScoringAssay(spotController);
                        var signals = (from p in assay.SpotConfigController.ProbeList select new SignalInfo() { SignalName = p.Name }).ToList();
                        signals.AddRange(from p in assay.SpotConfigController.FusionList select new SignalInfo() { SignalName = Constants.Fusions });
                        var scoredCells = (from s in scores select new ScoredCell(s)).ToArray();
                        Parallel.ForEach(scoredCells, (s) =>
                        {
                            s.ParseMeasurements(signals);
                        });
                        scoreStats = new ScoreStats();
                        scoreStats.CalculateStats(assay, scoredCells);
                    }
                }
                sessionSummary.ScoreStats = scoreStats;
            }
            return sessionSummary;
        }

        private void DeleteSession(ScanAreaViewModel scanAreaViewModel)
        {
            try
            {
                MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-DeleteSessionMessage"), Literal.Strings.Lookup("cbStr-DeleteSessionTitle"), MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        WaitCursor.Show();
                        _dataService.DeleteSession(scanAreaViewModel.SessionViewModel.Session);
                        scanAreaViewModel.SelectedScanArea.Sessions.Remove(scanAreaViewModel.SessionViewModel.Session);
                        scanAreaViewModel.SessionViewModelCollection.Remove(scanAreaViewModel.SessionViewModel);
                        scanAreaViewModel.SessionViewModel = null;
                        break;
                }
            }
            catch (DALException deleteSessionException)
            {
                LogManager.Error("Delete Session Error: ", deleteSessionException);
                CustomPopUp.Show(Literal.Strings.Lookup("cbStr-DeleteSessionExceptionMessage"), Literal.Strings.Lookup("cbStr-DeleteSessionExceptionTitle"), MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
