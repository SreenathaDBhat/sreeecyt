﻿using CytoApps.Exceptions;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Localization;
using CytoApps.SpotConfiguration;
using PCVImageRenderers.Models;
using Prism.Events;
using Prism.Regions;
using ProbeCaseViewDataAccess.Models;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Models;
using ProbeCaseViewModule.Services;
using ProbeCaseViewModule.ViewModels;
using ProbeCaseViewModule.Views;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ProbeCaseViewModule.Presenters
{
    /// <summary>
    /// This Presenter is used for plugin the pcv Module to Case Browser NavigationView. This is the entry point of PCV Module
    /// </summary>
    [Export]
    class ProbeCaseViewPresenter
    {
        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        private IDataService _dataService;
        private ScanArea _currentScanArea;

        private SharedViewModel _sharedVM;
        private GridViewPresenter _gridViewPresenter;
        private FrameWindowPresenter _frameWindowPresenter;
        private DualReaderFramesGalleryPresenter _dualReaderFramesGalleryPresenter;

        [ImportingConstructor]
        public ProbeCaseViewPresenter(IEventAggregator eventAggregator, IDataService dataService, IRegionManager regionManager)
        {
            _eventAggregator = eventAggregator;
            _dataService = dataService;
            _regionManager = regionManager;

            _eventAggregator.GetEvent<OpenSessionEvent>().Subscribe(OnOpenSession);
            _eventAggregator.GetEvent<CreateSessionEvent>().Subscribe(OnCreateNewSession);
            _eventAggregator.GetEvent<DeleteSessionEvent>().Subscribe(DeleteSession);
            _eventAggregator.GetEvent<CreateSessionWithSelectedFramesEvent>().Subscribe(OnCreateNewSessionWithSelectedFrames);
        }

        private void OnCreateNewSessionWithSelectedFrames(ScanAreaViewModel scanAreaViewModel)
        {
            WaitCursor.Show();
            if (_dataService.CanCreateNewSession(scanAreaViewModel.NewSessionName, scanAreaViewModel.SelectedScanArea.Slide, scanAreaViewModel.SelectedScanArea.Id))
            {
                if (_dualReaderFramesGalleryPresenter == null)
                {
                    _dualReaderFramesGalleryPresenter = new DualReaderFramesGalleryPresenter();
                }
                _dualReaderFramesGalleryPresenter.Show(scanAreaViewModel);
            }
            else
            {
                MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-SessionCreationFailedMessage"), Literal.Strings.Lookup("cbStr-SessionCreationFailedTitle"), MessageBoxButton.OK, MessageBoxImage.Error, true);
                if (result == MessageBoxResult.OK)
                {
                    _eventAggregator.GetEvent<CytoApps.Infrastructure.UI.Events.RefreshSelectedCaseEvent>().Publish(null);
                }
            }
        }

        private void ClearSessionData(object obj)
        {

            _frameWindowPresenter.Dispose();
            _gridViewPresenter.Dispose();
            _dualReaderFramesGalleryPresenter.Dispose();
            _frameWindowPresenter = null;
            _sharedVM = null;
            _gridViewPresenter = null;
            _dualReaderFramesGalleryPresenter = null;
            _eventAggregator.GetEvent<ClearSessionEvent>().Unsubscribe(ClearSessionData);

        }

        private void OnOpenSession(Session session)
        {
            if (null != session)
            {
                _eventAggregator.GetEvent<ClearSessionEvent>().Unsubscribe(ClearSessionData);
                _eventAggregator.GetEvent<ClearSessionEvent>().Subscribe(ClearSessionData);

                var lockResult = _dataService.CheckKLockExists(LoginHelper.CurrentUser, session.Slide.CaseId, session);
                if (!lockResult.IsLocked)
                {
                    _dataService.LockFeatureorObject(LoginHelper.CurrentUser, session.Slide.CaseId, session);
                    SessionScores sessionScores = null;
                    IEnumerable<Frame> sessionFrames = null;
                    List<ScoreSettings> scoreCellSettingList = null;
                    ProbeScoringAssay currentAssay = null;
                    AssayInfo assayinfo = null;
                    byte[] assayData = null;
                    List<Task> asyncTasks = new List<Task>();
                    List<string> annotationTexts = null;
                    _currentScanArea = new ScanArea(session.Slide, session.ScanAreaId, session.ScanAreaId);

                    asyncTasks.Add(Task.Run(() => sessionScores = _dataService.GetScores(session)));
                    asyncTasks.Add(Task.Run(() => sessionFrames = _dataService.GetFrames(_currentScanArea)));
                    asyncTasks.Add(Task.Run(() => scoreCellSettingList = _dataService.LoadScoredCellsInfo(session, _currentScanArea.Name)));
                    asyncTasks.Add(Task.Run(() => assayData = _dataService.GetAssayDataAndCreateBackup(session)));
                    asyncTasks.Add(Task.Run(() => assayinfo = _dataService.GetClassifierNamesAndScripts(session)));
                    asyncTasks.Add(Task.Run(() => annotationTexts = _dataService.GetStoredCommonAnnotations()));

                    Task.WaitAll(asyncTasks.ToArray());

                    if (null != assayData)
                    {
                        if (assayinfo != null)
                        {
                            var spotController = SpotConfigController.FromStream(assayData, assayinfo);
                            currentAssay = new ProbeScoringAssay(spotController);
                        }
                    }
                    else
                    {
                        currentAssay = GetAssayInferFromScores(sessionScores, assayinfo != null ? assayinfo.DefaultAssayName : string.Empty);
                    }

                    //Dual Reader - Fill VM with selected frames if sesion is created with selected frames
                    if (session.IsCreatedWithSelectedFrames)
                    {
                        var frameIds = _dataService.GetSelectedFramesForSession(session);
                        //Need Review ------------- If this DAL line adds lag to web request!!!!
                        //Need to discuss!!! we are getting frames from data service without checking whether session is created using selected frames
                        //we can avoid one serice call by filtering frames at data access level
                        sessionFrames = sessionFrames.Where(x => frameIds.Contains(x.Id.ToString())).ToObservableCollection();
                    }
                    // Set AssayName, When Scan area is already loaded and New Session.AssayName is null.
                    session.AssayName = currentAssay.Name;
                    _sharedVM = new SharedViewModel(sessionScores, currentAssay, sessionFrames, scoreCellSettingList);
                    _sharedVM.CommonAnnotations = annotationTexts.ToObservableCollection();
                    if (_gridViewPresenter == null) _gridViewPresenter = new GridViewPresenter();
                    if (_dualReaderFramesGalleryPresenter == null) _dualReaderFramesGalleryPresenter = new DualReaderFramesGalleryPresenter(); ;
                    _gridViewPresenter.InitializeGridViewModel(_sharedVM, _dualReaderFramesGalleryPresenter);
                    if (_frameWindowPresenter == null) _frameWindowPresenter = new FrameWindowPresenter();
                    _frameWindowPresenter.InitializeFrameViewModel(_sharedVM);
                }
                else
                {
                    switch (lockResult.LockType)
                    {
                        case CytoApps.Models.LockType.ObjectLock:
                            MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-SessionLockedSubstr1") + session.Name + Literal.Strings.Lookup("cbStr-SessionLockedSubstr2") + lockResult.User.Name + "(" + lockResult.User.MachineName + ").", Literal.Strings.Lookup("cbStr-SessionLockedTitle"), MessageBoxButton.OK, MessageBoxImage.Error);
                            break;
                        case CytoApps.Models.LockType.FileNotFound:
                            MessageBoxResult result1 = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-RefreshCase"), Literal.Strings.Lookup("cbStr-SessionNotFound"), MessageBoxButton.OK, MessageBoxImage.Error, true);
                            if (result1 == MessageBoxResult.OK)
                            {
                                _eventAggregator.GetEvent<CytoApps.Infrastructure.UI.Events.RefreshSelectedCaseEvent>().Publish(null);
                            }
                            break;

                    }
                }
            }
        }

        private void OpenGridView()
        {
            _regionManager.AddToRegion(RegionNames.MainRegion, _gridViewPresenter._gridView);
            _regionManager.Regions[RegionNames.MainRegion].Activate(_gridViewPresenter._gridView);
        }

        private ProbeScoringAssay GetAssayInferFromScores(SessionScores sessionScores, string assayName = "")
        {
            // An assay file couldn't be found, so infer the class list from any scored cells.
            // Don't try to create an assay file from this inferred list, as it would probably
            // not match the one used to create the scores (one that had the same class names
            // could be created, but it would not have the same analysis criteria, so would be
            // misleading - it wouldn't be the same assay used to create the scores, but the user
            // might not realise). If the user wants to be able to modify the class list, or do
            // reprocessing, they must manually restore the missing assay file to the default
            // assays folder, from where it will automatically be copied next time the scan area
            // is opened.
            return new ProbeScoringAssay(sessionScores, assayName);
        }

        private void OnCreateNewSession(ScanAreaViewModel ScanAreaViewModel)
        {
            WaitCursor.Show();
            ScanAreaViewModel scanAreaViewModel = ScanAreaViewModel as ScanAreaViewModel;
            if (scanAreaViewModel.SelectedScanArea != null)
            {
                var newSession = _dataService.CreateNewSession(scanAreaViewModel.SelectedScanArea, scanAreaViewModel.NewSessionName, scanAreaViewModel.UseSpotScores);
                if (newSession != null)
                {
                    SessionViewModel sessionviewModel = new SessionViewModel(newSession, _eventAggregator);
                    scanAreaViewModel.SessionViewModelCollection.Add(sessionviewModel);
                    OnOpenSession(sessionviewModel.Session);
                }
                else
                {
                    MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-SessionCreationFailedMessage"), Literal.Strings.Lookup("cbStr-SessionCreationFailedTitle"), MessageBoxButton.OK, MessageBoxImage.Error, true);
                    if (result == MessageBoxResult.OK)
                    {
                        _eventAggregator.GetEvent<CytoApps.Infrastructure.UI.Events.RefreshSelectedCaseEvent>().Publish(null);
                    }
                }
            }
        }

        private void DeleteSession(ScanAreaViewModel scanAreaViewModel)
        {
            try
            {
                var lockResult = _dataService.CheckKLockExists(LoginHelper.CurrentUser, scanAreaViewModel.SessionViewModel.Session.Slide.CaseId, scanAreaViewModel.SessionViewModel.Session);
                if (!lockResult.IsLocked)
                {
                    MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-DeleteSessionMessage"), Literal.Strings.Lookup("cbStr-DeleteSessionTitle"), MessageBoxButton.OKCancel, MessageBoxImage.Question);
                    switch (result)
                    {
                        case MessageBoxResult.OK:
                            WaitCursor.Show();
                            _dataService.DeleteSession(scanAreaViewModel.SessionViewModel.Session);
                            scanAreaViewModel.SelectedScanArea.Sessions.Remove(scanAreaViewModel.SessionViewModel.Session);
                            scanAreaViewModel.SessionViewModelCollection.Remove(scanAreaViewModel.SessionViewModel);
                            scanAreaViewModel.SessionViewModel = null;
                            break;
                    }
                }
                else
                {
                    switch (lockResult.LockType)
                    {
                        case CytoApps.Models.LockType.ObjectLock:
                            MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-SessionLockedSubstr1") + scanAreaViewModel.SessionViewModel.Session.Name + Literal.Strings.Lookup("cbStr-SessionLockedSubstr2") + lockResult.User.Name + "(" + lockResult.User.MachineName + ").", Literal.Strings.Lookup("cbStr-SessionLockedTitle"), MessageBoxButton.OK, MessageBoxImage.Error);
                            break;
                        case CytoApps.Models.LockType.FileNotFound:
                            MessageBoxResult result1 = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-RefreshCase"), Literal.Strings.Lookup("cbStr-SessionNotFound"), MessageBoxButton.OK, MessageBoxImage.Error, true);
                            if (result1 == MessageBoxResult.OK)
                            {
                                _eventAggregator.GetEvent<CytoApps.Infrastructure.UI.Events.RefreshSelectedCaseEvent>().Publish(null);
                            }
                            break;

                    }
                }
            }
            catch (DALException deleteSessionException)
            {
                LogManager.Error("Delete Session Error: ", deleteSessionException);
                CustomPopUp.Show(Literal.Strings.Lookup("cbStr-DeleteSessionExceptionMessage"), Literal.Strings.Lookup("cbStr-DeleteSessionExceptionTitle"), MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}