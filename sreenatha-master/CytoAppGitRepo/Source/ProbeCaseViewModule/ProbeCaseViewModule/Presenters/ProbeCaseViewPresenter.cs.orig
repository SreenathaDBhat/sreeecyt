﻿using Prism.Events;
using Prism.Regions;
using ProbeCaseViewModule.Services;
using ProbeCaseViewModule.ViewModels;
using System.ComponentModel.Composition;
using ProbeCaseViewModule.Models;
using System.IO;
using CytoApps.SpotConfiguration;
using ProbeCaseViewModule.Events;
using ProbeCaseViewDataAccess.Models;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Localization;
using System;
using System.Linq;
using CytoApps.Infrastructure.Helpers;
using System.Windows;
using CytoApps.Infrastructure.UI;
using CytoApps.Exceptions;
using System.Collections.Generic;

namespace ProbeCaseViewModule.Presenters
{
    /// <summary>
    /// This Presenter is used for plugin the pcv Module to Case Browser NavigationView. This is the entry point of PCV Module
    /// </summary>
    [Export]
    class ProbeCaseViewPresenter
    {
        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        private IDataService _dataService;
        private ScanArea _currentScanArea;

        private SharedViewModel _sharedVM;
        private GridViewPresenter _gridViewPresenter;
        private FrameWindowPresenter _frameWindowPresenter;
        private DualReaderFramesGalleryPresenter _dualReaderFramesGalleryPresenter;

        [ImportingConstructor]
        public ProbeCaseViewPresenter(IEventAggregator eventAggregator, IDataService dataService, IRegionManager regionManager)
        {
            _eventAggregator = eventAggregator;
            _dataService = dataService;
            _regionManager = regionManager;

            _eventAggregator.GetEvent<OpenSessionEvent>().Subscribe(OnOpenSession);
            _eventAggregator.GetEvent<CreateSessionEvent>().Subscribe(OnCreateNewSession);
            _eventAggregator.GetEvent<DeleteSessionEvent>().Subscribe(DeleteSession);
            _eventAggregator.GetEvent<ClearSessionEvent>().Subscribe(ClearSessionData);
            _eventAggregator.GetEvent<CreateSessionWithSelectedFramesEvent>().Subscribe(OnCreateNewSessionWithSelectedFrames);
        }

        private void OnCreateNewSessionWithSelectedFrames(ScanAreaViewModel scanAreaViewModel)
        {
            WaitCursor.Show();
            if (_dualReaderFramesGalleryPresenter == null)
                _dualReaderFramesGalleryPresenter = CreateDualReaderFramesGalleryPresenterInstance();
            _dualReaderFramesGalleryPresenter.Show(scanAreaViewModel);
        }

        private DualReaderFramesGalleryPresenter CreateDualReaderFramesGalleryPresenterInstance()
        {
            return new DualReaderFramesGalleryPresenter();
        }

        private void ClearSessionData(object obj)
        {
            _frameWindowPresenter.CloseFrameWindowOnSessionClose();
            _gridViewPresenter.Dispose();
            _frameWindowPresenter.Dispose();
            _dualReaderFramesGalleryPresenter.Dispose();
            _frameWindowPresenter = null;
            _sharedVM = null;
            _gridViewPresenter = null;
            _dualReaderFramesGalleryPresenter = null;
        }

        private void OnOpenSession(object selectedSession)
        {
            var session = selectedSession as Session;
            if (null != session)
            {
                _currentScanArea = new ScanArea(session.Slide, session.ScanAreaId, session.ScanAreaId);
                var sessionScores = _dataService.GetScores(session);
                var sessionFrames = _dataService.GetFrames(_currentScanArea);
                var scoreCellSettingList = _dataService.LoadScoredCellsInfo(session, _currentScanArea.Name);

                ProbeScoringAssay currentAssay = null;
                var assayData = _dataService.GetAssayData(session);
                AssayInfo _assayinfo = _dataService.GetClassifierNamesAndScripts(session);

                if (null != assayData)
                {
                    if (_assayinfo != null)
                    {
                        var spotController = SpotConfigController.FromStream(assayData, _assayinfo);
                        currentAssay = new ProbeScoringAssay(spotController);
                    }                        
                }
                else
                {
<<<<<<< HEAD
                    currentAssay = GetAssayInferFromScores(sessionScores); 
                    if(_assayinfo != null)
                    {
                        currentAssay.Name = _assayinfo.DefaultAssayName;
                    }                            
=======
                    // If Assay File does not exists
                    currentAssay = GetAssayInferFromScores(sessionScores, _dataService.GetDefaultAssayName(session, session.ScanAreaId));                   
>>>>>>> ca505ae0de2513b724d8700c4f65e93c1ddbd96e
                }

                _sharedVM = new SharedViewModel(sessionScores, currentAssay, sessionFrames, scoreCellSettingList);
                //Dual Reader - Fill VM with selected frames if sesion is created with selected frames
                if (session.IsCreatedWithSelectedFrames)
                {
                    var frameIds = _dataService.GetSelectedFramesForSession(session);//Need Review ------------- If this DAL line adds lag to web request!!!!
                    _sharedVM.Frames = _sharedVM.Frames.Where(x => frameIds.Contains(x.Image.Id.ToString())).ToObservableCollection();
                }
                if(_gridViewPresenter == null) _gridViewPresenter = new GridViewPresenter();
                if (_dualReaderFramesGalleryPresenter == null) _dualReaderFramesGalleryPresenter = CreateDualReaderFramesGalleryPresenterInstance();
                _gridViewPresenter.InitializeGridViewModel(_sharedVM, _dualReaderFramesGalleryPresenter);
                if(_frameWindowPresenter == null) _frameWindowPresenter = new FrameWindowPresenter();
                _frameWindowPresenter.InitializeFrameViewModel(_sharedVM);
            }
        }

        private ProbeScoringAssay GetAssayInferFromScores(SessionScores sessionScores, string assayName = "")
        {
            // An assay file couldn't be found, so infer the class list from any scored cells.
            // Don't try to create an assay file from this inferred list, as it would probably
            // not match the one used to create the scores (one that had the same class names
            // could be created, but it would not have the same analysis criteria, so would be
            // misleading - it wouldn't be the same assay used to create the scores, but the user
            // might not realise). If the user wants to be able to modify the class list, or do
            // reprocessing, they must manually restore the missing assay file to the default
            // assays folder, from where it will automatically be copied next time the scan area
            // is opened.
           return new ProbeScoringAssay(sessionScores, assayName);
        }

        private void OnCreateNewSession(ScanAreaViewModel ScanAreaViewModel)
        {
            WaitCursor.Show();
            ScanAreaViewModel scanAreaViewModel = ScanAreaViewModel as ScanAreaViewModel;
            if (scanAreaViewModel.SelectedScanArea != null)
            {
                var newSession = _dataService.CreateNewSession(scanAreaViewModel.SelectedScanArea, scanAreaViewModel.NewSessionName, scanAreaViewModel.UseSpotScores);
                if (newSession != null)
                {
                    SessionViewModel sessionviewModel = new SessionViewModel(newSession, _eventAggregator);
                    scanAreaViewModel.SessionViewModelCollection.Add(sessionviewModel);
                    OnOpenSession(sessionviewModel.Session);
                }
            }
        }

        private void DeleteSession(ScanAreaViewModel scanAreaViewModel)
        {
            try
            {
                MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-DeleteSessionMessage"), Literal.Strings.Lookup("cbStr-DeleteSessionTitle"), MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        WaitCursor.Show();
                        _dataService.DeleteSession(scanAreaViewModel.SessionViewModel.Session);
                        scanAreaViewModel.SelectedScanArea.Sessions.Remove(scanAreaViewModel.SessionViewModel.Session);
                        scanAreaViewModel.SessionViewModelCollection.Remove(scanAreaViewModel.SessionViewModel);
                        scanAreaViewModel.SessionViewModel = null;
                        break;
                }
            }
            catch (DALException deleteSessionException)
            {
                LogManager.Error("Delete Session Error: ", deleteSessionException);
                CustomPopUp.Show(Literal.Strings.Lookup("cbStr-DeleteSessionExceptionMessage"), Literal.Strings.Lookup("cbStr-DeleteSessionExceptionTitle"), MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}

