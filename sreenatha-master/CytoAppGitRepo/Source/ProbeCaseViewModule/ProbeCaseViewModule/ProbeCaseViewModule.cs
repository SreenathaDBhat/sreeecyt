﻿using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using ProbeCaseViewModule.Presenters;
using System.ComponentModel.Composition;

namespace ProbeCaseViewModule
{
    /// <summary>
    /// This class is used intialize the Probe Case View Module
    /// Configured module via configuration file i.e ModuleHost/app.config
    /// Import call will create instance of the class and later subcribes the published events
    /// </summary>
    [ModuleExport(typeof(ProbeCaseModule))]
    public class ProbeCaseModule : IModule
    {
        /// <summary>
        /// pragma warning disable 0649,0169 it will disable warning message for unassigned variable
        /// </summary>
#pragma warning disable 0649, 0169
        [Import]
        private IRegionManager _regionManager;

        [Import]
        private ProbeCaseNavigatorPresenter _probeCaseNavigatorPresenter;

        //[Import]
        //private EnhanceViewPresenter _enhanceViewPresenter;

        [Import]
        private ProbeCaseViewPresenter _probeCasePresenter;

#pragma warning restore 0649, 0169

        public void Initialize()
        {
        }
    }
}
