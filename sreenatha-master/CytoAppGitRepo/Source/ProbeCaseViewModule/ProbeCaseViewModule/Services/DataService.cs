﻿using System.Collections.Generic;
using CytoApps.Models;
using ProbeCaseViewDataAccess.Models;
using ProbeCaseViewDataAccess;
using System.ComponentModel.Composition;
using PCVImageRenderers.Models;
using System.Xml.Linq;
//using System;
using System.Windows.Media.Imaging;
using System;

namespace ProbeCaseViewModule.Services
{
    [Export(typeof(IDataService))]
    public class DataService : IDataService
    {
        private IProbeCaseViewDataAccess probeCaseViewDataAccess;

        // all DAL assemblies found that derive from ICVKaryoDataAccess
        private Type[] _availProbeDalTypes = null;

        #region "Methods"

        public void CreateDatasource(DataSource dataSource)
        {
            // import PCV DALS using reflection not MEF (ExportFactory) due to lifetime concerns
            if (_availProbeDalTypes == null)
                _availProbeDalTypes = DalReflectionFinder.GetTypesForInterface<IProbeCaseViewDataAccess>(@".");

            // Create a new instance every time, thread safety concerns
            probeCaseViewDataAccess = DalReflectionFinder.GetMatchingInstanceOf<IProbeCaseViewDataAccess>(_availProbeDalTypes, dataSource);

            // This will pass the connection parameters required (persisted by application per data source pointed at)
            probeCaseViewDataAccess.Initialize(dataSource);
        }

        public bool CanAnalyseSlide(Slide slide)
        {
            return probeCaseViewDataAccess.CanAnalyseSlide(slide);
        }

        public IEnumerable<ScanArea> GetScanAreas(Slide slide)
        {
            return probeCaseViewDataAccess.GetScanAreas(slide);
        }

        public SessionScores GetScores(Session session)
        {
            return probeCaseViewDataAccess.GetScores(session);
        }

        public IEnumerable<Frame> GetFrames(ScanArea scanArea)
        {
            return probeCaseViewDataAccess.GetFrames(scanArea);
        }

        public List<string> GetSelectedFramesForSession(Session session)
        {
            return probeCaseViewDataAccess.GetSelectedFramesForSession(session);
        }

        public byte[] GetThumbnailData(Session session, Score score)
        {
            return probeCaseViewDataAccess.GetThumbnailData(session, score);
        }

        public byte[] GetOverlayData(Session session, Score score, Overlay overlay)
        {
            return probeCaseViewDataAccess.GetOverlayData(session, score, overlay);
        }

        public byte[] GetFrameComponentData(Slide slide, string frameId, string componentId)
        {
            return probeCaseViewDataAccess.GetFrameComponentData(slide, frameId, componentId);
        }

        public BitmapImage GetFrameBitmap(Slide slide, string scanAreaId, Frame frame, IEnumerable<ComponentLevels> levels, int z, FrameRendererGlobalOptions options)
        {
            return BitmapImageFromByteArray(probeCaseViewDataAccess.GetFrameBitmap(slide, scanAreaId, frame, levels, z, options.RenderSettings));
        }

        public BitmapImage GetCachedFrameBitmap(Slide slide, string scanAreaId, Frame frame, int decodeWidth = 0)
        {
            return BitmapImageFromByteArray(probeCaseViewDataAccess.GetCachedFrameBitmap(slide.CaseId, slide.Id, scanAreaId, frame.Id.ToString()), decodeWidth);
        }

        public Score[] GetScoreStats(Session session)
        {
            return probeCaseViewDataAccess.GetScoreStats(session);
        }

        public string GetRenderInstructionCellInfo(Session selectedSession, string scoreID)
        {
            return probeCaseViewDataAccess.GetRenderInstructionCellInfo(selectedSession, scoreID);
        }

        public string GetEnhancedCellImageInfo(Session selectedSession, string scoreID)
        {
            return probeCaseViewDataAccess.GetEnhancedCellImageInfo(selectedSession, scoreID);
        }

        public bool WriteRenderInstructionToEnhancedCell(Session selectedSession, byte[] bmp, string scoreID, XElement renderInstructionForBmp)
        {
            return probeCaseViewDataAccess.WriteRenderInstructionToEnhancedCell(selectedSession, bmp, scoreID, renderInstructionForBmp);
        }

        public void SaveSession(Session session, List<ScoreSettings> scoreSettingsList, SessionScores sessionScores)
        {
            probeCaseViewDataAccess.SaveSession(session, scoreSettingsList, sessionScores);
        }

        public List<ScoreSettings> LoadScoredCellsInfo(Session session, string scanAreaName)
        {
            return probeCaseViewDataAccess.LoadScoredCellsInfo(session, scanAreaName);
        }

        public BitmapImage GetThumbnailBitmap(Session session, Score score, int stackIndex, IEnumerable<ChannelDisplay> channelDisplay, bool attenuateCounterstain, bool invertCounterstain, bool IsTemporarySaved)
        {
            return BitmapImageFromByteArray(probeCaseViewDataAccess.GetThumbnailBitmap(session, score, stackIndex, channelDisplay, attenuateCounterstain, invertCounterstain, IsTemporarySaved));
        }

        public void ClearTemporarySavedThumb(Session session)
        {
            probeCaseViewDataAccess.ClearTemporarySavedThumb(session);
        }

        public BitmapImage CreateThumbnailBitmap(ScoreThumbnail scoreThumbnail, Session session)
        {
            return BitmapImageFromByteArray(probeCaseViewDataAccess.CreateThumbnailBitmap(scoreThumbnail, session));
        }

        public Session CreateNewSession(ScanArea scanArea, string sessionName, bool useSpotScores, bool isCreatedWithSelectedFrames = false, List<string> selectedFrameIdList = null)
        {
            return probeCaseViewDataAccess.CreateNewSession(scanArea, sessionName, useSpotScores, isCreatedWithSelectedFrames, selectedFrameIdList);
        }

        public void DeleteSession(Session session)
        {
            probeCaseViewDataAccess.DeleteSession(session);
        }

        // TODO: put this in a separate class with other reusable Bitmap methods
        public static BitmapImage BitmapImageFromByteArray(byte[] array, int decodeWidth = 0)
        {
            if (array == null)
                return null;

            using (var stream = new System.IO.MemoryStream(array, false))
            {
                BitmapImage image = new BitmapImage();

                image.BeginInit();

                image.CacheOption = BitmapCacheOption.OnLoad;

                if (decodeWidth > 0)
                    image.DecodePixelWidth = decodeWidth;

                image.StreamSource = stream;

                image.EndInit();
                image.Freeze();

                stream.Close();

                return image;
            }
        }

        public long GetFrameCellCount(ScanArea scanArea, string frameId)
        {
            return probeCaseViewDataAccess.GetFrameCellCount(scanArea, frameId);
        }

        public void UpdateScores(Session session, List<string> frameIdList)
        {
            probeCaseViewDataAccess.UpdateScores(session, frameIdList);
        }

        public BitmapImage GetSlideOverviewImage(Slide slide, string etchImage, int width = 0)
        {
            return BitmapImageFromByteArray(probeCaseViewDataAccess.GetSlideOverviewImage(slide, etchImage), width);
        }

        public IEnumerable<FrameOutline> GetFrameOutlines(Slide slide)
        {
            return probeCaseViewDataAccess.GetFrameOutlines(slide);
        }

        // Assay & Spot Configuration
        public byte[] GetAssayData(Session session)
        {
            return probeCaseViewDataAccess.GetAssayData(session);
        }

        public CytoApps.SpotConfiguration.AssayInfo GetClassifierNamesAndScripts(Session session = null)
        {
            var _assayInfoWrapper = probeCaseViewDataAccess.GetClassifierNamesAndScripts(session);
            return _assayInfoWrapper != null ? new CytoApps.SpotConfiguration.AssayInfo()
            {
                CellClassifierNames = _assayInfoWrapper.CellClassifierNames,
                SpotScript = _assayInfoWrapper.SpotScript,
                PreScanScript = _assayInfoWrapper.PreScanScript,
                SpotScriptFilePath = _assayInfoWrapper.SpotScriptFilePath,
                PreScanScriptFilePath = _assayInfoWrapper.PreScanScriptFilePath,
                DefaultAssayName = _assayInfoWrapper.DefaultAssayName
            } : null;
        }

        public List<string> GetCompatibleAssays(FrameComponent[] component)
        {
            return probeCaseViewDataAccess.GetCompatibleAssays(component);
        }

        public byte[] GetDefaultAssayData(string assayName)
        {
            return probeCaseViewDataAccess.GetDefaultAssayData(assayName);
        }

        public void SaveAsDefaultAssay(byte[] AssayData, string assayName)
        {
            probeCaseViewDataAccess.SaveAsDefaultAssay(AssayData, assayName);
        }

        public List<string> GetAllDefaultAssays()
        {
            return probeCaseViewDataAccess.GetAllDefaultAssays();
        }

        public void QueueRegionForAnalysis(AnalysisRegion analysisRegion)
        {
            probeCaseViewDataAccess.QueueRegionForAnalysis(analysisRegion);
        }


        public void DeleteRegions(Session session, IEnumerable<string> regions)
        {
            probeCaseViewDataAccess.DeleteRegions(session, regions);
        }

        public bool SaveAnnotation(Session session, List<AnnotationIdentifier> frameAnnotations)
        {
            return probeCaseViewDataAccess.SaveAnnotation(session, frameAnnotations);
        }

        public List<AnnotationIdentifier> GetAllFramesAnnotation(Session session, List<Frame> frames)
        {
            return probeCaseViewDataAccess.GetAllFramesAnnotation(session, frames);
        }

        public List<Score> GetScoreByRegionAnalysis(IEnumerable<AnalysisRegion> analysisRegions)
        {
            return probeCaseViewDataAccess.GetScoreByRegionAnalysis(analysisRegions);
        }

        public List<string> GetStoredCommonAnnotations()
        {
            return probeCaseViewDataAccess.GetStoredCommonAnnotations();
        }

        public void SaveCommonStoredAnnotation(List<string> annotationTexts)
        {
            probeCaseViewDataAccess.SaveCommonStoredAnnotation(annotationTexts);
        }

        public IEnumerable<AnalysisRegion> GetAllAnalysisRegions(Session session)
        {
            return probeCaseViewDataAccess.GetAllAnalysisRegions(session);
        }

        public byte[] GetAssayDataAndCreateBackup(Session session)
        {
            return probeCaseViewDataAccess.GetAssayDataAndCreateBackup(session);
        }

        public void SaveAssayData(byte[] assayData, Session session)
        {
            probeCaseViewDataAccess.SaveAssayData(assayData, session);
        }

        public void RestoreAssayData(Session session)
        {
            probeCaseViewDataAccess.RestoreAssayData(session);
        }

        public LockResult CheckKLockExists(UserDetails userDetails, string caseName, object lockingObject)
        {
            return probeCaseViewDataAccess.CheckKLockExists(userDetails, caseName, lockingObject);
        }

        public void LockFeatureorObject(UserDetails userDetails, string caseName, object lockingObject)
        {
            probeCaseViewDataAccess.LockFeatureorObject(userDetails, caseName, lockingObject);
        }

        public void RemoveLock(UserDetails userDetails, string caseName)
        {
            probeCaseViewDataAccess.RemoveLock(userDetails, caseName);
        }

        public IEnumerable<string> GetSpotFolderNamesForReprocess(Session session, string frameId = null)
        {
            return probeCaseViewDataAccess.GetSpotFolderNamesForReprocess(session, frameId);
        }

        public bool StartReprocessing(Session session, IEnumerable<string> spotFolders)
        {
            return probeCaseViewDataAccess.StartReprocessing(session, spotFolders);
        }

        public ReprocessScores GetScoresFromSpotFoldersProcessed(Session session, IEnumerable<string> SpotFolders)
        {
            return probeCaseViewDataAccess.GetScoresFromSpotFoldersProcessed(session, SpotFolders);
        }

        public bool CanCreateNewSession(string sessionName, Slide slide, string scanAreaName)
        {
            return probeCaseViewDataAccess.CanCreateNewSession(sessionName, slide, scanAreaName);
        }
        public void RestoreSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions)
        {
            probeCaseViewDataAccess.RestoreSession(session, scoreSettingsList, regions);
        }

        public void DeleteNewScoresAndRegionsFromSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions)

        {
            probeCaseViewDataAccess.DeleteNewScoresAndRegionsFromSession(session, scoreSettingsList, regions);

        }
        #endregion "Methods"
    }
}
