﻿using CytoApps.Models;
using PCVImageRenderers.Models;
using ProbeCaseViewDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

namespace ProbeCaseViewModule.Services
{
    public interface IDataService
    {
        void CreateDatasource(DataSource dataSource);
        bool CanAnalyseSlide(Slide slide);
        IEnumerable<ScanArea> GetScanAreas(Slide slide);
        SessionScores GetScores(Session session);
        IEnumerable<Frame> GetFrames(ScanArea scanArea);
        List<string> GetSelectedFramesForSession(Session session);
        byte[] GetThumbnailData(Session session, Score score);
        BitmapImage GetThumbnailBitmap(Session session, Score score, int stackIndex, IEnumerable<ChannelDisplay> channelDisplay, bool attenuateCounterstain, bool invertCounterstain, bool IsTemporarySaved);
        byte[] GetOverlayData(Session session, Score score, Overlay overlay);
        byte[] GetFrameComponentData(Slide slide, string frameId, string componentId);
        BitmapImage GetFrameBitmap(Slide slide, string scanAreaId, Frame frame, IEnumerable<ComponentLevels> levels, int z, FrameRendererGlobalOptions options);
        BitmapImage GetCachedFrameBitmap(Slide slide, string scanAreaId, Frame frame, int decodeWidth = 0);
        BitmapImage GetSlideOverviewImage(Slide slide, string etchImage, int decodeWidth);
        IEnumerable<FrameOutline> GetFrameOutlines(Slide slide);


        Score[] GetScoreStats(Session session);
        string GetRenderInstructionCellInfo(Session selectedSession, string scoreID);
        string GetEnhancedCellImageInfo(Session selectedSession, string scoreID);
        bool WriteRenderInstructionToEnhancedCell(Session selectedSession, byte[] bmp, string scoreID, XElement renderInstructionForBmp);
        void SaveSession(Session session, List<ScoreSettings> scoreSettingsList, SessionScores scores);

        //Returns newly created thumbnail saved status
        BitmapImage CreateThumbnailBitmap(ScoreThumbnail scoreThumbnail, Session session);
        void ClearTemporarySavedThumb(Session session);
        List<ScoreSettings> LoadScoredCellsInfo(Session session, string scanAreaName);
        bool CanCreateNewSession(string sessionName, Slide slide, string scanAreaName);
        Session CreateNewSession(ScanArea scanArea, string sessionName, bool useSpotScores, bool isCreatedWithSelectedFrames = false, List<string> imageFrameIdList = null);
        void DeleteSession(Session session);
        long GetFrameCellCount(ScanArea scanArea, string frameId);
        void UpdateScores(Session session, List<string> frameIdList);


        // Assay & Spot Configuration
        byte[] GetAssayData(Session session);
        CytoApps.SpotConfiguration.AssayInfo GetClassifierNamesAndScripts(Session session = null);
        bool SaveAnnotation(Session session, List<AnnotationIdentifier> frameAnnotations);
        byte[] GetDefaultAssayData(string assayName);
        List<string> GetCompatibleAssays(FrameComponent[] component);
        void SaveAsDefaultAssay(byte[] AssayData, string assayName);
        List<string> GetAllDefaultAssays();
        List<string> GetStoredCommonAnnotations();
        void SaveCommonStoredAnnotation(List<string> annotationTexts);

        void QueueRegionForAnalysis(AnalysisRegion analysisRegion);
        void DeleteRegions(Session session, IEnumerable<string> regions);
        List<Score> GetScoreByRegionAnalysis(IEnumerable<AnalysisRegion> analysisRegions);
        List<AnnotationIdentifier> GetAllFramesAnnotation(Session session, List<Frame> frames);
        IEnumerable<AnalysisRegion> GetAllAnalysisRegions(Session session);

        byte[] GetAssayDataAndCreateBackup(Session session);
        void SaveAssayData(Byte[] assayData, Session session);
        void RestoreAssayData(Session session);

        LockResult CheckKLockExists(UserDetails userDetails, string caseName, object lockingObject);
        void LockFeatureorObject(UserDetails userDetails, string caseName, object lockingObject);
        void RemoveLock(UserDetails userDetails, string caseName);
        IEnumerable<string> GetSpotFolderNamesForReprocess(Session session, string frameId = null);
        bool StartReprocessing(Session session, IEnumerable<string> spotFolders);
        ReprocessScores GetScoresFromSpotFoldersProcessed(Session session, IEnumerable<string> spotFolders);
        void RestoreSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions);
        void DeleteNewScoresAndRegionsFromSession(Session session, List<ScoreSettings> scoreSettingsList, IEnumerable<string> regions);
    }
}
