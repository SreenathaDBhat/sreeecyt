﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using ProbeCaseViewDataAccess.Models;
using ProbeCaseViewModule.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;
using PCVImageRenderers.Models;
using ProbeCaseViewModule.Events;
using Microsoft.Practices.ServiceLocation;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    /// View Model responisble for the drawing and display the regions on framewindow
    /// </summary>
    public class AanalysisRegionSurfaceViewModel : BindableBase
    {
        #region PrivateMembers
        private IEventAggregator _eventAggregator;
        private SharedViewModel _sharedViewModel;
        #endregion

        #region "Constructor"
        public AanalysisRegionSurfaceViewModel(SharedViewModel sharedVm)
        {
            _sharedViewModel = sharedVm;
            _eventAggregator = ServiceLocator.Current.TryResolve<IEventAggregator>();
        }
        #endregion

        #region Properties

        private AnalysisRegionInfo _currentRegion;
        public AnalysisRegionInfo CurrentRegion
        {
            get
            {
                return _currentRegion;
            }
        }

        private ObservableCollection<AnalysisRegionInfo> _regions = new ObservableCollection<AnalysisRegionInfo>();
        public ObservableCollection<AnalysisRegionInfo> Regions
        {
            get
            {
                return _regions;
            }
            set
            {
                _regions = value;
                OnPropertyChanged();
            }
        }

        private double lineThickness;
        public double LineThickness
        {
            get
            {
                return lineThickness;
            }
            set
            {
                lineThickness = value;
                OnPropertyChanged("LineThickness");
            }
        }

        private double mousePosX;
        public double MousePosX
        {
            get
            {
                return mousePosX;
            }
            set
            {
                mousePosX = value;
                OnPropertyChanged("MousePosX");
            }
        }

        private double mousePosY;
        public double MousePosY
        {
            get
            {
                return mousePosY;
            }
            set
            {
                mousePosY = value;
                OnPropertyChanged("MousePosY");
            }
        }

        public bool shiftPressed;
        public bool ShiftPressed
        {
            get
            {
                return shiftPressed;
            }
            set
            {
                shiftPressed = value;
                OnPropertyChanged("ShiftPressed");
            }
        }

        private double circleDiameterPixels = 100;
        public double CircleDiameterPixels
        {
            get
            {
                return circleDiameterPixels;
            }
            set
            {
                circleDiameterPixels = value;
                OnPropertyChanged("CircleDiameterPixels");
            }
        }

        public bool Cancelled
        {
            get;
            set;
        }
        private Point? lastFreehandPoint;

        #endregion        

        #region Commands

        private DelegateCommand<object> _mouseMoveCommand;
        public ICommand MouseMoveCommand
        {
            get
            {
                return _mouseMoveCommand ?? (_mouseMoveCommand = new DelegateCommand<object>((regionEventArgs) =>
                {
                    OnMouseMove((RegionEventArgs)regionEventArgs);
                }));
            }
        }

        private DelegateCommand<object> _mouseDownCommand;
        public ICommand MouseDownCommand
        {
            get
            {
                return _mouseDownCommand ?? (_mouseDownCommand = new DelegateCommand<object>((regionEventArgs) =>
                {
                    OnMouseDown((RegionEventArgs)regionEventArgs);
                }));
            }
        }

        private DelegateCommand<object> _mouseUpCommand;
        public ICommand MouseUpCommand
        {
            get
            {
                return _mouseUpCommand ?? (_mouseUpCommand = new DelegateCommand<object>((regionEventArgs) =>
                {
                    OnMouseUp((RegionEventArgs)regionEventArgs);
                }));
            }
        }

        private DelegateCommand<object> _mouseScrollCommand;
        public ICommand MouseScrollCommand
        {
            get
            {
                return _mouseScrollCommand ?? (_mouseScrollCommand = new DelegateCommand<object>((mouseScrollDelta) =>
                {
                    var delta = Math.Sign((int)mouseScrollDelta);

                    if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
                    {
                        if (delta > 0)
                            CircleDiameterPixels = Math.Min(CircleDiameterPixels + 5, 250);
                        else
                            CircleDiameterPixels = Math.Max(CircleDiameterPixels - 3, 50);
                    }

                }));
            }
        }

        public DelegateCommand<AnalysisRegionInfo> _deleteRegionCommand;
        public ICommand DeleteRegionCommand
        {
            get
            {
                return _deleteRegionCommand ?? (_deleteRegionCommand = new DelegateCommand<AnalysisRegionInfo>((selectedRegion) =>
                {
                    RemoveRegion(selectedRegion);
                }));
            }
        }

        #endregion

        #region  Methods
        private Point AddPoint(double x, double y)
        {
            var pt = new Point(x, y);
            CurrentRegion.AddPoint(pt);
            return pt;
        }
        private void AddNewRegion()
        {
            _currentRegion = new AnalysisRegionInfo();
            _currentRegion.FrameId = _sharedViewModel.CurrentFrame.Image.Id.ToString();
            _regions.Add(_currentRegion);
        }

        public void AddRegion(AnalysisRegionInfo newRegion)
        {
            _regions.Add(newRegion);
        }

        private void RemoveLastRegion()
        {
            _regions.Remove(_regions.Last());
            _currentRegion = (_regions.Count() > 0) ? _regions.Last() : null;
        }

        public void RemoveRegion(AnalysisRegionInfo region)
        {
            region.CanDelete = true;
            _regions.Remove(region);
            _currentRegion = (_regions.Count() > 0) ? _regions.Last() : null;
        }

        private void ClearRegions()
        {
            _regions.Clear();
        }

        private void ShowAllRegion(bool show)
        {
            foreach (var r in _regions)
                r.IsVisible = show;
        }

        private void AllowDeleteAllRegion(bool allow)
        {
            foreach (var r in _regions)
                r.AllowDelete = allow;
        }
        private void OnMouseDown(RegionEventArgs regionArgs)
        {
            if (regionArgs != null && regionArgs.IsMouseCapture)
            {
                lastFreehandPoint = regionArgs.CurrentMousePosition;
                AddNewRegion();
                CurrentRegion.IsFilled = true;
                CurrentRegion.AddPoint(regionArgs.CurrentMousePosition);
            }
        }

        private void OnMouseMove(RegionEventArgs regionArgs)
        {
            if (regionArgs.Width > regionArgs.CurrentMousePosition.X)
            {
                MousePosX = Math.Min(regionArgs.Width, regionArgs.CurrentMousePosition.X);
            }
            else
            {
                MousePosX = Math.Max(regionArgs.Width, regionArgs.CurrentMousePosition.X);
            }
            if (regionArgs.Height > regionArgs.CurrentMousePosition.Y)
            {
                MousePosY = Math.Min(regionArgs.Height, regionArgs.CurrentMousePosition.Y);
            }
            else
            {
                MousePosY = Math.Max(regionArgs.Height, regionArgs.CurrentMousePosition.Y);
            }

            if (regionArgs.isDrawing)
            {
                if (lastFreehandPoint != null)
                {
                    CurrentRegion.AddPoint(regionArgs.CurrentMousePosition);
                    lastFreehandPoint = regionArgs.CurrentMousePosition;
                }
            }
        }

        private void OnMouseUp(RegionEventArgs regionArgs)
        {
            if (regionArgs.isDrawing)
            {
                if (regionArgs.IsShiftPressed)
                {
                    var start = regionArgs.CurrentMousePosition;
                    var radius = (CircleDiameterPixels / 2) - 1;
                    var pts = ScriptProcHelper.CreateEllipsePoints(start.X, start.Y, radius, radius, 0, 25);
                    CurrentRegion.SetPoints(pts);
                }
                lastFreehandPoint = null;
                if (CurrentRegion == null)
                    return;
                if (CurrentRegion.Points.Count() < 10) // too tiny
                {
                    RemoveLastRegion();
                    return;
                }
                CurrentRegion.IsFilled = false;
                CurrentRegion.SetLabelPosition();
                CurrentRegion.AdjustLabelToFit(_sharedViewModel.CurrentFrame.FrameWidth, _sharedViewModel.CurrentFrame.FrameHeight);
                CurrentRegion.IsNewRegion = true;
                _eventAggregator.GetEvent<SaveRegionEvent>().Publish(CurrentRegion);
            }
        }

        public void Dispose()
        {
            Regions.Clear();
            Regions = null;
            _eventAggregator = null;
            _sharedViewModel = null;
        }

        #endregion
    }
}
