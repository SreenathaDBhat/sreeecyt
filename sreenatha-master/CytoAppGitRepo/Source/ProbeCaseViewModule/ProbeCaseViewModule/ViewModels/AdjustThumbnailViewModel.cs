﻿using Microsoft.Practices.ServiceLocation;
using PCVImageRenderers;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    /// Viewmodel responsible for binding adjustments to a grid image (contrast, stackindex, brightness etc
    /// </summary>
    public class AdjustThumbnailViewModel : BindableBase
    {
        private IEventAggregator _eventAggregator;

        public object Clone()
        {
            return MemberwiseClone();
        }


        public AdjustThumbnailViewModel()
        {
            _eventAggregator = ServiceLocator.Current.TryResolve<IEventAggregator>();
            _isMaxProjection = true;
            LastRenderTime = DateTime.MinValue;
        }

        /// <summary>
        /// Temporary copy of global channels, we may throw these away without applying to all images
        /// </summary>

        private IEnumerable<ChannelDisplayInfo> _channelDisplays;
        public IEnumerable<ChannelDisplayInfo> ChannelDisplays
        {
            get { return _channelDisplays; }
            set
            {
                _channelDisplays = value; base.OnPropertyChanged(); base.OnPropertyChanged("CounterstainChannel");
                SetTheDefaultChannel();
            }
        }

        private bool _isMaxProjection;
        public bool IsMaxProjection
        {
            get { return _isMaxProjection; }
            set
            {
                _isMaxProjection = value;
                OnPropertyChanged();
                if (ChannelDisplays != null)
                    _eventAggregator.GetEvent<CurrentChannelDisplayEvent>().Publish(this);
            }
        }

        public ChannelDisplayInfo CounterstainChannel
        {
            get { return ChannelDisplays.Where(channel => channel.IsCounterstain).FirstOrDefault(); }
        }

        private DelegateCommand<ChannelDisplayInfo> _cellDisplayChangeCommand;
        public ICommand CellDisplayChangeCommand
        {
            get
            {
                return _cellDisplayChangeCommand ?? (_cellDisplayChangeCommand = new DelegateCommand<ChannelDisplayInfo>((channel) =>
                {
                    if (channel != null)
                    {
                        foreach (var item in ChannelDisplays)
                        {
                            if (channel.Name == Constants.AllProbes)
                            {
                                if (item.ChannelDisplay.IsCounterstain)
                                {
                                    return;
                                }

                                item.HistogramLow = channel.HistogramLow;
                                item.HistogramHigh = channel.HistogramHigh;
                            }
                        }
                        _eventAggregator.GetEvent<CurrentChannelDisplayEvent>().Publish(this);
                    }
                }));
            }
        }

        private int _stackSizeMinusOne;
        public int StackSizeMinusOne
        {
            get
            {
                return _stackSizeMinusOne;
            }
            set
            {
                _stackSizeMinusOne = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///  Z Stack index to display
        /// </summary>
        private int _currentStackIndex;
        public int CurrentStackIndex
        {
            get { return _currentStackIndex; }
            set
            {
                _currentStackIndex = value;
                OnPropertyChanged();
                _isMaxProjection = false;
                OnPropertyChanged("IsMaxProjection");
                if (ChannelDisplays != null)
                    _eventAggregator.GetEvent<CurrentChannelDisplayEvent>().Publish(this);
            }
        }

        /// <summary>
        /// Rendered thumnail for display
        /// </summary>
        private BitmapSource _thumbnail;
        public BitmapSource Thumbnail
        {
            get
            {
                return _thumbnail;
            }
            set
            {
                _thumbnail = value;
                OnPropertyChanged();
            }
        }

        private object _selectedChannel;
        public object SelectedChannel
        {
            get { return _selectedChannel; }
            set
            {
                _selectedChannel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Cached Raw propietary thumbnail data (all channels and stacks (.blb file) data for the renderer
        /// </summary>
        private byte[] _thumbnailData;
        public byte[] ThumbnailData
        {
            get { return _thumbnailData; }
            set { _thumbnailData = value; OnPropertyChanged(); }
        }

        /// <summary>
        /// Instance of the thumbnail renderer
        /// </summary>
        public StackedThumbnailRenderer ThumbRenderer { get; set; }

        public DateTime RenderRequestTime { get; set; }
        public DateTime LastRenderTime { get; set; }

        public Task RenderTask { get; set; }


        public void SetTheDefaultChannel()
        {
            bool histogramsAreAllDefault = true;
            foreach (var i in ChannelDisplays.Where(x => x.IsCounterstain == false).ToList())
            {
                if (i.HistogramLow > 0 || i.HistogramHigh < 1)
                {
                    histogramsAreAllDefault = false;
                    break;
                }
            }

            if (histogramsAreAllDefault)
                SelectedChannel = ChannelDisplays.Where(x => x.IsCounterstain == true).FirstOrDefault();
            else
                SelectedChannel = ChannelDisplays.Where(x => x.IsCounterstain == false).FirstOrDefault();
        }
    }
}
