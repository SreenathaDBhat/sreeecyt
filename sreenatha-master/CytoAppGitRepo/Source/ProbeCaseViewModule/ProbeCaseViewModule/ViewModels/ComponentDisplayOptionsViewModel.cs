﻿using Microsoft.Practices.ServiceLocation;
using PCVImageRenderers;
using PCVImageRenderers.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    /// This is class is used for display to Histogram,channels and changing the frame settings
    /// </summary>
    public class ComponentDisplayOptionsViewModel : BindableBase
    {
        #region Private members
        private IEventAggregator _eventAggregator;
        private ComponentLevels dragging;
        private bool draggingLow;
        #endregion

        public ComponentDisplayOptionsViewModel()
        {
            _eventAggregator = ServiceLocator.Current.TryResolve<IEventAggregator>();
            _eventAggregator.GetEvent<RenderFrameEvent>().Subscribe(OnFrameChange);
            RendererOptions = new FrameRendererGlobalOptions();
        }

        #region Properties
        private List<ComponentLevels> _rendererLevels;

        public List<ComponentLevels> RendererLevels
        {
            get
            {
                return _rendererLevels;
            }
            set
            {
                _rendererLevels = value;
                OnPropertyChanged();
            }
        }


        private FrameRendererGlobalOptions _rendererOptions;
        public FrameRendererGlobalOptions RendererOptions
        {
            get
            {
                return _rendererOptions;
            }
            set
            {
                _rendererOptions = value;
                OnPropertyChanged();
            }
        }

        public bool HasCounterstain
        {
            get
            {
                return true;// TODO CurrentFrame != null ? CurrentFrame.Image.Components.Where(x => x.IsCounterstain).FirstOrDefault() != null : false;
            }
        }

        public bool AttenuateCounterstain
        {
            get
            {
                return RendererOptions.AttenuateCounterstain;
            }
            set
            {
                RendererOptions.AttenuateCounterstain = value;
                OnPropertyChanged();
                if (RendererOptions.AttenuateCounterstain)
                {
                    InvertCounterstain = false;
                }
            }
        }

        public bool InvertCounterstain
        {
            get
            {
                return RendererOptions.InvertCounterstain;
            }
            set
            {
                RendererOptions.InvertCounterstain = value;
                OnPropertyChanged();
                if (RendererOptions.InvertCounterstain)
                {
                    AttenuateCounterstain = false;
                }
            }
        }

        public bool SubtractBackground
        {
            get
            {
                return RendererOptions.SubtractBackground;
            }
            set
            {
                RendererOptions.SubtractBackground = value;
                OnPropertyChanged();
            }
        }

        public bool MaximumProjection
        {
            get
            {
                return RendererOptions.MaximumProjection;
            }
            set
            {
                RendererOptions.MaximumProjection = value;
                OnPropertyChanged();
                _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
            }
        }


        private int _zLevel;
        public int ZLevel
        {
            get
            {
                return _zLevel;
            }
            set
            {
                _zLevel = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Commands 
        private DelegateCommand<object> _mouseMoveCommand;
        public ICommand MouseMoveCommand
        {
            get
            {
                return _mouseMoveCommand ?? (_mouseMoveCommand = new DelegateCommand<object>((histogramEventArgs) =>
                {
                    if (dragging == null)
                        return;
                    var args = histogramEventArgs as HistogramMouseEventArgs;
                    DoDrag(args.Point);
                }));
            }
        }

        private DelegateCommand<object> _mouseLeftButtonDownCommand;
        public ICommand MouseLeftButtonDownCommand
        {
            get
            {
                return _mouseLeftButtonDownCommand ?? (_mouseLeftButtonDownCommand = new DelegateCommand<object>((histogramEventArgs) =>
                {
                    MouseLeftButtonDownExecute(histogramEventArgs);
                }));
            }
        }

        private DelegateCommand _mouseLeftButtonUpCommand;
        public ICommand MouseLeftButtonUpCommand
        {
            get
            {
                return _mouseLeftButtonUpCommand ?? (_mouseLeftButtonUpCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
                    dragging = null;
                }));
            }
        }

        private DelegateCommand _invertCounterstainCommand;
        public ICommand InvertCounterstainCommand
        {
            get
            {
                return _invertCounterstainCommand ?? (_invertCounterstainCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
                }));
            }
        }

        private DelegateCommand _attenuateCounterstainCommand;
        public ICommand AttenuateCounterstainCommand
        {
            get
            {
                return _attenuateCounterstainCommand ?? (_attenuateCounterstainCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
                }));
            }
        }

        private DelegateCommand _fitHistogramCommand;
        public ICommand FitHistogramCommand
        {
            get
            {
                return _fitHistogramCommand ?? (_fitHistogramCommand = new DelegateCommand(() =>
                {
                    FitHistogramExecute();
                }));
            }
        }

        private DelegateCommand _resetHistogramCommand;
        public ICommand ResetHistogramCommand
        {
            get
            {
                return _resetHistogramCommand ?? (_resetHistogramCommand = new DelegateCommand(() =>
                {
                    ResetHistogramExecute();
                }));
            }
        }

        private DelegateCommand _backgroundSubtractionSelectionCommand;
        public ICommand BackgroundSubtractionSelectionCommand
        {
            get
            {
                return _backgroundSubtractionSelectionCommand ?? (_backgroundSubtractionSelectionCommand = new DelegateCommand(() =>
                {
                    BackgroundSubtractionSelectionExecute();
                }));
            }
        }

        private DelegateCommand<object> _backgroundSubtractionCommand;
        public ICommand BackgroundSubtractionCommand
        {
            get
            {
                return _backgroundSubtractionCommand ?? (_backgroundSubtractionCommand = new DelegateCommand<object>((level) =>
                {
                    BackgroundSubtractionExecute(level);
                }));
            }
        }

        private DelegateCommand<ComponentLevels> _increaseXCommand;
        public ICommand IncreaseXCommand
        {
            get
            {
                return _increaseXCommand ?? (_increaseXCommand = new DelegateCommand<ComponentLevels>((componentLevel) =>
                {
                    IncreaseXExecute(componentLevel);
                }));
            }
        }

        private DelegateCommand<ComponentLevels> _decreaseXCommand;
        public ICommand DecreaseXCommand
        {
            get
            {
                return _decreaseXCommand ?? (_decreaseXCommand = new DelegateCommand<ComponentLevels>((componentLevel) =>
                {
                    DecreaseXExecute(componentLevel);
                }));
            }
        }

        private DelegateCommand<ComponentLevels> _decreaseYCommand;
        public ICommand DecreaseYCommand
        {
            get
            {
                return _decreaseYCommand ?? (_decreaseYCommand = new DelegateCommand<ComponentLevels>((componentLevel) =>
                {
                    DecreaseYExecute(componentLevel);
                }));
            }
        }

        private DelegateCommand<ComponentLevels> _increaseYCommand;
        public ICommand IncreaseYCommand
        {
            get
            {
                return _increaseYCommand ?? (_increaseYCommand = new DelegateCommand<ComponentLevels>((componentLevel) =>
                {
                    IncreaseYExecute(componentLevel);
                }));
            }
        }

        private DelegateCommand<ComponentLevels> _resetXYCommand;
        public ICommand ResetXYCommand
        {
            get
            {
                return _resetXYCommand ?? (_resetXYCommand = new DelegateCommand<ComponentLevels>((componentLevel) =>
                {
                    ResetXYExecute(componentLevel);
                }));
            }
        }

        private DelegateCommand<ComponentLevels> _toggleChannelVisibilityCommand;
        public ICommand ToggleChannelVisibilityCommand
        {
            get
            {
                return _toggleChannelVisibilityCommand ?? (_toggleChannelVisibilityCommand = new DelegateCommand<ComponentLevels>((componentLevel) =>
                {
                    ToggleChannelVisibilityCommandExecute(componentLevel);
                }));
            }
        }

        private DelegateCommand _zStackCommand;
        public ICommand ZStackCommand
        {
            get
            {
                return _zStackCommand ?? (_zStackCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
                }));
            }
        }
        #endregion

        #region Private methods
        private void MouseLeftButtonDownExecute(object obj)
        {
            var args = obj as HistogramMouseEventArgs;
            var pd = args.ComponentLevels;
            double x = Normalize(args.Point.X);
            if (x < (pd.Low + pd.High) / 2.0)
                draggingLow = true;
            else
                draggingLow = false;
            dragging = pd;
            DoDrag(args.Point);
        }

        private void DoDrag(Point e)
        {
            int maxGrey = dragging.Histogram != null ? dragging.Histogram.MaximumGrey : 255;
            float x = Normalize(e.X);
            float one = 1 / maxGrey;
            if (draggingLow)
            {
                dragging.Low = Math.Max(0, x);
                dragging.Low = Math.Min(dragging.High - one, dragging.Low);
            }
            else
            {
                dragging.High = Math.Max(dragging.Low + one, x);
                dragging.High = Math.Min(maxGrey, dragging.High);
            }
        }

        private float Normalize(double x)
        {
            return (float)(x / 256);
        }
        private void OnFrameChange(object obj)
        {
            OnPropertyChanged("RendererLevels");
        }

        private void ResetHistogramExecute()
        {
            foreach (var c in this.RendererLevels)
            {
                // do not use SetLowHighValues because that would clear the 'setbyuser' flag and the histograms
                // would just get estimated again during the render.
                // TODO Renderer.BeginUpdates();
                c.Low = 0;
                c.High = 1;
                // TODO Renderer.EndUpdates();
            }
            _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
        }

        private void FitHistogramExecute()
        {
            foreach (var c in RendererLevels)
            {
                var est = c.EstimateHistogram();
                c.SetLowHighValues(est[0], est[1], true);
            }
            _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
        }

        private void IncreaseXExecute(ComponentLevels pd)
        {
            if (null == pd)
            {
                return;
            }
            var newOffset = RendererOptions.NudgeComponentX(pd.Component.Name, 1);
            pd.OffsetX = newOffset.X;
            _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
        }

        private void DecreaseXExecute(ComponentLevels pd)
        {
            if (null == pd)
            {
                return;
            }
            var newOffset = RendererOptions.NudgeComponentX(pd.Component.Name, -1);
            pd.OffsetX = newOffset.X;
            _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
        }

        private void IncreaseYExecute(ComponentLevels pd)
        {
            if (null == pd)
            {
                return;
            }
            var newOffset = RendererOptions.NudgeComponentY(pd.Component.Name, 1);
            pd.OffsetY = newOffset.Y;
            _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
        }

        private void DecreaseYExecute(ComponentLevels pd)
        {
            if (null == pd)
            {
                return;
            }
            var newOffset = RendererOptions.NudgeComponentY(pd.Component.Name, -1);
            pd.OffsetY = newOffset.Y;
            _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
        }

        private void ResetXYExecute(ComponentLevels pd)
        {
            if (null == pd)
            {
                return;
            }
            pd.OffsetX = 0;
            pd.OffsetY = 0;
            _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
        }

        private void ToggleChannelVisibilityCommandExecute(ComponentLevels pd)
        {
            if (pd != null)
            {
                // The channel visibility itself will already have been updated via the binding,
                // so just need to re-render the frame with the new settings.
                _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
            }
        }

        private void BackgroundSubtractionSelectionExecute()
        {
            foreach (var c in this.RendererLevels)
            {
                c.SetLowHighValues(c.Low, c.High, false);
            }
            _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
        }

        private void BackgroundSubtractionExecute(object obj)
        {
            int level = Convert.ToInt16(obj);
            foreach (var c in this.RendererLevels)
            {
                c.SetLowHighValues(c.Low, c.High, false);
            }
            RendererOptions.BackgroundLevel = level;
            _eventAggregator.GetEvent<RenderFrameEvent>().Publish(null);
        }

        public void Dispose()
        {
            _eventAggregator.GetEvent<RenderFrameEvent>().Unsubscribe(OnFrameChange);
            _eventAggregator = null;
            if (RendererLevels != null)
            {
                RendererLevels.Clear();
            }
            RendererOptions = null;
        }
        #endregion
    }
}
