﻿using Prism.Events;
using Prism.Mvvm;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Models;
using System.Windows.Media;

namespace ProbeCaseViewModule.ViewModels
{
    //This view model represents each thumbnail frame in Dual reader Frame Gallery
    class DualReaderFrameDetailsViewModel: BindableBase
    {
        #region Private Variables

        private IEventAggregator _eventAggregator;

        #endregion

        #region Constructor

        public DualReaderFrameDetailsViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }

        #endregion

        #region Properties

        private bool _isSelected;
        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                   _eventAggregator.GetEvent<CalculateTotalCellsEvent>().Publish(null);
                    OnPropertyChanged();
                }
            }
        }

        private FrameInfo _frame;
        public FrameInfo Frame
        {
            get
            {
                return _frame;
            }
            set
            {
                if (_frame != value)
                {
                    _frame = value;
                    OnPropertyChanged();
                }
            }
        }

        private ImageSource _thumbnail;
        public ImageSource Thumbnail
        {
            get
            {
                return _thumbnail;
            }
            set
            {
                if (_thumbnail != value)
                {
                    _thumbnail = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool _isAlreadySelected;
        public bool IsAlreadySelected
        {
            get
            {
                return _isAlreadySelected;
            }
            set
            {
                if (_isAlreadySelected != value)
                {
                    _isAlreadySelected = value;
                    OnPropertyChanged();
                }
            }
        }

        private long _cellCount;
        public long CellCount
        {
            get
            {
                return _cellCount;
            }
            set
            {
                if (_cellCount != value)
                {
                    _cellCount = value;
                    OnPropertyChanged();
                }
            }
        }

        #endregion
    }
}
