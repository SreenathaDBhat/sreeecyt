﻿using CytoApps.Infrastructure.UI.Events;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Models;
using ProbeCaseViewModule.Properties;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    /// This View Model is used to display Dual Reader Frame Gallery
    /// </summary>
    class DualReaderFramesGalleryViewModel : BindableBase
    {
        #region Private Variables

        private IEventAggregator _eventAggregator;

        #endregion

        #region Constructor

        public DualReaderFramesGalleryViewModel()
        {
            _eventAggregator = ServiceLocator.Current.TryResolve<IEventAggregator>();
            CytoApps.Infrastructure.UI.ApplicationCommands.HostCommands.ShutdownCommand.RegisterCommand(CloseDualReaderFramesCommand);

        }

        #endregion

        #region properties

        public ObservableCollection<FrameInfo> SelectedFrames
        {
            get { return new ObservableCollection<FrameInfo>(from f in _frameList where f.IsSelected == true select f.Frame); }
        }

        private ObservableCollection<DualReaderFrameDetailsViewModel> _frameList;
        public ObservableCollection<DualReaderFrameDetailsViewModel> FrameList
        {
            get { return _frameList; }
            set
            {
                if (_frameList != value)
                {
                    _frameList = value;
                    OnPropertyChanged();
                }
            }
        }

        private long _totalCells;
        public long TotalCells
        {
            get { return _totalCells; }
            set
            {
                if (_totalCells != value)
                {
                    _totalCells = value;
                    OnPropertyChanged();
                }
            }
        }

        public string CaseName
        {
            get;
            set;
        }

        public string SlideName
        {
            get;
            set;
        }

        public string ScoresName
        {
            get;
            set;
        }

        public string ScanAreaName
        {
            get;
            set;
        }

        /// <summary>
        /// Initialise the GridSize to default settings value.
        /// </summary>
        private int gridSize = ProbeCaseViewSettings.Default.FrameSize;

        /// <summary>
        /// Sets the GridSize value according to the User preference.
        /// </summary>
        public int GridSize
        {
            get { return gridSize; }
            set
            {
                if (value == 0)
                    return;

                gridSize = value;
                ProbeCaseViewSettings.Default.FrameSize = gridSize;
                ProbeCaseViewSettings.Default.Save();
                OnPropertyChanged();
            }
        }

        #endregion

        #region Commands

        private DelegateCommand _scoresPageCommand;
        public ICommand ScoresPageCommand
        {
            get
            {
                return _scoresPageCommand ?? (_scoresPageCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<DisplayScorePageEvent>().Publish(SelectedFrames);
                }));
            }
        }

        private DelegateCommand _navigateBackCommand;
        public ICommand NavigateBackCommand
        {
            get
            {
                return _navigateBackCommand ?? (_navigateBackCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<CloseDualReaderFramesGalleryEvent>().Publish(null);
                }));
            }
        }

        private DelegateCommand<ShutDownEventArgs> _closeDualReaderFramesCommand;
        public DelegateCommand<ShutDownEventArgs> CloseDualReaderFramesCommand
        {
            get
            {
                return _closeDualReaderFramesCommand ?? (_closeDualReaderFramesCommand = new DelegateCommand<ShutDownEventArgs>((args) =>
                {

                    args.CanClose = false;
                    _eventAggregator.GetEvent<CloseDualReaderFramesGalleryEvent>().Publish(null);
                }));
            }
        }

        public void Dispose()
        {
            CytoApps.Infrastructure.UI.ApplicationCommands.HostCommands.ShutdownCommand.UnregisterCommand(CloseDualReaderFramesCommand);
            _eventAggregator = null;
        }

        #endregion
    }
}
