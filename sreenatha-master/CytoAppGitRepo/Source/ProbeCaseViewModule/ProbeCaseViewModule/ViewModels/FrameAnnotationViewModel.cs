﻿using CytoApps.Infrastructure.Helpers;
using Prism.Mvvm;
using ProbeCaseViewModule.Logic;
using ProbeCaseViewModule.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    /// Contains the logic for adding, deleting & editing of annotation
    /// </summary>
    public class FrameAnnotationViewModel : BindableBase, IAnnotationContainer
    {
        private const int defaultContentOffset = 10;

        public FrameAnnotationViewModel(string frameId, List<ProbeCaseViewDataAccess.Models.Annotation> annotations)
        {
            _frameAnnotations = new ObservableCollection<FrameAnnotation>();
            _frameId = frameId;
            _frameAnnotations = (from frAnnotation in annotations select new FrameAnnotation(frAnnotation)).ToObservableCollection();
        }

        private ObservableCollection<FrameAnnotation> _frameAnnotations;

        private string _frameId;
        public string FrameId
        {
            get { return _frameId; }
            set
            {
                _frameId = value;
                OnPropertyChanged();
            }
        }

        public IEnumerable<FrameAnnotation> Annotations
        {
            get
            {
                return _frameAnnotations;
            }
        }

        public void DeleteAnnotation(FrameAnnotation annotation)
        {
            if (_frameAnnotations != null && _frameAnnotations.Count > 0)
            {
                _frameAnnotations.Where(a => a == annotation).FirstOrDefault().IsDeleted = true;
            }
        }

        public FrameAnnotation NewAnnotation(Point position, string annotationText = "annotation")
        {
            ProbeCaseViewDataAccess.Models.Annotation annotation = new ProbeCaseViewDataAccess.Models.Annotation()
            {
                Content = annotationText,
                X = position.X,
                Y = position.Y,
                Scale = 2,
                ContentOffsetX = defaultContentOffset,
                ContentOffsetY = defaultContentOffset,
                ShowArrowLine = true
            };
            FrameAnnotation frameAnnotation = new FrameAnnotation(annotation);
            _frameAnnotations.Add(frameAnnotation);
            OnPropertyChanged("Annotations");
            return frameAnnotation;
        }
    }
}
