﻿using CytoApps.Infrastructure.CommonEnums;
using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Localization;
using Microsoft.Practices.ServiceLocation;
using PCVImageRenderers.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using ProbeCaseViewDataAccess.Models;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Models;
using ProbeCaseViewModule.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    /// This ViewModel is used to display Frame and manage its functionalities
    /// </summary>
    public class FrameWindowViewModel : BindableBase
    {
        #region Private members
        private BitmapSource displayImage;
        private IEventAggregator _eventAggregator;
        private FrameAnnotation _annotationBeingPlaced;
        private Point _annotationBeingPlacedDropPoint;
        #endregion

        #region "Constructor"
        public FrameWindowViewModel(SharedViewModel sharedVm)
        {
            _eventAggregator = ServiceLocator.Current.TryResolve<IEventAggregator>();
            _sharedViewModel = sharedVm;
            _componentDisplayOptionsVM = new ComponentDisplayOptionsViewModel();
            _analysisRegionSurfaceVM = new AanalysisRegionSurfaceViewModel(_sharedViewModel);
            _slideOverviewVM = new SlideOverviewViewModel(this);
            IsAssayExist = CheckAssayisExist();
        }

        #endregion "Constructor"

        #region Properties
        public Point MousePosition { get; set; }

        private AanalysisRegionSurfaceViewModel _analysisRegionSurfaceVM;

        public AanalysisRegionSurfaceViewModel AnalysisRegionSurfaceVM
        {
            get
            {
                return _analysisRegionSurfaceVM;
            }
            set
            {
                _analysisRegionSurfaceVM = value;
                OnPropertyChanged();
            }
        }

        public FrameAnnotationViewModel CurrentFrameAnnotation
        {
            get
            {
                return SharedVM.AllFrameAnnotations.Where(frameAnnotation => frameAnnotation.FrameId == SharedVM.CurrentFrame.Image.Id.ToString()).FirstOrDefault();
            }
        }

        public string WindowTitle
        {
            get
            {
                return Application.Current.MainWindow.Title + " - " + SelectedSession.Slide.CaseId + " : " + SelectedSession.Slide.Name + " : " + SelectedSession.ScanAreaId + ": " + SelectedSession.Name;
            }
        }


        private SharedViewModel _sharedViewModel;
        public SharedViewModel SharedVM
        {
            get
            {
                return _sharedViewModel;
            }
            set
            {
                _sharedViewModel = value;
                OnPropertyChanged();
            }
        }

        private ComponentDisplayOptionsViewModel _componentDisplayOptionsVM;
        public ComponentDisplayOptionsViewModel ComponentDisplayOptionsVM
        {
            get
            {
                return _componentDisplayOptionsVM;
            }
            set
            {
                _componentDisplayOptionsVM = value;
                OnPropertyChanged();
            }
        }

        private SlideOverviewViewModel _slideOverviewVM;
        public SlideOverviewViewModel SlideOverviewVM
        {
            get
            {
                return _slideOverviewVM;
            }
            set
            {
                _slideOverviewVM = value;
                OnPropertyChanged();
            }
        }

        public double FrameWidth
        {
            get
            {
                return SharedVM.CurrentFrame != null ? SharedVM.CurrentFrame.Image.Width : 0;
            }
        }

        public double FrameHeight
        {
            get
            {
                return SharedVM.CurrentFrame != null ? SharedVM.CurrentFrame.Image.Height : 0;
            }
        }

        public Session SelectedSession
        {
            get
            {
                return _sharedViewModel.SelectedSession;
            }
        }

        // TODO - revisit - find better way instead of  wrapping properties.
        public string CaseId
        {
            get
            {
                return SelectedSession.Slide.CaseId;
            }
        }

        // TODO - revisit - find better way instead of  wrapping properties.
        public string SlideName
        {
            get
            {
                return SelectedSession.Slide.Name + " : " + SelectedSession.ScanAreaId; //TODO HACK quick and dirty Fix need to chagne
            }
        }

        public BitmapSource DisplayImage
        {
            get
            {
                return displayImage;
            }
            set
            {
                displayImage = value;
                if (displayImage == null)
                {
                    OnPropertyChanged();
                    return;
                }

                // HACK, refactor, weird stuff for set heights in local DAL, not much use in remote situ
                if (SharedVM.CurrentFrame != null)
                {
                    SharedVM.CurrentFrame.FrameWidth = (int)displayImage.Width;
                    SharedVM.CurrentFrame.FrameHeight = (int)displayImage.Height;
                }

                OnPropertyChanged();
                OnPropertyChanged("ScoresForCurrentFrame");
            }
        }

        public bool _isChannelPopupOpen;
        public bool IsChannelPopupOpen
        {
            get
            {
                return _isChannelPopupOpen;
            }
            set
            {
                _isChannelPopupOpen = value;
                OnPropertyChanged();
            }
        }

        public IEnumerable<Score> AllScores
        {
            get
            {
                return from s in _sharedViewModel.ScoredCells select s.Score;
            }
        }

        public IEnumerable<ScoredCell> ScoresForCurrentFrame
        {
            get
            {
                if (SharedVM.CurrentFrame.Image == null)
                {
                    return new ScoredCell[0];
                }
                else
                {
                    return ScoresForFrame(SharedVM.CurrentFrame.Image);
                }
            }
        }

        public IEnumerable<ScoredCell> ScoresForFrame(Frame frame)
        {
            return _sharedViewModel.ScoredCells.Where(s => s.FrameId == frame.Id && !s.IsDeleted);
        }

        public double ScoreSize
        {
            get
            {
                return ProbeCaseViewSettings.Default.ScoreSize;
            }
            set
            {
                var v = Math.Max(70, Math.Min(300, value));
                ProbeCaseViewSettings.Default.ScoreSize = v;
                OnPropertyChanged("ScoreSize");
                OnPropertyChanged("ScoreSizeUiOffset");
            }
        }

        public double ScoreSizeUiOffset
        {
            get
            {
                return ScoreSize / -2;
            }
        }

        private bool _showAllScores = true;
        public bool ShowAllScores
        {
            get { return _showAllScores; }
            set
            {
                _showAllScores = value;
                OnPropertyChanged();
            }
        }

        //Assay Properties TODO Re-visit Populate Assay View using Prism Navigation
        public bool ShowSelectAssayButton
        {
            get
            {
                return SharedVM.CurrentAssay == null; // TODO  && scores.Count == 0;
            }
        }

        public bool ShowAssayMissingButton
        {
            get
            {
                return SharedVM.CurrentAssay == null; // TODO  && scores.Count > 0;
            }
        }

        public bool MoreThanTenClasses
        {
            get
            {
                return SharedVM.CurrentAssay != null && SharedVM.CurrentAssay.Classes.Count() > 10;
            }
        }

        public int ZLevelsMinusOne
        {
            get
            {
                return SharedVM.CurrentFrame.Image.ZLevelsMinusOne;
            }
        }

        public bool HasZStack
        {
            get
            {
                return SharedVM.CurrentFrame.Image.HasZStacks;
            }
        }

        private bool _isResultsPopupOpen;
        public bool IsResultsPopupOpen
        {
            get
            {
                return _isResultsPopupOpen;
            }
            set
            {
                _isResultsPopupOpen = value;
                OnPropertyChanged();
            }
        }

        private bool _isAnnotationPanelOpen;
        public bool IsAnnotationPanelOpen
        {
            get { return _isAnnotationPanelOpen; }
            set
            {
                _isAnnotationPanelOpen = value;
                OnPropertyChanged();
            }
        }

        private bool _isImageToolsPanelOpen;
        public bool IsImageToolsPanelOpen
        {
            get
            {
                return _isImageToolsPanelOpen;
            }
            set
            {
                _isImageToolsPanelOpen = value;
                OnPropertyChanged();
            }
        }

        private bool _isAssayPopupOpen;
        public bool IsAssayPopupOpen
        {
            get
            {
                return _isAssayPopupOpen;
            }
            set
            {
                _isAssayPopupOpen = value;
                OnPropertyChanged();
            }
        }

        private bool _isNewAssayPopupOpen;
        public bool IsNewAssayPopupOpen
        {
            get
            {
                return _isNewAssayPopupOpen;
            }
            set
            {
                _isNewAssayPopupOpen = value;
                OnPropertyChanged();
            }
        }

        private bool _isSaveAssayPopupOpen;
        public bool IsSaveAssayPopupOpen
        {
            get
            {
                return _isSaveAssayPopupOpen;
            }
            set
            {
                _isSaveAssayPopupOpen = value;
                OnPropertyChanged();
            }
        }

        private bool _isLoadAssayPopupOpen;
        public bool IsLoadAssayPopupOpen
        {
            get
            {
                return _isLoadAssayPopupOpen;
            }
            set
            {
                _isLoadAssayPopupOpen = value;
                OnPropertyChanged();
            }
        }


        private string _newAssayName;
        public string NewAssayName
        {
            get
            {
                return _newAssayName;
            }
            set
            {
                _newAssayName = value;
                IsValidAssayName = CheckValidAssayName(_newAssayName);
                OnPropertyChanged();
            }
        }

        private bool _isValidAssayName = false;
        public bool IsValidAssayName
        {
            get
            {
                return _isValidAssayName;
            }
            set
            {
                _isValidAssayName = value;
                OnPropertyChanged();
            }
        }

        private bool showOverview = false;
        public bool ShowOverview
        {
            get { return showOverview; }
            set
            {
                showOverview = value;
                OnPropertyChanged();
            }
        }

        public bool ShowAnnotation
        {
            get { return ProbeCaseViewSettings.Default.DisplayAnnotation; }
            set
            {
                ProbeCaseViewSettings.Default.DisplayAnnotation = value;
                OnPropertyChanged();
            }
        }

        private string _newAnnotationText;
        public string NewAnnotationText
        {
            get { return _newAnnotationText; }
            set
            {
                _newAnnotationText = value;
                OnPropertyChanged();
                _createCommonAnnotationCommand.RaiseCanExecuteChanged();
            }
        }

        private string _selectedSavedAnnotationText;
        public string SelectedSavedAnnotationText
        {
            get { return _selectedSavedAnnotationText; }
            set
            {
                _selectedSavedAnnotationText = value;
                OnPropertyChanged();
                _addSelectedCommonAnnotation.RaiseCanExecuteChanged();
            }
        }

        private double circleDiameterPixels = 100;
        public double CircleDiameterPixels
        {
            get { return circleDiameterPixels; }
            set
            {
                circleDiameterPixels = value;
                OnPropertyChanged();
            }
        }

        private bool _flipSlideOverview;
        public bool FlipSlideOverview
        {
            get { return _flipSlideOverview; }
            set { _flipSlideOverview = value; }
        }


        private List<string> _availebleAssays;
        public List<string> AvailebleAssays
        {
            get
            {
                return _availebleAssays;
            }
            set
            {
                _availebleAssays = value;
                OnPropertyChanged();
            }
        }

        private string _selectedAssay;

        public string SelectedAssay
        {
            get
            {
                return _selectedAssay;
            }
            set
            {
                _selectedAssay = value;
                OnPropertyChanged();
            }
        }

        private string _newSaveAsAssayName;

        public string NewSaveAsAssayName
        {
            get
            {
                return _newSaveAsAssayName;
            }
            set
            {
                _newSaveAsAssayName = value;
                IsValidAssayName = CheckValidAssayName(_newSaveAsAssayName);
                OnPropertyChanged();
            }
        }


        private int _reprocessCurrent;
        public int ReprocessCurrent
        {
            get
            {
                return _reprocessCurrent;
            }
            set
            {
                _reprocessCurrent = value;
                OnPropertyChanged();
            }
        }

        private int _reprocessMaximum;
        public int ReprocessMaximum
        {
            get
            {
                return _reprocessMaximum;
            }
            set
            {
                _reprocessMaximum = value;
                OnPropertyChanged();
            }
        }

        private bool _canReprocessCurrentFrame;
        public bool CanReprocessCurrentFrame
        {
            get
            {
                return _canReprocessCurrentFrame;
            }
            set
            {
                _canReprocessCurrentFrame = value;
                OnPropertyChanged();
            }
        }


        private bool _CanReprocessAllFrames;
        public bool CanReprocessAllFrames
        {
            get
            {
                return _CanReprocessAllFrames;
            }
            set
            {
                _CanReprocessAllFrames = value;
                OnPropertyChanged();
            }
        }

        private bool _isAssayExist;
        public bool IsAssayExist
        {
            get
            {
                return _isAssayExist;
            }
            set
            {
                _isAssayExist = value;
                OnPropertyChanged();
            }
        }    

        #endregion

        #region Commands

        private DelegateCommand<CancelEventArgs> _closeCommand;
        public ICommand CloseCommand
        {
            get
            {
                return _closeCommand ?? (_closeCommand = new DelegateCommand<CancelEventArgs>((cancelArgs) =>
                {
                    if (cancelArgs != null)
                    {
                        cancelArgs.Cancel = true;
                    }
                    _eventAggregator.GetEvent<CloseFrameWindowEvent>().Publish(null);
                }));
            }
        }


        private DelegateCommand _toggleSlideOverviewCommand;
        public ICommand ToggleSlideOverviewCommand
        {
            get
            {
                return _toggleSlideOverviewCommand ?? (_toggleSlideOverviewCommand = new DelegateCommand(() =>
                {
                    ShowOverview = !ShowOverview && _slideOverviewVM.OverViewEnabled && _slideOverviewVM.IsViewable;
                }));
            }
        }

        private DelegateCommand<AnnotationEventArgs> _onAnnotationLeftMouseButtonDownCommand;
        public ICommand OnAnnotationLeftMouseButtonDownCommand
        {
            get
            {
                return _onAnnotationLeftMouseButtonDownCommand ?? (_onAnnotationLeftMouseButtonDownCommand = new DelegateCommand<AnnotationEventArgs>((annotationArgs) =>
                {
                    if (annotationArgs.IsValid)
                    {
                        if (annotationArgs.IsMouseCapture)
                        {
                            _annotationBeingPlaced = CurrentFrameAnnotation.NewAnnotation(annotationArgs.CurrentMousePosition);
                            _annotationBeingPlacedDropPoint = annotationArgs.WindowMousePosition;
                            ShowAnnotation = true;
                            SharedVM.IsDirty = true;
                        }
                    }
                }));
            }
        }

        private DelegateCommand<AnnotationEventArgs> _onAnnotationMouseMoveCommand;
        public ICommand OnAnnotationMouseMoveCommand
        {
            get
            {
                return _onAnnotationMouseMoveCommand ?? (_onAnnotationMouseMoveCommand = new DelegateCommand<AnnotationEventArgs>((annotationArgs) =>
                {
                    if (!annotationArgs.IsMouseCapture || _annotationBeingPlaced == null)
                        return;
                    if (Math.Abs(annotationArgs.WindowMousePosition.X - _annotationBeingPlacedDropPoint.X) > 10 ||
                    Math.Abs(annotationArgs.WindowMousePosition.Y - _annotationBeingPlacedDropPoint.Y) > 10)
                    {
                        _annotationBeingPlaced.ContentOffsetX = annotationArgs.WindowMousePosition.X - _annotationBeingPlacedDropPoint.X;
                        _annotationBeingPlaced.ContentOffsetY = annotationArgs.WindowMousePosition.Y - _annotationBeingPlacedDropPoint.Y;
                    }
                }));
            }
        }

        private DelegateCommand<object> _onAnnotationLeftMouseButtonUpCommand;
        public ICommand OnAnnotationLeftMouseButtonUpCommand
        {
            get
            {
                return _onAnnotationLeftMouseButtonUpCommand ?? (_onAnnotationLeftMouseButtonUpCommand = new DelegateCommand<object>((annotationArgs) =>
                {
                    _annotationBeingPlaced = null;
                }));
            }
        }

        private DelegateCommand<string> _createCommonAnnotationCommand;
        public ICommand CreateCommonAnnotationCommand
        {
            get
            {
                return _createCommonAnnotationCommand ?? (_createCommonAnnotationCommand = new DelegateCommand<string>((annotationText) =>
                 {
                     if (SharedVM.CommonAnnotations != null && !SharedVM.CommonAnnotations.Contains(annotationText))
                     {
                         SharedVM.CommonAnnotations.Add(annotationText);
                         NewAnnotationText = string.Empty;
                     }
                 }, CanCreateStoredAnnotation));
            }
        }

        private bool CanCreateStoredAnnotation(string annotationText)
        {
            return annotationText != null && !string.IsNullOrWhiteSpace(annotationText) && SharedVM != null && !SharedVM.CommonAnnotations.Contains(annotationText);
        }

        private DelegateCommand<string> _deleteCommonAnnotationCommand;
        public ICommand DeleteCommonAnnotationCommand
        {
            get
            {
                return _deleteCommonAnnotationCommand ?? (_deleteCommonAnnotationCommand = new DelegateCommand<string>((annotationText) =>
                {
                    if (SharedVM.CommonAnnotations != null && SharedVM.CommonAnnotations.Contains(annotationText))
                    {
                        SharedVM.CommonAnnotations.Remove(annotationText);
                    }
                }));
            }
        }

        private DelegateCommand<string> _addSelectedCommonAnnotation;
        public ICommand AddSelectedCommonAnnotation
        {
            get
            {
                return _addSelectedCommonAnnotation ?? (_addSelectedCommonAnnotation = new DelegateCommand<string>((annotationText) =>
                {
                    var centerScreen = new Point(SharedVM.CurrentFrame.FrameWidth / 2, SharedVM.CurrentFrame.FrameHeight / 2);
                    CurrentFrameAnnotation.NewAnnotation(centerScreen, annotationText);
                    ShowAnnotation = true;
                    SharedVM.IsDirty = true;
                }, CanAddAnnotationtoFrame));
            }
        }

        private bool CanAddAnnotationtoFrame(string annotationString)
        {
            return annotationString != null && !string.IsNullOrWhiteSpace(annotationString);
        }

        private DelegateCommand _editCommonAnnotationsCommand;
        public ICommand EditCommonAnnotationsCommand
        {
            get
            {
                return _editCommonAnnotationsCommand ?? (_editCommonAnnotationsCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<DisplayStoredAnnotationViewEvent>().Publish(null);
                }));
            }
        }


        private DelegateCommand _nextFrameCommand;
        public DelegateCommand NextFrameCommand
        {
            get
            {
                return _nextFrameCommand ?? (_nextFrameCommand = new DelegateCommand(() =>
                {
                    MovetoNextFrame();
                }, CanMovetoNextFrame));
            }
        }

        public void RefreshCanExecute()
        {
            NextFrameCommand.RaiseCanExecuteChanged();
            PreviousFrameCommand.RaiseCanExecuteChanged();
        }

        private DelegateCommand _previousFrameCommand;
        public DelegateCommand PreviousFrameCommand
        {
            get
            {
                return _previousFrameCommand ?? (_previousFrameCommand = new DelegateCommand(() =>
                {
                    MovetoPreviousFrame();
                }, CanMovetoPreviousFrame));
            }
        }

        private DelegateCommand _clearScoresFromFrameCommand;
        public ICommand ClearScoresFromFrameCommand
        {
            get
            {
                return _clearScoresFromFrameCommand ?? (_clearScoresFromFrameCommand = new DelegateCommand(() =>
                {
                    ExecuteClearScoresFromFrame();
                }, CheckNoReprocessing));
            }
        }

        private DelegateCommand _clearScoresFromSessionCommand;
        public ICommand ClearScoresFromSessionCommand
        {
            get
            {
                return _clearScoresFromSessionCommand ?? (_clearScoresFromSessionCommand = new DelegateCommand(() =>
                {
                    ClearScoresFromSession();
                }, CheckNoReprocessing));
            }
        }

        private DelegateCommand<object> _mouseMoveCommand;

        public ICommand MouseMoveCommand
        {
            get
            {
                return _mouseMoveCommand ?? (_mouseMoveCommand = new DelegateCommand<object>((mousePosition) =>
                {
                    MousePosition = (Point)mousePosition;
                }));
            }
        }

        private DelegateCommand<object> _mouseRightButtonDownCommand;
        public ICommand MouseRightButtonDownCommand
        {
            get
            {
                return _mouseRightButtonDownCommand ?? (_mouseRightButtonDownCommand = new DelegateCommand<object>((selectedScore) =>
                {
                    ExecuteMouseRightDown(selectedScore);

                }));
            }
        }

        private DelegateCommand<ScoredCell> _mouseLeftButtonDownCommand;
        public ICommand MouseLeftButtonDownCommand
        {
            get
            {
                return _mouseLeftButtonDownCommand ?? (_mouseLeftButtonDownCommand = new DelegateCommand<ScoredCell>((selectedScore) =>
                {

                    ClearHighlightedCells();
                    selectedScore.IsHighlighted = true;
                    _eventAggregator.GetEvent<SelectedFrameScoreEvent>().Publish(selectedScore);
                }));
            }
        }

        private DelegateCommand<object> _keyDownCommand;
        public ICommand KeyDownCommand
        {
            get
            {
                return _keyDownCommand ?? (_keyDownCommand = new DelegateCommand<object>((keyEventArgs) =>
                {
                    // If TextBox has focus we need to pass control to textbox
                    if (Keyboard.FocusedElement is System.Windows.Controls.TextBox)
                    {
                        return;
                    }
                    else
                    {
                        var args = (KeyEventArgs)keyEventArgs;
                        OnKeyDown(args);
                        args.Handled = true;
                    }
                }));
            }
        }
        //TO-DO After all slider popup implementation move to properties
        private DelegateCommand<object> _togglePopupCommand;
        public ICommand TogglePopupCommand
        {
            get
            {
                return _togglePopupCommand ?? (_togglePopupCommand = new DelegateCommand<object>((selectedPopupCheckbox) =>
                {
                    if (selectedPopupCheckbox != null)
                    {
                        IsNewAssayPopupOpen = false;
                        IsLoadAssayPopupOpen = false;
                        IsSaveAssayPopupOpen = false;
                        switch (selectedPopupCheckbox.ToString())
                        {
                            case "Annotations":
                                IsChannelPopupOpen = false;
                                IsResultsPopupOpen = false;
                                IsAssayPopupOpen = false;
                                IsImageToolsPanelOpen = false;
                                break;
                            case "Results":
                                IsChannelPopupOpen = false;
                                IsAssayPopupOpen = false;
                                IsAnnotationPanelOpen = false;
                                IsImageToolsPanelOpen = false;
                                break;
                            case "Channels":
                                IsResultsPopupOpen = false;
                                IsAnnotationPanelOpen = false;
                                IsAssayPopupOpen = false;
                                IsImageToolsPanelOpen = false;
                                break;
                            case "Image":
                                IsChannelPopupOpen = false;
                                IsResultsPopupOpen = false;
                                IsAnnotationPanelOpen = false;
                                IsAssayPopupOpen = false;
                                break;
                            case "Assay":
                                IsChannelPopupOpen = false;
                                IsAnnotationPanelOpen = false;
                                IsResultsPopupOpen = false;
                                IsImageToolsPanelOpen = false;
                                break;
                            default: return;
                        }
                    }
                }));
            }
        }

        private DelegateCommand<object> _assayOpenCommand;
        public ICommand AssayOpenCommand
        {
            get
            {
                return _assayOpenCommand ?? (_assayOpenCommand = new DelegateCommand<object>((selectedParameter) =>
                {

                    if (selectedParameter != null)
                    {
                        HidePopups();
                        NewAssayName = string.Empty;
                        switch (selectedParameter.ToString().ToLower())
                        {
                            case "new":
                                IsNewAssayPopupOpen = true;
                                break;
                            case "load":
                                _eventAggregator.GetEvent<GetComptibleAssaysEvent>().Publish(null);
                                IsLoadAssayPopupOpen = true;
                                break;
                            case "save":
                                NewSaveAsAssayName = SharedVM.CurrentAssay.Name;
                                IsSaveAssayPopupOpen = true;
                                break;
                        }
                    }
                }, canAssayOpenCommand));
            }
        }

        private bool canAssayOpenCommand(object arg)
        {
            return !SharedVM.IsReprocessing;

        }

        private DelegateCommand _openSpotConfigurationCommand;
        public DelegateCommand OpenSpotConfigurationCommand
        {
            get
            {
                return _openSpotConfigurationCommand ?? (_openSpotConfigurationCommand = new DelegateCommand(() =>
                  {
                      OpenSpotConfiguration();
                  }, CheckNoReprocessing));
            }
        }

        private DelegateCommand _createAssayCommand;
        public ICommand CreateAssayCommand
        {
            get
            {
                return _createAssayCommand ?? (_createAssayCommand = new DelegateCommand(() =>
                {
                    IsNewAssayPopupOpen = false;
                    NewAssayName = NewAssayName.Trim();
                    _eventAggregator.GetEvent<CreateNewAssayEvent>().Publish(NewAssayName);

                }, CheckNoReprocessing));
            }
        }



        private DelegateCommand _cancelAssayCommand;
        public ICommand CancelAssayCommand
        {
            get
            {
                return _cancelAssayCommand ?? (_cancelAssayCommand = new DelegateCommand(() =>
                {
                    NewAssayName = string.Empty;
                    HidePopups();

                }));
            }
        }


        private DelegateCommand _loadAssayCommand;
        public ICommand LoadAssayCommand
        {
            get
            {
                return _loadAssayCommand ?? (_loadAssayCommand = new DelegateCommand(() =>
                    {
                        IsLoadAssayPopupOpen = false;
                        _eventAggregator.GetEvent<LoadAssayEvent>().Publish(SelectedAssay);
                    }, CheckNoReprocessing));
            }
        }


        private DelegateCommand _saveAsAssayCommand;
        public ICommand SaveAsAssayCommand
        {
            get
            {
                return _saveAsAssayCommand ?? (_saveAsAssayCommand = new DelegateCommand(() =>

                {
                    IsSaveAssayPopupOpen = false;
                    NewSaveAsAssayName = NewSaveAsAssayName.Trim();
                    _eventAggregator.GetEvent<SaveAsAssayEvent>().Publish(NewSaveAsAssayName);

                }, CheckNoReprocessing));
            }
        }


        private DelegateCommand<object> _keyUpCommand;
        public ICommand KeyUpCommand
        {
            get
            {
                return _keyUpCommand ?? (_keyUpCommand = new DelegateCommand<object>((keyEventArgs) =>
                {
                    // If TextBox has focus we need to pass control to textbox
                    if (Keyboard.FocusedElement is System.Windows.Controls.TextBox)
                    {
                        return;
                    }
                    else
                    {
                        if ((Key)keyEventArgs == Key.LeftShift || (Key)keyEventArgs == Key.RightShift)
                            AnalysisRegionSurfaceVM.ShiftPressed = false;
                    }
                }));
            }
        }

        private DelegateCommand _reProcessCommand;
        public ICommand ReprocessCommand
        {
            get
            {
                return _reProcessCommand ?? (_reProcessCommand = new DelegateCommand(() =>
                {
                    HidePopups();
                    CanReprocessAllFrames = true;
                    _eventAggregator.GetEvent<ReprocessEvent>().Publish(true);
                }, CheckNoReprocessing));
            }
        }



        private DelegateCommand _reProcessCurrentFrameCommand;
        public ICommand ReProcessCurrentFrameCommand
        {
            get
            {
                return _reProcessCurrentFrameCommand ?? (_reProcessCurrentFrameCommand = new DelegateCommand(() =>
                {

                    HidePopups();
                    CanReprocessCurrentFrame = true;
                    _eventAggregator.GetEvent<ReprocessEvent>().Publish(false);
                }, CheckNoReprocessing));
            }
        }
        #endregion

        #region  "Methods"

        private void HidePopups()
        {
            IsAssayPopupOpen = false;
            IsNewAssayPopupOpen = false;
            IsLoadAssayPopupOpen = false;
            IsSaveAssayPopupOpen = false;
        }
        private void OnKeyDown(KeyEventArgs args)
        {
            switch (args.Key)
            {
                case Key.LeftShift:
                case Key.RightShift:
                    AnalysisRegionSurfaceVM.ShiftPressed = true;
                    break;
                case Key.Up:
                    if (ShowOverview)
                    {
                        Navigate(NavigationDirection.Up);
                    }
                    break;
                case Key.Down:
                    if (ShowOverview)
                    {
                        Navigate(NavigationDirection.Down);
                    }
                    break;
                case Key.Left:
                    if (ShowOverview)
                    {
                        Navigate(NavigationDirection.Left);
                    }
                    else
                    {
                        if (((null != SharedVM.Frames) || (null != SharedVM.CurrentFrame)) ? SharedVM.Frames.IndexOf(SharedVM.CurrentFrame) > 0 : false)
                        {
                            MovetoPreviousFrame();
                        }
                    }
                    break;
                case Key.Right:
                    if (ShowOverview)
                    {
                        Navigate(NavigationDirection.Right);
                    }
                    else
                    {
                        if (((null != SharedVM.Frames) || (null != SharedVM.CurrentFrame)) ? SharedVM.Frames.IndexOf(SharedVM.CurrentFrame) < SharedVM.Frames.Count() - 1 : false)
                        {
                            MovetoNextFrame();
                        }
                    }
                    break;
                case Key.Enter:
                    ShowOverview = !ShowOverview && _slideOverviewVM.OverViewEnabled;
                    break;
                case Key.A:
                    ShowAllScores = !ShowAllScores;
                    break;
                case Key.P:
                    //TODO - after shared view model implemented
                    ComponentDisplayOptionsVM.MaximumProjection = !ComponentDisplayOptionsVM.MaximumProjection;
                    break;
                case Key.R:
                    SharedVM.CurrentFrame.IsStarred = !SharedVM.CurrentFrame.IsStarred;
                    break;
                case Key.T:
                    if (Keyboard.IsKeyDown(Key.RightCtrl) || Keyboard.IsKeyDown(Key.LeftCtrl))
                        ShowAnnotation = !ShowAnnotation;
                    break;
                case Key.E: //shortcut key for Export frame image
                    if (Keyboard.IsKeyDown(Key.RightCtrl) || Keyboard.IsKeyDown(Key.LeftCtrl))
                        ExportFrameImageCommand.Execute(null);
                    break;
                case Key.Space:
                    //TODO-Need shared viewmodel
                    // FrameClosingCommand.Execute();
                    break;
                default:
                    if ((args.Key == Key.System && args.SystemKey == Key.F4) || (args.Key == Key.F4 && Keyboard.IsKeyDown(Key.RightAlt)))
                    {
                        _eventAggregator.GetEvent<CloseFrameWindowEvent>().Publish(null);
                    }
                    else
                    {
                        var IsManualScored = AddScoreUsingShortcut(args.Key);
                        if (IsManualScored)
                        {
                            UpdateFrame();
                        }
                    }
                    break;
            }
        }

        public bool CheckNoReprocessing()
        {
            return !SharedVM.IsReprocessing;
        }


        /// <summary>
        /// Check the assay file exist if not disable the drawing regions, open spot config window and disable reprocess buttons
        /// </summary>
        /// <returns></returns>
        private bool CheckAssayisExist()
        {
            bool isValid = true;
            if (SharedVM.CurrentAssay == null ||
                string.IsNullOrEmpty(SharedVM.CurrentAssay.Name) ||
                CytoApps.SpotConfiguration.AssayInfo.IsCEPXYAssayName(SharedVM.CurrentAssay.Name))
            {
                isValid = false;
            }
            return isValid;
        }

        private void OpenSpotConfiguration()
        {
            _eventAggregator.GetEvent<OpenSpotConfigWindowEvent>().Publish(false);
        }

        private void ClearHighlightedCells()
        {
            // can we be more specific here?
            foreach (var c in _sharedViewModel.ScoredCells)
                c.IsHighlighted = false;
        }

        private void ExecuteMouseRightDown(object args)
        {
            var selectedScore = args as ScoredCell;
            selectedScore.IsDeleted = true;           
            if (selectedScore.Region != null)
            {
                AnalysisRegionInfo analisysRegion = AnalysisRegionSurfaceVM.Regions.Where(x => x.ID == selectedScore.Score.RegionId).FirstOrDefault();
                if (analisysRegion != null)
                {
                    AnalysisRegionSurfaceVM.RemoveRegion(analisysRegion);                   
                }

            }
            //delete the regions has not scored
            foreach (AnalysisRegionInfo analysisRegionInfo in AnalysisRegionSurfaceVM.Regions.ToList())
            {
                if (analysisRegionInfo.IsRegionScored == false)
                {
                    AnalysisRegionSurfaceVM.Regions.Remove(analysisRegionInfo);                   
                }
            }
            OnPropertyChanged("ScoresForCurrentFrame");
            _eventAggregator.GetEvent<ScoresUpdatedEvent>().Publish(false);
        }

        public void ClearScoresFromSession() // TODO - Need to verify : clear Session or Slide
        {
            SharedVM.IsDirty = true;
            foreach (var score in _sharedViewModel.ScoredCells.ToList())
            {
                score.IsDeleted = true;
            }
            ClearRegionsForSession();
            OnPropertyChanged("ScoresForCurrentFrame");

            _eventAggregator.GetEvent<ScoresUpdatedEvent>().Publish(false);
        }


        private void ExecuteClearScoresFromFrame()
        {
            SharedVM.IsDirty = true;

            foreach (var score in _sharedViewModel.ScoredCells.Where(s => s.FrameId == SharedVM.CurrentFrame.Image.Id).ToList())
            {
                score.IsDeleted = true;                
            }
            foreach (AnalysisRegionInfo region in AnalysisRegionSurfaceVM.Regions)
            {
                region.CanDelete = true;
            }
            AnalysisRegionSurfaceVM.Regions.Clear();
            OnPropertyChanged("ScoresForCurrentFrame");

            _eventAggregator.GetEvent<ScoresUpdatedEvent>().Publish(false);
        }


        private bool CheckValidAssayName(string assayName)
        {
            assayName = assayName.Trim();
            bool validAssayName = false;
            if (string.IsNullOrWhiteSpace(assayName))
                IsValidAssayName = false;
            else
                IsValidAssayName = true;
            if (assayName.Length > 0 && !(CytoApps.SpotConfiguration.AssayInfo.IsCEPXYAssayName(assayName)))
            {
                // Name can't include any characters that are invalid for a filename, or that would need escaping in XML.
                // Other invalid characters are there for historical reasons.
                // Invalid characters should be the same as those in AssaySelectionWindow.OnNewAssay(), in WinCV solution.
                char[] blackList = new char[] { '{', '}', '!', '"', '\'', '$', '%', '^', '&', '*', '(', ')', '[', ']', '@', ':', ';', '~', '#', '<', '>', ',', '?', '/', '\\', '=', '|' };
                if (assayName.IndexOfAny(blackList) < 0)
                    validAssayName = true;
            }
            return validAssayName;
        }
        private bool CanMovetoNextFrame()
        {
            return ((null != SharedVM.Frames) || (null != SharedVM.CurrentFrame)) ? SharedVM.Frames.IndexOf(SharedVM.CurrentFrame) < SharedVM.Frames.Count() - 1 : false;
        }

        public void MovetoNextFrame()
        {
            var list = SharedVM.Frames.ToList();
            SharedVM.CurrentFrame = list[list.IndexOf(SharedVM.CurrentFrame) + 1];
        }

        private bool CanMovetoPreviousFrame()
        {
            return ((null != SharedVM.Frames) || (null != SharedVM.CurrentFrame)) ? SharedVM.Frames.IndexOf(SharedVM.CurrentFrame) > 0 : false;
        }

        public void MovetoPreviousFrame()
        {
            var list = SharedVM.Frames.ToList();
            SharedVM.CurrentFrame = list[list.IndexOf(SharedVM.CurrentFrame) - 1];
        }

        public void UpdateFrame()
        {
            OnPropertyChanged("AllScores");
            OnPropertyChanged("MoreThanTenClasses");
            OnPropertyChanged("ScoresForCurrentFrame");
            OnPropertyChanged("ComponentDisplayOptionsVM");
            if (SharedVM.AllFrameAnnotations != null)
                OnPropertyChanged("CurrentFrameAnnotation");
        }

        private void Navigate(NavigationDirection navigate)
        {
            var currentCentre = new Point(SlideOverviewVM.CurrentFrameOutline.CentreX, SlideOverviewVM.CurrentFrameOutline.CentreY);
            var closest = GetClosestNavigatableFrame(currentCentre, navigate);
            if (closest != null)
            {
                SharedVM.CurrentFrame = closest;
                _eventAggregator.GetEvent<ZoomAndPanToCurrentFrameEvent>().Publish(SharedVM.CurrentFrame);
            }
        }

        private FrameInfo GetClosestNavigatableFrame(Point pt, NavigationDirection direction)
        {
            // we've rotate the whole slide so up is now down
            if (direction == NavigationDirection.Up)
                direction = NavigationDirection.Down;
            else if (direction == NavigationDirection.Down)
                direction = NavigationDirection.Up;
            else if (direction == NavigationDirection.Left)
                direction = NavigationDirection.Right;
            else if (direction == NavigationDirection.Right)
                direction = NavigationDirection.Left;

            double distance = double.MaxValue;
            FrameInfo closest = null;
            foreach (var f in SlideOverviewVM.SlideOverViewFrames)
            {
                // is it in the right direction
                if (direction == NavigationDirection.Up && f.CentreY + 1 >= pt.Y || direction == NavigationDirection.Down && f.CentreY - 1 <= pt.Y ||
                    direction == NavigationDirection.Left && f.CentreX + 1 >= pt.X || direction == NavigationDirection.Right && f.CentreX - 1 <= pt.X)
                    continue;

                var x = Math.Abs(f.CentreX - pt.X);
                var y = Math.Abs(f.CentreY - pt.Y);

                var d = (x * x) + (y * y);
                if (d < distance)
                {
                    closest = SharedVM.Frames.Where(fr => fr.Image == f.Frame).FirstOrDefault();
                    distance = d;
                }
            }
            return closest;
        }

        private bool AddScoreUsingShortcut(Key key)
        {
            if (key >= Key.NumPad1 && key <= Key.NumPad9)
                key = Key.F1 + (key - Key.NumPad1);
            else if (key == Key.NumPad0)
                key = Key.F10;

            if (SharedVM.CurrentAssay == null)
            {
                CustomPopUp.Show(Literal.Strings.Lookup("cbStr-MissingAssayError"), Literal.Strings.Lookup("cbStr-PcvTitle"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                return false;
            }
            var selectedClass = SharedVM.CurrentAssay.Classes.Where(cl => cl.ShortcutKey == key).FirstOrDefault();
            if (selectedClass == null)
            {
                return false;
            }

            // Check if mouse pointer is on frame. if true Add Score.
            var _rect = new Rect(new Size(SharedVM.CurrentFrame.FrameWidth, SharedVM.CurrentFrame.FrameHeight));
            if (!_rect.Contains(MousePosition))
            {
                return false;
            }
            Score probeScore = new Score()
            {
                Id = Guid.NewGuid(),
                Class = selectedClass.Text,
                ColourString = selectedClass.Color.ToString(),
                X = MousePosition.X,
                Y = MousePosition.Y,
                ImageFrameId = SharedVM.CurrentFrame.Image.Id
            };
            _eventAggregator.GetEvent<NewScoreEvent>().Publish(probeScore);

            return true;
        }

        public void ClearRegionsForSession()
        {
            foreach (AnalysisRegionInfo region in SharedVM.AllFrameRegions)
            {
                region.CanDelete = true;
            }
            //   AnalysisRegionSurfaceVM.Regions.Clear();
        }

        /// <summary>
        /// Relase the holding objects
        /// </summary>
        public void Dispose()
        {
            _mouseLeftButtonDownCommand = null;
            _mouseRightButtonDownCommand = null;
            ComponentDisplayOptionsVM.Dispose();
            AnalysisRegionSurfaceVM.Dispose();
            SlideOverviewVM.Dispose();
            ComponentDisplayOptionsVM = null;
            AnalysisRegionSurfaceVM = null;
            SlideOverviewVM = null;
            DisplayImage = null;
            SharedVM = null;
            _eventAggregator = null;
        }
        #endregion "Methods"

        #region Frame Image Tools ( Print, Export and Copy frame )
        private DelegateCommand<BitmapSource> _printFrameImageCommand;
        public ICommand PrintFrameImageCommand
        {
            get
            {
                return _printFrameImageCommand ?? (_printFrameImageCommand = new DelegateCommand<BitmapSource>((printVisual) =>
                {
                    //RVCMT-HA - Should we move this to Presenter ? ( use of EventAggregator required )  
                    Logic.FrameImageToolsLogic.PrintFrame(printVisual, SharedVM.SelectedSession.Slide.CaseId, SharedVM.SelectedSession.Slide.Name);
                }));
            }
        }

        private DelegateCommand<BitmapSource> _exportFrameImageCommand;
        public ICommand ExportFrameImageCommand
        {
            get
            {
                return _exportFrameImageCommand ?? (_exportFrameImageCommand = new DelegateCommand<BitmapSource>((printVisual) =>
                {
                    Logic.FrameImageToolsLogic.SaveAs(printVisual);
                }));
            }
        }

        private DelegateCommand<BitmapSource> _copyFrameImageCommand;
        public ICommand CopyFrameImageCommand
        {
            get
            {
                return _copyFrameImageCommand ?? (_copyFrameImageCommand = new DelegateCommand<BitmapSource>((printVisual) =>
                {
                    Clipboard.SetImage(printVisual);
                    IsImageToolsPanelOpen = false;
                }));
            }
        }
        #endregion
    }
}
