﻿using AI;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Infrastructure.UI.Events;
using Microsoft.Practices.ServiceLocation;
using PCVImageRenderers;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using ProbeCaseViewDataAccess.Models;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Models;
using ProbeCaseViewModule.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    /// This class is used to display cells in Grid View and can change the settings of the same like Overlays On/Off,channel On/Off and Histogram values etc.
    /// </summary>
    class GridViewModel : BindableBase
    {
        #region "Private Variables"
        private IEventAggregator _eventAggregator;
        private DateTime _invalidateTime = DateTime.Now;
        private ScoredCell _shiftSelectedCell = null;
        private SharedViewModel _sharedViewModel;
        #endregion "Private Variables"

        #region "Constructor"
        public GridViewModel(SharedViewModel sharedVM)
        {
            _eventAggregator = ServiceLocator.Current.TryResolve<IEventAggregator>();
            _sharedViewModel = sharedVM;
            SelectedSessionScores = sharedVM.SessionScores;
            PaginatedScores = new PaginatedCollection<ScoredCell>();
            // GridScrollThumbHeight = 150;
            SetOverlays();
            SetChannelDisplays();
            _isMaxProjection = true;
            AdjustThumbnailVM = new AdjustThumbnailViewModel();
            AdjustThumbnailVM.ChannelDisplays = ChannelDisplays;

            // Registering to global shutdown command, allowing the modules to set the Cancel property to true if the module wants to cancel the shutdown.
            CytoApps.Infrastructure.UI.ApplicationCommands.HostCommands.ShutdownCommand.RegisterCommand(ShutdownGridViewCommand);

        }
        #endregion "Constructor"

        #region "Properties"



        public SharedViewModel SharedVM
        {
            get
            {
                return _sharedViewModel;
            }
            set
            {
                _sharedViewModel = value;
                OnPropertyChanged();
            }
        }

        public PaginatedCollection<ScoredCell> PaginatedScores
        {
            get;
            set;
        }

        public ThumbnailCutTrickler ThumbnailGenerator { get; set; }


        private ScoredCell _currentCell;
        public ScoredCell CurrentCell
        {
            get { return _currentCell; }
            set
            {
                _currentCell = value;
                if (value != null)
                {
                    _currentCell.Selected = true;
                    OnPropertyChanged();
                }
            }
        }

        private int _numberOfColumns;
        public int NumberOfColumns
        {
            get { return _numberOfColumns; }
            set
            {
                _numberOfColumns = value;
                OnPropertyChanged();
            }
        }


        private ClassFilter _selectedClassSlice;
        public ClassFilter SelectedClassSlice
        {
            get { return _selectedClassSlice; }
            set
            {
                _selectedClassSlice = value;
                OnPropertyChanged();
                if (_selectedClassSlice != null)
                {
                    SharedVM.SessionSummary.Classes.ForEach(classFilter => classFilter.IsChecked = classFilter.ClassName == SelectedClassSlice.ClassName ? true : false);
                    //When the  total cell count is less than Current Page Range Set FirstVisibleItemIndex = 0
                    if (PaginatedScores.Source.Count <= PaginatedScores.FirstVisibleItemIndex)
                        PaginatedScores.FirstVisibleItemIndex = 0;
                }
            }
        }

        private double _gridScrollThumbHeight;
        public double GridScrollThumbHeight
        {
            get { return _gridScrollThumbHeight; }
            set
            {
                _gridScrollThumbHeight = value;
                OnPropertyChanged();
            }
        }

        private SessionScores _selectedSessionScores;
        public SessionScores SelectedSessionScores
        {
            get
            {
                return _selectedSessionScores;
            }
            set
            {
                _selectedSessionScores = value;
                OnPropertyChanged();
            }
        }

        private HashSet<OverlayInfo> _overlays;
        public HashSet<OverlayInfo> Overlays
        {
            get
            {
                return _overlays;
            }
            set
            {
                _overlays = value;
                OnPropertyChanged();
            }
        }

        private HashSet<ChannelDisplayInfo> _channelDisplays;
        public HashSet<ChannelDisplayInfo> ChannelDisplays
        {
            get
            {
                return _channelDisplays;
            }
            set
            {
                _channelDisplays = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Temporary setting when adjusting thumbnail display setting
        /// </summary>
        /// 
        private AdjustThumbnailViewModel _adjustedThumnbnail;
        public AdjustThumbnailViewModel AdjustThumbnailVM
        {
            get
            {
                return _adjustedThumnbnail;
            }
            set
            {
                _adjustedThumnbnail = value;
                OnPropertyChanged();
            }
        }

        private AdjustThumbnailViewModel _clonedAdjustThumbnail;
        public AdjustThumbnailViewModel ClonedAdjustThumnailVM
        {
            get
            {
                return _clonedAdjustThumbnail;
            }
            set
            {
                _clonedAdjustThumbnail = value;
                OnPropertyChanged();
            }
        }
        private Size _scoreViewSize;
        public Size ScoreViewSize
        {
            get { return _scoreViewSize; }
            set
            {
                if (value != null && value != Size.Empty && _scoreViewSize != value)
                {
                    _scoreViewSize = value;
                    OnPropertyChanged();
                    RecalculateNumberOfVisibleItems();
                }
            }
        }

        #region Grid Options Properties
        private bool _isOptionPopupOpen;
        public bool IsOptionPopupOpen
        {
            get { return _isOptionPopupOpen; }
            set
            {
                _isOptionPopupOpen = value;
                OnPropertyChanged();
            }
        }

        private bool _isAdjustImagePopupOpen;
        public bool IsAdjustImagePopupOpen
        {
            get { return _isAdjustImagePopupOpen; }
            set
            {
                _isAdjustImagePopupOpen = value;
                if (_isAdjustImagePopupOpen)
                {
                    ClonedAdjustThumnailVM = (AdjustThumbnailViewModel)AdjustThumbnailVM.Clone();
                }
                OnPropertyChanged();
            }
        }


        public bool DisplayGridScroll
        {
            get
            {
                return ProbeCaseViewSettings.Default.DisplayGridScroll;
            }
            set
            {
                ProbeCaseViewSettings.Default.DisplayGridScroll = value;
                OnPropertyChanged();
            }
        }

        public int GridSize
        {
            get
            {
                return ProbeCaseViewSettings.Default.GridSize;
            }
            set
            {
                var v = Math.Max(200, Math.Min(950, value));
                if (ProbeCaseViewSettings.Default.GridSize != v)
                {
                    ProbeCaseViewSettings.Default.GridSize = v;
                    OnPropertyChanged();
                    InvalidateView();
                    RecalculateNumberOfVisibleItems();
                }
            }
        }

        public bool DisplayCellIDs
        {
            get
            {
                return ProbeCaseViewSettings.Default.DisplayCellIDs;
            }
            set
            {
                ProbeCaseViewSettings.Default.DisplayCellIDs = value;
                OnPropertyChanged();
            }
        }

        public bool DisplayIndividualCounts
        {
            get
            {
                return ProbeCaseViewSettings.Default.DisplayIndividualCounts;
            }
            set
            {
                ProbeCaseViewSettings.Default.DisplayIndividualCounts = value;
                OnPropertyChanged();
            }
        }

        private bool _attenuateCounterstain = false;
        public bool AttenuateCounterstain
        {
            get
            {
                return _attenuateCounterstain;
            }
            set
            {
                _attenuateCounterstain = value;
                OnPropertyChanged();
                InvalidateView();
                UpdateCells();
            }
        }

        private bool _invertCounterstain = false;
        public bool InvertCounterstain
        {
            get
            {
                return _invertCounterstain;
            }
            set
            {
                _invertCounterstain = value;
                OnPropertyChanged();
                InvalidateView();
                UpdateCells();
            }
        }

        private bool _hideReviewedCells = false;
        public bool HideReviewedCells
        {
            get
            {
                return _hideReviewedCells;
            }
            set
            {
                _hideReviewedCells = value;
                OnPropertyChanged();
                UpdateScoreOrder();
                InvalidateView();
            }
        }
        #endregion

        private bool _isReclassifyPopupOpen;
        public bool IsReclassifyPopupOpen
        {
            get { return _isReclassifyPopupOpen; }
            set
            {
                _isReclassifyPopupOpen = value;
                OnPropertyChanged();
            }
        }

        private int _gridScrollIndex;
        public int GridScrollIndex
        {
            get { return _gridScrollIndex; }
            set
            {
                _gridScrollIndex = value;
                OnPropertyChanged();
            }
        }

        private bool _flip;
        public bool Flip
        {
            get { return _flip; }
            set
            {
                _flip = value;
                OnPropertyChanged();
            }
        }

        private bool _isMaxProjection;
        public bool IsMaxProjection
        {
            get { return _isMaxProjection; }
            set
            {
                _isMaxProjection = value;
                OnPropertyChanged();
            }
        }

        private int _currentStackIndex;
        public int CurrentStackIndex
        {
            get { return _currentStackIndex; }
            set
            {
                _currentStackIndex = value;
                OnPropertyChanged();
                //// change MaxP without kicking off event
                //_isMaxProjection = false;
                //OnPropertyChanged("IsMaxProjection");
                //InvalidateView();
                //_eventAggregator.GetEvent<RefreshGridDisplayEvent>().Publish(this);
            }
        }

        public int StackSize
        {
            get;
            set;
        }

        private List<ScoreSettings> _scoreCellSettingList;
        public List<ScoreSettings> ScoreCellSettingList
        {
            get
            {
                if (_scoreCellSettingList == null)
                {
                    _scoreCellSettingList = new List<ScoreSettings>();
                }
                return _scoreCellSettingList;
            }
            set
            {
                _scoreCellSettingList = value;
                OnPropertyChanged();
            }
        }


        private string _reClassifyToOther;
        public string ReClassifyToOther
        {
            get { return _reClassifyToOther; }
            set
            {
                _reClassifyToOther = value;
                OnPropertyChanged();
            }
        }



        #region "Sorting Properties"
        //TODO: Sorting needs to tested 
        public ScoreSorter[] signalSorters;
        public ScoreSorter[] allOtherSorters;

        private ObservableCollection<ScoreSorter> _primaryScoreSorters;
        public ObservableCollection<ScoreSorter> PrimaryScoreSorters
        {
            get
            {
                if (_primaryScoreSorters == null)
                    _primaryScoreSorters = new ObservableCollection<ScoreSorter>();
                return _primaryScoreSorters;
            }
            set
            {
                _primaryScoreSorters = value;
                OnPropertyChanged();
            }
        }

        private ScoreSorter _selectedPrimarySorter;
        public ScoreSorter SelectedPrimarySorter
        {
            get { return _selectedPrimarySorter; }
            set
            {
                if (value != null && _selectedPrimarySorter != value)
                {
                    if (SelectedPrimarySorter != null)
                        SelectedPrimarySorter.ConflictSorter = null;
                    _selectedPrimarySorter = value;
                    SelectedSecondarySorter = null;
                    UpdateSecondarySorters();
                    OnPropertyChanged();
                    UpdateScoreOrder();
                    SelectFirstCellOfCurrentPage();
                }
            }
        }

        private ScoreSorter _selectedSecondarySorter;
        public ScoreSorter SelectedSecondarySorter
        {
            get { return _selectedSecondarySorter; }
            set
            {
                if (value != null && _selectedSecondarySorter != value)
                {
                    _selectedSecondarySorter = value;
                    if (SelectedPrimarySorter != null)
                        SelectedPrimarySorter.ConflictSorter = value;
                    this.OnPropertyChanged();
                    UpdateScoreOrder();
                    SelectFirstCellOfCurrentPage();
                }
            }
        }

        private ObservableCollection<ScoreSorter> _secondaryScoreSorters;
        public ObservableCollection<ScoreSorter> SecondaryScoreSorters
        {
            get
            {
                if (_secondaryScoreSorters == null)
                    _secondaryScoreSorters = new ObservableCollection<ScoreSorter>();
                return _secondaryScoreSorters;
            }
            set
            {
                _secondaryScoreSorters = value;
                this.OnPropertyChanged();
            }
        }
        #endregion "Sorting Properties"
        #endregion "Properties"       

        #region Commands

        private DelegateCommand<ShutDownEventArgs> _shutdownGridViewCommand;
        public DelegateCommand<ShutDownEventArgs> ShutdownGridViewCommand
        {
            get
            {
                return _shutdownGridViewCommand ?? (_shutdownGridViewCommand = new DelegateCommand<ShutDownEventArgs>((args) =>
                 {
                     
                     args.CanClose=false;
                     if (SharedVM.IsReprocessing)
                     {
                         SharedVM.ShowReprocessingBusyMessage();
                     }
                     else
                     {
                         _eventAggregator.GetEvent<CloseSessionEvent>().Publish(null);
                     }

                 }));
            }
        }



        private DelegateCommand _saveAllDataCommand;
        public ICommand SaveAllDataCommand
        {
            get
            {
                return _saveAllDataCommand ?? (_saveAllDataCommand = new DelegateCommand(() =>
                {
                    if (SharedVM.IsReprocessing)
                    {
                        SharedVM.ShowReprocessingBusyMessage();
                    }
                    else
                    {
                        SharedVM.IsDirty = false;
                        ProbeCaseViewSettings.Default.Save();
                        _eventAggregator.GetEvent<SaveAllDataEvent>().Publish(SharedVM.SelectedSession);
                    }

                }));
            }
        }

        private DelegateCommand _navigateBackCommand;
        public ICommand NavigateBackCommand
        {
            get
            {
                return _navigateBackCommand ?? (_navigateBackCommand = new DelegateCommand(() =>
                {
                    if (SharedVM.IsReprocessing)
                    {
                        SharedVM.ShowReprocessingBusyMessage();
                    }
                    else
                    {
                        _eventAggregator.GetEvent<CloseSessionEvent>().Publish(null);
                    }

                }));
            }
        }

        private DelegateCommand _optionCommand;
        public ICommand OptionCommand
        {
            get
            {
                return _optionCommand ?? (_optionCommand = new DelegateCommand(() =>
                {
                    IsOptionPopupOpen = true;
                }));
            }
        }

        private DelegateCommand<ScoredCell> _adjustImageCommand;
        public ICommand AdjustImageCommand
        {
            get
            {
                return _adjustImageCommand ?? (_adjustImageCommand = new DelegateCommand<ScoredCell>((scoredCell) =>
                {
                    _eventAggregator.GetEvent<AdjustGridImageEvent>().Publish(scoredCell);
                }));
            }
        }

        private DelegateCommand _overlaySelectCommand;
        public ICommand OverlaySelectCommand
        {
            get
            {
                return _overlaySelectCommand ?? (_overlaySelectCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<RenderOverlaysEvent>().Publish(null);
                }));
            }
        }

        private DelegateCommand _channelDisplayCommand;
        public ICommand ChannelDisplayCommand
        {
            get
            {
                return _channelDisplayCommand ?? (_channelDisplayCommand = new DelegateCommand(() =>
                {
                    InvalidateView();
                    _eventAggregator.GetEvent<RefreshGridDisplayEvent>().Publish(null);
                }));
            }
        }

        private DelegateCommand _applyImageSettingToGridCommand;
        public ICommand ApplyImageSettingToGridCommand
        {
            get
            {
                return _applyImageSettingToGridCommand ?? (_applyImageSettingToGridCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<ApplyImageSettingsToGridEvent>().Publish(true);
                }));
            }
        }
        private DelegateCommand _applyImageSettingToGridCancelCommand;
        public ICommand ApplyImageSettingToGridCancelCommand
        {
            get
            {
                return _applyImageSettingToGridCancelCommand ?? (_applyImageSettingToGridCancelCommand = new DelegateCommand(() =>
                {
                    AdjustThumbnailVM = ClonedAdjustThumnailVM;
                    _eventAggregator.GetEvent<ApplyImageSettingsToGridEvent>().Publish(false);
                }));
            }
        }

        private DelegateCommand _filterCommand;
        public ICommand FilterCommand
        {
            get
            {
                return _filterCommand ?? (_filterCommand = new DelegateCommand(() =>
                {
                    InvalidateView();
                    UpdateScoreOrder();
                    SelectFirstCellOfCurrentPage();
                    // Set Scroll Thumb Size
                    GridScrollThumbHeight = CalculateScrollThumbSize();
                }));
            }
        }

        private DelegateCommand<ScoredCell> _selectedCellCommand;
        public ICommand SelectedCellCommand
        {
            get
            {
                return _selectedCellCommand ?? (_selectedCellCommand = new DelegateCommand<ScoredCell>((cell) =>
                {
                    ScoredCellSelection(cell);
                }));
            }
        }

        private DelegateCommand _checkAllClassCommand;
        public ICommand CheckAllClassCommand
        {
            get
            {
                return _checkAllClassCommand ?? (_checkAllClassCommand = new DelegateCommand(() =>
                {
                    SharedVM.SessionSummary.Classes.ForEach(classFilter => classFilter.IsChecked = true);
                    ResetPieChartSelectedItem();
                    // Set Scroll Thumb Size
                    GridScrollThumbHeight = CalculateScrollThumbSize();
                }));
            }
        }

        private DelegateCommand _clearAllClassCommand;
        public ICommand ClearAllClassCommand
        {
            get
            {
                return _clearAllClassCommand ?? (_clearAllClassCommand = new DelegateCommand(() =>
                {
                    SharedVM.SessionSummary.Classes.ForEach(classFilter => classFilter.IsChecked = false);
                    ResetPieChartSelectedItem();
                    // Set Scroll Thumb Size
                    GridScrollThumbHeight = CalculateScrollThumbSize();
                }));
            }
        }

        private DelegateCommand _scoringWindowCommand;
        public ICommand ScoringWindowCommand
        {
            get
            {
                return _scoringWindowCommand ?? (_scoringWindowCommand = new DelegateCommand(() =>
                {
                    FrameInfo currentFrame = null;
                    if (null != CurrentCell)
                    {
                        currentFrame = SharedVM.Frames.Where(frame => frame.Image.Id == CurrentCell.FrameId).SingleOrDefault();
                    }
                    else
                    {
                        currentFrame = SharedVM.Frames.FirstOrDefault();
                    }
                    _eventAggregator.GetEvent<ScoringWindowOpenEvent>().Publish(currentFrame);
                }));
            }
        }

        private DelegateCommand<ScoredCell> _openReClassifyPopupCommand;
        public ICommand OpenReClassifyPopupCommand
        {
            get
            {
                return _openReClassifyPopupCommand ?? (_openReClassifyPopupCommand = new DelegateCommand<ScoredCell>((cell) =>
                {
                    ScoredCellSelection(cell);
                    IsReclassifyPopupOpen = true;
                }));
            }
        }

        private DelegateCommand<ScoredCell> _rightClickSelectionCommand;
        public ICommand RightClickSelectionCommand
        {
            get
            {
                return _openReClassifyPopupCommand ?? (_rightClickSelectionCommand = new DelegateCommand<ScoredCell>((cell) =>
                {
                    RighClickSelectionWithPopup(cell);
                }));
            }
        }

        private DelegateCommand _markasReviewedCommand;
        public ICommand MarkasReviewedCommand
        {
            get
            {
                return _markasReviewedCommand ?? (_markasReviewedCommand = new DelegateCommand(() =>
                {
                    ReviewScoredCells(true);
                }));
            }
        }

        private DelegateCommand _markasUnReviewedCommand;
        public ICommand MarkasUnReviewedCommand
        {
            get
            {
                return _markasUnReviewedCommand ?? (_markasUnReviewedCommand = new DelegateCommand(() =>
                {
                    ReviewScoredCells(false);
                }));
            }
        }

        private DelegateCommand _gridScrollValueChangedCommand;
        public ICommand GridScrollValueChangedCommand
        {
            get
            {
                return _gridScrollValueChangedCommand ?? (_gridScrollValueChangedCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<GridScrollValueChangeEvent>().Publish(true);
                }));
            }
        }

        private DelegateCommand _flipStatsCommand;
        public ICommand FlipStatsCommand
        {
            get
            {
                return _flipStatsCommand ?? (_flipStatsCommand = new DelegateCommand(() =>
                {
                    Flip = !Flip;
                    // Set Scroll Thumb Size
                    GridScrollThumbHeight = CalculateScrollThumbSize();
                }));
            }
        }

        private DelegateCommand _enhanceCellCommand;
        public ICommand EnhanceCellCommand
        {
            get
            {
                return _enhanceCellCommand ?? (_enhanceCellCommand = new DelegateCommand(() =>
                {
                    //_eventAggregator.GetEvent<EnhanceCurrentCellEvent>().Publish(GetCurrentGridSettings(CurrentCell));
                }));
            }
        }

        private DelegateCommand<ScoredCell> _openEnhancedImageCommand;
        public ICommand OpenEnhancedImageCommand
        {
            get
            {
                return _openEnhancedImageCommand ?? (_openEnhancedImageCommand = new DelegateCommand<ScoredCell>((scoredCell) =>
                {
                    _eventAggregator.GetEvent<EnhanceCurrentCellEvent>().Publish(GetCurrentGridSettings(scoredCell));
                }, (ScoredCell) => true));
            }
        }

        private DelegateCommand<ProbeScoringClass> _reclassifyCommand;
        public ICommand ReclassifyCommand
        {
            get
            {
                return _reclassifyCommand ?? (_reclassifyCommand = new DelegateCommand<ProbeScoringClass>((probeScoringClasss) =>
                {
                    _eventAggregator.GetEvent<ReclassifyEvent>().Publish(probeScoringClasss);
                }));
            }
        }

        private DelegateCommand _reScoreCommand;
        public ICommand RescoreCommand
        {
            get
            {
                return _reScoreCommand ?? (_reScoreCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<ReScoreEvent>().Publish(CurrentCell);
                }));
            }
        }

        private DelegateCommand _reclassifyPopupClosedCommand;
        public ICommand ReclassifyPopupClosedCommand
        {
            get
            {
                return _reclassifyPopupClosedCommand ?? (_reclassifyPopupClosedCommand = new DelegateCommand(() =>
                {
                    foreach (var cellSignalCount in CurrentCell.Counts)
                    {
                        cellSignalCount.ResetEdit();
                    }
                    ReClassifyToOther = string.Empty;
                    _eventAggregator.GetEvent<ReClassifyUsingNewClassEvent>().Publish(null);

                }));
            }
        }

        private DelegateCommand _reClassifyUsingNewClassCommand;
        public ICommand ReClassifyUsingNewClassCommand
        {
            get
            {
                return _reClassifyUsingNewClassCommand ?? (_reClassifyUsingNewClassCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<ReClassifyUsingNewClassEvent>().Publish(ReClassifyToOther);
                }));
            }
        }

        private DelegateCommand<object> _mouseScrollCommand;
        public ICommand MouseScrollCommand
        {
            get
            {
                return _mouseScrollCommand ?? (_mouseScrollCommand = new DelegateCommand<object>((mouseScrollDelta) =>
                {
                    int delta = (int)mouseScrollDelta;
                    GridScroll(delta);
                }));
            }
        }

        private DelegateCommand _addCellsCommand;
        public ICommand AddCellsCommand
        {
            get
            {
                return _addCellsCommand ?? (_addCellsCommand = new DelegateCommand(() =>
                {
                    if (SharedVM.IsReprocessing)
                    {
                        SharedVM.ShowReprocessingBusyMessage();
                    }
                    else
                    {
                        CytoApps.Infrastructure.UI.ApplicationCommands.HostCommands.ShutdownCommand.UnregisterCommand(ShutdownGridViewCommand);
                        _eventAggregator.GetEvent<AddCellsEvent>().Publish(SharedVM.SelectedSession);
                    }
                }));
            }
        }

        private DelegateCommand<object> _keyDownCommand;
        public ICommand KeyDownCommand
        {
            get
            {
                return _keyDownCommand ?? (_keyDownCommand = new DelegateCommand<object>((keyEventArgs) =>
                {
                    // If TextBox has focus we need to pass control to textbox
                    if (Keyboard.FocusedElement is System.Windows.Controls.TextBox)
                    {
                        return;
                    }
                    else
                    {
                        var args = (KeyEventArgs)keyEventArgs;
                        OnKeyDown(args);
                        args.Handled = true;
                    }
                }));
            }
        }
        #endregion

        #region Methods
        #region "Selection Mechanism"
        /// <summary>
        /// Selecting A Cell or Selecting Cells in range(using shift key) Or MultiSelection(using ctrl key) 
        /// </summary>
        /// <param name="theSelectedCell">Mouse click on Scored cell(Selected Scored Cell)</param>
        public void ScoredCellSelection(ScoredCell theSelectedCell)
        {
            if (SharedVM.ScoredCells != null)
            {
                var shift = Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift);
                if (shift)
                {
                    if (_shiftSelectedCell == null)
                        _shiftSelectedCell = (CurrentCell == null) ? theSelectedCell : CurrentCell;
                    SelectCellRange(_shiftSelectedCell, theSelectedCell);
                    return;
                }
                var multiSelect = (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl));
                if (!multiSelect)
                {
                    Parallel.ForEach(SharedVM.ScoredCells, (scorcell) =>
                    {
                        scorcell.Selected = false;
                    });
                    CurrentCell = theSelectedCell;
                }
                else
                {
                    theSelectedCell.Selected = true;
                    if (theSelectedCell.Selected)
                        CurrentCell = theSelectedCell;
                }
                _eventAggregator.GetEvent<SelectedGridScoreEvent>().Publish(theSelectedCell);
                _shiftSelectedCell = null;
            }
        }

        /// <summary>
        /// Selecting the Cells in Range
        /// </summary>
        /// <param name="first">First Selected Cell</param>
        /// <param name="last"> Last Selected Cell</param>
        public void SelectCellRange(ScoredCell first, ScoredCell last)
        {
            bool select = false;
            ScoredCell end = null;
            foreach (var cell in PaginatedScores.Source)
            {
                if (!select && (cell == first || cell == last))
                {
                    select = true;
                    end = (cell == first) ? last : first;
                }
                cell.Selected = select;

                if (cell == end)
                    select = false;
            }
            CurrentCell = last;
        }

        private void RighClickSelectionWithPopup(ScoredCell theSelectedCell)
        {
            var multiSelect = (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl));
            if (!multiSelect)
            {
                Parallel.ForEach(SharedVM.ScoredCells, (scorcell) =>
                {
                    scorcell.Selected = false;
                });
                CurrentCell = theSelectedCell;
            }
            else
            {
                theSelectedCell.Selected = true;
                if (theSelectedCell.Selected)
                    CurrentCell = theSelectedCell;
            }
            IsReclassifyPopupOpen = true;
        }
        #endregion "Selection Mechanism"

        #region "Sorting"
        public void UpdateSecondarySorters()
        {
            SecondaryScoreSorters.Clear();
            if (SelectedPrimarySorter != null && !SelectedPrimarySorter.IsSecondarySortAllowed)
            {
                SecondaryScoreSorters.Clear();
                return;
            }
            // primary is a count sorter
            if (SelectedPrimarySorter == null || signalSorters.Contains(SelectedPrimarySorter))
            {
                foreach (var sort in allOtherSorters)
                    SecondaryScoreSorters.Add(sort); ;
                foreach (var secSorter in signalSorters)
                    if (secSorter != SelectedPrimarySorter)
                        SecondaryScoreSorters.Add(secSorter);
            }
            else
            {
                foreach (var sort in allOtherSorters)
                    if (sort != SelectedPrimarySorter)
                        SecondaryScoreSorters.Add(sort);
                foreach (var secSorter in signalSorters)
                    SecondaryScoreSorters.Add(secSorter);
            }
        }

        public void UpdateScoreOrder()
        {
            if (PaginatedScores.Source != null)
            {
                ScoredCell[] scoredcells;
                //Array.Sort(ScoredCells.ToArray(), SelectedPrimarySorter);
                var classes = SharedVM.SessionSummary.Classes.Where(classFilter => classFilter.IsChecked == true).Select(y => y.ClassName);
                if (HideReviewedCells)
                {
                    scoredcells = SharedVM.ScoredCells.Where(cell => classes.Contains(cell.Class) && !cell.Reviewed).ToArray();
                }
                else
                {
                    scoredcells = SharedVM.ScoredCells.Where(cell => classes.Contains(cell.Class)).ToArray();
                }
                Array.Sort(scoredcells, SelectedPrimarySorter);

                PaginatedScores.Source = scoredcells.Where(cell => !cell.IsDeleted).ToArray();
                if (PaginatedScores.Source.Count <= PaginatedScores.FirstVisibleItemIndex)
                    PaginatedScores.FirstVisibleItemIndex = 0;
                // HACK, above is presenter code really, Move on REFACTOR and add new event
                _eventAggregator.GetEvent<RefreshGridDisplayEvent>().Publish(this);
            }
        }
        #endregion "Sorting"

        #region "Cell Displays"
        private void SetOverlays()
        {
            Overlays = new HashSet<OverlayInfo>();
            foreach (var overlay in SelectedSessionScores.Overlays)
            {
                Overlays.Add(new OverlayInfo(overlay));
            }
        }

        private void SetChannelDisplays()
        {
            ChannelDisplays = new HashSet<ChannelDisplayInfo>();
            foreach (var channelDisplay in SelectedSessionScores.ChannelDisplays)
            {
                ChannelDisplays.Add(new ChannelDisplayInfo(channelDisplay) { IsChecked = true });
            }
        }

        #endregion "Cell Displays"

        /// <summary>
        /// Calculate Number of Items per Page
        /// </summary>
        public void RecalculateNumberOfVisibleItems()
        {
            // There's always at least one row and column, even if only partially visible.
            NumberOfColumns = Math.Max(1, (int)(ScoreViewSize.Width / GridSize));
            int rows = Math.Max(1, (int)(ScoreViewSize.Height / GridSize));
            PaginatedScores.NumberOfItemsPerPage = (NumberOfColumns * rows);
            // Set Scroll Thumb Size
            GridScrollThumbHeight = CalculateScrollThumbSize(rows);
            // inform we have changed the number of visible items in grid
            _eventAggregator.GetEvent<GridResizeEvent>().Publish(this);
        }

        private double CalculateScrollThumbSize(int? rows = null)
        {
            int _rows = rows != null ? Convert.ToInt32(rows) : Math.Max(1, (int)(ScoreViewSize.Height / GridSize));
            int viewportHeight = _rows * GridSize;
            var viewableRatio = viewportHeight / (GridSize * Math.Ceiling((double)PaginatedScores.Source.Count / NumberOfColumns));
            return Math.Max(viewportHeight * viewableRatio, 6d);
        }

        /// <summary>
        /// Returns Selected cell and Required Grid setiings for displaying image similar to GridView
        /// </summary>
        /// <param name="cell">Selected cell</param>
        /// <returns>Current Grid Settings & Cell</returns>
        private Dictionary<ScoredCell, EnhanceViewOptions> GetCurrentGridSettings(ScoredCell cell)
        {
            EnhanceViewOptions options = new EnhanceViewOptions()
            {
                AttenuateCounterstain = AttenuateCounterstain,
                ChannelDisplays = ChannelDisplays,
                CurrentStackIndex = CurrentStackIndex,
                DisplayCellIDs = DisplayCellIDs,
                DisplayIndividualCounts = DisplayIndividualCounts,
                InvertCounterstain = InvertCounterstain,
                Overlays = Overlays
            };
            Dictionary<ScoredCell, EnhanceViewOptions> currentCellSettings = new Dictionary<ScoredCell, EnhanceViewOptions>();
            currentCellSettings.Add(cell, options);
            return currentCellSettings;
        }

        public void UpdateScoreCollection()
        {
            if (PaginatedScores.Source != null)
            {
                var classes = SharedVM.SessionSummary.Classes.Where(classFiler => classFiler.IsChecked == true).Select(classFilter => classFilter.ClassName);
                PaginatedScores.Source = SharedVM.ScoredCells.Where(cell => classes.Contains(cell.Class)).ToArray();
                if (PaginatedScores.Source.Count <= PaginatedScores.FirstVisibleItemIndex)
                    PaginatedScores.FirstVisibleItemIndex = 0;
            }
        }

        private bool CanExecuteScoringWindowOpen()
        {
            return null != SharedVM.Frames;
        }

        public DateTime InvalidateTime
        {
            get { return _invalidateTime; }
        }

        public void InvalidateView()
        {
            _invalidateTime = DateTime.Now;
        }

        private void ResetPieChartSelectedItem()
        {
            SelectedClassSlice = null;
        }

        private void UpdateCells()
        {
            _eventAggregator.GetEvent<UpdateScoredCellEvent>().Publish(true);
        }

        public int FindFrameNumber(Guid guid)
        {
            var f = SharedVM.Frames.FirstOrDefault(frame => frame.Image.Id == guid);
            return f == null ? -1 : f.Identifier;
        }

        private void OnKeyDown(KeyEventArgs args)
        {
            bool controlKeyDown = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
            switch (args.Key)
            {
                case Key.Up:
                    if (controlKeyDown)
                    {
                        IncrementOrDecrementStack(1);
                    }
                    else
                    {
                        PreviousRow(true);
                    }
                    break;
                case Key.Down:
                    if (controlKeyDown)
                    {
                        IncrementOrDecrementStack(-1);
                    }
                    else
                    {
                        NextRow(true);
                    }
                    break;
                case Key.Right:
                    NextCell();
                    break;
                case Key.Left:
                    PrevCell();
                    break;
                case Key.PageUp:
                    PrevPage();
                    break;
                case Key.PageDown:
                    NextPage();
                    break;
                case Key.Home:
                    FirstPage();
                    break;
                case Key.End:
                    LasPage();
                    break;
                case Key.P:
                    if (IsAdjustImagePopupOpen)
                    {
                        if (AdjustThumbnailVM.IsMaxProjection)
                            AdjustThumbnailVM.IsMaxProjection = false;
                        else
                            AdjustThumbnailVM.IsMaxProjection = true;
                    }
                    break;
                case Key.N:
                    ReviewScoredCells(false);
                    break;
                case Key.R:
                    ReviewScoredCells(true);
                    break;
                case Key.A:
                    if (controlKeyDown)
                        PaginatedScores.Source.Select(x => { x.Selected = true; x.IsHighlighted = true; return x; }).ToList();
                    break;
                case Key.NumPad0:
                case Key.NumPad1:
                case Key.NumPad2:
                case Key.NumPad3:
                case Key.NumPad4:
                case Key.NumPad5:
                case Key.NumPad6:
                case Key.NumPad7:
                case Key.NumPad8:
                case Key.NumPad9:
                    if ((Keyboard.Modifiers & ModifierKeys.Control) != ModifierKeys.Control)
                    {
                        ToggleChannelVisibility(args.Key);
                    }
                    else
                    {
                        ToggleOverlayVisibility(args.Key);
                    }
                    break;
                case Key.U:
                    break;
                case Key.Space:
                    OpenFramesWindow();
                    break;
                case Key.Q:
                    break;
                default:
                    if ((args.Key == Key.System && args.SystemKey == Key.F4) || (args.Key == Key.F4 && Keyboard.IsKeyDown(Key.RightAlt)))
                    {
                        // close entire application.
                        Application.Current.MainWindow.Close();
                    }
                    else
                    {
                        ToggleFunctionalKey(args.Key);
                    }
                    break;
                    //TODO:Key 'U'  Ctrl+Q need  clarifications
                    //TODO Function keys to be binded for the scores                  
            }
        }

        #region Navigation Methods
        /// <summary>
        /// Increment or Decrement Stack 
        /// TODO Revisit this as new class AdjustThumnail set first later applying for gridviewmodel properties
        /// </summary>
        /// <param name="index"></param>
        private void IncrementOrDecrementStack(int index)
        {
            if (IsAdjustImagePopupOpen)
            {
                int stackIndex = AdjustThumbnailVM.CurrentStackIndex;
                int newIndex = stackIndex + index;
                if (newIndex > AdjustThumbnailVM.StackSizeMinusOne || newIndex < 0)
                    return;
                AdjustThumbnailVM.CurrentStackIndex = newIndex;
            }
        }

        private void NextRow(bool moveCurrCell = false)
        {
            if (moveCurrCell) // Move the current cell to the next row.
            {
                int currIndex = PaginatedScores.Source.ToList().IndexOf(CurrentCell) + NumberOfColumns;
                if (currIndex >= PaginatedScores.Source.Count)
                {
                    currIndex = PaginatedScores.Source.Count - 1; // No corresponding cell in next row, so move to last cell.
                }
                if (currIndex >= 0)
                {
                    if (PaginatedScores.LastVisibleItemIndex < currIndex)
                        PaginatedScores.RowDown(NumberOfColumns);
                }
                ClearHighlightedandSelectedCells();
                CurrentCell = PaginatedScores.Source.ElementAt(currIndex);
                CurrentCell.Selected = true;
            }
            else // Move all the rows up by one row.
            {
                PaginatedScores.RowDown(NumberOfColumns);
            }
        }

        private void PreviousRow(bool moveCurrCell = false)
        {
            if (moveCurrCell) // Move the current cell to the prev row.
            {
                int currIndex = PaginatedScores.Source.ToList().IndexOf(CurrentCell) - NumberOfColumns;
                if (currIndex < 0)
                    currIndex = 0; // No corresponding cell in prev row, so move to first cell.

                if (currIndex < PaginatedScores.Source.Count)
                {
                    if (PaginatedScores.FirstVisibleItemIndex > currIndex)
                    {
                        PaginatedScores.RowUp(NumberOfColumns);
                    }
                    ClearHighlightedandSelectedCells();
                    CurrentCell = PaginatedScores.Source.ElementAt(currIndex);
                    CurrentCell.Selected = true;
                }
            }
            else // Move all the rows down by one row.
            {
                PaginatedScores.RowUp(NumberOfColumns);
            }
            //TODO - Need to check if its needed.
            //RenderAll();
        }

        public void ClearHighlightedandSelectedCells()
        {
            // can we be more specific here?
            foreach (var c in SharedVM.ScoredCells)
            {
                c.IsHighlighted = false;
                c.Selected = false;
            }
        }

        private void NextCell()
        {
            int newIndex = 0;
            if (CurrentCell != null)
            {
                newIndex = PaginatedScores.Source.ToList().IndexOf(CurrentCell);
            }
            if (newIndex + 1 < PaginatedScores.Source.Count)
            {
                newIndex++;
            }
            if (newIndex > PaginatedScores.LastVisibleItemIndex)
            {
                NextPage();
            }
            // Have to set CurrentCell only after paging it into view, otherwise any ListBox
            // bound to the page and to CurrentCell wouldn't be able to show it.
            if (newIndex >= 0 && newIndex < PaginatedScores.Source.Count)
            {
                ClearHighlightedandSelectedCells();
                CurrentCell = PaginatedScores.Source.ElementAt(newIndex);
                CurrentCell.Selected = true;
            }
        }

        private void PrevCell()
        {
            int newIndex = 0;
            if (CurrentCell != null)
            {
                newIndex = PaginatedScores.Source.ToList().IndexOf(CurrentCell);
            }
            if (newIndex - 1 >= 0)
            {
                newIndex--;
            }
            if (newIndex < PaginatedScores.FirstVisibleItemIndex)
            {
                PrevPage();
            }
            // Have to set CurrentCell only after paging it into view, otherwise any ListBox
            // bound to the page and to CurrentCell wouldn't be able to show it.
            if (newIndex >= 0 && newIndex < PaginatedScores.Source.Count)
            {
                ClearHighlightedandSelectedCells();
                CurrentCell = PaginatedScores.Source.ElementAt(newIndex);
                CurrentCell.Selected = true;
            }
        }

        public void NextPage()
        {
            int newIndex = PaginatedScores.FirstVisibleItemIndex + PaginatedScores.NumberOfItemsPerPage;
            if (newIndex < PaginatedScores.Source.Count)
            {
                PaginatedScores.FirstVisibleItemIndex = newIndex;
            }
        }

        public void PrevPage()
        {
            PaginatedScores.FirstVisibleItemIndex -= PaginatedScores.NumberOfItemsPerPage;
        }

        public void FirstPage()
        {
            PaginatedScores.FirstVisibleItemIndex = 0;
        }

        private void LasPage()
        {
            if (PaginatedScores.LastVisibleItemIndex != PaginatedScores.Source.Count() - 1)
            {
                int remiander = PaginatedScores.Source.Count() % NumberOfColumns;
                if (remiander > 0)
                    PaginatedScores.FirstVisibleItemIndex = PaginatedScores.Source.Count() - remiander;
                else
                    PaginatedScores.FirstVisibleItemIndex = PaginatedScores.Source.Count() - NumberOfColumns;
            }
        }

        private void ReviewScoredCells(bool reviewStatus)
        {
            IEnumerable<ScoredCell> selectedCells = SharedVM.ScoredCells.Where(cell => cell.Selected == true).ToList();
            SetReviewStatus(selectedCells, reviewStatus);
            IsReclassifyPopupOpen = false;
            NextCell();
            ScoredCellSelection(CurrentCell);
            SharedVM.IsDirty = true;
        }

        private void SetReviewStatus(IEnumerable<ScoredCell> selectedCells, bool reviewStatus)
        {
            foreach (var cell in selectedCells)
            {
                cell.Reviewed = reviewStatus;
            }
        }

        /// <summary>
        /// Set the Overlays through keypad
        /// </summary>
        /// <param name="pressedKey"></param>
        private void ToggleOverlayVisibility(Key pressedKey)
        {
            int index = pressedKey - Key.NumPad1;
            if (index < 0 || index >= Overlays.Count())
                return;
            Overlays.ElementAt(index).IsChecked = !Overlays.ElementAt(index).IsChecked;
        }

        /// <summary>
        /// Set the channels through keypad
        /// </summary>
        /// <param name="pressedKey"></param>
        private void ToggleChannelVisibility(Key pressedKey)
        {
            int index = pressedKey - Key.NumPad1;
            if (index < 0 || index >= ChannelDisplays.Count())
                return;
            ChannelDisplays.ElementAt(index).IsChecked = !ChannelDisplays.ElementAt(index).IsChecked;
        }

        private void ToggleFunctionalKey(Key pressedKey)
        {
            if ((pressedKey >= Key.F1 && pressedKey <= Key.F11) || (pressedKey >= Key.D0 && pressedKey <= Key.D9))
            {
                if ((Keyboard.Modifiers & ModifierKeys.Shift) != ModifierKeys.Shift)
                {
                    //RVCMT - HA - both Reclassify and ReviewScoredCells required ?
                    var c = ClassFromShortcutKey(pressedKey);
                    if (c != null)
                    {
                        Reclassify(GetSelectedCells(), c);
                        ReviewScoredCells(true);
                    }
                }
                else
                {
                    SharedVM.SessionSummary.Classes.ToList().ForEach(classFilter => classFilter.IsChecked = false);
                    SharedVM.SessionSummary.Classes.Where(classFilter => classFilter.ShortcutKey == pressedKey).ForEach(classFilter => classFilter.IsChecked = true);
                }
            }
        }

        public IEnumerable<ScoredCell> GetSelectedCells()
        {
            List<ScoredCell> selectedCells = new List<ScoredCell>();
            foreach (var selectedCell in SharedVM.ScoredCells)
            {
                if (selectedCell.Selected)
                    selectedCells.Add(selectedCell);
            }
            return selectedCells;
        }
        internal void Reclassify(IEnumerable<ScoredCell> cells, ProbeScoringClass c)
        {
            foreach (var cell in cells)
                Reclassify(cell, c);
            GetFilteredClass();
        }

        public void Reclassify(ScoredCell cell, ProbeScoringClass classs)
        {
            cell.Class = classs.Text;
            cell.Color = classs.Color.ToString();
            //isModified = true;

            //TODO
            //if (!cell.ProbeScore.IsManualOrRegionScore())
            //    AutoScoresHaveBeenManuallyModified = true;

            //// Must recreate the classFilterList, not just update it, in case the reclassification
            //// involved creating a completely new class, so that the new class appears in the correct
            //// position in the classFilterList (this is important since ClassFilterList is bound to the GUI).
            //CreateClassFilterList();
            //UpdateClassFilterList();

            //Notify("IsModified");

            //if (AutoReviewOnReclassify)
            //    cell.Reviewed = true;
        }

        private ProbeScoringClass ClassFromShortcutKey(Key key)
        {
            if (key >= Key.NumPad1 && key <= Key.NumPad9)
                key = Key.F1 + (key - Key.NumPad1);

            else if (key == Key.NumPad0)
                key = Key.F10;

            var c = SharedVM.CurrentAssay.Classes.Where(cl => cl.ShortcutKey == key).FirstOrDefault();

            if (c == null)
                return null;

            return c;
        }

        public void GetFilteredClass()
        {
            SharedVM.SessionSummary.TotalInformativeCell = SharedVM.ScoredCells.Count((cell => cell.Class.ToLower() != "uninformative" && !cell.IsDeleted));
            double totalinformatives = SharedVM.SessionSummary.TotalInformativeCell > 0 ? SharedVM.SessionSummary.TotalInformativeCell : 1;

            //if Assay present
            if (SharedVM.CurrentAssay != null)
            {
                var classFilters = (from probeClass in SharedVM.CurrentAssay.Classes
                                    select new ClassFilter
                                    {
                                        ClassName = probeClass.Text,
                                        ClassCount = SharedVM.ScoredCells.Where(cell => !cell.IsDeleted && cell.Class.ToLower() == probeClass.Text.ToLower()).Count(),
                                        IsChecked = true,
                                        ClassPercentage = (SharedVM.ScoredCells.Where(cell => !cell.IsDeleted && cell.Class.ToLower() == probeClass.Text.ToLower()).Count() / totalinformatives) * 100,
                                        Color = probeClass.Color.ToString(),
                                        ShortcutKey = probeClass.ShortcutKey
                                    }).ToList();
                if (classFilters != null)
                {
                    if (classFilters.Where(x => x.ClassName.ToLower() == Constants.Uninformative).FirstOrDefault() != null)
                        classFilters.Where(x => x.ClassName.ToLower() == Constants.Uninformative).FirstOrDefault().ClassPercentage = 0;
                    SharedVM.SessionSummary.Classes = classFilters.ToObservableCollection();
                }
            }
            //if assay not present create class filters from scroecells
            else
            {
                //Total Informative cells
                SharedVM.SessionSummary.TotalInformativeCell = SharedVM.ScoredCells.Count((cell => cell.Class.ToLower() != "uninformative" && !cell.IsDeleted));
                var scoredCellsbyGroup = SharedVM.ScoredCells.Where(cell => !cell.IsDeleted).GroupBy(x => x.Class).ToList();

                var scoredClasses = (from scoredCellClass in scoredCellsbyGroup
                                     select new ClassFilter
                                     {
                                         ClassName = scoredCellClass.Key,
                                         ClassCount = scoredCellClass.Count(),
                                         IsChecked = true,
                                         ClassPercentage = (scoredCellClass.Count() / (double)SharedVM.SessionSummary.TotalInformativeCell) * 100,
                                         Color = scoredCellClass.Select(cell => cell.Color).FirstOrDefault()
                                     }).Where(c => c.ClassName.ToLower() != Constants.Uninformative).ToObservableCollection();

                //insert only uninformative cells at the last
                if (scoredClasses != null)
                {
                    SharedVM.SessionSummary.Classes = scoredClasses.OrderByDescending(classFilter => classFilter.ClassName).ToObservableCollection();
                    if (scoredClasses != null && scoredCellsbyGroup != null)
                    {

                        var uniformativeClass = (from scoredCellClass in scoredCellsbyGroup
                                                 select new ClassFilter
                                                 {
                                                     ClassName = scoredCellClass.Key,
                                                     ClassCount = scoredCellClass.Count(),
                                                     IsChecked = true,
                                                     ClassPercentage = 0,
                                                     Color = scoredCellClass.Select(cell => cell.Color).FirstOrDefault()
                                                 }).Where(c => c.ClassName.ToLower() == Constants.Uninformative).FirstOrDefault();
                        if (uniformativeClass != null)
                            scoredClasses.Insert(scoredClasses.Count, uniformativeClass);
                        SharedVM.SessionSummary.Classes = scoredClasses;
                    }

                }

                //set signal information, ratios and fusions
                ScoreStats scoreStats = new ScoreStats();
                scoreStats.CalculateStats(SharedVM.CurrentAssay, SharedVM.ScoredCells);
                SharedVM.SessionSummary.ScoreStats = scoreStats;
            }

            bool informativeCellsArePresentInFilter = false;
            foreach (var c in SharedVM.SessionSummary.Classes)
            {
                if (c != null && !c.IsUninformativeClass && SharedVM.ScoredCells.Where(s => s.Class.Equals(c.ClassName, StringComparison.OrdinalIgnoreCase)).Count() > 0)
                    informativeCellsArePresentInFilter = true;
            }
            foreach (var c in SharedVM.SessionSummary.Classes)
            {
                if (c != null)
                {
                    c.IsChecked = !c.IsUninformativeClass || !informativeCellsArePresentInFilter;
                }
            }
        }

        /// <summary>
        /// open the frame window if no scored cells present or if open current scored cell frame in frame window 
        /// </summary>
        private void OpenFramesWindow()
        {
            if (SharedVM.ScoredCells != null && SharedVM.ScoredCells.Count > 0)
            {
                FrameInfo currentFrame = null;
                if (null != CurrentCell)
                {
                    currentFrame = SharedVM.Frames.Where(frame => frame.Image.Id == CurrentCell.FrameId).SingleOrDefault();
                }
                else
                {
                    currentFrame = SharedVM.Frames.FirstOrDefault();
                }
                _eventAggregator.GetEvent<ScoringWindowOpenEvent>().Publish(currentFrame);
            }
        }

        private void GridScroll(int delta)
        {
            if (delta > 0)
            {
                PreviousRow();
            }
            else
            {
                NextRow();
            }
        }
        #endregion

        private void SelectFirstCellOfCurrentPage()
        {
            if (PaginatedScores.CurrentPage != null && PaginatedScores.CurrentPage.Count() > 0)
            {
                if (CurrentCell != null && !PaginatedScores.CurrentPage.Contains(CurrentCell))
                {
                    CurrentCell.Selected = false;
                    CurrentCell = PaginatedScores.CurrentPage.First();
                }
            }
        }
        #endregion "Methods"       

        #region Add Cells

        private bool _isAddCellsVisible;
        public bool IsAddCellsVisible
        {
            get { return _isAddCellsVisible; }
            set
            {
                if (_isAddCellsVisible != value)
                {
                    _isAddCellsVisible = value;
                    OnPropertyChanged();
                }
            }
        }

        public void ClearSorterAndDelegateReferences()
        {
            FrameSorter framesorter = (FrameSorter)allOtherSorters.Where(x => x is FrameSorter).FirstOrDefault();
            if (framesorter != null)
            {
                framesorter.Tidy();
            }
            Array.Clear(allOtherSorters, 0, allOtherSorters.Length);
        }


        public void Dispose()
        {
            PaginatedScores = null;
            PrimaryScoreSorters.Clear();
            PrimaryScoreSorters = null;
            ClearSorterAndDelegateReferences();
            AdjustThumbnailVM = null;
            PaginatedScores = null;
            ChannelDisplays = null;
            Overlays = null;
            SharedVM = null;
            CytoApps.Infrastructure.UI.ApplicationCommands.HostCommands.ShutdownCommand.UnregisterCommand(ShutdownGridViewCommand);
            _eventAggregator = null;
        }

        #endregion
    }
}

