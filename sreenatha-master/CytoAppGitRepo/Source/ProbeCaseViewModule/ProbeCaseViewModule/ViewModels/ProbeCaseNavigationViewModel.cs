﻿using CytoApps.Infrastructure.UI.Events;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Services;
using System.ComponentModel.Composition;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    ///This ViewModel is used for plugin the pcv Module to the Case Browser Navigation View. This is the entry point of PCV Module
    /// </summary>
  
    public class ProbeCaseNavigationViewModel : BindableBase
    {
        #region "Private Variables"
        private IEventAggregator _eventAggregator;
        #endregion "Private Variables"

        #region "Constructor"
        /// <summary>
        /// Default contructor
        /// </summary>
        public ProbeCaseNavigationViewModel()
        {
        }

        /// <summary>
        /// Importing constructor to create instance of regionmanager,event aggregator, dataService
        /// </summary>
        /// <param name="regionManager"></param>
        /// <param name="eventAggregator"></param>
        /// <param name="dataService"></param>
        //[ImportingConstructor]
        public ProbeCaseNavigationViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }
        #endregion "Constructor"

        #region "Properties"
        private SlideViewModel _slideViewModel;
        public SlideViewModel SlideViewModel
        {
            get { return _slideViewModel; }
            set
            {
                _slideViewModel = value;
                OnPropertyChanged();
            }
        }

        private bool _isPieChartVisible;
        public bool IsPieChartVisible
        {
            get
            {
                return _isPieChartVisible;
            }
            set
            {
                _isPieChartVisible = value;
                OnPropertyChanged();
                // Check if IsPieChartVisible = false
                if (!IsPieChartVisible)
                    ClearCombinedResults();
                _eventAggregator.GetEvent<PieChartChangeEvent>().Publish(_slideViewModel);
            }
        }
        #endregion "Properties"


        #region Methods
        /// <summary>
        /// This method is used to set ShowCombinedSummary = false on activate scan area.
        /// To collapsed Expander, when user returns back to session view. 
        /// </summary>
        private void ClearCombinedResults()
        {
            if (SlideViewModel != null)
            {
                foreach (var scanarea in SlideViewModel.ScanAreaViewModelCollection)
                {
                    scanarea.ShowCombinedSummary = false;
                }
            }
        }
        #endregion
    }
}
