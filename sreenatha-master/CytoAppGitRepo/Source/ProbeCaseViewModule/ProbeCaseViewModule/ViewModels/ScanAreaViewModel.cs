﻿using Prism.Mvvm;
using ProbeCaseViewDataAccess.Models;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using CytoApps.Infrastructure.Helpers;
using System.Windows.Input;
using Prism.Commands;
using System;
using ProbeCaseViewModule.Events;
using Prism.Events;
using ProbeCaseViewModule.Services;
using System.IO;
using CytoApps.Infrastructure.UI;
using System.Linq;
using ProbeCaseViewModule.Models;
using System.Collections.Generic;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    /// This viewmodel is used bind scanArea view peoperties 
    /// </summary>
     public class ScanAreaViewModel : BindableBase
    {
        #region "Private members"

        private IEventAggregator _eventAggregator;
        private const string autoSessionName = "Session";

        #endregion "Private variables"

        #region "Constructor"

        public ScanAreaViewModel(ScanArea scanArea, IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _selectedScanArea = scanArea;
            IsSpotScoresAvailable = scanArea.HasSpotScores;
            _sessionViewModelCollection = new ObservableCollection<SessionViewModel>();

            // update the CanCreateAnotherSession flag based on number of sessions in the list
            SessionViewModelCollection.CollectionChanged += SessionViewModelCollection_CollectionChanged;
            if (_selectedScanArea != null)
            {
                NewSessionName = SetNewSessionName();
            }
            SubscribeEvents();
        }
        #endregion "Constructor"

        #region Subscribe Events
        private void SubscribeEvents()
        {
            _eventAggregator.GetEvent<ActivatedEvent>().Subscribe(Activated);
        }
        #endregion

        #region  "Properties"

        private bool _isSpotScoresAvailable;
        public bool IsSpotScoresAvailable
        {
            get { return _isSpotScoresAvailable; }
            set
            {
                _isSpotScoresAvailable = value;
                OnPropertyChanged();
            }
        }

        private ScanArea _selectedScanArea;
        public ScanArea SelectedScanArea
        {
            get { return _selectedScanArea; }
            set
            {
                _selectedScanArea = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<SessionViewModel> _sessionViewModelCollection;
        public ObservableCollection<SessionViewModel> SessionViewModelCollection
        {
            get { return _sessionViewModelCollection; }
            set
            {
                _sessionViewModelCollection = value;
                OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// For Delete a session
        /// </summary>
        private SessionViewModel sessionViewModel;
        public SessionViewModel SessionViewModel
        {
            get { return sessionViewModel; }
            set
            {
                sessionViewModel = value;
                OnPropertyChanged();
            }
        }

        private string _newSessionName;
        public string NewSessionName
        {
            get
            {
                return _newSessionName;
            }
            set
            {
                _newSessionName = value;
                ValidateSessionName();
                OnPropertyChanged();
            }
        }

        private void ValidateSessionName()
        {
            if (string.IsNullOrWhiteSpace(NewSessionName) || SessionViewModelCollection.Any(x => x.Session.Name.ToLower() == NewSessionName.ToLower()))
            {
                ValidSessionName = false;
            }
            else
            {
                ValidSessionName = true;
            }
        }

        private bool _validSessionName;
        public bool ValidSessionName
        {
            get { return _validSessionName; }
            set
            {
                _validSessionName = value;
                OnPropertyChanged();
            }
        }

        private bool _useSpotScores;
        public bool UseSpotScores
        {
            get { return _useSpotScores; }
            set
            {
                _useSpotScores = value;
                OnPropertyChanged();
            }
        }

        private bool _canCreateAnotherSession=true;
        public bool CanCreateAnotherSession
        {
            get
            {
                return _canCreateAnotherSession;
            }
            set
            {
                _canCreateAnotherSession = value;
                OnPropertyChanged();
            }
        }

        // Use to bind combined summary with Pie Chart
        private SessionSummary _combinedSummary;
        public SessionSummary CombinedSummary
        {
            get
            {
                return _combinedSummary;
            }
            set
            {
                _combinedSummary = value;
                OnPropertyChanged();
            }
        }

        // Use to Show Combined Results
        private bool _showCombinedSummary;
        public bool ShowCombinedSummary
        {
            get
            {
                return _showCombinedSummary;
            }
            set
            {
                _showCombinedSummary = value;
                OnPropertyChanged();
                if (ShowCombinedSummary && (SessionViewModelCollection != null && SessionViewModelCollection.Count > 1))
                {
                    var sessionSummaries = SessionViewModelCollection.Select(m => m.SessionSummary);
                    CombinedSummary = CalculateAndDisplayCombinedResults(sessionSummaries);
                }
            }
        }

        // It is used to check if Assay Name of both the session is equal or not
        private bool _isAssaysEqual;
        public bool IsAssaysEqual
        {
            get
            {
                return _isAssaysEqual;
            }
            set
            {
                _isAssaysEqual = value;
                OnPropertyChanged();
            }
        }

        #endregion "Properties"

        #region "Commands"
        private DelegateCommand<object> _createSessionCommand;
        public ICommand CreateSessionCommand
        {
            get
            {
                return _createSessionCommand ?? (_createSessionCommand = new DelegateCommand<object>((useSpotScores) =>
                {
                    UseSpotScores = Convert.ToBoolean(useSpotScores);
                    _eventAggregator.GetEvent<CreateSessionEvent>().Publish(this);
                    NewSessionName = SetNewSessionName();
                    CanCreateAnotherSession = SessionViewModelCollection.Count() < 2;

                }, (useSpotScores) => true));
            }
        }

        private DelegateCommand<SessionViewModel> _deleteSessionCommand;
        public ICommand DeleteSessionCommand
        {
            get
            {
                return _deleteSessionCommand ?? (_deleteSessionCommand = new DelegateCommand<SessionViewModel>((sessionViewModel) =>
                {
                    WaitCursor.Show();
                    SessionViewModel = sessionViewModel;
                    _eventAggregator.GetEvent<DeleteSessionEvent>().Publish(this);
                    NewSessionName = SetNewSessionName();
                    CanCreateAnotherSession = SessionViewModelCollection.Count() < 2;


                }));
            }
        }

        private DelegateCommand<object> _createSelectedFramesSessionCommand;
        public ICommand CreateSelectedFramesSessionCommand
        {
            get
            {
                return _createSelectedFramesSessionCommand ?? (_createSelectedFramesSessionCommand = new DelegateCommand<object>((useSpotScores) =>
                {
                    UseSpotScores = Convert.ToBoolean(useSpotScores);
                    _eventAggregator.GetEvent<CreateSessionWithSelectedFramesEvent>().Publish(this);
                    //NewSessionName = SetNewSessionName();
                    //CanCreateAnotherSession = SessionViewModelCollection.Count() < 2;

                }, (useSpotScores) => true));
            }
        }
        #endregion "Commands"

        #region "Methods"

        /// <summary>
        /// set New Name for the Session
        /// </summary>
        /// <returns></returns>
        private string SetNewSessionName()
        {
            int n = 1;
            string sessionName = autoSessionName + " " + n.ToString();
            if (SessionViewModelCollection.Count == 0)
            {
                return sessionName;
            }
            else if (SessionViewModelCollection.Count == 1)
            {
                var name = SessionViewModelCollection.Select(x => x.Session.Name).FirstOrDefault().ToLower().Trim();
                if (name == sessionName.ToLower())
                    sessionName = autoSessionName + " " + (++n).ToString();
            }
            return sessionName;
        }

        private void SessionViewModelCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            NewSessionName = SetNewSessionName();
            CanCreateAnotherSession = SessionViewModelCollection.Count() < 2;
        }

        /// <summary>
        /// This method is used to calculate the Average Result between two Sessions
        /// And Display Combined Results Pie Chart.
        /// </summary>
        /// <param name="sessionSummaries"></param>
        /// <returns></returns>
        private SessionSummary CalculateAndDisplayCombinedResults(IEnumerable<SessionSummary> sessionSummaries)
        {
            SessionSummary combinedSummary = new SessionSummary();
            if (sessionSummaries != null && sessionSummaries.Count() > 1)
            {
                combinedSummary.TotalInformativeCell = sessionSummaries.Sum(ti => ti.TotalInformativeCell);
                var allClasses = sessionSummaries.SelectMany(l => l.Classes).GroupBy(n => n.ClassName).Select(g => g.First()); // get distinct classes
                var total = (from t in allClasses select new ClassFilter { ClassName = t.ClassName, ClassCount = 0, Color = t.Color }).ToArray();

                foreach (var blind in sessionSummaries)
                {
                    foreach (var classTotal in total)
                    {
                        var theClass = blind.Classes.Where(a => a.ClassName == classTotal.ClassName).FirstOrDefault();
                        if (theClass != null)
                            classTotal.ClassCount += theClass.ClassCount;
                    }
                }
                var max = total.Where(a => a.IsUninformativeClass == false).Sum(a => a.ClassCount);
                foreach (var x in total)
                    x.ClassPercentage = (max > 0) ? ((double)x.ClassCount * 100.0) / (double)max : 0.0;
                if (total != null)
                {
                    combinedSummary.Classes = total.OrderByDescending(classFilter => classFilter.ClassName).Where(x => x.ClassName.ToLower() != Constants.Uninformative).ToObservableCollection();
                }
            }

            return combinedSummary;
        }

        /// <summary>
        /// This method is called when user return back to Scan Area from GridView.
        /// It is used to Update changes done in Selection Session.
        /// To Check if Assay has been changed or not and
        /// To Update Session Summary of selected Session.
        /// To Update Combined Summary.
        /// </summary>
        /// <param name="value"></param>
        private void Activated(object value)
        {            
            if (value != null)
            {
                //Update Pie Chart
                var sessioninfo = (value as Dictionary<Session, SessionSummary>).First();
                var session = SessionViewModelCollection.SingleOrDefault(s => s.Session.Id == sessioninfo.Key.Id && s.Session.ScanAreaId == sessioninfo.Key.ScanAreaId);
                if (session != null)
                {
                    var ss = sessioninfo.Value;
                    session.SessionSummary = ss;
                    // Remove uninformative class
                    session.SessionSummary.Classes = ss.Classes.OrderByDescending(classFilter => classFilter.ClassName).Where(x => x.ClassName.ToLower() != Constants.Uninformative).ToObservableCollection();
                    IsAssaysEqual = SessionViewModelCollection.Select(x => x.Session.AssayName).Distinct().Count() < 2;
                    if (SessionViewModelCollection != null && SessionViewModelCollection.Count > 1)
                    {
                        var sessionSummaries = SessionViewModelCollection.Select(m => m.SessionSummary).Where(w => w != null);
                        CombinedSummary = CalculateAndDisplayCombinedResults(sessionSummaries);
                    }
                }
            }
            
        }
        #endregion "Methods"
    }
}
