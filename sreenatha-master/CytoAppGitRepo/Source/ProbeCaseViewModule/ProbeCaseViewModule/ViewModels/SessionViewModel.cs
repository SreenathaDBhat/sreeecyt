﻿using CytoApps.Exceptions;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using ProbeCaseViewDataAccess.Models;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Models;
using System;
using System.ComponentModel.Composition;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    /// This ViewModel binds properties of Session View and opening of a session
    /// </summary>    
    public class SessionViewModel : BindableBase
    {
        #region "Private variables"
        private IEventAggregator _eventAggregator;
        #endregion "Private variables"

        #region "Constructor"
        public SessionViewModel(Session sessionDetails, IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _session = sessionDetails;
        }
        #endregion "Constructor"

        #region "Properties"
        private Session _session;
        public Session Session
        {
            get { return _session; }
            set
            {
                _session = value;
                OnPropertyChanged();
            }
        }

        private SessionSummary _sessionSummary;
        public SessionSummary SessionSummary
        {
            get { return _sessionSummary; }
            set
            {
                _sessionSummary = value;
                OnPropertyChanged();
            }
        }
        #endregion "Properties"

        #region "Commands"
        private DelegateCommand _openSessionCommand;
        public ICommand OpenSessionCommand
        {
            get
            {
                return _openSessionCommand ?? (_openSessionCommand = new DelegateCommand(() =>
                {
                    WaitCursor.Show();
                    _eventAggregator.GetEvent<OpenSessionEvent>().Publish(_session);
                }));
            }
        }

        //private DelegateCommand _deleteSessionCommand;
        //public ICommand DeleteSessionCommand
        //{
        //    get
        //    {
        //        return _deleteSessionCommand ?? (_deleteSessionCommand = new DelegateCommand(() =>
        //        {
        //            WaitCursor.Show();
        //            _eventAggregator.GetEvent<DeleteSessionEvent>().Publish(_session);
        //        }));
        //    }
        //}
        #endregion "Commands"

        #region "Methods"
        #endregion "Methods"
    }
}
