﻿using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Localization;
using PCVImageRenderers;
using PCVImageRenderers.Models;
using Prism.Mvvm;
using ProbeCaseViewDataAccess.Models;
using ProbeCaseViewModule.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    /// This ViewModel class shares a single instance betweeen GridViewModel and FrameViewModel helps to create scored cell in respective frame
    /// </summary>
    public class SharedViewModel : BindableBase
    {
        public delegate void FrameChangeHandler();
        public event FrameChangeHandler FrameChangeEvent;

        #region "Constructor"
        public SharedViewModel(SessionScores sessionScores, ProbeScoringAssay assay, IEnumerable<Frame> frames, IEnumerable<ScoreSettings> scoreSetting)
        {
            _sessionScores = sessionScores;
            _selectedSession = sessionScores.Session;
            _currentAssay = assay;
            SetCommonInformation(frames, scoreSetting);
        }

        private void SetCommonInformation(IEnumerable<Frame> sessionFrames, IEnumerable<ScoreSettings> scoreCellSettingList)
        {
            //Load frames to shared viewmodel
            int framenum = 1;
            Frames = new ObservableCollection<FrameInfo>(from frame in sessionFrames select new FrameInfo(framenum++) { Image = frame });
            if (Frames != null && Frames.Count > 0)
                _currentFrame = Frames.First();

            //Load Scores to shared viewmodel
            var components = CurrentFrame != null ? CurrentFrame.Image.Components : null;
            ScoredCells = (from scoredCell in _sessionScores.Scores select CreateScoredCell(scoredCell, components)).ToObservableCollection();

            // Get list of enhanced/reviewed/rescored cells
            foreach (var cellSetting in scoreCellSettingList)
            {
                ScoredCells.Where(cell => cell.Score.Id == cellSetting.scoreid).
                    ForEach(cell =>
                    {
                        cell.Reviewed = cellSetting.IsReviewed;
                        cell.IsRescored = cellSetting.IsRescored;
                        cell.HasEnhancements = cellSetting.IsEnhanced;
                        cell.IsDeleted = false;
                    });
            }
            SessionSummary = new SessionSummary();
        }

        private ScoredCell CreateScoredCell(Score score, IEnumerable<FrameComponent> components)
        {
            var thumbnailRenderer = new StackedThumbnailRenderer(components);
            return new ScoredCell(score, thumbnailRenderer);
        }
        #endregion "Constructor"

        #region "Properties"

        private Session _selectedSession;
        public Session SelectedSession
        {
            get
            {
                return _selectedSession;
            }
            set
            {
                _selectedSession = value;
            }
        }

        private ObservableCollection<FrameInfo> _frames;
        public ObservableCollection<FrameInfo> Frames
        {
            get
            {
                if (_frames == null)
                    _frames = new ObservableCollection<FrameInfo>();
                return _frames;
            }
            set
            {
                _frames = value;
                OnPropertyChanged();
            }
        }

        private FrameInfo _currentFrame;
        public FrameInfo CurrentFrame
        {
            get
            {
                if (_currentFrame == null)
                    _currentFrame = new FrameInfo(0);
                return _currentFrame;
            }
            set
            {
                if (_currentFrame != value)
                {
                    _currentFrame = value;
                    OnPropertyChanged();
                    FrameChangeEvent();
                }
            }
        }

        private ObservableCollection<ScoredCell> _scoredCells;
        public ObservableCollection<ScoredCell> ScoredCells
        {
            get
            {
                return _scoredCells;
            }
            set
            {
                _scoredCells = value;
                OnPropertyChanged();
            }
        }

        private SessionSummary _sessionSummary;
        public SessionSummary SessionSummary
        {
            get
            {
                return _sessionSummary;
            }
            set
            {
                _sessionSummary = value;
                OnPropertyChanged();
            }
        }

        private SessionScores _sessionScores;
        public SessionScores SessionScores
        {
            get
            {
                return _sessionScores;
            }
            set
            {
                _sessionScores = value;
                OnPropertyChanged();
            }
        }

        private ProbeScoringAssay _currentAssay;
        public ProbeScoringAssay CurrentAssay
        {
            get
            {
                return _currentAssay;
            }
            set
            {
                _currentAssay = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<FrameAnnotationViewModel> _allFrameAnnotations;
        public ObservableCollection<FrameAnnotationViewModel> AllFrameAnnotations
        {
            get
            {
                if (_allFrameAnnotations == null)
                    _allFrameAnnotations = new ObservableCollection<FrameAnnotationViewModel>();
                return _allFrameAnnotations;
            }
            set
            {
                _allFrameAnnotations = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<string> _commonAnnotations;
        public ObservableCollection<string> CommonAnnotations
        {
            get
            {
                if (_commonAnnotations == null)
                    _commonAnnotations = new ObservableCollection<string>();
                return _commonAnnotations;
            }
            set
            {
                _commonAnnotations = value;
                OnPropertyChanged();
            }
        }

      
        private ObservableCollection<AnalysisRegionInfo> _allFrameRegions;
        public ObservableCollection<AnalysisRegionInfo> AllFrameRegions
        {
            get
            {
                if (_allFrameRegions == null)
                {
                    _allFrameRegions = new ObservableCollection<AnalysisRegionInfo>();
                }
                return _allFrameRegions;
            }
            set
            {
                _allFrameRegions = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// isReporcessing flag is true  to indicates the reporcess is going on
        /// </summary>
        private bool _isReprocessing;
        public bool IsReprocessing
        {
            get
            {
                return _isReprocessing;
            }
            set
            {
                _isReprocessing = value;              
                OnPropertyChanged();
            }
        }

      
        public bool IsDirty { get; set; }

        private List<SignalInfo> _signals;
        public List<SignalInfo> Signals
        {
            get
            {
                if (_signals == null)
                    _signals = new List<SignalInfo>();
                return _signals;
            }
            set
            {
                _signals = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// While Close window, show a popup message when reprocess is going on
        /// </summary>
        public void ShowReprocessingBusyMessage()
        {
            CustomPopUp.Show(Literal.Strings.Lookup("cbStr-ReprocessBusyMessage"), Literal.Strings.Lookup("cbStr-PcvTitle"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
        }


        #endregion
    }
}
