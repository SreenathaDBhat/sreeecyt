﻿using CytoApps.Infrastructure.UI.Controls;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Events;
using ProbeCaseViewModule.Events;
using ProbeCaseViewModule.Logic;
using ProbeCaseViewModule.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    /// This ViewModel is used to display whole slide information in one view such as Top Overlay Image(etch image), Frames and All scores
    /// Pan and zoom frames, Navigate between the frames by finding the closest frame and toggle scores
    /// </summary>
    public class SlideOverviewViewModel : INotifyPropertyChanged
    {
        private bool lowSpec;
        private ThumbnailsLoader thumbnailLoader;
        private IEventAggregator _eventAggregator;
        private SlideOverViewLogic _slideOverviewLogic;

        #region "Constructor"
        public SlideOverviewViewModel(FrameWindowViewModel frameVM)
        {
            _frameVM = frameVM;
            _eventAggregator = ServiceLocator.Current.TryResolve<IEventAggregator>();
            _slideOverviewLogic = new SlideOverViewLogic();
            OverViewEnabled = true;
            ShowScores = true;
            RotateSlide = 180.0;
            Magnification = 1.0;
            CheckPerformance();
            LoadSlideOverviewImages();
        }

        #endregion "Constructor"

        #region "Methods"
        /// <summary>
        /// Load frames and etch images and intialization of Slide overview 
        /// </summary>
        private void LoadSlideOverviewImages()
        {
            int h, w;
            RotateSlide = (_frameVM.FlipSlideOverview) ? 0.0 : 180.0;
            // HACK!!! .imageframes diesn't have width or height, is set when image loaded, so 1st image is set
            // use that for now but needs to be added to imageframes
            if (_frameVM.SharedVM.Frames.Count() > 0)
            {
                var useful = _frameVM.SharedVM.Frames.Where(a => a.Image.Width > 0).FirstOrDefault();
                if (useful == null)
                {
                    //Hack need to get height and Width from frame // TempFix
                    h = 1200;
                    w = 1600;
                    useful = _frameVM.SharedVM.Frames.FirstOrDefault();
                }
                else
                {
                    h = useful.Image.Height;
                    w = useful.Image.Width;
                }
                ScoresDiameter = (_frameVM.CircleDiameterPixels * useful.Image.XScale) * ScaleX;
                foreach (var f in _frameVM.SharedVM.Frames)
                {
                    f.Image.Width = w;
                    f.Image.Height = h;
                    if (f.Image.Calibrated == false)
                        OverViewEnabled = false;
                }
            }
            var slideFrames = from f in _frameVM.SharedVM.Frames
                              select new FrameOutlineInfo(f.Image, ScaleX, ScaleY);
            _slideOverViewFrames = slideFrames.ToArray();
            TopLevel = GetOverviewImages();
            IsViewable = _slideOverViewFrames.Where(a => a.PositionX != 0.0 && a.PositionY != 0.0).Count() > 0;
            SlideName = _frameVM.SharedVM.SelectedSession.Slide.Name;
            OnPropertyChanged("Frames", "TopLevel", "IsViewable", "SlideName", "ScoresDiameter");
            if (thumbnailLoader != null)
                thumbnailLoader.Stop();
            thumbnailLoader = new ThumbnailsLoader(this, Dispatcher.CurrentDispatcher, lowSpec, GetFramesAndEtchImagesFromServer);
            thumbnailLoader.Start();
        }

        /// <summary>
        /// Frame changed event to select respected frame outline image
        /// </summary>
        public void CurrentFrameChanged()
        {
            CurrentFrameOutline = SlideOverViewFrames == null ? null : SlideOverViewFrames.Where(f => f.Frame == CurrentFrame.Image).FirstOrDefault();
        }

        /// <summary>
        /// Delegate method to download frame and etch images from the server
        /// </summary>
        /// <param name="frame"> frame outline</param>
        /// <param name="decodeWidth"> decode width to create new image</param>
        /// <param name="bigImage">check for etch images or normal frame image</param>
        /// <returns></returns>
        public ImageSource GetFramesAndEtchImagesFromServer(FrameOutlineInfo frame, int decodeWidth, bool bigImage)
        {
            if (bigImage)
            {
                return _slideOverviewLogic.GetSlideOverviewImage(_frameVM.SelectedSession.Slide, frame.ThumbnailPath, decodeWidth);
            }
            else
                return _slideOverviewLogic.GetCachedFrameBitmap(_frameVM.SelectedSession.Slide, _frameVM.SelectedSession.ScanAreaId, frame.Frame, decodeWidth);
        }

        /// <summary>
        /// Check system performance based on total RAM
        /// </summary>
        private void CheckPerformance()
        {
            var ci = new Microsoft.VisualBasic.Devices.ComputerInfo();
            var totalRam = ci.TotalPhysicalMemory;
            lowSpec = totalRam < Constants.RequiredRAMMemory;
        }

        //MARV- This can be technical ClearCache ?
        /// <summary>
        /// Clear all cached images on session close
        /// </summary>
        public void TidyUp()
        {
            if (thumbnailLoader != null)
            {
                thumbnailLoader.TidyUp();
            }
        }

        /// <summary>
        /// Get collection of overview images
        /// </summary>
        /// <returns></returns>
        private IEnumerable<FrameOutlineInfo> GetOverviewImages()
        {
            List<FrameOutlineInfo> frameOutlineInfo = new List<FrameOutlineInfo>();
            var frameOutlines = _slideOverviewLogic.GetFrameOutlines(_frameVM.SharedVM.SelectedSession.Slide);
            foreach (var frameOutline in frameOutlines)
            {
                frameOutlineInfo.Add(new FrameOutlineInfo(ScaleX, ScaleY, frameOutline));
            }
            return frameOutlineInfo;
        }

        /// <summary>
        /// Find the closest frame based on the mouse click point
        /// </summary>
        /// <param name="pt">mouse point</param>
        /// <returns></returns>
        private FrameInfo GetClosestFrame(Point pt)
        {
            double distance = double.MaxValue;
            FrameInfo closest = null;
            foreach (var f in SlideOverViewFrames)
            {
                var x = Math.Abs(f.CentreX - pt.X);
                var y = Math.Abs(f.CentreY - pt.Y);

                var d = (x * x) + (y * y);
                if (d < distance)
                {
                    closest = FrameVM.SharedVM.Frames.Where(fr => fr.Image == f.Frame).FirstOrDefault();
                    distance = d;
                }
            }
            return closest;
        }

        public void Dispose()
        {
            TidyUp();
            _eventAggregator = null;
            _frameVM = null;
           
            TopLevel = null;
            if (_slideOverViewFrames != null)
                _slideOverViewFrames = null;
            CurrentFrameOutline = null;
        }

        #endregion "Methods"

        #region "Commands"
        private DelegateCommand<ZoomChangedEventArgs> _slideOverviewZoomChangedCommand;
        public ICommand SlideOverviewZoomChangedCommand
        {
            get
            {
                return _slideOverviewZoomChangedCommand ?? (_slideOverviewZoomChangedCommand = new DelegateCommand<ZoomChangedEventArgs>((zoomEventArgs) =>
                {
                    var args = zoomEventArgs;
                    _zoom = args.Zoom;
                    OnPropertyChanged("LineScale", "Zoom");
                }));
            }
        }

        private DelegateCommand _showFrameViewCommand;
        public ICommand ShowFrameViewCommand
        {
            get
            {
                return _showFrameViewCommand ?? (_showFrameViewCommand = new DelegateCommand(() =>
                {
                    FrameVM.ShowOverview = false;
                }));
            }
        }

        private DelegateCommand<object> _slideOverviewViewportChangedCommand;
        public ICommand SlideOverviewViewportChangedCommand
        {
            get
            {
                return _slideOverviewViewportChangedCommand ?? (_slideOverviewViewportChangedCommand = new DelegateCommand<object>((viewPortArgs) =>
                {
                    var args = viewPortArgs as ViewportChangedEventArgs;
                    var vp = args.Viewport;
                    if (thumbnailLoader != null)
                        thumbnailLoader.SetViewport(vp, _zoom);
                }));
            }
        }

        private DelegateCommand<object> _selectNearestFrameCommand;
        public ICommand SelectNearestFrameCommand
        {
            get
            {
                return _selectNearestFrameCommand ?? (_selectNearestFrameCommand = new DelegateCommand<object>((leftMouseButtonEvent) =>
                {
                    var args = leftMouseButtonEvent as MouseClickEventArgs;
                    if (args.ClickCount == 2)
                    {

                    }
                    else
                    {
                        CurrentFrame = GetClosestFrame(args.Points);
                        _eventAggregator.GetEvent<ZoomAndPanToCurrentFrameEvent>().Publish(CurrentFrame);
                    }

                }));
            }
        }

        private DelegateCommand _fitAllInViewFramesCommand;
        public ICommand FitAllInViewFramesCommand
        {
            get
            {
                return _fitAllInViewFramesCommand ?? (_fitAllInViewFramesCommand = new DelegateCommand(() =>
                {
                    if (!IsViewable)
                        return;
                    _eventAggregator.GetEvent<FitAllFramesInSlideEvent>().Publish(this);
                }));
            }
        }

        private DelegateCommand _intializeSlideOverviewCommand;
        public ICommand IntializeSlideOverviewCommand
        {
            get
            {
                return _intializeSlideOverviewCommand ?? (_intializeSlideOverviewCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<FitAllFramesInSlideEvent>().Publish(this);
                }));
            }
        }

        private DelegateCommand _toggleAllScoresCommand;
        public ICommand ToggleAllScoresCommand
        {
            get
            {
                return _toggleAllScoresCommand ?? (_toggleAllScoresCommand = new DelegateCommand(() =>
                {
                    ShowScores = !ShowScores;
                }));
            }
        }
        #endregion "Commands"

        #region "Properties"
        private IEnumerable<FrameOutlineInfo> _topLevel;
        public IEnumerable<FrameOutlineInfo> TopLevel
        {
            get { return _topLevel; }
            set
            {
                _topLevel = value;
                OnPropertyChanged();
            }
        }

        public string SlideName { get; private set; }
        public double SlideWidth { get { return 1250; } }
        public double SlideHeight { get { return 3800; } }
        public double ScaleX { get { return SlideWidth / 25000.0; } }
        public double ScaleY { get { return SlideHeight / 76000.0; } }

        private FrameWindowViewModel _frameVM;
        public FrameWindowViewModel FrameVM
        {
            get
            {
                return _frameVM;
            }
        }

        private bool _overViewEnabled;
        public bool OverViewEnabled
        {
            get { return _overViewEnabled; }
            set
            {
                _overViewEnabled = value;
                OnPropertyChanged();
            }
        }

        private bool _isViewable;
        public bool IsViewable
        {
            get { return _isViewable; }
            set
            {
                _isViewable = value;
                OnPropertyChanged();
            }
        }

        private FrameOutlineInfo currentFrameOutline;
        public FrameOutlineInfo CurrentFrameOutline
        {
            get { return currentFrameOutline; }
            set
            {
                currentFrameOutline = value;
                OnPropertyChanged();
            }
        }

        private double _scoreDiameter;
        public double ScoresDiameter
        {
            get { return _scoreDiameter; }
            set
            {
                _scoreDiameter = value;
                OnPropertyChanged();
            }
        }

        private double _magnification;
        public double Magnification
        {
            get { return _magnification; }
            set
            {
                _magnification = value;
                OnPropertyChanged();
            }
        }

        public FrameInfo CurrentFrame
        {
            get
            {
                return _frameVM.SharedVM.CurrentFrame;
            }
            set
            {
                _frameVM.SharedVM.CurrentFrame = value;
                OnPropertyChanged();
            }
        }

        private bool _showScores;
        public bool ShowScores
        {
            get { return _showScores; }
            set
            {
                _showScores = value;
                OnPropertyChanged();
            }
        }

        private double _zoom = 1.0;
        public double Zoom
        {
            get
            {
                return _zoom;
            }
            set
            {
                _zoom = value;
                OnPropertyChanged();
            }
        }

        public double LineScale
        {
            get { return 10.0 / _zoom; }
        }

        private IEnumerable<FrameOutlineInfo> _slideOverViewFrames;
        public IEnumerable<FrameOutlineInfo> SlideOverViewFrames
        {
            get { return _slideOverViewFrames; }
        }

        private double _rotateSlide;
        public double RotateSlide
        {
            get { return _rotateSlide; }
            set
            {
                _rotateSlide = value;
                OnPropertyChanged();
            }
        }

        #endregion "Properties"

        #region Notify stuff
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null)
            {
                foreach (var propertyName in propertyNames)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// This class is used load slide overview images and maintain performance and memory management
    /// </summary>
    internal class ThumbnailsLoader
    {
        private Dispatcher _dispatcher;
        private Task _loader;
        private Task _loadBigOverviewImage;
        private AutoResetEvent _pulse = new AutoResetEvent(true);
        private bool _stop;
        private bool _reset;
        private SlideOverviewViewModel _slideOverview;
        private Rect _viewport;
        private double zoom;
        private List<FrameOutlineInfo> _allImages;
        Object _lockObj = new Object();
        bool _lowSpec;
        double _loadZoomLevel;
        int _decodeWidth;
        CancellationTokenSource _loadEtchCancelToken;

        public delegate ImageSource LoadslideOverviewFramesDelegate(FrameOutlineInfo frame, int decodeWidth, bool bigImage = false);
        LoadslideOverviewFramesDelegate _slideOverviewFrames;

        /// <summary>
        /// Initaite Thumbnail frame loaded and delegate callback
        /// </summary>
        /// <param name="slideOverview">Slide OverviewViewModel</param>
        /// <param name="dispatcher">dispatcher thread</param>
        /// <param name="lowspec">check system performance</param>
        /// <param name="slideOverviewFrames">download frameimages from the parent VM</param>
        public ThumbnailsLoader(SlideOverviewViewModel slideOverview, Dispatcher dispatcher, bool lowspec, LoadslideOverviewFramesDelegate slideOverviewFrames)
        {
            this._lowSpec = lowspec;
            // things you can tweak for memory and speed
            MaximumFrames = _lowSpec ? 250 : 400;
            _loadZoomLevel = _lowSpec ? 1 : 1;
            _decodeWidth = _lowSpec ? Constants.MinDecodeWidth : Constants.DefaultDecodeWidth;

            _slideOverviewFrames = slideOverviewFrames;
            this._dispatcher = dispatcher;
            this._slideOverview = slideOverview;
        }

        public int MaximumFrames
        {
            get;
            set;
        }

        public void Start()
        {
            if (_slideOverview == null || _slideOverview.SlideOverViewFrames == null)
                return;

            _allImages = new List<FrameOutlineInfo>();
            _allImages.AddRange(_slideOverview.SlideOverViewFrames);
            _stop = false;
            _reset = false;
            _loader = Task.Factory.StartNew(() => LoadThumbnails(null));
            _loadEtchCancelToken = new CancellationTokenSource();
            _loadBigOverviewImage = Task.Factory.StartNew(() => LoadBigOverviewImage(), _loadEtchCancelToken.Token);

        }

        private void LoadBigOverviewImage()
        {
            foreach (var toplevelImage in _slideOverview.TopLevel)
            {
                if (toplevelImage.Thumbnail == null)
                {
                    //512 decode width results in good image quality for large Image displayed in the frame
                    ImageSource largeImage = _slideOverviewFrames(toplevelImage, Constants.MaxDecodeWidth, true);
                    _dispatcher.Invoke((ThreadStart)delegate
                    {
                        toplevelImage.Thumbnail = largeImage;

                    }, DispatcherPriority.Normal);
                }
            }
        }

        public void SetViewport(Rect viewport, double zoom)
        {
            _viewport = viewport;
            this.zoom = zoom;
            _reset = true;
            _pulse.Set();
        }

        public void Stop()
        {
            _stop = true;
            _pulse.Set();
        }

        private void DumpAll()
        {
            _dispatcher.Invoke((ThreadStart)delegate
            {
                foreach (var f in _slideOverview.SlideOverViewFrames)
                {
                    if (f.Thumbnail != null)
                        f.Thumbnail = null;
                }
            }, DispatcherPriority.Normal);
        }

        void LoadThumbnails(object state)
        {
            while (_pulse.WaitOne())
            {
                if (_stop)
                    break;

                var inviewbox = from im in _allImages
                                where new Rect(im.PositionX, im.PositionY, im.Width, im.Height).IntersectsWith(_viewport)
                                select im;

                if (zoom < _loadZoomLevel)
                {
                    DumpAll();
                    Thread.Sleep(100);
                    continue;
                }
                _reset = false;
                if (_stop || _reset)
                    break;
                int width = _allImages.Count() < MaximumFrames ? _decodeWidth : Constants.MinDecodeWidth;
                bool update = false;

                var inViewFrames = _allImages.Where(frame => inviewbox.Contains(frame)); ;

                Parallel.ForEach(inViewFrames, (frameInView) =>
                {
                    if (frameInView.Thumbnail == null)
                    {
                        var frameImage = _slideOverviewFrames(frameInView, width);
                        lock (_lockObj)
                        {
                            if (frameInView.Thumbnail == null)
                                update = true;
                            _dispatcher.Invoke((ThreadStart)delegate
                            {
                                if (update)
                                    frameInView.Thumbnail = frameImage;
                            }, DispatcherPriority.Normal);
                        }
                    }
                });
                Thread.Sleep(100);
            }
        }

        public void TidyUp()
        {
            if (_loader != null && _loadBigOverviewImage != null)
            {
                if (_loadEtchCancelToken != null)
                {
                    _loadEtchCancelToken.Cancel();
                    _loadEtchCancelToken.Dispose();

                }
                _stop = true;
                _pulse.Set();
            }
        }
    }
}
