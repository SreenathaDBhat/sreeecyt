﻿using CytoApps.Models;
using Prism.Mvvm;
using ProbeCaseViewDataAccess.Models;
using System.Collections.ObjectModel;

namespace ProbeCaseViewModule.ViewModels
{
    /// <summary>
    /// This viewmodel binds Slide View Properties
    /// </summary>
    public class SlideViewModel : BindableBase
    {
        #region "Constructor"
        public SlideViewModel(Slide slideDetails)
        {
            _slide = slideDetails;
            _scanAreaViewModelCollection = new ObservableCollection<ScanAreaViewModel>();
        }
        #endregion "Constructor"

        #region "Properties"
        private Slide _slide;
        public Slide Slide
        {
            get { return _slide; }
            set
            {
                _slide = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<ScanAreaViewModel> _scanAreaViewModelCollection;
        public ObservableCollection<ScanAreaViewModel> ScanAreaViewModelCollection
        {
            get { return _scanAreaViewModelCollection; }
            set
            {
                _scanAreaViewModelCollection = value;
                OnPropertyChanged();
            }
        }
        #endregion "Properties"
    }
}
