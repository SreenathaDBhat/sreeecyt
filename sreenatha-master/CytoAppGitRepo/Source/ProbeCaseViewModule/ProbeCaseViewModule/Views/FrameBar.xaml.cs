﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProbeCaseViewModule.Views
{
    /// <summary>
    /// Interaction logic for FrameBar.xaml
    /// </summary>
    public partial class FrameBar : UserControl
    {
        public FrameBar()
        {
            InitializeComponent();
        }

        public static DependencyProperty ItemTemplateProperty = DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(FrameBar), 
            new FrameworkPropertyMetadata(new PropertyChangedCallback(OnItemTemplateChanged)));
        public DataTemplate ItemTemplate
        {
            get
            {
                return (DataTemplate)GetValue(ItemTemplateProperty);
            }
            set
            {
                SetValue(ItemTemplateProperty, value);
            }
        }

        private static void OnItemTemplateChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {

        }
    }
}
