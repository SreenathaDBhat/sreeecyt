﻿using Prism.Regions;
using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace ProbeCaseViewModule.Views
{
    /// <summary>
    /// Interaction logic for GridView.xaml

    [RegionMemberLifetime(KeepAlive = false)]/// </summary>
    public partial class GridView : UserControl
    {
        public GridView()
        {
            InitializeComponent();
        }

    }
}
