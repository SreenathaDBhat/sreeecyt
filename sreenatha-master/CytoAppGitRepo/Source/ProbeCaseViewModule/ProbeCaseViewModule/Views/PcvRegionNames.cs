﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProbeCaseViewModule.Views
{
    public class PcvRegionNames
    {
        // ProbeCaseViewModule Regions
        public static string ScoresViewRegion = "ScoresViewRegion";
        public static string ControlsPanelRegion = "ControlsPanelRegion";
    }
}
