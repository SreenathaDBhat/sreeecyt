﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProbeCaseViewModule.Views
{
    /// <summary>
    /// Interaction logic for ScoredCellView.xaml
    /// </summary> 
    public partial class ScoredCellView : UserControl
    {
        public ScoredCellView()
        {
            InitializeComponent();
        }

        /// <summary>
        ///  Check Wheather Scored Cell View Is Selected Or Not
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(ScoredCellView), new PropertyMetadata(false));

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = null;
           
        }
    }
}
