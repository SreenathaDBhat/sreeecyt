﻿using CytoApps.Infrastructure.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProbeCaseViewModule.Views
{
    /// <summary>
    /// Interaction logic for FrameWindowView.xaml
    /// </summary>
    public partial class StoredAnnotationEditView : Window
    {
        public StoredAnnotationEditView()
        {
            InitializeComponent();
        }

        private void Close(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnMouseDownDrag(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
