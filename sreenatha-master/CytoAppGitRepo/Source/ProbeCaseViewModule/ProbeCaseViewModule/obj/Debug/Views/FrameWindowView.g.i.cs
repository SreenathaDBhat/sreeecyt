﻿#pragma checksum "..\..\..\Views\FrameWindowView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4552CA0CCB4FE42B6B5988578EBB0799"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Infrastructure.UI.Converters;
using CytoApps.Infrastructure.UI.Utilities;
using Prism.Interactivity;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Regions.Behaviors;
using ProbeCaseViewModule.Controls;
using ProbeCaseViewModule.Converters;
using ProbeCaseViewModule.Views;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ProbeCaseViewModule.Views {
    
    
    /// <summary>
    /// FrameWindowView
    /// </summary>
    public partial class FrameWindowView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ProbeCaseViewModule.Views.FrameWindowView FrameView;
        
        #line default
        #line hidden
        
        
        #line 233 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel topBar;
        
        #line default
        #line hidden
        
        
        #line 334 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border printVisual;
        
        #line default
        #line hidden
        
        
        #line 335 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CytoApps.Infrastructure.UI.Controls.StretchOrSizeCanvas stretchSizeCanvas;
        
        #line default
        #line hidden
        
        
        #line 336 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image frameImage;
        
        #line default
        #line hidden
        
        
        #line 338 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border overlayControls;
        
        #line default
        #line hidden
        
        
        #line 347 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl countsItemsControl;
        
        #line default
        #line hidden
        
        
        #line 420 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CytoApps.Infrastructure.UI.Controls.SlidingDrawer ComponentDisplayOptions;
        
        #line default
        #line hidden
        
        
        #line 428 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CytoApps.Infrastructure.UI.Controls.SlidingDrawer Results;
        
        #line default
        #line hidden
        
        
        #line 433 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CytoApps.Infrastructure.UI.Controls.SlidingDrawer NewAssay;
        
        #line default
        #line hidden
        
        
        #line 453 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CytoApps.Infrastructure.UI.Controls.SlidingDrawer LoadAssay;
        
        #line default
        #line hidden
        
        
        #line 483 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CytoApps.Infrastructure.UI.Controls.SlidingDrawer SaveAssay;
        
        #line default
        #line hidden
        
        
        #line 503 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CytoApps.Infrastructure.UI.Controls.SlidingDrawer Assays;
        
        #line default
        #line hidden
        
        
        #line 520 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CytoApps.Infrastructure.UI.Controls.SlidingDrawer Annotations;
        
        #line default
        #line hidden
        
        
        #line 531 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox annotationsCombo;
        
        #line default
        #line hidden
        
        
        #line 539 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CytoApps.Infrastructure.UI.Controls.SlidingDrawer ImageTools;
        
        #line default
        #line hidden
        
        
        #line 582 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.RotateTransform rotation;
        
        #line default
        #line hidden
        
        
        #line 612 "..\..\..\Views\FrameWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ProbeCaseViewModule.Views.SlideOverview slideOverView;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ProbeCaseViewModule;component/views/framewindowview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\FrameWindowView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.FrameView = ((ProbeCaseViewModule.Views.FrameWindowView)(target));
            return;
            case 2:
            this.topBar = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 3:
            this.printVisual = ((System.Windows.Controls.Border)(target));
            return;
            case 4:
            this.stretchSizeCanvas = ((CytoApps.Infrastructure.UI.Controls.StretchOrSizeCanvas)(target));
            return;
            case 5:
            this.frameImage = ((System.Windows.Controls.Image)(target));
            return;
            case 6:
            this.overlayControls = ((System.Windows.Controls.Border)(target));
            return;
            case 7:
            this.countsItemsControl = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 8:
            this.ComponentDisplayOptions = ((CytoApps.Infrastructure.UI.Controls.SlidingDrawer)(target));
            return;
            case 9:
            this.Results = ((CytoApps.Infrastructure.UI.Controls.SlidingDrawer)(target));
            return;
            case 10:
            this.NewAssay = ((CytoApps.Infrastructure.UI.Controls.SlidingDrawer)(target));
            return;
            case 11:
            this.LoadAssay = ((CytoApps.Infrastructure.UI.Controls.SlidingDrawer)(target));
            return;
            case 12:
            this.SaveAssay = ((CytoApps.Infrastructure.UI.Controls.SlidingDrawer)(target));
            return;
            case 13:
            this.Assays = ((CytoApps.Infrastructure.UI.Controls.SlidingDrawer)(target));
            return;
            case 14:
            this.Annotations = ((CytoApps.Infrastructure.UI.Controls.SlidingDrawer)(target));
            return;
            case 15:
            this.annotationsCombo = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 16:
            this.ImageTools = ((CytoApps.Infrastructure.UI.Controls.SlidingDrawer)(target));
            return;
            case 17:
            this.rotation = ((System.Windows.Media.RotateTransform)(target));
            return;
            case 18:
            this.slideOverView = ((ProbeCaseViewModule.Views.SlideOverview)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

