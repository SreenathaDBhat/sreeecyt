﻿using CytoApps.Models;
using Prism.Events;

/// <summary>
/// Contains events to add/save/edit/delete data source information
/// </summary>
namespace ToolBarModule.Events
{
    public class OpenDataSourceWindowEvent : PubSubEvent<object> { }
    public class CloseDataSourceWindowEvent : PubSubEvent<object> { }
    public class SaveDataSourceEvent : PubSubEvent<DataSource> { }
    public class EditDataSourceEvent : PubSubEvent<DataSource> { }
    public class DeleteDataSourceEvent : PubSubEvent<DataSource> { }
    public class TestConnectionEvent : PubSubEvent<DataSource> { }
}
