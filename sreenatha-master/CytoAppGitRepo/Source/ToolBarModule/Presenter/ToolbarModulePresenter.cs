﻿using Prism.Events;
using Prism.Regions;
using System.ComponentModel.Composition;
using ToolBarModule.Events;
using ToolBarModule.ViewModels;
using ToolBarModule.Views;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI;
using CytoApps.Models;
using CaseDataAccess;
using CytoApps.Localization;
using System;
using CytoApps.Exceptions;
using CytoApps.Infrastructure.UI.Events;
using System.ComponentModel.Composition.Hosting;
using System.Collections.Generic;
using System.Linq;


/// <summary>
/// This presenter will leverage functionality to Add/Edit/Save/Delete data source items and updated the observable collection binded to the view
/// Implements 'TestConnection' functionality to test whether the selected data source is accessible or not.
/// </summary>
namespace ToolBarModule.Presenter
{
    [Export]
    class ToolbarModulePresenter
    {
        #region Private members
        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        private AddDataSourceViewModel _addDataSourceViewModel;
        private AddDataSourceView _addDataSourceView;
        private ToolBarViewModel _toolBarViewModel;
        private ToolBarView _toolBarView;

        // DataSourcTemplatese defines the DataTypeId and connection parameters for a particular type of datasource
        private IDataSourceTemplate [] _availDataSourceTemplates = null;

        #endregion

        #region Constructor
        [ImportingConstructor]
        public ToolbarModulePresenter(IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _toolBarView = new ToolBarView();
            _toolBarViewModel = new ToolBarViewModel(_eventAggregator, _regionManager);
            _toolBarView.DataContext = _toolBarViewModel;
            _regionManager.RegisterViewWithRegion(RegionNames.ToolBarRegion, () => _toolBarView);

            // use reflection to load any IDataSourceTemplate plugins instances we find
            ImportAvailableDataSourceTemplates();

            SubscribeEvents();
        }
        #endregion

        /// <summary>
        /// Look in defined folder to resolve all IDataSourceTemplates, could use MEF but reflection is
        /// neater
        /// </summary>
        private void ImportAvailableDataSourceTemplates()
        {
            // load the data source templates instances from any implememting assembly
            if (_availDataSourceTemplates == null)
            {
                // Extract all Types from assemblies
                var availTemplateTypes =  DalReflectionFinder.GetTypesForInterface<IDataSourceTemplate>(@".");
                // create instances from those types
                var instances = from type in availTemplateTypes select DalReflectionFinder.GetInstanceOfType<IDataSourceTemplate>(type);
                // these can then be used to enumerate the types of datasource that we can connect to e.g local or eSm or ....
                _availDataSourceTemplates = instances.ToArray();
            }
        }

        #region Methods
        private void SubscribeEvents()
        {
            _eventAggregator.GetEvent<OpenDataSourceWindowEvent>().Subscribe(OpenDataSourceWindow);
            _eventAggregator.GetEvent<CloseDataSourceWindowEvent>().Subscribe(CloseDataSourceWindow);
            _eventAggregator.GetEvent<SaveDataSourceEvent>().Subscribe(SaveDataSource);
            _eventAggregator.GetEvent<EditDataSourceEvent>().Subscribe(EditDataSource);
            _eventAggregator.GetEvent<DeleteDataSourceEvent>().Subscribe(DeleteDataSource);
            _eventAggregator.GetEvent<TestConnectionEvent>().Subscribe(TestConnection);
        }

        private void OpenDataSourceWindow(object obj)
        {
            _addDataSourceView = new AddDataSourceView();
            _addDataSourceViewModel = new AddDataSourceViewModel(_eventAggregator);
            _addDataSourceViewModel.SelectedDataSource = new DataSource();
            _addDataSourceViewModel.AvailableDataSources = _availDataSourceTemplates;

            // Added this line to load Default DataSourceType Connection Parameters
           // _addDataSourceViewModel.SelectedDataSource.CreateConnectionParamters(_addDataSourceViewModel.SelectedDataSource.DataSourceType);
            _addDataSourceView.DataContext = _addDataSourceViewModel;
            _addDataSourceView.BringIntoView();
            _addDataSourceView.ShowDialog();
        }

        private void CloseDataSourceWindow(object obj)
        {
            _addDataSourceViewModel.Dispose();
            _addDataSourceView.Close();
        }

        private void EditDataSource(DataSource dataSourceItem)
        {
            if (dataSourceItem != null)
            {
                _addDataSourceView = new AddDataSourceView();
                _addDataSourceViewModel = new AddDataSourceViewModel(_eventAggregator);
                // add available datasource templates
                _addDataSourceViewModel.AvailableDataSources = _availDataSourceTemplates;
                _addDataSourceViewModel.CloneDataSource = (DataSource)dataSourceItem.Clone();
                _addDataSourceViewModel.SelectedDataSource = (DataSource)dataSourceItem.Clone();
                // Added this line to Enable TestConnection Button By Default on Edit
                _addDataSourceViewModel.IsValid = true;
                _addDataSourceView.DataContext = _addDataSourceViewModel;
                _addDataSourceView.ShowDialog();
            }
        }

        private void SaveDataSource(DataSource dataSourceItem)
        {
            if (dataSourceItem != null)
            {
                DataSourceManager.Instance.SaveDataSource(dataSourceItem);
                _addDataSourceView.Close();
            }
        }

        private void DeleteDataSource(DataSource dataSourceItem)
        {
            if (dataSourceItem != null)
            {
                DataSourceManager.Instance.DeleteDataSource(dataSourceItem);
            }
        }

        private void TestConnection(DataSource selectedDataSource)
        {
            try
            {
                // try and find a matching ICaseDataAccess for the dataSource
                var availCaseDalTypes = DalReflectionFinder.GetTypesForInterface<ICaseDataAccess>(@".");
                var caseDAL = DalReflectionFinder.GetMatchingInstanceOf<ICaseDataAccess>(availCaseDalTypes, selectedDataSource);

                if (caseDAL == null)
                {
                    LogManager.Error("TestConnection: Can't find matching Case Data Access layer for type - " + selectedDataSource.DataSourceType);
                    _addDataSourceViewModel.TestConnectionStatus = Literal.Strings.Lookup("cbStr-EnterValidDataSourceConnectionValue");
                    return;
                }

                // This will pass the connection parameters required (persisted by application per data source pointed at)
                caseDAL.Initialize(selectedDataSource);

                _addDataSourceViewModel.TestConnectionStatus = string.Empty;
                _addDataSourceViewModel.IsTestConnectionSuccessful = caseDAL.TestConnection(selectedDataSource);

                if (_addDataSourceViewModel.IsTestConnectionSuccessful)
                    _addDataSourceViewModel.TestConnectionStatus = Literal.Strings.Lookup("cbStr-ConnectionSuccessful");
                else
                    _addDataSourceViewModel.TestConnectionStatus = Literal.Strings.Lookup("cbStr-EnterValidDataSourceConnectionValue");
            }
            catch (DALException exception)
            {
                LogManager.Error("TestConnection: ", exception.InnerException);
                _addDataSourceViewModel.TestConnectionStatus = Literal.Strings.Lookup("cbStr-EnterValidDataSourceConnectionValue");
            }
            catch (Exception exception)
            {
                LogManager.Error("TestConnection: ", exception);
                _addDataSourceViewModel.TestConnectionStatus = Literal.Strings.Lookup("cbStr-EnterValidDataSourceConnectionValue");
            }
        }
        #endregion
    }
}
