﻿using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using System.ComponentModel.Composition;
using ToolBarModule.Presenter;

namespace ToolBarModule
{
    [ModuleExport(typeof(ToolBarModuleModule))]
    public class ToolBarModuleModule : IModule
    {
        [Import]
        public IRegionManager _regionManager;

        #pragma warning disable 0649, 0169
        [Import]
        private ToolbarModulePresenter _toolbarModulePresenter;
        #pragma warning restore 0649, 0169

        public void Initialize()
        {
        }
    }
}
