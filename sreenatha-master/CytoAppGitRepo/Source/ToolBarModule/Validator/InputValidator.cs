﻿using CytoApps.Localization;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace ToolBarModule.Validator
{
    public class InputValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // Call GetBoundValue() method to resolve BindingExpression.
            string stringValue = GetBoundValue(value) as string;

            if (string.IsNullOrEmpty(stringValue))
                return new ValidationResult(false, BindingPropertyName + Literal.Strings.Lookup("cbStr-validationErrorMessage"));
            return ValidationResult.ValidResult;
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo, BindingExpressionBase owner)
        {
            BindingPropertyName = ((BindingExpression)owner).ResolvedSourcePropertyName;
            if (((BindingExpression)owner).DataItem is CytoApps.Models.ConnectionParameter)
                BindingPropertyName = ((CytoApps.Models.ConnectionParameter)((BindingExpression)owner).DataItem).Key.ToString();
            return base.Validate(value, cultureInfo, owner);
        }

        public string BindingPropertyName { get; set; }

        /// <summary>
        /// Can be used to set Error Message in view
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// This mehtod is use to resolve BindingExpression value when ValidationStep="UpdateValue".
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private object GetBoundValue(object value)
        {
            if (value is BindingExpression)
            {
                // ValidationStep was UpdatedValue or CommittedValue (Validate after setting)
                // Need to pull the value out of the BindingExpression.
                BindingExpression binding = (BindingExpression)value;

                // Get the bound object and name of the property
                object dataItem = binding.DataItem;
                string propertyName = binding.ParentBinding.Path.Path;

                // Extract the value of the property.
                object propertyValue = dataItem.GetType().GetProperty(propertyName).GetValue(dataItem, null);

                // This is what we want.
                return propertyValue;
            }
            else
            {
                // ValidationStep was RawProposedValue or ConvertedProposedValue
                // The argument is already what we want!
                return value;
            }
        }
    }
}
