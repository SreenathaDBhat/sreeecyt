﻿using CytoApps.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows.Input;
using ToolBarModule.Events;
using System.Linq;

/// <summary>
/// This viewmodel implements DelegateCommand to save/cancel the entered data source information.
/// </summary>
namespace ToolBarModule.ViewModels
{
    public class AddDataSourceViewModel : BindableBase, IDisposable
    {
        #region private members
        [Import]
        private IEventAggregator _eventAggregator;
        #endregion

        private IDataSourceTemplate _selectedDataSourceTemplate;

        #region Constructor
        public AddDataSourceViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;

            _selectedDataSource = new DataSource();
        }
        #endregion

        public IEnumerable<IDataSourceTemplate> AvailableDataSources
        {
            get; set;
        }

        #region Commands
        private DelegateCommand _cancelDataSourceCommand;
        public ICommand CancelDataSourceCommand
        {
            get
            {
                return _cancelDataSourceCommand ?? (_cancelDataSourceCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<CloseDataSourceWindowEvent>().Publish(null);
                }, () => true));
            }
        }

        private DelegateCommand<DataSource> _saveDataSourceCommand;
        public ICommand SaveDataSourceCommand
        {
            get
            {
                return _saveDataSourceCommand ?? (_saveDataSourceCommand = new DelegateCommand<DataSource>((dataSource) =>
                {
                    _eventAggregator.GetEvent<SaveDataSourceEvent>().Publish(dataSource);
                }, (dataSource) => true));
            }
        }

        private DelegateCommand<DataSource> _dataSourceTypeChangedCommand;
        public ICommand DataSourceTypeChangedCommand
        {
            get
            {
                return _dataSourceTypeChangedCommand ?? (_dataSourceTypeChangedCommand = new DelegateCommand<DataSource>((dataSource) => 
                {
                    // find and retain the selected datasource template chosen
                    _selectedDataSourceTemplate = AvailableDataSources.Where(ds => ds.DataSourceTypeId == dataSource.DataSourceType).FirstOrDefault();
                    // Disable Test Connection Button when Datasource changes
                    this.IsValid = false;
                    if (CloneDataSource != null && CloneDataSource.ID != Guid.Empty && CloneDataSource.DataSourceType == dataSource.DataSourceType)
                    {
                        SelectedDataSource.ConnectionParameter = new System.Collections.ObjectModel.ObservableCollection<ConnectionParameter>();
                        foreach (var item in CloneDataSource.ConnectionParameter)
                        {
                            SelectedDataSource.ConnectionParameter.Add(item);
                        }
                    }
                    else
                    {
                        SelectedDataSource.CreateConnectionParamters(_selectedDataSourceTemplate);
                    }
                }));
            }
        }

        private DelegateCommand<DataSource> _testConnectionCommand;
        public ICommand TestConnectionCommand
        {
            get
            {
                return _testConnectionCommand ?? (_testConnectionCommand = new DelegateCommand<DataSource>((dataSource) =>
                {
                    _eventAggregator.GetEvent<TestConnectionEvent>().Publish(dataSource);
                }, (dataSource) => true));
            }
        }

        /// <summary>
        /// This command is added to handle TextChange on TextBox items in ItemsControl
        /// Its required to check for validation error on each Textbox in ItemsControl and
        /// Then set IsValid flag. To Enable TestConnection Button.
        /// </summary>
        private DelegateCommand _raiseTextChangedCommand;
        public ICommand RaiseTextChangedCommand
        {
            get
            {
                return _raiseTextChangedCommand ?? (_raiseTextChangedCommand = new DelegateCommand(() =>
                {
                    bool _result = false;
                    try
                    {
                        foreach (var item in SelectedDataSource.ConnectionParameter)
                        {
                            if (string.IsNullOrEmpty(item.Value))
                            {
                                _result = true;
                                break;
                            }
                        }
                    }
                    catch (Exception)
                    {
                        _result = true;
                    }
                    IsValid = !_result;
                }));
            }
        }
        #endregion

        #region Properties
        private DataSource _selectedDataSource;
        public DataSource SelectedDataSource
        {
            get
            {
                return _selectedDataSource;
            }
            set
            {
                _selectedDataSource = value;
                OnPropertyChanged();
            }
        }

        public DataSource CloneDataSource
        {
            get;
            set;
        }

        private bool _isTestConnectionSuccessful;
        public bool IsTestConnectionSuccessful
        {
            get { return _isTestConnectionSuccessful; }
            set
            {
                _isTestConnectionSuccessful = value;
                OnPropertyChanged();
            }
        }

        private string _testConnectionStatus;
        public string TestConnectionStatus
        {
            get { return _testConnectionStatus; }
            set
            {
                _testConnectionStatus = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// This property is used to validate data in ConnectionParameters
        /// </summary>
        private bool _isValid;
        public bool IsValid
        {
            get { return _isValid; }
            set { _isValid = value; OnPropertyChanged(); }
        }
        #endregion

        public void Dispose()
        {
            _selectedDataSource = null;
            CloneDataSource = null;
        }
    }
}
