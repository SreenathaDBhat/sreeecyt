﻿using CytoApps.Infrastructure;
using CytoApps.Infrastructure.CommonEnums;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.UI.Events;
using CytoApps.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Input;
using ToolBarModule.Events;

/// <summary>
/// Contains methods and properties to set the theme, language preferences, add/remove/update the date source information from the UI list
/// </summary>
namespace ToolBarModule.ViewModels
{
    [Export]
    public class ToolBarViewModel : BindableBase
    {
        #region "Private members"
        private ResourceDictionary dictionary;
        private IRegionManager _regionManager;
        private IEventAggregator _eventAggregator;
        #endregion

        #region "Constructor"
        [ImportingConstructor]
        public ToolBarViewModel(IEventAggregator eventAggregator, IRegionManager regionManager)
        {

            _regionManager = regionManager;
            _eventAggregator = eventAggregator;
            //DataSourceCollection = DataSourceManager.Instance.ReadDataSources();
            switch (Application.Current.Dispatcher.Thread.CurrentUICulture.Name)
            {
                case "de":
                    SelectedLanguage = Languages.German;
                    break;
                case "zh-CN":
                    SelectedLanguage = Languages.Chinese;
                    break;
                default:
                    SelectedLanguage = Languages.English;
                    break;
            }
        }
        #endregion "Constructor"

        #region Commands
        private DelegateCommand _addNewDataSourceCommand;
        public ICommand AddNewDataSourceCommand
        {
            get
            {
                return _addNewDataSourceCommand ?? (_addNewDataSourceCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<OpenDataSourceWindowEvent>().Publish(null);
                }, () => true));
            }
        }

        private DelegateCommand<DataSource> _deleteDataSourceCommand;
        public ICommand DeleteDataSourceCommand
        {
            get
            {
                return _deleteDataSourceCommand ?? (_deleteDataSourceCommand = new DelegateCommand<DataSource>((dataSource) =>
                {
                    _eventAggregator.GetEvent<DeleteDataSourceEvent>().Publish(dataSource);
                    _eventAggregator.GetEvent<DataSourceDeletedEvent>().Publish(dataSource);
                }, (dataSource) => true));
            }
        }

        private DelegateCommand<DataSource> _editDataSourceCommand;
        public ICommand EditDataSourceCommand
        {
            get
            {
                return _editDataSourceCommand ?? (_editDataSourceCommand = new DelegateCommand<DataSource>((dataSource) =>
                {
                    _eventAggregator.GetEvent<EditDataSourceEvent>().Publish(dataSource);
                }, (dataSource) => true));
            }
        }

        private DelegateCommand _toolbarInitializedCommand;
        public ICommand ToolbarInitializedCommand
        {
            get
            {
                return _toolbarInitializedCommand ?? (_toolbarInitializedCommand = new DelegateCommand(() =>
                {
                    WaitCursor.Show();
                    SelectedTheme = Properties.Settings.Default.Themes;
                    LoginHelper.SetUser();
                    UserName = LoginHelper.CurrentUser.Name;
                }, () => true));
            }
        }

        private DelegateCommand _openSettingsCommand;
        public ICommand OpenSettingsCommand
        {
            get
            {
                return _openSettingsCommand ?? (_openSettingsCommand = new DelegateCommand(() =>
                {
                    IsSettingsClicked = true;
                }, () => true));
            }
        }
        #endregion

        #region "Properties"
        private Themes _selectedTheme;
        public Themes SelectedTheme
        {
            get { return _selectedTheme; }
            set
            {
                _selectedTheme = value;
                Properties.Settings.Default.Themes = _selectedTheme;
                SetTheme(_selectedTheme);
                Properties.Settings.Default.Save();
                OnPropertyChanged();
            }
        }

        private Languages _selectedLanguage;
        public Languages SelectedLanguage
        {
            get { return _selectedLanguage; }
            set
            {
                _selectedLanguage = value;
                if (_selectedLanguage == Languages.German)
                {

                    if (Application.Current.Dispatcher.Thread.CurrentUICulture.Name != "de")
                    {
                        Properties.Settings.Default.Language = "de";
                        Properties.Settings.Default.Save();
                        System.Windows.Forms.Application.Restart();
                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                    }
                }
                else if (_selectedLanguage == Languages.Chinese)
                {
                    if (Application.Current.Dispatcher.Thread.CurrentUICulture.Name != "zh-CN")
                    {
                        Properties.Settings.Default.Language = "zh-CN";
                        Properties.Settings.Default.Save();
                        System.Windows.Forms.Application.Restart();
                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                    }
                }
                else
                {
                    if (!Application.Current.Dispatcher.Thread.CurrentUICulture.Name.ToString().Equals("en"))
                    {
                        Properties.Settings.Default.Language = "en";
                        Properties.Settings.Default.Save();
                        System.Windows.Forms.Application.Restart();
                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                    }
                }
                OnPropertyChanged();
            }
        }

        private bool _isSettingsClicked;
        public bool IsSettingsClicked
        {
            get { return _isSettingsClicked; }
            set
            {
                _isSettingsClicked = value;
                OnPropertyChanged();
            }
        }

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                OnPropertyChanged();
            }
        }


        //private ObservableCollection<DataSource> _dataSourceCollection;
        public ObservableCollection<DataSource> DataSourceCollection
        {
            get
            {
                return DataSourceManager.Instance.DataSourceCollection;
            }
        }
        #endregion "Properties"

        #region "Methods"

        /// <summary>
        /// Set selected theme
        /// </summary>
        /// <param name="selectedTheme">Themes</param>
        private void SetTheme(Themes selectedTheme)
        {
            try
            {
                switch (selectedTheme)
                {
                    case Themes.LightTheme:
                        LightStyleSelection(selectedTheme);
                        break;
                    case Themes.DarkTheme:
                        DarkStyleSelection(selectedTheme);
                        break;
                    default:
                        return;
                }
            }
            catch (Exception exception)
            {
                LogManager.Debug(exception.Message);
            }
        }

        /// <summary>
        /// Clear previous selected dictionary and set selected theme dictionary
        /// </summary>
        /// <param name="selectedTheme">DarkTheme</param>
        private void DarkStyleSelection(object selectedTheme)
        {
            dictionary = new ResourceDictionary();
            dictionary.Source = new Uri(@"/Themes/Dark/" + selectedTheme + ".xaml", UriKind.Relative);
            Application.Current.Resources.MergedDictionaries.Clear();
            Application.Current.Resources.MergedDictionaries.Add(dictionary);

            LogManager.Info("Theme: " + selectedTheme.ToString() + " loaded");
        }

        /// <summary>
        /// Clear previous selected dictionary and set selected theme dictionary
        /// </summary>
        /// <param name="selectedTheme">LightTheme</param>
        private void LightStyleSelection(object selectedTheme)
        {
            dictionary = new ResourceDictionary();
            dictionary.Source = new Uri(@"/Themes/Light/" + selectedTheme + ".xaml", UriKind.Relative);
            Application.Current.Resources.MergedDictionaries.Clear();
            Application.Current.Resources.MergedDictionaries.Add(dictionary);

            LogManager.Info("Theme: " + selectedTheme.ToString() + " loaded");
        }

        #endregion "Methods"
    }
}
