﻿using System.Windows;

namespace ToolBarModule.Views
{
    /// <summary>
    /// Interaction logic for AddDataSourceView.xaml
    /// </summary>
    public partial class AddDataSourceView : Window
    {
        public AddDataSourceView()
        {
            Owner = Application.Current.MainWindow;
            InitializeComponent();
        }
    }
}
