﻿using CaseStatisticsModule.Presenters;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using System.ComponentModel.Composition;

namespace CaseStatisticsModule
{
    [ModuleExport(typeof(CaseStatisticsModule))]
    public class CaseStatisticsModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        [Import]
        public CaseStatisticsPresenter _caseStatisticsPresenter;

        public void Initialize()
        {
            // this.RegionManager.RegisterViewWithRegion(RegionNames.ToolBarRegion, typeof(CaseStatisticsNavigationView));
        }
    }
}
