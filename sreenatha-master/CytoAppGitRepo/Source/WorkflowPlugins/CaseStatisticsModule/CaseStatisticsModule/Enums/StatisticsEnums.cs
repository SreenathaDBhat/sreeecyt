﻿namespace CaseStatisticsModule.Enums
{
    public enum TimelineTypes
    {
        Year = 1,
        Month = 2,
        Day = 3,
        Hour=4,
        Minute=5
    }

    public enum ChartType
    {
        Pie_Chart = 1,
        Percentage_Chart = 2,
        None = 3
    }
}
