﻿using CytoApps.Models;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseStatisticsModule.Events
{
    public class CaseStatsOpenEvent : PubSubEvent<object /* Unused */>
    {
    }

    public class CaseStatsDataSourceChangeEvent : PubSubEvent<DataSource>
    {
    }

    public class AddDynamicChartsEvent : PubSubEvent<IEnumerable<Models.StatisticsFilter>>
    {
    }

    public class SaveSearchFilterEvent : PubSubEvent<object /* Unused */>
    {
    }
}
