﻿using CaseStatisticsModule.Utilities;
using Prism.Mvvm;
using System.Collections.Generic;

namespace CaseStatisticsModule.Models
{
    public class Condition : BindableBase
    {
        private string _fieldName;
        public string FieldName
        {
            get
            {
                return _fieldName;
            }
            set
            {
                _fieldName = value;
                OnPropertyChanged("FieldName");
            }
        }

        private Operator _wildCard;
        public Operator WildCard
        {
            get
            {
                return _wildCard;
            }
            set
            {
                _wildCard = value;
                OnPropertyChanged("WildCard");
            }
        }

        private string _value;
        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                OnPropertyChanged("Value");
            }
        }

        private List<string> _fields;
        public List<string> Fields
        {
            get
            {
                return _fields;
            }
            set
            {
                _fields = value;
                OnPropertyChanged("Fields");
            }
        }

        private List<Operator> _wildCards;
        public List<Operator> WildCards
        {
            get
            {
                return _wildCards;
            }
            set
            {
                _wildCards = value;
                OnPropertyChanged("WildCards");
            }
        }

        public Condition(List<string> fields )
        {
            Fields = fields;

            FieldName = Fields[0];

            WildCards = new List<Operator>
            {
                new Operator("Equal","=","="),
                new Operator("Does not equal","NOT","!="),
                new Operator("Is greater than",">",">"),
                new Operator("Is greater than or equal to",">=",">="),
                new Operator("Is Less than","<","<"),
                new Operator("Is Less than or equal to","<=","<="),
                new Operator("Is like","Like","%")
            };
            WildCard = WildCards[0];
        }

    }
}
