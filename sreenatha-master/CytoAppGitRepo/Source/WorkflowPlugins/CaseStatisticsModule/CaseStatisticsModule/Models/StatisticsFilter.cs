﻿using CaseStatisticsModule.Enums;
using LiveCharts;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace CaseStatisticsModule.Models
{
    public class StatisticsFilter : BindableBase
    {
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        private Condition _condition;
        public Condition Condition
        {
            get
            {
                return _condition;
            }
            set
            {
                _condition = value;
                OnPropertyChanged("Condition");
            }
        }

        private IList _charts;
        public IList Charts
        {
            get
            {
                return _charts;
            }
            set
            {
                _charts = value;
                OnPropertyChanged("Charts");

            }
        }

        private ChartType _chart;
        public ChartType Chart
        {
            get
            {
                return _chart;
            }
            set
            {
                _chart = value;
                OnPropertyChanged("Chart");
            }
        }

        private ObservableCollection<Condition> _conditions;
        public ObservableCollection<Condition> Conditions
        {
            get
            {
                return _conditions;
            }
            set
            {
                _conditions = value;
                OnPropertyChanged("Conditions");
            }
        }

        private SeriesCollection _data;
        public SeriesCollection Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
                OnPropertyChanged("Data");
            }
        }

        private double _percentageValue;
        public double PercentageValue
        {
            get
            {
                return _percentageValue;
            }
            set
            {
                try
                {
                    _percentageValue = value;
                    OnPropertyChanged("PercentageValue");
                }
                catch (Exception ex)
                {
                    // Hack to handle exception temporary
                    CytoApps.Infrastructure.Helpers.LogManager.Error("Case Statistics :", ex);
                }
            }
        }

        private double _maxValue;
        public double MaxValue
        {
            get
            {
                return _maxValue;
            }
            set
            {
                _maxValue = value;
                OnPropertyChanged("MaxValue");
            }
        }

        private Func<double, string> _percentageFormatter;
        public Func<double, string> PercentageFormatter
        {
            get
            {
                return _percentageFormatter;
            }
            set
            {
                _percentageFormatter = value;
                OnPropertyChanged("PercentageFormatter");
            }
        }

        private DelegateCommand _addConditionCommand;
        public ICommand AddConditionCommand
        {
            get
            {
                return _addConditionCommand ?? (_addConditionCommand = new DelegateCommand(() =>
                {
                    // Add Statistics Filter
                    Conditions.Add(new Condition( Fields));
                }));
            }
        }

        private DelegateCommand<Condition> _removeCondtionCommand;
        public ICommand RemoveConditionCommand
        {
            get
            {
                return _removeCondtionCommand ?? (_removeCondtionCommand = new DelegateCommand<Condition>((condition) =>
                {
                    // Remove Statistics Filter
                    if(Conditions != null && Conditions.Count > 0)
                        Conditions.Remove(condition);
                }));
            }
        }

        public StatisticsFilter( List<string> fields)
        {
            Charts = GetChartTypes();
            Conditions = new ObservableCollection<Condition>();
            Chart = ChartType.Pie_Chart;
            Fields = fields;
        }

        private IList GetChartTypes()
        {
            IList iLst = null;
            try
            {
                iLst = Enum.GetValues(typeof(ChartType)).Cast<Enum>().Where(w => (ChartType)w != ChartType.None).Select(s => new
                {
                    Text = s.ToString().Replace("_", " "),
                    Value = s
                }).ToList();
            }
            catch (Exception ex)
            {
            }
            return iLst;
        }

        #region TEMPORARY  
        private List<string> _fields;
        public List<string> Fields
        {
            get
            {
                return _fields;
            }
            set
            {
                _fields = value;
                OnPropertyChanged("Fields");
            }
        }
        #endregion

    }
}
