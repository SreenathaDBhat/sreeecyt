﻿using CaseDataAccess.CaseSearch;
using CaseStatisticsModule.Events;
using CaseStatisticsModule.Models;
using CaseStatisticsModule.Services;
using CaseStatisticsModule.ViewModels;
using CaseStatisticsModule.Views;
using CytoApps.Exceptions;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Models;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using Prism.Events;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace CaseStatisticsModule.Presenters
{
    [Export]
    public class CaseStatisticsPresenter
    {
        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        private IDataService _dataService;

        private CaseStatisticsNavigationView _caseStatisticsNavigationView;
        private CaseStatisticsNavigationViewModel _caseStatisticsNavigationViewModel;

        CaseStatisticsWorkspaceView _caseStatisticsWorkspaceView;
        CaseStatisticsWorkspaceViewModel _caseStatisticsWorkspaceViewModel;
        // DataSourcTemplatese defines the DataTypeId and connection parameters for a particular type of datasource
        private IDataSourceTemplate[] _availDataSourceTemplates = null;

        [ImportingConstructor]
        public CaseStatisticsPresenter(IEventAggregator eventAggregator, IRegionManager regionManager, IDataService dataService)
        {
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _dataService = dataService;
            _caseStatisticsNavigationView = new CaseStatisticsNavigationView();
            _caseStatisticsNavigationViewModel = new CaseStatisticsNavigationViewModel(eventAggregator);
            _caseStatisticsNavigationView.DataContext = _caseStatisticsNavigationViewModel;

            _regionManager.RegisterViewWithRegion(RegionNames.ToolBarRegion, () => _caseStatisticsNavigationView);

            // use reflection to load any IDataSourceTemplate plugins instances we find
            ImportAvailableDataSourceTemplates();

            _eventAggregator.GetEvent<CaseStatsOpenEvent>().Subscribe(OnCaseStatsOpen);
            _eventAggregator.GetEvent<CaseStatsDataSourceChangeEvent>().Subscribe(OnCaseStatsDataSourceChange);
            _eventAggregator.GetEvent<SaveSearchFilterEvent>().Subscribe(OnSaveSearchFilter);
        }

        /// <summary>
        /// Look in defined folder to resolve all IDataSourceTemplates, could use MEF but reflection is
        /// neater
        /// </summary>
        private void ImportAvailableDataSourceTemplates()
        {
            // load the data source templates instances from any implememting assembly
            if (_availDataSourceTemplates == null)
            {
                // Extract all Types from assemblies
                var availTemplateTypes = DalReflectionFinder.GetTypesForInterface<IDataSourceTemplate>(@".");
                // create instances from those types
                var instances = from type in availTemplateTypes select DalReflectionFinder.GetInstanceOfType<IDataSourceTemplate>(type);
                // these can then be used to enumerate the types of datasource that we can connect to e.g local or eSm or ....
                _availDataSourceTemplates = instances.ToArray();
            }
        }

        private void OnCaseStatsOpen(object obj)
        {
            if (_caseStatisticsWorkspaceView == null)
            {
                _caseStatisticsWorkspaceView = new CaseStatisticsWorkspaceView();
                _caseStatisticsWorkspaceView.Closing += _caseStatisticsWorkspaceView_Closing;
                _caseStatisticsWorkspaceViewModel = new CaseStatisticsWorkspaceViewModel(_eventAggregator);
                _caseStatisticsWorkspaceView.DataContext = _caseStatisticsWorkspaceViewModel;
                _caseStatisticsWorkspaceView.Show();
            }
        }

        private void _caseStatisticsWorkspaceView_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _caseStatisticsWorkspaceView.Closing -= _caseStatisticsWorkspaceView_Closing;
            _caseStatisticsWorkspaceView = null;
        }

        private void OnCaseStatsDataSourceChange(DataSource dataSource)
        {
            var flags = new List<string>();
            List<Filter> caseFilters = new List<Filter>();

            try
            {
                var caseDAL = _dataService.CreateCaseDAL(dataSource);

                flags = caseDAL.GetUniqueCaseFlags().ToList();
                caseFilters = caseDAL.GetCaseDetailsFields().ToList();

                List<string> fields = new List<string>();

                foreach (var filter in caseFilters)
                {
                    fields.Add(filter.FieldName);
                }

                _caseStatisticsWorkspaceViewModel.StatisticsFilterViewModel.Fields = fields;


                _caseStatisticsWorkspaceViewModel.Cases = caseDAL.GetCases(null).ToList();

            }
            catch (DALException exception)
            {
                LogManager.Error("Case Statistics :", exception);
                throw;
            }
        }

        private void OnSaveSearchFilter(object obj)
        {
            try
            {
                var caseDAL = _dataService.CreateCaseDAL(_caseStatisticsWorkspaceViewModel.SelectedDataSource);
                foreach (var filter in _caseStatisticsWorkspaceViewModel.StatisticsFilterViewModel.StatisticsFilters)
                {
                    if (filter.Conditions != null && filter.Conditions.Count > 0)
                    {
                        bool isValid = true;
                        Criteria caseSearch = new Criteria();
                        caseSearch.Filters = new List<Filter>();
                      
                        foreach (var searchCriteria in filter.Conditions)
                        {
                            if (!string.IsNullOrEmpty(searchCriteria.Value))
                            {
                                Filter caseFilter = new Filter()
                                {
                                    FieldName = searchCriteria.FieldName,
                                    Value = searchCriteria.Value,
                                    //FilterType = searchFilter.CaseFilterType,
                                    Operator = searchCriteria.WildCard.SqlOperator,
                                    //IsCaseField = searchFilter.IsCaseField
                                };
                                caseSearch.Filters.Add(caseFilter);
                            }
                            else
                            {
                                CustomPopUp.Show("Please enter value for search criteria", "Case statistics", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                                isValid = false;
                                break;
                            }
                        }
                        if(isValid)
                            CreateChart(filter, caseDAL.GetCasesForStats(caseSearch).ToList());
                    }
                }

            }
            catch (DALException exception)
            {
                LogManager.Error("Case Statistics :", exception);
                throw;
            }
        }

        private void CreateChart(StatisticsFilter filter, List<CaseWithCaseDetails> cases)
        {
            //Pie chart
            SeriesCollection CaseStatusSeries = new SeriesCollection();
            CaseStatusSeries.Add(
            new PieSeries
            {
                Title = filter.Name,
                Values = new ChartValues<ObservableValue> { new ObservableValue(cases.Count) },
                DataLabels = true,
                LabelPoint = chartPoint =>
                    string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation.ToString("P"))
            });
            CaseStatusSeries.Add(
            new PieSeries
            {
                Title = "Total Cases",
                Values = new ChartValues<ObservableValue> { new ObservableValue(_caseStatisticsWorkspaceViewModel.FilteredCases.Count - cases.Count) },
                DataLabels = true,
                LabelPoint = chartPoint =>
                    string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation.ToString("P"))
            });
            filter.Data = CaseStatusSeries;

            //Percentage chart
            filter.PercentageValue = Math.Round(Convert.ToDouble(cases.Count) / Convert.ToDouble(_caseStatisticsWorkspaceViewModel.FilteredCases.Count), 3);
            filter.MaxValue = _caseStatisticsWorkspaceViewModel.Cases.Count;
            filter.PercentageFormatter = x => x.ToString("P");//string.Format(" {0} ({1:P}) ", Convert.ToDouble(cases.Count), x.ToString("P"));
        }
    }
}

