﻿using CaseDataAccess;
using CytoApps.Models;
using System.Collections.Generic;

namespace CaseStatisticsModule.Services
{
    public interface IDataService
    {
        ICaseDataAccess CreateCaseDAL(DataSource dataSource);
    }
}
