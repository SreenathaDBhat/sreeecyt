﻿namespace CaseStatisticsModule.Utilities
{
    public class Operator
    {
        #region Properties
        public string SqlOperator
        {
            get;
            private set;
        }
        public string DisplayOperator
        {
            get;
            private set;
        }
        public string Name
        {
            get;
            private set;
        }

        #endregion

        #region Constructor

        public Operator(string name, string sql, string display)
        {
            Name = name;
            SqlOperator = sql;
            DisplayOperator = display;
        }
        public override string ToString()
        {
            return Name;
        }
        #endregion
    }
}
