﻿using CaseStatisticsModule.Enums;
using CaseStatisticsModule.Models;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Linq;
using CaseStatisticsModule.Events;
using Prism.Events;

namespace CaseStatisticsModule.ViewModels
{
    public class CaseStatisticsFilterViewModel : BindableBase
    {
        #region Fields
        private IEventAggregator _eventAggregator;
        #endregion

        #region Constructors
        public CaseStatisticsFilterViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            StatisticsFilters = new ObservableCollection<StatisticsFilter>();
        }
        #endregion

        #region Properties
        private ObservableCollection<StatisticsFilter> _statisticsFilters;
        public ObservableCollection<StatisticsFilter> StatisticsFilters
        {
            get
            {
                return _statisticsFilters;
            }
            set
            {
                _statisticsFilters = value;
                OnPropertyChanged("StatisticsFilters");
            }
        }

        private StatisticsFilter _selectedFilter;
        public StatisticsFilter SelectedFilter
        {
            get
            {
                return _selectedFilter;
            }
            set
            {
                if (value != null && _selectedFilter != value)
                {
                    _selectedFilter = value;
                    OnPropertyChanged("SelectedFilter");
                }
            }
        }
        #endregion

        #region Commands
        private DelegateCommand _addFilterCommand;
        public ICommand AddFilterCommand
        {
            get
            {
                return _addFilterCommand ?? (_addFilterCommand = new DelegateCommand(() =>
                {
                    // Add Statistics Filter
                    StatisticsFilters.Add(new StatisticsFilter( Fields));
                }));
            }
        }

        private DelegateCommand _removeFilterCommand;
        public ICommand RemoveFilterCommand
        {
            get
            {
                return _removeFilterCommand ?? (_removeFilterCommand = new DelegateCommand(() =>
                {
                    // Remove Statistics Filter
                    if(StatisticsFilters.Count > 0 && SelectedFilter != null )
                        StatisticsFilters.Remove(SelectedFilter);
                }));
            }
        }

        private DelegateCommand _saveFilterCommand;
        public ICommand SaveFilterCommand
        {
            get
            {
                return _saveFilterCommand ?? (_saveFilterCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<SaveSearchFilterEvent>().Publish(null);
                }));
            }
        }
        #endregion

        #region Methods
        #endregion

        #region Events
        #endregion


        #region TEMPORARY  
        private List<string> _fields;
        public List<string> Fields
        {
            get
            {
                return _fields;
            }
            set
            {
                _fields = value;
                OnPropertyChanged("Fields");
            }
        }
        #endregion

    }
}
