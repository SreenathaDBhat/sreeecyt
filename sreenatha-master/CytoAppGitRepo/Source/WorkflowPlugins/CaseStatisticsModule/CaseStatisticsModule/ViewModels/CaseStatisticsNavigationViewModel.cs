﻿using CaseStatisticsModule.Events;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Models;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CaseStatisticsModule.ViewModels
{
    public class CaseStatisticsNavigationViewModel : BindableBase
    {
        private IEventAggregator _eventAggregator;

        public CaseStatisticsNavigationViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }
        private DelegateCommand _openCaseStatsCommand;
        public ICommand OpenCaseStatsCommand
        {
            get
            {
                return _openCaseStatsCommand ?? (_openCaseStatsCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<CaseStatsOpenEvent>().Publish(null);
                }));
            }
        }
    }
}
