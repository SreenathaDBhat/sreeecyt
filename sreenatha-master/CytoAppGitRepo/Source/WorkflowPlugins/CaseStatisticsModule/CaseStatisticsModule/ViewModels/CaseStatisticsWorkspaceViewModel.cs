﻿using CaseStatisticsModule.Enums;
using CaseStatisticsModule.Events;
using CaseStatisticsModule.Models;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Models;
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace CaseStatisticsModule.ViewModels
{
    class CaseStatisticsWorkspaceViewModel : BindableBase
    {
        #region Fields
        private IEventAggregator _eventAggregator;
        private ChartValues<DateTimePoint> _TimelineValues;
        #endregion

        #region Constructors
        public CaseStatisticsWorkspaceViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            TimelineType = TimelineTypes.Year;
            StatisticsFilterViewModel = new CaseStatisticsFilterViewModel(eventAggregator);
            DefaultCharts = new ObservableCollection<StatisticsFilter>();
            DefaultCharts.Add(new StatisticsFilter(null) { Name = "Case Status", Chart = ChartType.Pie_Chart });
            DefaultCharts.Add(new StatisticsFilter(null)
            {
                Name = "Users",
                Chart = ChartType.None,
                Data = new SeriesCollection {
                    new PieSeries
                    {
                        Title = "Hareesh A",
                        Values = new ChartValues<ObservableValue> { new ObservableValue(8) },
                        DataLabels = true,
                        LabelPoint = chartPoint =>
                                string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation.ToString("P"))
                    },
                    new PieSeries
                    {
                        Title = "Swastik D",
                        Values = new ChartValues<ObservableValue> { new ObservableValue(6) },
                        DataLabels = true,
                        LabelPoint = chartPoint =>
                                string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation.ToString("P"))
                    },
                    new PieSeries
                    {
                        Title = "Shreenath B",
                        Values = new ChartValues<ObservableValue> { new ObservableValue(10) },
                        DataLabels = true,
                        LabelPoint = chartPoint =>
                                string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation.ToString("P"))
                    },
                    new PieSeries
                    {
                        Title = "Ashwini MN",
                        Values = new ChartValues<ObservableValue> { new ObservableValue(4) },
                        DataLabels = true,
                        LabelPoint = chartPoint =>
                                string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation.ToString("P"))
                    }
                }
            });
        }
        #endregion

        #region Methods

        private void DisplayTimeLine(List<DateTime> dates)
        {
            MinDate = dates.Min().AddDays(-1);
            MaxDate = dates.Max().AddDays(2);
            LowerValue = dates.Min();
            UpperValue = dates.Max().AddDays(1);

            _TimelineValues = GetData(dates);
            SetXAxis(TimelineType, _TimelineValues);            
        }

        private void DisplayCaseStatus(List<string> flags)
        {
            SeriesCollection CaseStatusSeries = new SeriesCollection();
            foreach (var flag in flags)
            {
                CaseStatusSeries.Add(
                    new PieSeries
                    {
                        Title = flag,
                        Values = new ChartValues<ObservableValue> { new ObservableValue(Cases.Where(w => w.Flag == flag).Count()) },
                        DataLabels = true,
                        LabelPoint = chartPoint =>
                            string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation.ToString("P"))
                });
            }
            DefaultCharts.Single(s => s.Name == "Case Status").Data = CaseStatusSeries;
        }

        private ChartValues<DateTimePoint> GetData(List<DateTime> _dates)
        {
            var values = new ChartValues<DateTimePoint>();
            foreach (var date in _dates)
            {
                values.Add(new DateTimePoint(date, Cases.Where(w => w.Created.Date == date).Count()));
            }
            return values;
        }

        private void SetXAxis(TimelineTypes timelineType, ChartValues<DateTimePoint> timelineValues)
        {
            try
            {
                if (timelineType == TimelineTypes.Year)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromDays(1).Ticks * 365.2425));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromDays(1).Ticks * 365.2425);
                    var config = Mappers.Xy<DateTimePoint>()
                      .X(dateModel => dateModel.DateTime.Ticks / (TimeSpan.FromDays(1).Ticks * 365.2425))
                      .Y(dateModel => dateModel.Value);

                    //365.2425 is the days in a solar year
                    TimelineSeries = new SeriesCollection(config)
                {
                    //new LineSeries
                    //{
                    //    Values = values
                    //},
                    new ColumnSeries
                    {
                        Values = timelineValues,
                        Title = "No of Cases"
                    }
                };

                    XFormatter = value => new DateTime((long)(value * (TimeSpan.FromDays(1).Ticks * 365.2425))).ToString("yyyy");
                    YFormatter = val => ((int)val).ToString();

                }
                else if (timelineType == TimelineTypes.Month)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromDays(1).Ticks * 30.44));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromDays(1).Ticks * 30.44);
                    //Months
                    var config = Mappers.Xy<DateTimePoint>()
                      .X(dateModel => dateModel.DateTime.Ticks / (TimeSpan.FromDays(1).Ticks * 30.44))
                      .Y(dateModel => dateModel.Value);

                    TimelineSeries = new SeriesCollection(config)
                {
                    //new LineSeries
                    //{
                    //    Values = values
                    //},
                    new ColumnSeries
                    {
                        Values = timelineValues,
                        Title = "No of Cases"
                    }
                };

                    //30.44 is the average days in a solar year
                    XFormatter = value => new DateTime((long)(value * (TimeSpan.FromDays(1).Ticks * 30.44))).ToString("M");
                    YFormatter = val => ((int)val).ToString();
                }
                else if (timelineType == TimelineTypes.Day)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromDays(1).Ticks));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromDays(1).Ticks);
                    //Days
                    var config = Mappers.Xy<DateTimePoint>()
                      .X(dateModel => dateModel.DateTime.Ticks / (TimeSpan.FromDays(1).Ticks))
                      .Y(dateModel => dateModel.Value);

                    TimelineSeries = new SeriesCollection(config)
                {
                    //new LineSeries
                    //{
                    //    Values = values
                    //},
                    new ColumnSeries
                    {
                        Values = timelineValues,
                        Title = "No of Cases"
                    }
                };

                    XFormatter = value => new DateTime((long)(value * TimeSpan.FromDays(1).Ticks)).ToString("d");
                    YFormatter = val => ((int)val).ToString();

                }
                else if (timelineType == TimelineTypes.Hour)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromHours(1).Ticks));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromHours(1).Ticks);
                    var config = Mappers.Xy<DateTimePoint>()
                      .X(dateModel => dateModel.DateTime.Ticks / (TimeSpan.FromHours(1).Ticks))
                      .Y(dateModel => dateModel.Value);

                    TimelineSeries = new SeriesCollection(config)
                {
                    //new LineSeries
                    //{
                    //    Values = values
                    //},
                    new ColumnSeries
                    {
                        Values = timelineValues,
                        Title = "No of Cases"
                    }
                };

                    XFormatter = value => new DateTime((long)(value * TimeSpan.FromHours(1).Ticks)).ToString("t");
                    YFormatter = val => ((int)val).ToString();
                }
                else if (timelineType == TimelineTypes.Minute)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromMinutes(5).Ticks));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromMinutes(5).Ticks);
                    var config = Mappers.Xy<DateTimePoint>()
                      .X(dateModel => dateModel.DateTime.Ticks / (TimeSpan.FromMinutes(5).Ticks))
                      .Y(dateModel => dateModel.Value);

                    TimelineSeries = new SeriesCollection(config)
                {
                    //new LineSeries
                    //{
                    //    Values = values
                    //},
                    new ColumnSeries
                    {
                        Values = timelineValues,
                        Title = "No of Cases"
                    }
                };

                    XFormatter = value => new DateTime((long)(value * TimeSpan.FromMinutes(5).Ticks)).ToString("t");
                    YFormatter = val => ((int)val).ToString();
                }
                else
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromDays(1).Ticks));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromDays(1).Ticks);
                    var config = Mappers.Xy<DateTimePoint>()
                      .X(dateModel => dateModel.DateTime.Ticks / (TimeSpan.FromDays(1).Ticks))
                      .Y(dateModel => dateModel.Value);

                    TimelineSeries = new SeriesCollection(config)
                {
                    //new LineSeries
                    //{
                    //    Values = values
                    //},
                    new ColumnSeries
                    {
                        Values = timelineValues,
                        Title = "No of Cases"
                    }
                };

                    XFormatter = value => new DateTime((long)(value * TimeSpan.FromDays(1).Ticks)).ToString("d");
                    YFormatter = val => ((int)val).ToString();
                }
            }
            catch (Exception ex)
            {
                LogManager.Error("Case Statistics :", ex);
                throw;
            }
        }

        #endregion

        #region Properties
        public ObservableCollection<DataSource> DataSources
        {
            get
            {
                return DataSourceManager.Instance.DataSourceCollection;
            }
        }

        public DataSource _selectedDataSource;
        public DataSource SelectedDataSource
        {
            get
            {
                return _selectedDataSource;
            }
            set
            {
                _selectedDataSource = value;
                OnPropertyChanged();
                _eventAggregator.GetEvent<CaseStatsDataSourceChangeEvent>().Publish(value);
            }
        }

        private List<Case> _cases;
        public List<Case> Cases
        {
            get
            {
                if (_cases == null)
                {
                    _cases = new List<Case>();
                }
                return _cases;
            }
            set
            {
                _cases = value;
                this.OnPropertyChanged();
                if (Cases != null && Cases.Count > 0)
                {
                    FilteredCases = Cases;
                    DisplayTimeLine(Cases.Select(s => s.Created.Date).Distinct().ToList());
                    DisplayCaseStatus(Cases.Select(s => s.Flag).Distinct().ToList());
                }
            }
        }

        private List<Case> _filteredCases;
        public List<Case> FilteredCases
        {
            get
            {
                return _filteredCases;
            }
            set
            {
                _filteredCases = value;
                OnPropertyChanged("FilteredCases");
            }
        }


        private SeriesCollection _timelineSeries;
        public SeriesCollection TimelineSeries
        {
            get
            {
                return _timelineSeries;
            }
            set
            {
                _timelineSeries = value;
                OnPropertyChanged();
            }
        }

        private Func<double, string> _xFormatter;
        public Func<double, string> XFormatter
        {
            get
            {
                return _xFormatter;
            }
            set
            {
                _xFormatter = value;
                this.OnPropertyChanged();
            }
        }

        private Func<double, string> _yFormatter;
        public Func<double, string> YFormatter
        {
            get
            {
                return _yFormatter;
            }
            set
            {
                _yFormatter = value;
                this.OnPropertyChanged();
            }
        }

        private TimelineTypes _timelineType;
        public TimelineTypes TimelineType
        {
            get
            {
                return _timelineType;
            }
            set
            {
                _timelineType = value;
                OnPropertyChanged();
                if (_TimelineValues != null)
                {
                    SetXAxis(TimelineType, _TimelineValues);
                }
            }
        }

        private DateTime _minDate;
        public DateTime MinDate
        {
            get
            {
                return _minDate;
            }
            set
            {
                _minDate = value;
                OnPropertyChanged();
            }
        }

        private DateTime _maxDate;
        public DateTime MaxDate
        {
            get
            {
                return _maxDate;
            }
            set
            {
                _maxDate = value;
                OnPropertyChanged();
            }
        }

        private DateTime _upperValue;
        public DateTime UpperValue
        {
            get
            {
                return _upperValue;
            }
            set
            {
                if (value > LowerValue)
                {
                    _upperValue = value;
                }
                else
                {
                    _upperValue = LowerValue.AddDays(1);
                }
                OnPropertyChanged();
                if (TimelineType == TimelineTypes.Year)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromDays(1).Ticks * 365.2425));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromDays(1).Ticks * 365.2425);
                }
                else if (TimelineType == TimelineTypes.Month)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromDays(1).Ticks * 30.44));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromDays(1).Ticks * 30.44);
                }
                else if (TimelineType == TimelineTypes.Day)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromDays(1).Ticks));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromDays(1).Ticks);
                }
                else if (TimelineType == TimelineTypes.Hour)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromHours(1).Ticks));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromHours(1).Ticks);
                }
                else if (TimelineType == TimelineTypes.Minute)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromMinutes(5).Ticks));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromMinutes(5).Ticks);
                }
                else
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromDays(1).Ticks));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromDays(1).Ticks);
                }
                if (Cases != null && Cases.Count > 0)
                {
                    FilteredCases = Cases.Where(w => w.Created.Date >= LowerValue.Date && w.Created.Date <= UpperValue.Date).ToList();
                    DisplayCaseStatus(Cases.Where(w => w.Created.Date >= LowerValue.Date && w.Created.Date <= UpperValue.Date).Select(s => s.Flag).Distinct().ToList());
                }
            }
        }

        private DateTime _lowerValue;
        public DateTime LowerValue
        {
            get
            {
                return _lowerValue;
            }
            set
            {
                if (value < UpperValue)
                {
                    _lowerValue = value;
                }
                else
                {
                    _lowerValue = UpperValue.AddDays(-1);
                }
                    OnPropertyChanged();
                    
                if (TimelineType == TimelineTypes.Year)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromDays(1).Ticks * 365.2425));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromDays(1).Ticks * 365.2425);
                }
                else if (TimelineType == TimelineTypes.Month)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromDays(1).Ticks * 30.44));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromDays(1).Ticks * 30.44);
                }
                else if (TimelineType == TimelineTypes.Day)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromDays(1).Ticks));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromDays(1).Ticks);
                }
                else if (TimelineType == TimelineTypes.Hour)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromHours(1).Ticks));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromHours(1).Ticks);
                }
                else if (TimelineType == TimelineTypes.Minute)
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromMinutes(5).Ticks));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromMinutes(5).Ticks);
                }
                else
                {
                    AxisMax = (UpperValue.Ticks / (TimeSpan.FromDays(1).Ticks));
                    AxisMin = LowerValue.Ticks / (TimeSpan.FromDays(1).Ticks);
                }
                if (Cases != null && Cases.Count > 0)
                {
                    FilteredCases = Cases.Where(w => w.Created.Date >= LowerValue.Date && w.Created.Date <= UpperValue.Date).ToList();
                    DisplayCaseStatus(Cases.Where(w => w.Created.Date >= LowerValue.Date && w.Created.Date <= UpperValue.Date).Select(s => s.Flag).Distinct().ToList());
                }
            }
        }

        private double _axisMax = 1d;
        public double AxisMax
        {
            get { return _axisMax; }
            set
            {
                _axisMax = value;
                OnPropertyChanged("AxisMax");
            }
        }

        private double _axisMin = 0d;
        public double AxisMin
        {
            get { return _axisMin; }
            set
            {
                _axisMin = value;
                OnPropertyChanged("AxisMin");
            }
        }

        private double _barSize = 1d;
        public double BarSize
        {
            get { return _barSize; }
            set
            {
                _barSize = value;
                OnPropertyChanged("BarSize");
            }
        }

        private CaseStatisticsFilterViewModel _statisticsFilterViewModel;
        public CaseStatisticsFilterViewModel StatisticsFilterViewModel
        {
            get
            {
                return _statisticsFilterViewModel;
            }
            set
            {
                _statisticsFilterViewModel = value;
                OnPropertyChanged("StatisticsFilterViewModel");
            }
        }

        private ObservableCollection<StatisticsFilter> _defaultCharts;
        public ObservableCollection<StatisticsFilter> DefaultCharts
        {
            get { return _defaultCharts; }
            set
            {
                _defaultCharts = value;
                OnPropertyChanged("DefaultCharts");
            }
        }
        #endregion

        #region Commands
        private DelegateCommand<Calendar> _selectedDatesChangedCommand;
        public ICommand SelectedDatesChangedCommand
        {
            get
            {
                return _selectedDatesChangedCommand ?? (_selectedDatesChangedCommand = new DelegateCommand<Calendar>((calender) =>
                {
                    DisplayTimeLine(calender.SelectedDates.ToList());
                }));
            }
        }

        
        #endregion
    }
}
