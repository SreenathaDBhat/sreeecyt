﻿using CaseStatisticsModule.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CaseStatisticsModule.Views
{
    /// <summary>
    /// Interaction logic for CaseStatisticsNavigationView.xaml
    /// </summary>
    [Export]
    public partial class CaseStatisticsNavigationView : UserControl
    {
        public CaseStatisticsNavigationView()
        {
            InitializeComponent();
        }
    }
}
